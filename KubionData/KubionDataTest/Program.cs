using System;
using System.Collections.Generic;
using System.Text;
using KubionDataNamespace;
using System.Data;

namespace KubionDataTest
{
    class Program
    {
        static void Main(string[] args)
        {
			//string strCommand = "";
			//strCommand += "&url=" + System.Web.HttpUtility.UrlEncode("http://irisontwikkel/IRIS3_REST/GridX");
			//strCommand += "&rootnode=" + System.Web.HttpUtility.UrlEncode("");
			//strCommand += "&xpath=" + System.Web.HttpUtility.UrlEncode("/GridXList/GridX"); //GridXList/GridX[GridXID=3]
			//strCommand += "&contenttype=" + System.Web.HttpUtility.UrlEncode("text/xml; charset=utf-8");
			//strCommand += "&method=" + System.Web.HttpUtility.UrlEncode("POST");
			//strCommand += "&xml=" +System.Web.HttpUtility.UrlEncode("<resource><atest>m�e��</atest></resource>", Encoding.Default);

			string m_Response = "";
			string m_OuterXml = "";
			string m_InnerXml = "";

			//RESTConsumer objRESTConsumer = new RESTConsumer();
			//DataTable objDataTable = objRESTConsumer.GetDataTable(strCommand, ref m_Response, ref m_OuterXml, ref m_InnerXml);

			//WebserviceConsumer objWebserviceConsumer = new WebserviceConsumer();
			//DataTable objDataTable = objWebserviceConsumer.GetDataTable(strCommand, ref m_Response, ref m_OuterXml, ref m_InnerXml);

//            ClientData objClientData = new ClientData("Provider=WEBSERVICE;URI=http://irisontwikkel/IRIS3_REST/;UserName=michael.de.graaf;Password=Graaf02MR;Domain=kubion");
//            DataTable objDataTable = objClientData.DTExecuteQuery(@"GridX
//
///GridXList/GridX
//
//POST
//<resource><atest>m�e��</atest></resource>", ref m_Response, ref m_OuterXml, ref m_InnerXml);

            ClientData objClientData = new ClientData("Provider=REST;URI=http://irisontwikkel/Util_REST/");
            DataTable objDataTable = objClientData.DTExecuteQuery(@"Inspecteur/1/MutatieInspectie

/serviceresponse

PUT
<servicerequestList><servicerequest><method>ID_DELETE</method><MutatieInspectie>30</MutatieInspectie></servicerequest></servicerequestList>", ref m_Response, ref m_OuterXml, ref m_InnerXml);
			Console.WriteLine("Done. Response: " + m_Response);
			Console.ReadKey();
        }
    }
}
