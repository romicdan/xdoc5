using System;
using System.Data;
using System.Data.Odbc ;
using System.Configuration;
using System.Diagnostics;
using System.Collections;
////using KubionLogNamespace;

namespace KubionDataNamespace
{
    public class OdbcData : IData, IDisposable
    {
        OdbcConnection m_conn = null;
        OdbcTransaction m_trans = null;
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }

        public OdbcData(string connectionString)
        {
                m_conn = new OdbcConnection(connectionString);
                OpenConnection();
        }

        public void Dispose()
        {
            if (m_conn != null && m_conn.State != ConnectionState.Closed)
            {
                    m_conn.Close();
            }
        }

        private void OpenConnection()
        {
            if (m_conn.State != ConnectionState.Open)
            {
                    m_conn.Open();
            }
        }

        public bool ConnOpen()
        {
            OpenConnection();
            return (m_conn.State == ConnectionState.Open);
        }

        public DataTable GetSchema(string sqlString)
        {
            if (!ConnOpen()) return null;
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
                da.FillSchema(ds, SchemaType.Source);

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }

        public IDataReader GetDataReader(string sqlString)
        {
            OpenConnection();
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans == null) BeginTransaction(IsolationLevel.ReadUncommitted);
            command.Transaction = m_trans;
            return command.ExecuteReader(CommandBehavior.SequentialAccess);
        }
        public string GetResponse(string sqlString, ref string strCookies)
        {
            string ret = "";
            string sTables = "";
            if (sqlString.LastIndexOf("--tables:") > 0)
            {

                sTables = sqlString.Substring(sqlString.LastIndexOf("--tables:") + "--tables:".Length);
                sqlString = sqlString.Substring(0, sqlString.LastIndexOf("--tables:"));
            }
            if (!ConnOpen()) return null;
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
                System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "GetResponse start" + "-");
                da.Fill(ds);
                System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "GetResponse filled" + "-");
                if (sTables != "")
                {
                    string[] aTables = sTables.Split(',');
                    for (int i = 0; i < aTables.Length; i++)
                        if (aTables[i].Trim() != "") ds.Tables[i].TableName = aTables[i].Trim();
                }
                else
                {
                    int index = 0;
                    for (int i = ds.Tables.Count - 1; i >= 0; i--)
                    {
                        if (index == 0) ds.Tables[i].TableName = "result";
                        else ds.Tables[i].TableName = "result" + index.ToString();
                        index++;
                    }
                }
                ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
                System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "GetResponse end" + "-");
            ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            return ret;
        }

        public string GetResponse_old(string sqlString)
        {
            OpenConnection();
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
                da.Fill(ds);
            string ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            return ret;
        }

        public DataTable GetDataTable(string sqlString)
        {
            if (!ConnOpen()) return null;
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
                da.Fill(ds);
            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return GetDataTable(sqlString);
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            DataSet ds = new DataSet();
            OdbcCommand comm = new OdbcCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            DataTable dt = null;
                foreach (OdbcParameter  paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                OdbcDataAdapter da = new OdbcDataAdapter(comm);
                //DateTime dt1 = DateTime.Now;
                da.Fill(ds);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString); 
                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];

            return dt;
        }

        public string ExecuteNonQuery(string sqlString)
        {
            OpenConnection();
            OdbcCommand comm = new OdbcCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null) comm.Transaction = m_trans;

            int rowsAffected = -1;
                rowsAffected = comm.ExecuteNonQuery();
            return rowsAffected.ToString ();
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return ExecuteNonQuery(sqlString);
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            OdbcCommand comm = new OdbcCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            int rowsAffected = -1;

                foreach (OdbcParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                rowsAffected = comm.ExecuteNonQuery();

            return rowsAffected.ToString ();
        }

        public void BeginTransaction()
        {
            OpenConnection();
            try
            {
                m_trans = m_conn.BeginTransaction();
            }
            catch (Exception)
            {
            }
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            OpenConnection();
            try
            {
                if (m_trans != null) RollbackTransaction();
                m_trans = m_conn.BeginTransaction(isolationLevel);
            }
            catch (Exception)
            {
            }
        }
        public void CommitTransaction()
        {
            try
            {

                m_trans.Commit();
                m_trans = null;
            }
            catch (Exception)
            {
            }

        }
        public void RollbackTransaction()
        {
            try
            {
                m_trans.Rollback();
                m_trans = null;
            }
            catch (Exception)
            {
            }

        }

    }
    public class OdbcParData : IData, IDisposable
    {
        OdbcConnection m_conn = null;
        OdbcTransaction m_trans = null;
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }

        public OdbcParData(string connectionString)
        {
            m_conn = new OdbcConnection(connectionString);
            OpenConnection();
        }

        public void Dispose()
        {
            if (m_conn != null && m_conn.State != ConnectionState.Closed)
            {
                m_conn.Close();
            }
        }

        private void OpenConnection()
        {
            if (m_conn.State != ConnectionState.Open)
            {
                m_conn.Open();
            }
        }

        public bool ConnOpen()
        {
            OpenConnection();
            return (m_conn.State == ConnectionState.Open);
        }

        public DataTable GetSchema(string sqlString)
        {
            if (!ConnOpen()) return null;
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
            da.FillSchema(ds, SchemaType.Source);

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
    

        public IDataReader GetDataReader(string sqlString)
        {
            OpenConnection();
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans == null) BeginTransaction(IsolationLevel.ReadUncommitted);
            command.Transaction = m_trans;
            return command.ExecuteReader(CommandBehavior.SequentialAccess);
        }
        public string GetResponse(string sqlString, ref string strCookies)
        {
            string ret = "";
            string sTables = "";
            if (sqlString.LastIndexOf("--tables:") > 0)
            {

                sTables = sqlString.Substring(sqlString.LastIndexOf("--tables:") + "--tables:".Length);
                sqlString = sqlString.Substring(0, sqlString.LastIndexOf("--tables:"));
            }
            if (!ConnOpen()) return null;
            //parameter sniffing :: ::
            ArrayList aParams = null;
            if (sqlString.Contains("::"))
            {
                aParams = new ArrayList();
                string[] aData = ClientData.MySplit(sqlString, "::");
                sqlString = "";
                int i = 0;
                for (i = 0; i < aData.GetLength(0) - 1; i += 2)
                {
                    //                    sqlString = sqlString + aData[i] + "@P" + i.ToString();
                    sqlString = sqlString + aData[i] + "?";
                    string v = aData[i + 1];
                    if (v.StartsWith("'") && v.EndsWith("'"))
                        v = v.Substring(1, v.Length - 2);
                    OdbcParameter oParameter = new OdbcParameter("@P" + i.ToString(), v);
                    aParams.Add(oParameter);
                }
                if (i < aData.GetLength(0))
                    sqlString = sqlString + aData[i];
            }
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            if (aParams != null)
                foreach (OdbcParameter oParameter in aParams)
                    command.Parameters.Add(oParameter);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "GetResponse start" + "-");
            da.Fill(ds);
            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "GetResponse filled" + "-");
            if (sTables != "")
            {
                string[] aTables = sTables.Split(',');
                for (int i = 0; i < aTables.Length; i++)
                    if (aTables[i].Trim() != "") ds.Tables[i].TableName = aTables[i].Trim();
            }
            else
            {
                int index = 0;
                for (int i = ds.Tables.Count - 1; i >= 0; i--)
                {
                    if (index == 0) ds.Tables[i].TableName = "result";
                    else ds.Tables[i].TableName = "result" + index.ToString();
                    index++;
                }
            }
            ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "GetResponse end" + "-");

            ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            return ret;
        }

        public string GetResponse_old(string sqlString)
        {
            OpenConnection();
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);

            da.Fill(ds);

            string ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            return ret;
        }

        public DataTable GetDataTable(string sqlString)
        {
            if (!ConnOpen()) return null;
            //parameter sniffing :: ::
            ArrayList aParams = null;
            if (sqlString.Contains("::"))
            {
                aParams = new ArrayList();
                string[] aData = ClientData.MySplit(sqlString, "::");
                sqlString = "";
                int i = 0;
                for (i = 0; i < aData.GetLength(0) - 1; i += 2)
                {
                    //                    sqlString = sqlString + aData[i] + "@P" + i.ToString();
                    sqlString = sqlString + aData[i] + "?";
                    string v = aData[i + 1];
                    if (v.StartsWith("'") && v.EndsWith("'"))
                        v = v.Substring(1, v.Length - 2);
                    OdbcParameter oParameter = new OdbcParameter("@P" + i.ToString(), v);
                    aParams.Add(oParameter);
                }
                if (i < aData.GetLength(0))
                    sqlString = sqlString + aData[i];
            }
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            if (aParams != null)
                foreach (OdbcParameter oParameter in aParams)
                    command.Parameters.Add(oParameter);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OdbcDataAdapter da = new OdbcDataAdapter(command);
            da.Fill(ds);
            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return GetDataTable(sqlString);
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            DataSet ds = new DataSet();
            OdbcCommand comm = new OdbcCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            DataTable dt = null;
            foreach (OdbcParameter paramValue in arrParams)
            {
                comm.Parameters.Add(paramValue);
            }
            OdbcDataAdapter da = new OdbcDataAdapter(comm);
            //DateTime dt1 = DateTime.Now;
            da.Fill(ds);
            //DateTime dt2 = DateTime.Now;
            //TimeSpan ts = dt2.Subtract(dt1);
            //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
            //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString); 
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }

        public string ExecuteNonQuery(string sqlString)
        {
            OpenConnection();
            //parameter sniffing :: ::
            ArrayList aParams = null;
            if (sqlString.Contains("::"))
            {
                aParams = new ArrayList();
                string[] aData = ClientData.MySplit(sqlString, "::");
                sqlString = "";
                int i = 0;
                for (i = 0; i < aData.GetLength(0) - 1; i += 2)
                {
                    //                    sqlString = sqlString + aData[i] + "@P" + i.ToString();
                    sqlString = sqlString + aData[i] + "?";
                    string v = aData[i + 1];
                    if (v.StartsWith("'") && v.EndsWith("'"))
                        v = v.Substring(1, v.Length - 2);
                    OdbcParameter oParameter = new OdbcParameter("@P" + i.ToString(), v);
                    aParams.Add(oParameter);
                }
                if (i < aData.GetLength(0))
                    sqlString = sqlString + aData[i];
            }
            OdbcCommand command = new OdbcCommand(sqlString, m_conn);
            if (aParams != null)
                foreach (OdbcParameter oParameter in aParams)
                    command.Parameters.Add(oParameter);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            int rowsAffected = -1;
            rowsAffected = command.ExecuteNonQuery();

            return rowsAffected.ToString();
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return ExecuteNonQuery(sqlString);
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            OdbcCommand comm = new OdbcCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            int rowsAffected = -1;
            foreach (OdbcParameter paramValue in arrParams)
            {
                comm.Parameters.Add(paramValue);
            }
            rowsAffected = comm.ExecuteNonQuery();
            return rowsAffected.ToString();
        }

        public void BeginTransaction()
        {
            OpenConnection();
            try
            {
                m_trans = m_conn.BeginTransaction();
            }
            catch (Exception)
            {
            }
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            OpenConnection();
            try
            {
                if (m_trans != null) RollbackTransaction();
                m_trans = m_conn.BeginTransaction(isolationLevel);
            }
            catch (Exception)
            {
            }
        }
        public void CommitTransaction()
        {
            try
            {

                m_trans.Commit();
                m_trans = null;
            }
            catch (Exception)
            {
            }

        }
        public void RollbackTransaction()
        {
            try
            {
                m_trans.Rollback();
                m_trans = null;
            }
            catch (Exception)
            {
            }

        }

    }
}