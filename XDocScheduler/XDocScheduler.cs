using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using System.Threading;

namespace XDocScheduler
{

    class XDocScheduleA
    {
        public string ServiceName = "XDocSchedulerAsync";
        private string sVersion = "";
        public string m_sScheduleTemplate = null;
        public double m_dInterval = 1000;
        private Timer m_timer = new Timer();
        private bool m_bInProcess = false;
        public bool bEventLog = false;
        public XDocServices.XDocService4 xds = null;//new XDocServices.XDocService4();
        XHTMLMerge.AppCache m_AppCache = null;
        XHTMLMerge.SVCache m_SVCache = null;
        private System.Threading.ManualResetEventSlim resetEvent = new System.Threading.ManualResetEventSlim(false);

        public void OnStartA(string[] args)
        {
            try
            {
                if (m_AppCache == null) m_AppCache = new XHTMLMerge.AppCache();
                m_AppCache.ClearTemplatesCache();
                if (bEventLog) LogEventLog("Start schedule template: " + m_sScheduleTemplate + ", interval: " + m_dInterval.ToString());
                m_timer.Start();
                m_timer.Elapsed += new ElapsedEventHandler(XDocSchedulerElapsedEventHandlerA);
                m_timer.Interval = m_dInterval;
                m_timer.Enabled = true;
                XDocSchedulerElapsedEventHandlerA(null, null);
            }
            catch (Exception ex)
            {
                LogEventError("Problem executing " + ServiceName + ": " + ex.Message);
            }
        }
        private void XDocSchedulerElapsedEventHandlerA(object sender, ElapsedEventArgs e)
        {
            if (!m_bInProcess)
            {
                m_bInProcess = true;
                try
                {
                    resetEvent.Reset();
                    if (m_AppCache == null) m_AppCache = new XHTMLMerge.AppCache();
                    if (m_SVCache == null) m_SVCache = new XHTMLMerge.SVCache();
                    //                    string s = xds.RunTemplate(ScheduleTemplate);
                    if (bEventLog) LogEventLog("Executing " + ServiceName + ": " + m_sScheduleTemplate);
                    string s = "";
                    if (m_sScheduleTemplate != "")
                        xds.RunTemplate(null, m_sScheduleTemplate, null, ref m_AppCache, ref m_SVCache);
                    if (bEventLog) LogEventLog("Executed " + ServiceName + ": " + s);
                }
                catch (Exception ex)
                {
                    LogEventError("Error executing " + ServiceName + ": " + ex.Message + "\n " + ex.StackTrace);
                }
                finally
                {
                    resetEvent.Set();
                    m_bInProcess = false;
                }
            }
        }
        private void LogEventError(string sLog)
        {
            try
            {
                string sSource = ServiceName;
                string sType = "Application";

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sType);
                EventLog.WriteEntry(sSource, sLog, EventLogEntryType.Error);
            }
            catch { }
        }
        private void LogEventLog(string sLog)
        {
            try
            {
                string sSource = ServiceName;
                string sType = "Application";

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sType);
                EventLog.WriteEntry(sSource, sLog, EventLogEntryType.Warning);
            }
            catch { }
        }

        public void OnStopA()
        {
            try
            {
                if (bEventLog) LogEventLog("Stopping schedule template: " + m_sScheduleTemplate + ", interval: " + m_dInterval.ToString());
                if (m_timer != null && m_timer.Enabled)
                {
                    m_timer.Enabled = false;
                    m_timer.Stop();
                }
                resetEvent.Wait();
                if (bEventLog) LogEventLog("Stopped schedule template: " + m_sScheduleTemplate + ", interval: " + m_dInterval.ToString());
            }
            catch (Exception ex)
            {
            }
        }
    }

    public partial class XDocScheduler : ServiceBase
    {
        private string m_sScheduleTemplate=null;
        private double m_dInterval = 0;
        private double m_dIntervalSec = 0;
        private Timer m_timer = new Timer();
        private Timer m_timer1 = new Timer();
        private Timer m_timer2 = new Timer();
        private bool m_bInProcess = false;
        private bool bEventLog = false;
        XDocServices.XDocService4 xds = null;//new XDocServices.XDocService4();
        XHTMLMerge.AppCache m_AppCache = null;
        XHTMLMerge.SVCache m_SVCache = null;
        XDocScheduleA[] XDocScheduleAArray = null;
        private System.Threading.ManualResetEventSlim resetEvent = new System.Threading.ManualResetEventSlim(false);
        private  System.Threading.CancellationTokenSource cts = new System.Threading.CancellationTokenSource();
        HttpListener listener = null;
        private Task task = null;
        private string sServiceName = "XDocScheduler5";
        public XDocScheduler()
        {
            InitializeComponent();
        }

        #region Confiruration 
        private string ScheduleTemplate
        {
            get 
            {
                if (m_sScheduleTemplate == null)
                    m_sScheduleTemplate = ConfigurationManager.AppSettings.Get("Schedule_Template");
                if (m_sScheduleTemplate == null)
                    m_sScheduleTemplate = "Schedule";
                    return m_sScheduleTemplate;
            }
        }

        private double Interval
        {
            get
            {
                if (m_dInterval == 0)
                {
                    string si = ConfigurationManager.AppSettings.Get("Interval");
                    if (si == null) si = "0";
                    m_dInterval = 60000 * Double.Parse(si);
                }
                return m_dInterval;
            }
        }
        private double IntervalSec
        {
            get
            {
                if (m_dIntervalSec == 0)
                {
                    string si = ConfigurationManager.AppSettings.Get("IntervalSec");
                    if (si == null) si = "0";
                    m_dIntervalSec = 1000 * Double.Parse(si);
                }
                return m_dIntervalSec;
            }
        }
        #endregion

        #region ServiceBase methods
        
        protected override void OnStart(string[] args)
        {
            try
            {
                bEventLog = false;
                if (ConfigurationManager.AppSettings.Get("EventLog") == "1") bEventLog = true;
                xds = new XDocServices.XDocService4();
                if (m_AppCache == null) m_AppCache = new XHTMLMerge.AppCache();

                double d_interval = IntervalSec;
                if (d_interval == 0)
                    d_interval = Interval;
                //LogEventLog("To start Listener: " + "http://localhost:8080/" + ServiceName + " ");
                Task task = StartListener(cts.Token);
                resetEvent.Set();


                //if (d_interval == 0)
                //    d_interval = 1000*60*60;
                if (d_interval != 0)
                {
                    if (d_interval > 100)
                    {
                        m_timer1.Start();
                        m_timer1.Elapsed += new ElapsedEventHandler(XDocSchedulerElapsedEventHandler);
                        m_timer1.Interval = 100;
                        m_timer1.Enabled = true;
                    }
                    //XDocSchedulerElapsedEventHandler(this, null);
                    m_timer.Start();
                    m_timer.Elapsed += new ElapsedEventHandler(XDocSchedulerElapsedEventHandler);
                    m_timer.Interval = d_interval;
                    m_timer.Enabled = true;
                }
                m_timer2.Elapsed += new ElapsedEventHandler(SchedulersEventHandler);
                m_timer2.Interval = 150;
                m_timer2.Enabled = true;
                m_timer2.Start();
                //LogEventLog("Started " + ServiceName + ": ");
            }
            catch (Exception ex)
            {
                LogEventError("Problem executing " + ServiceName + ": " + ex.Message);
                //this.Stop();
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (bEventLog) LogEventLog("Stopping " + ServiceName + ": ");
                if (m_timer != null && m_timer.Enabled)
                {
                    m_timer.Enabled = false;
                    m_timer.Stop();
                }
                if (m_timer1 != null && m_timer1.Enabled)
                {
                    m_timer1.Enabled = false;
                    m_timer1.Stop();
                }
                if(XDocScheduleAArray!=null)
                foreach (XDocScheduleA sch in XDocScheduleAArray)
                {
                        if (sch != null) sch.OnStopA();
                }

                cts.Cancel();
                if(task!=null)                task.Wait();

                //if (m_bInProcess)
                resetEvent.Wait();
                //while (m_bInProcess) ;
                if (bEventLog) LogEventLog("Stopped " + ServiceName + ": ");

            }
            catch (Exception ex)
            {
                LogEventError("Problem stopping " + ServiceName + ": " + ex.Message);
            }
        }
        #endregion
        async Task StartListener(System.Threading.CancellationToken token)
        {
            try
            {
                //LogEventLog("Listener stating: " + "http://localhost:8080/" + ServiceName + "/ ");
                listener = new HttpListener();
                listener.Prefixes.Add("http://localhost:8080/" + sServiceName + "/");
                listener.Start();
                LogEventLog("Listener stated: " + "http://localhost:8080/" + sServiceName + "/ ");

                token.Register(() => listener.Abort());

                while (!token.IsCancellationRequested)
                {
                    HttpListenerContext context;

                    try
                    {
                        context = await listener.GetContextAsync().ConfigureAwait(false);

                        HandleRequest(context); // Note that this is *not* awaited
                    }
                    catch
                    {
                        // Handle errors
                    }
                }
            }
            catch(Exception ex)
            {
                LogEventError("Problem adding listener " + ServiceName + ": " + ex.Message);

            }
        }
        private void HandleRequest(HttpListenerContext context)
        {
//            LogEventLog("Handle request on: " + "http://localhost:8080/" + ServiceName + "/ ");

            NameValueCollection qs = context.Request.QueryString;
            string sSchedule = "";
            string sInterval = "", sTemplate = "", sEnabled ="1";
            int iSchedule = 0;
            sSchedule = qs.Get("id");
            sInterval = qs.Get("interval");
            sTemplate = qs.Get("template");
            sEnabled = qs.Get("enabled");
            int.TryParse(sSchedule, out iSchedule);
            string responseString = " ";
            if (iSchedule<XDocScheduleAArray.GetLength(0))
            {
                XDocScheduleA sch_old = XDocScheduleAArray[iSchedule];
                if (sch_old != null) sch_old.OnStopA();

                int iInterval = 0;
                if ((sEnabled=="1") && int.TryParse(sInterval, out iInterval) && (iInterval > 0) && (sTemplate != ""))
                {
                    XDocScheduleA sch = new XDocScheduleA();
                    sch.m_dInterval = iInterval;
                    sch.m_sScheduleTemplate = sTemplate;
                    sch.bEventLog = bEventLog;
                    sch.xds = xds;
                    XDocScheduleAArray[iSchedule] = sch;
                    sch.OnStartA(null);
                }
                else
                {
                    XDocScheduleAArray[iSchedule] = null;
                }
            }
            HttpListenerResponse response = context.Response;

            responseString += "{\"id\":" + sSchedule + ",\"interval\":" + sInterval + ",\"template\":\"" + sTemplate + "\",\"enabled\":" + sEnabled + "}";
            byte[] buffer = Encoding.UTF8.GetBytes(responseString);

            response.ContentLength64 = buffer.Length;
            Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);

            output.Close();

        }
        #region implementation
        private void SchedulersEventHandler(object sender, ElapsedEventArgs e)
        {
            XDocScheduleAArray = new XDocScheduleA[101];
            m_timer2.Enabled = false;
            m_timer2.Stop();
            string sSchedules = "";
            sSchedules = m_AppCache.GetSchedules();
            if (sSchedules != "")
            {
                try
                {

                    JToken jt = (JObject.Parse(sSchedules)).SelectToken("Schedules");
                    if (jt is JArray)
                    {
                        JArray ja = (JArray)jt;
                        for (int i = 0; i < ja.Count; i++)
                        {
                            JToken j = ja[i];
                            string sID = "";
                            string sTemplate = "";
                            string sInterval = "";
                            string sEnabled = "1";
                            int ID = 0, iInterval = 0;
                            foreach (JProperty jp in j.Children<JProperty>())
                            {
                                if (jp.Name.ToLower() == "id") sID = jp.Value.ToString();
                                if (jp.Name.ToLower() == "template") sTemplate = jp.Value.ToString();
                                if (jp.Name.ToLower() == "interval") sInterval = jp.Value.ToString();
                                if (jp.Name.ToLower() == "enabled") sEnabled = jp.Value.ToString();
                            }
                            //LogEventLog("sID: " + sID + " sTemplate: " + sTemplate + " sInterval: " +  sInterval + " sEnabled: " + sEnabled +  " ");
                            if ((sEnabled == "1") && int.TryParse(sID, out ID) && int.TryParse(sInterval, out iInterval) && (iInterval > 0) && (sTemplate != ""))
                            {
                                XDocScheduleA sch = new XDocScheduleA();
                                sch.ServiceName = this.ServiceName + "_" + sID;
                                sch.m_dInterval = iInterval;
                                sch.m_sScheduleTemplate = sTemplate;
                                sch.bEventLog = bEventLog;
                                sch.xds = xds;
                                XDocScheduleAArray[ID] = sch;
                                sch.OnStartA(null);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    LogEventError("Problem Schedules.json " + sSchedules + ": " + ex.Message + "\n " );
                }
            }

        }

        private void XDocSchedulerElapsedEventHandler(object sender, ElapsedEventArgs e)
        {
            if (!m_bInProcess)
            {               
                m_bInProcess = true;
                if (m_timer1 != null && m_timer1.Enabled)
                {
                    m_timer1.Enabled = false;
                    m_timer1.Stop();
                }
                try
                {
                    resetEvent.Reset();
                    if (m_AppCache == null) m_AppCache = new XHTMLMerge.AppCache();
                    if (m_SVCache == null) m_SVCache = new XHTMLMerge.SVCache();
                    if (bEventLog) LogEventLog("Executing " + ServiceName + ": " + ScheduleTemplate);
                    string s = "";
                    if(ScheduleTemplate!="")
                        xds.RunTemplate(null,ScheduleTemplate,null, ref m_AppCache, ref m_SVCache);
                    if (bEventLog) LogEventLog("Executed " + ServiceName + ": " + s);
                }
                catch (Exception ex)
                {
                    LogEventError("Problem executing " + ServiceName + ": " + ex.Message + "\n " );
//                    LogEventError("Problem executing " + ServiceName + ": " + ex.Message + "\n " + ex.StackTrace);
                }
                finally
                {
                    resetEvent.Set();
                    m_bInProcess = false;
                }
            }
        }


        private void LogEventError(string sLog)
        {
            try
            {
                string sSource = this.ServiceName;
                string sType = "Application";

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sType);
                EventLog.WriteEntry(sSource, sLog, EventLogEntryType.Error);
            }
            catch { }
        }
        private void LogEventLog(string sLog)
        {
            try
            {
                string sSource = this.ServiceName;
                string sType = "Application";

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sType);
                EventLog.WriteEntry(sSource, sLog, EventLogEntryType.Warning);
            }
            catch { }
        }
        #endregion
        #region Component Designer 
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            if (System.Configuration.ConfigurationManager.AppSettings["ServiceName"] != null) sServiceName = (string)System.Configuration.ConfigurationManager.AppSettings["ServiceName"];
            this.ServiceName = sServiceName;
        }

        #endregion
        #endregion

    }
}
