﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.IdentityModel.Protocols.WSFederation;
using System.Web.Util;
using Microsoft.IdentityModel.Web;


//<link rel = "icon" href="data:;base64,iVBORw0KGgo=">

//namespace XDocServices
//{

//    public class CustomWSFederationAuthenticationModule : WSFederationAuthenticationModule
//    {
//        protected override void InitializePropertiesFromConfiguration(string serviceName)
//        {
//            //this.Realm = "http://localhost:81/";
//            //this.Issuer = "https://acsnamespace.accesscontrol.windows.net/v2/wsfederation";
//            this.Realm = "https://5.175.84.232/xDocClaims/";
//            this.Issuer = "https://kubionadfs.northeurope.cloudapp.azure.com/adfs/ls/";
//            this.RequireHttps = false;
//            this.PassiveRedirectEnabled = true;
//        }
//    }
//}
public class SampleRequestValidator : RequestValidator
{
    //protected override bool IsValidRequestString(HttpContext context, string value, RequestValidationSource requestValidationSource, string collectionKey, out int validationFailureIndex)
    //{
    //    validationFailureIndex = 0;
    //    if (requestValidationSource == RequestValidationSource.Form && collectionKey.Equals(WSFederationConstants.Parameters.Result, StringComparison.Ordinal))
    //    {
    //        SignInResponseMessage message = WSFederationMessage.CreateFromFormPost(context.Request) as SignInResponseMessage;

    //        if (message != null)
    //        {
    //            return true;
    //        }
    //    }

    //    return base.IsValidRequestString(context, value, requestValidationSource, collectionKey, out validationFailureIndex);

    //}
    protected override bool IsValidRequestString(HttpContext context, string value, RequestValidationSource requestValidationSource, string collectionKey, out int validationFailureIndex)
    {
        validationFailureIndex = 0;
        if (requestValidationSource == RequestValidationSource.Form &&
            true)
        //collectionKey.Equals(WSFederationConstants.Parameters.Result, StringComparison.Ordinal))
        {
            if (WSFederationMessage.CreateFromNameValueCollection(WSFederationMessage.GetBaseUrl(context.Request.Url), context.Request.Unvalidated.Form) as SignInResponseMessage != null)
            {
                return true;
            }
        }
        return base.IsValidRequestString(context, value, requestValidationSource, collectionKey, out validationFailureIndex);
    }
}