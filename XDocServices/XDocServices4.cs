﻿using System;
using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using XHTMLMerge;
using System.Web.SessionState;
using Microsoft.IdentityModel.Claims;
using Microsoft.IdentityModel.Web;

namespace XDocServices
{
    public class XDocService
    {
        bool bDebug = false;
        bool bAppCache = true;
        bool bSVCache = true;
        bool bAcceptTypes = false;
        bool bHeaders = false;
        bool bCookies = false;
        bool bFiles = false;
        bool bUserLanguages = false;
        bool bContent = false;
        bool bUser = false;
        bool bParams = false;
        bool bPathParams = true;
        bool bUri = false;
        bool bJWT = false;
        bool bClaims = false;
        bool bWS = false;
        string sJWTSecret = "Kubion";
        string sRootTemplate = "A";
        string sVersion = "";
        WSClients wsClients = null;

        private void LoadConfig()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["Debug"] != null) bDebug = (string)System.Configuration.ConfigurationManager.AppSettings["Debug"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Cache"] != null) bAppCache = (string)System.Configuration.ConfigurationManager.AppSettings["Cache"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Session"] != null) bSVCache = (string)System.Configuration.ConfigurationManager.AppSettings["Session"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] != null) bUri = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_AcceptTypes"] != null) bAcceptTypes = (string)System.Configuration.ConfigurationManager.AppSettings["Request_AcceptTypes"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Headers"] != null) bHeaders = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Headers"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Cookies"] != null) bCookies = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Cookies"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Files"] != null) bFiles = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Files"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_UserLanguages"] != null) bUserLanguages = (string)System.Configuration.ConfigurationManager.AppSettings["Request_UserLanguages"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Content"] != null) bContent = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Content"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_User"] != null) bUser = (string)System.Configuration.ConfigurationManager.AppSettings["Request_User"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Params"] != null) bParams = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Params"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_PathParams"] != null) bPathParams = (string)System.Configuration.ConfigurationManager.AppSettings["Request_PathParams"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] != null) bUri = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_JWT"] != null) bJWT = (string)System.Configuration.ConfigurationManager.AppSettings["Request_JWT"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Claims"] != null) bClaims = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Claims"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["JWTSecret"] != null) sJWTSecret = (string)System.Configuration.ConfigurationManager.AppSettings["JWTSecret"];
            if (System.Configuration.ConfigurationManager.AppSettings["Signature"] != null) sJWTSecret = (string)System.Configuration.ConfigurationManager.AppSettings["Signature"];
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Template"] != null) sRootTemplate = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Template"];
            if (System.Configuration.ConfigurationManager.AppSettings["WS"] != null) bWS = (string)System.Configuration.ConfigurationManager.AppSettings["WS"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Version"] != null) sVersion = (string)System.Configuration.ConfigurationManager.AppSettings["Version"];
            if (sVersion == "") sVersion = "3";
            // 3 = old ReplaceParams and Settings, not tabs, JCMD
            // 4 = new ReplaceParams and Settings, smart tabs, JCMD
            // 5 = new ReplaceParams and Settings, smart tabs, JCMD4
        }

        #region XDoc Template

        private string ParseBlocks(string sText, int iBlocks)
        {//to do: remove "empty" style and script blocks
            sText = "[block_xdocmain]" + sText.Trim() + "[block]";

            int i = 0;
            int lvl = -1;
            string[] aLvlText = new string[20];
            string[] aLvlBlock = new string[20];

            int ni = sText.IndexOf("[block", 1);
            while (ni != -1)
            {
                string s = sText.Substring(i, ni - i);
                if (s.StartsWith("[block]"))
                {
                    if (lvl == 0) throw new Exception("invalid blocks construction: lvl-1");
                    aLvlText[lvl] += "[block]";
                    s = s.Substring("[block]".Length, s.Length - "[block]".Length);
                    i = ni;// +"[block]".Length - 1;
                    aLvlBlock[lvl - 1] += aLvlText[lvl] + aLvlBlock[lvl];
                    //aLvlText [lvl]="";
                    //aLvlBlock [lvl]="";
                    lvl--;
                    aLvlText[lvl] += s;
                }
                else
                {
                    lvl++;
                    if (lvl == 20) throw new Exception("invalid blocks construction lvl20");
                    aLvlText[lvl] = s;
                    aLvlBlock[lvl] = "";
                    i = ni;
                }
                ni = sText.IndexOf("[block", i + 1);
            }
            if (lvl != 0)
                throw new Exception("invalid blocks construction lvl0");
            aLvlText[0] += sText.Substring(i, sText.Length - i);
            aLvlText[0] = aLvlText[0].Substring("[block_xdocmain]".Length, aLvlText[0].Length - "[block_xdocmain]".Length - "[block]".Length);
            string sResult;
            if (iBlocks == 1)
            {
                sResult = aLvlBlock[0];
            }
            else
            {
                sResult = aLvlText[0];
                //if ((aLvlBlock[0].Trim() != "") && (aLvlBlock[0].Trim() != sSeed))
                if (aLvlBlock[0].Trim() != "")
                {
                    sResult += aLvlBlock[0].Trim();
                }
            }
            sResult = sResult.Replace("[block_script][block]", "");
            sResult = sResult.Replace("[block_style][block]", "");
            return sResult;
        }

        private string ContextCompileLibrary(HttpContext m_context, string strTemplatePath)
        {
            string sReturn = "";
            AppCache m_AppCache = null;
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, null);
            m_AppCache = xrun.AppCache;
            sReturn = m_AppCache.CompiledLibrary(strTemplatePath);
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            return sReturn;
        }
        private string ContextCompileTemplate(HttpContext m_context, string strTemplateName)
        {
            string sReturn = "";
            AppCache m_AppCache = null;
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, null);
            m_AppCache = xrun.AppCache;
            if (strTemplateName == "")
                sReturn = m_AppCache.CompileTemplates();
            else
                sReturn = m_AppCache.CompileTemplate(strTemplateName);
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            return sReturn;
        }
        private string ContextUncompileTemplate(HttpContext m_context, string strTemplateName)
        {
            string sReturn = "";
            AppCache m_AppCache = null;
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, null);
            m_AppCache = xrun.AppCache;
            if (strTemplateName == "")
                sReturn = m_AppCache.UncompileTemplates();
            else
                sReturn = m_AppCache.UncompileTemplate(strTemplateName);
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            return sReturn;
        }
        private string ContextExtractTemplate(HttpContext m_context)
        {
            string sReturn = "";
            AppCache m_AppCache = null;
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, null);
            m_AppCache = xrun.AppCache;
            sReturn = m_AppCache.ExtractTemplatesDB();
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            return sReturn;
        }
        public string RunTemplate(string strTemplateName)
        {
            AppCache m_AppCache = null;
            SVCache m_SVCache = null;
            return RunTemplate(strTemplateName, ref m_AppCache, ref m_SVCache);
        }
        public string RunTemplate(string strTemplateName, ref AppCache m_AppCache, ref SVCache m_SVCache)
        {
            if (m_AppCache == null) m_AppCache = new AppCache();
            if (m_SVCache == null) m_SVCache = new SVCache();
            CRuntime xrun = new CRuntime(m_AppCache, m_SVCache);
            //xrun.Clients = cClients;
            ////            xrun.Clients = wsClients;
            //XDocManager xm = new XDocManager();
            //xrun.Manager = null;
            StringBuilder sbResponse = new StringBuilder();
            string sExit = "";
            xrun.myHttpContext = null;
            xrun.sVersion = sVersion;
            string sResult = xrun.RunTemplate(strTemplateName, null, sbResponse, ref sExit);
            string sResponse = sbResponse.ToString();
            if (xrun.ExitValue != "") sExit = xrun.ExitValue;
            xrun.Dispose();
            if (sExit == "")
                return sResponse;
            else
                return sExit;
        }
        public string RunTemplate(HttpContext m_context, string strTemplateName, Hashtable hParameters, ref AppCache m_AppCache, ref SVCache m_SVCache)
        {
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, m_SVCache);
            //xrun.Clients = cClients;
            ////            xrun.Clients = wsClients;
            //XDocManager xm = new XDocManager();
            //xrun.Manager = xm;
            StringBuilder sbResponse = new StringBuilder();
            string sExit = "";
            string sResponse = "";
            xrun.myHttpContext = m_context;
            xrun.sVersion = sVersion;
            try
            {
                string sResult = xrun.RunTemplate(strTemplateName, hParameters, sbResponse, ref sExit);
                sResponse = sbResponse.ToString();

                //if ((hParameters["params_NOBLOCKS"] == null) || (hParameters["params_BLOCKS"] != null))
                //{
                //    if ((hParameters["params_BLOCK"] != null) && ((string)hParameters["params_BLOCK"]=="true"))
                //        sResponse = ParseBlocks(sResponse, 1);
                //    else
                //        sResponse = ParseBlocks(sResponse, 0);
                //}
                if (xrun.ExitValue != "") sExit = xrun.ExitValue;
            }
            catch (Exception ex)
            {
                sExit = ex.Message;
            }
            m_AppCache = xrun.AppCache;
            m_SVCache = xrun.SVCache;
            xrun.Dispose();
            if (sExit == "")
                return sResponse;
            else
                return sExit;
        }
        private string ContextRunTemplate(HttpContext m_context, string strTemplateName, Hashtable hParameters, ref SVCache m_SVCache)
        {
            AppCache m_AppCache = null;
            if (bSVCache && (m_context != null) && (m_context.Session != null)) m_SVCache = (SVCache)m_context.Session["SVCache"];
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if ((m_AppCache == null) && (m_context.Request.Params["cache"] == "1") && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_context.Request.Params["cache"] == "0") m_AppCache = null;
            if ((m_SVCache != null) && (m_context.Request.QueryString["callbackGuid"] == null) && (m_context.Request.QueryString["postbackGuid"] == null)) m_SVCache.ClearIncludeOnce();
            if (m_context != null) { if (m_SVCache == null) m_SVCache = new SVCache(); m_SVCache.FillCookies(m_context.Request.Cookies); }
            string sXDocBlocks = "";
            if (m_context.Request.Params["noblocks"] == "1") sXDocBlocks = "-1";
            if (m_context.Request.Params["noblocks"] == null)
                if (m_context.Request.Params["blocks"] == "true") sXDocBlocks = "1";
                else sXDocBlocks = "0";
            if (sXDocBlocks != "") m_SVCache.SetSV("XDocBlocks", "Response_", sXDocBlocks);
            //if ((hParameters["params_NOBLOCKS"] == null) || (hParameters["params_BLOCKS"] != null))
            //{
            //    if ((hParameters["params_BLOCK"] != null) && ((string)hParameters["params_BLOCK"]=="true"))
            //        sResponse = ParseBlocks(sResponse, 1);
            //    else
            //        sResponse = ParseBlocks(sResponse, 0);
            //}
            string sResult = RunTemplate(m_context, strTemplateName, hParameters, ref m_AppCache, ref m_SVCache);
            //if ((Cache != "0") && (m_context != null) && (m_context.Application != null)) m_context.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            //if ((CacheSV != "0") && (m_context != null) && (m_context.Session != null)) m_context.Session["XDocCache"] = m_xdocCache;
            if ((m_context != null) && (m_SVCache != null)) m_SVCache.WriteCookies(m_context.Response.Cookies);
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            m_SVCache.SaveSVCache0();
            if ((m_context != null) && (m_context.Session != null)) m_context.Session["SVCache"] = m_SVCache;
            if ((m_context != null) && (m_context.Session != null)) m_context.Session["SessionID"] = m_SVCache.SessionID;
            return sResult;
        }
        #endregion

        #region util
        private string myRequestRender(HttpRequest req, StringBuilder sJSON)
        {//URI = scheme "://" authority "/" path [ "?" query ] [ "#" fragment ]
            string sResult = "";
            string sApplicationPath = req.ApplicationPath;
            string sPath = req.Path;
            string sRelativePath = "";
            if (sPath.Length > sApplicationPath.Length)
            {
                sRelativePath = sPath.Substring(sApplicationPath.Length, sPath.Length - sApplicationPath.Length);
                sPath = sPath.Substring(0, sApplicationPath.Length);
            }
            if (sRelativePath.StartsWith("/")) sRelativePath = sRelativePath.Substring(1, sRelativePath.Length - 1);
            if (sApplicationPath.StartsWith("/")) sApplicationPath = sApplicationPath.Substring(1, sApplicationPath.Length - 1);
            if (sPath.StartsWith("/")) sPath = sPath.Substring(1, sPath.Length - 1);
            if (sRelativePath.ToLower().EndsWith(".nosession")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".nosession".Length);
            if (sRelativePath.ToLower().EndsWith(".rosession")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".rosession".Length);
            if (sRelativePath.ToLower().EndsWith(".session")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".session".Length);
            if (sRelativePath.ToLower().EndsWith(".asyncnosession")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".asyncnosession".Length);
            if (sRelativePath.ToLower().EndsWith(".asyncrosession")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".asyncrosession".Length);
            if (sRelativePath.ToLower().EndsWith(".async")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".async".Length);
            sResult = sRelativePath;

            //if (sRelativePath == "dummy")
            //    return "dummy";
            //if (sRelativePath.StartsWith("compile"))
            //    return sRelativePath;
            //if (sRelativePath.StartsWith("library"))
            //    return sRelativePath;
            if (sRelativePath.StartsWith("_v"))
            {
                sResult = sRelativePath.Substring(0, 3);
                sRelativePath = sRelativePath.Substring("_v0/".Length);
            }

            string postedData = myReadContent(req);
            try
            {
                NameValueCollection rp = req.Params;
            }
            catch (Exception) { };

            sJSON.Append("{\"request\":{");
            //json=result
            //noblock=1
            //uc=1
            sJSON.Append("\"Handler\":\"XDocServices.XDocService4\",");
            if (bParams) { sJSON.Append("\"Params\":"); myNameValueCollectionRender(sJSON, req.Params, req.ServerVariables); sJSON.Append(","); }
            if (bPathParams) { sJSON.Append("\"PathParams\":"); Utils.myPathParamsRender(sJSON, sRelativePath); sJSON.Append(","); }
            if (bAcceptTypes) { sJSON.Append("\"AcceptTypes\":"); myStringArrayRender(sJSON, req.AcceptTypes); sJSON.Append(","); }
            if (bHeaders) { sJSON.Append("\"Headers\":"); myNameValueCollectionRender(sJSON, req.Headers, null); sJSON.Append(","); }
            if (bCookies)
            {
                sJSON.Append("\"CookiesArray\":"); myCookieCollectionArrayRender(sJSON, req.Cookies); sJSON.Append(",");
                sJSON.Append("\"Cookies\":"); myCookieCollectionRender(sJSON, req.Cookies); sJSON.Append(",");
            }
            if (bFiles) { sJSON.Append("\"Files\":"); myFilesCollectionArrayRender(sJSON, req.Files); sJSON.Append(","); }
            if (bUserLanguages) { sJSON.Append("\"UserLanguages\":"); myStringArrayRender(sJSON, req.UserLanguages); sJSON.Append(","); }

            if (bUri)
            {
                sJSON.Append("\"RequestApplicationPath\":\""); sJSON.Append(Utils._JsonEscape(req.ApplicationPath)); sJSON.Append("\",");
                sJSON.Append("\"RequestPath\":\""); sJSON.Append(Utils._JsonEscape(req.Path)); sJSON.Append("\",");
                sJSON.Append("\"RequestWebsite\":\""); sJSON.Append(Utils._JsonEscape(sPath)); sJSON.Append("\",");
                string sBaseURL = req.Url.Scheme + "://" + req.Url.Host + ":" + req.Url.Port.ToString();
                if (sPath != "") sBaseURL += "/" + sPath;
                sJSON.Append("\"RequestBaseURL\":\""); sJSON.Append(Utils._JsonEscape(sBaseURL)); sJSON.Append("\",");
                //  #SPAR.BaseURL.%req_UriScheme%://%req_UriHost%:%req_UriPort%/%req_URIWebsite%#
                //  #IF(#PAR.req_URIWebsite#==)# #SPAR.BaseURL.%req_UriScheme%://%req_UriHost%:%req_UriPort%# #ENDIF#
                sJSON.Append("\"UriScheme\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Scheme)); sJSON.Append("\",");
                sJSON.Append("\"UriHost\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Host)); sJSON.Append("\",");
                sJSON.Append("\"UriPort\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Port.ToString())); sJSON.Append("\",");
                sJSON.Append("\"UriServer\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Scheme + ":\\" + req.Url.Host + ":" + req.Url.Port.ToString())); sJSON.Append("\",");
                sJSON.Append("\"UriWebsite\":\""); sJSON.Append(Utils._JsonEscape(sApplicationPath)); sJSON.Append("\",");
                string sBaseURL1 = req.Url.Scheme + "://" + req.Url.Host + ":" + req.Url.Port.ToString();
                if (sApplicationPath != "") sBaseURL1 += "/" + sApplicationPath;
                sJSON.Append("\"UriBaseURL\":\""); sJSON.Append(Utils._JsonEscape(sBaseURL1)); sJSON.Append("\",");
                sJSON.Append("\"UriPath\":\""); sJSON.Append(Utils._JsonEscape(sRelativePath)); sJSON.Append("\",");
                myUriPathRender(sJSON, sRelativePath);
                sJSON.Append("\"UriQuery\":\""); sJSON.Append(Utils._JsonEscape(req.QueryString.ToString())); sJSON.Append("\",");
            }
            if (bContent)
            {
                sJSON.Append("\"ContentType\":\""); sJSON.Append(Utils._JsonEscape(req.ContentType)); sJSON.Append("\",");
                int iContentLength = req.ContentLength;
                sJSON.Append("\"ContentLength\":\""); sJSON.Append(Utils._JsonEscape(iContentLength.ToString())); sJSON.Append("\",");
                sJSON.Append("\"Content\":\""); sJSON.Append(Utils._JsonEscape(postedData)); sJSON.Append("\",");
                //////#IF(#PAR.req_ContentType#===^multipart/form-data;)##JPAR.Data.%req_Params%.#
                //////#ELSEIF(#PAR.req_ContentType#==application/x-www-form-urlencoded)##JPAR.Data.%req_Params%.#
                //////#ELSEIF(#PAR.req_ContentType#===^text/plain;)##JPAR.Data.%req_Content%.#
                //////#ENDIF#
                if (req.ContentType.StartsWith("multipart/form-data") || req.ContentType.StartsWith("application/x-www-form-urlencoded")) { sJSON.Append("\"Data\":"); myNameValueCollectionRender(sJSON, req.Params, req.ServerVariables); sJSON.Append(","); }
                else if (req.ContentType.StartsWith("text/plain")) { sJSON.Append("\"Data\":\""); sJSON.Append(Utils._JsonEscape(postedData)); sJSON.Append("\","); }
            }
            if (bUser)
            {
                sJSON.Append("\"UserHostAddress\":\""); sJSON.Append(Utils._JsonEscape(req.UserHostAddress)); sJSON.Append("\",");
                sJSON.Append("\"UserHostName\":\""); sJSON.Append(Utils._JsonEscape(req.UserHostName)); sJSON.Append("\",");
                sJSON.Append("\"UserAgent\":\""); sJSON.Append(Utils._JsonEscape(req.UserAgent)); sJSON.Append("\",");
                sJSON.Append("\"UserName\":\""); sJSON.Append(Utils._JsonEscape(req.LogonUserIdentity.Name)); sJSON.Append("\",");
            }
            if (bClaims)
            {
                IClaimsIdentity claimsPrincipal = System.Threading.Thread.CurrentPrincipal.Identity as IClaimsIdentity;
                if (claimsPrincipal != null)
                {
                    sJSON.Append("\"ClaimIsAuthenticated\":\""); sJSON.Append(Utils._JsonEscape(claimsPrincipal.IsAuthenticated)); sJSON.Append("\",");
                    sJSON.Append("\"ClaimName\":\""); sJSON.Append(Utils._JsonEscape(claimsPrincipal.Name)); sJSON.Append("\",");
                    sJSON.Append("\"ClaimLabel\":\""); sJSON.Append(Utils._JsonEscape(claimsPrincipal.Label)); sJSON.Append("\",");
                    if (claimsPrincipal != null && claimsPrincipal.IsAuthenticated)
                    {
                        sJSON.Append("\"ClaimsArray\":"); myClaimCollectionArrayRender(sJSON, claimsPrincipal.Claims); sJSON.Append(",");
                        sJSON.Append("\"Claims\":"); myClaimCollectionRender(sJSON, claimsPrincipal.Claims); sJSON.Append(",");
                    }
                }
                else
                {
                    sJSON.Append("\"ClaimIsAuthenticated\":null,");
                    sJSON.Append("\"ClaimName\":null,");
                    sJSON.Append("\"ClaimLabel\":null,");
                }
                //Microsoft.IdentityModel.Protocols.WSFederation.SignInRequestMessage signInRequest = FederatedAuthentication.WSFederationAuthenticationModule.CreateSignInRequest("passive", HttpContext.Current.Request.RawUrl, false);
                //string myurl = signInRequest.RequestUrl;
                //HttpContext.Current.Response.Redirect(myurl);

            }
            if (bJWT)
            {
                string sSecret = "";
                if (System.Configuration.ConfigurationManager.AppSettings["Signature"] != null) sSecret = (string)System.Configuration.ConfigurationManager.AppSettings["Signature"];
                string sJWTHeader = "", sJWTHeaderPayload = "{}";
                string sJWTCookie = "", sJWTCookiePayload = "{}";
                string s = req.Headers["Authorization"];
                if ((s != null) && s.StartsWith("Bearer "))
                {
                    try
                    {
                        sJWTHeader = s.Substring("Bearer ".Length);
                        sJWTHeaderPayload = JWT.JsonWebToken.JWTDecode(sJWTHeader, sSecret, false);
                    }
                    catch (Exception) { };
                }
                s = req.Cookies.Get("Authorization") != null ? req.Cookies.Get("Authorization").Value : null;
                if ((s != null) && s.StartsWith("Bearer "))
                {
                    try
                    {
                        sJWTCookie = s.Substring("Bearer ".Length);
                        sJWTCookiePayload = JWT.JsonWebToken.JWTDecode(sJWTCookie, sSecret, false); ;
                    }
                    catch (Exception) { };
                }
                sJSON.Append("\"JWTHeader\":\""); sJSON.Append(Utils._JsonEscape(sJWTHeader)); sJSON.Append("\",");
                sJSON.Append("\"JWTHeaderPayload\":"); sJSON.Append(sJWTHeaderPayload); sJSON.Append(",");
                //                sJSON.Append("\"JWTHeaderPayload\":\""); sJSON.Append(Utils._JsonEscape(sJWTHeaderPayload)); sJSON.Append("\",");
                //                sJSON.Append("\"JWTHeaderClaim\":"); sJSON.Append(Utils._JsonEscape(sJWTHeaderClaim)); sJSON.Append(",");
                sJSON.Append("\"JWTCookie\":\""); sJSON.Append(Utils._JsonEscape(sJWTCookie)); sJSON.Append("\",");
                sJSON.Append("\"JWTCookiePayload\":"); sJSON.Append(sJWTCookiePayload); sJSON.Append(",");
                //                sJSON.Append("\"JWTCookiePayload\":\""); sJSON.Append(Utils._JsonEscape(sJWTCookiePayload)); sJSON.Append("\",");
                //                sJSON.Append("\"JWTCookieClaim\":"); sJSON.Append(Utils._JsonEscape(sJWTCookieClaim)); sJSON.Append(",");
            }
            sJSON.Append("\"HttpMethod\":\""); sJSON.Append(Utils._JsonEscape(req.HttpMethod)); sJSON.Append("\",");
            sJSON.Append("\"HttpUrl\":\""); sJSON.Append(Utils._JsonEscape(req.Url.ToString())); sJSON.Append("\"");
            sJSON.Append("}}");

            return sResult;
        }
        public static int myUriPathRender(StringBuilder sJSON, string sPath)
        {
            int i = 0;
            string[] aPath = sPath.Split('/');
            string sIdsPath = "";
            sIdsPath = aPath[0];
            for (i = 0; i < aPath.Length; i++)
            {
                sJSON.Append("\"");
                sJSON.Append(Utils._JsonEscape("UriPath" + (i + 1).ToString()));
                sJSON.Append("\":\"");
                sJSON.Append(Utils._JsonEscape(aPath[i]));
                sJSON.Append("\",");
            }
            return i;
        }
        private string myReadContent(HttpRequest req)
        {
            string postedData = "";
            int iContentLength = req.ContentLength;
            //if (iContentLength < 5000)
            //{
            StreamReader reader = new StreamReader(req.InputStream, req.ContentEncoding);
            postedData = reader.ReadToEnd().Trim();
            return postedData;
            //}
            //else
            //    return "[[datasize exceeds limit]]";
        }
        private int myNameValueCollectionRender(StringBuilder sJSON, NameValueCollection col, NameValueCollection exclude)
        {
            int i = 0;
            try
            {
                if ((col == null) || (col.Keys == null))
                {
                    sJSON.Append("null");
                    return -1;
                }
                sJSON.Append("{");
                foreach (string key in col.Keys)
                {
                    if ((exclude == null) || (key == null) || (exclude[key] == null))
                    {
                        sJSON.Append("\"");
                        sJSON.Append(Utils._JsonEscape(key));
                        sJSON.Append("\":\"");
                        try
                        {
                            sJSON.Append(Utils._JsonEscape(col[key].ToString()));
                        }
                        catch (Exception e)
                        {
                            sJSON.Append(Utils._JsonEscape(e.Message));
                        }
                        sJSON.Append("\",");
                        i++;
                    }
                }
                sJSON.Append("\"Count\":\"");
                sJSON.Append(Utils._JsonEscape(i.ToString()));
                sJSON.Append("\"");
                sJSON.Append("}");
                return col.Count;
            }
            catch (Exception)
            { }
            sJSON.Append("null");
            return -1;
        }
        private int myStringArrayRender(StringBuilder sJSON, string[] col)
        {
            bool isFirst = true;
            sJSON.Append("[");
            if (col != null)
                foreach (string key in col)
                {
                    if (isFirst)
                        isFirst = false;
                    else
                        sJSON.Append(",");
                    sJSON.Append("\""); sJSON.Append(Utils._JsonEscape((key))); sJSON.Append("\"");
                }
            sJSON.Append("]");
            if (col != null)
                return col.GetLength(0);
            else
                return 0;
        }
        private int myCookieCollectionRender(StringBuilder sJSON, HttpCookieCollection col)
        {
            sJSON.Append("{");
            foreach (string skey in col)
            {
                HttpCookie key = col[skey];
                sJSON.Append("\"");
                sJSON.Append(Utils._JsonEscape(key.Name));
                sJSON.Append("\":\"");
                sJSON.Append(Utils._JsonEscape(key.Value));
                sJSON.Append("\",");
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(Utils._JsonEscape(col.Count.ToString()));
            sJSON.Append("\"");
            sJSON.Append("}");
            return col.Count;
        }
        private int myClaimCollectionRender(StringBuilder sJSON, ClaimCollection col)
        {
            sJSON.Append("{");
            foreach (Claim claim in col)
            {
                sJSON.Append("\"");
                sJSON.Append(Utils._JsonEscape(claim.ClaimType));
                sJSON.Append("\":\"");
                sJSON.Append(Utils._JsonEscape(claim.Value));
                sJSON.Append("\",");
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(Utils._JsonEscape(col.Count.ToString()));
            sJSON.Append("\"");
            sJSON.Append("}");
            return col.Count;
        }
        private int myClaimCollectionArrayRender(StringBuilder sJSON, ClaimCollection col)
        {
            bool isFirst = true;
            sJSON.Append("[");
            foreach (Claim claim in col)
            {
                if (isFirst)
                    isFirst = false;
                else
                    sJSON.Append(",");
                sJSON.Append("{");
                sJSON.Append("\"ClaimType\":\""); sJSON.Append(Utils._JsonEscape(claim.ClaimType)); sJSON.Append("\",");
                sJSON.Append("\"Value\":\""); sJSON.Append(Utils._JsonEscape(claim.Value)); sJSON.Append("\",");
                sJSON.Append("\"ValueType\":\""); sJSON.Append(Utils._JsonEscape(claim.ValueType)); sJSON.Append("\",");
                sJSON.Append("\"Subject\":\""); sJSON.Append(Utils._JsonEscape(claim.Subject.ToString())); sJSON.Append("\",");
                sJSON.Append("\"Issuer\":\""); sJSON.Append(Utils._JsonEscape(claim.Issuer)); sJSON.Append("\"");
                sJSON.Append("}");
            }
            sJSON.Append("]");
            return col.Count;
        }
        private int myCookieCollectionArrayRender(StringBuilder sJSON, HttpCookieCollection col)
        {
            bool isFirst = true;
            sJSON.Append("[");
            foreach (string skey in col)
            {
                HttpCookie key = col[skey];
                if (isFirst)
                    isFirst = false;
                else
                    sJSON.Append(",");
                sJSON.Append("{");
                sJSON.Append("\"Domain\":\""); sJSON.Append(Utils._JsonEscape(key.Domain)); sJSON.Append("\",");
                sJSON.Append("\"Name\":\""); sJSON.Append(Utils._JsonEscape(key.Name)); sJSON.Append("\",");
                sJSON.Append("\"Path\":\""); sJSON.Append(Utils._JsonEscape(key.Path)); sJSON.Append("\",");
                sJSON.Append("\"Value\":\""); sJSON.Append(Utils._JsonEscape(key.Value)); sJSON.Append("\",");
                sJSON.Append("\"Expires\":\""); sJSON.Append(Utils._JsonEscape(key.Expires.ToString())); sJSON.Append("\"");
                sJSON.Append("}");
            }
            sJSON.Append("]");
            return col.Count;
        }
        private int myFilesCollectionArrayRender(StringBuilder sJSON, HttpFileCollection col)
        {
            sJSON.Append("[");
            if (col.Count > 0)
            {
                bool isFirst = true;
                foreach (string key in col)
                {
                    HttpPostedFile _file = col[key];
                    if (isFirst)
                        isFirst = false;
                    else
                        sJSON.Append(",");
                    sJSON.Append("{");
                    // juni UPLOADS
                    // S_Files
                    //CRuntime runtime = new CRuntime();
                    //sJSON.Append("\"FileId\":\""); sJSON.Append(Utils._JsonEscape(mySaveUploadFile(_file, runtime))); sJSON.Append("\",");
                    sJSON.Append("\"FileName\":\""); sJSON.Append(Utils._JsonEscape(_file.FileName)); sJSON.Append("\",");
                    sJSON.Append("\"ContentType\":\""); sJSON.Append(Utils._JsonEscape(_file.ContentType)); sJSON.Append("\",");
                    sJSON.Append("\"ContentLength\":\""); sJSON.Append(Utils._JsonEscape(_file.ContentLength.ToString())); sJSON.Append("\",");
                    sJSON.Append("}");
                }
                //xm.Dispose();
            }
            sJSON.Append("]");
            return col.Count;
        }

        private string[] MySplit(string sVal, char cSeparator)
        {
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());
            }
            return aData;
        }
        // ^[ ]*(id|name)[ ]*$|^[ ]*\([ ]*(id|name)[ ]*\)[ ]*([ ]*(OR|AND)[ ]*\([ ]*(id|name)[ ]*\))*[ ]*$|^[ ]*\([ ]*\([ ]*(id|name)[ ]*\)[ ]*([ ]*(OR|AND)[ ]*\([ ]*(id|name)[ ]*\))*[ ]*\)[ ]*$
        //  (name) 
        //(id) 
        // name  
        //( name  ) OR (id)
        // ( (name) AND ( id ) OR (name)  OR  (name)   ) 
        //(id)AND (name) 
        #endregion

        public void ProcessRequest(HttpContext context)
        {
            SVCache m_SVCache = null;
            /*syntax:
             *
             * 
             * /library/<path>
             * 
             * /compile/<path>
             * 
             * /extract
             * 
             * /_appcache/[CFGGET|CFGDEL|GET|DEL|CLEAR]
             * 
             * /_svcache/[TEMPLATEONCE|TEMPLATEONCEDEL|TEMPLATEONCECLEAR|DEL|GET]
             * 
             * /__version
             * 
             */

            //System.Diagnostics.Debug.WriteLine("start" + "" + ": " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            LoadConfig();
            if (bWS)
            {
                if (context.IsWebSocketRequest)
                {
                    if (wsClients == null)
                        wsClients = (WSClients)context.Application["wsClients"];
                    if (wsClients == null)
                    {
                        wsClients = new WSClients();
                        context.Application["wsClients"] = wsClients;
                    }
                    if (wsClients != null)
                        context.AcceptWebSocketRequest(wsClients.Connect);
                    return;
                }
            }
            //m_context = context;
            context.Request.ValidateInput();
            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "input validated");
            StringBuilder sJSON = new StringBuilder();
            string sResult = myRequestRender(context.Request, sJSON);
            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "request builded");
            if (bDebug)
            {
                if (sResult.IndexOf("adfs", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    var signInRequest = FederatedAuthentication.WSFederationAuthenticationModule.CreateSignInRequest("passive", context.Request.RawUrl, false);
                    context.Response.Redirect(signInRequest.RequestUrl);
                }
                if (sResult.IndexOf("library", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (sResult.Length > "library/".Length)
                        sResult = sResult.Substring("library/".Length);
                    else
                        sResult = "";
                    string sResponse = ContextCompileLibrary(context, sResult);
                    context.Response.Write("LIBRARY:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("extract", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string sResponse = ContextExtractTemplate(context);
                    context.Response.Write("Extract:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("compile", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (sResult.Length > "compile/".Length)
                        sResult = sResult.Substring("compile/".Length);
                    else
                        sResult = "";
                    string sResponse = ContextCompileTemplate(context, sResult);
                    context.Response.Write("COMPILE:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("uncompile", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (sResult.Length > "uncompile/".Length)
                        sResult = sResult.Substring("uncompile/".Length);
                    else
                        sResult = "";
                    string sResponse = ContextUncompileTemplate(context, sResult);
                    context.Response.Write("UNCOMPILE:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("_appcache", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string sResponse = "";
                    sResult = sResult.Substring("_appcache".Length);
                    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);
                    if (sResult == "") sResult = "GET";

                    AppCache m_AppCache = null;
                    if (sResult.ToUpper() == "CLEAR") { m_AppCache = new AppCache(); context.Application["AppCache"] = m_AppCache; }
                    if (bAppCache && (context != null) && (context.Application != null)) m_AppCache = (AppCache)context.Application["AppCache"];
                    if (m_AppCache == null) m_AppCache = new AppCache();
                    m_AppCache.GetSchedules();
                    sResponse = m_AppCache.AppCacheCommand(sResult);
                    context.Response.Write("_appcache:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("_svcache", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string sResponse = "";
                    sResult = sResult.Substring("_svcache".Length);
                    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);
                    if (sResult == "") sResult = "GET";

                    if (bSVCache && (context != null) && (context.Session != null)) m_SVCache = (SVCache)context.Session["SVCache"];
                    if (m_SVCache == null) m_SVCache = new SVCache();

                    sResponse = m_SVCache.SVCacheCommand(sResult);
                    context.Response.Write("_svcache:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("_svcache0", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string sResponse = "";
                    sResult = sResult.Substring("_svcache0".Length);
                    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);
                    if (sResult == "") sResult = "GET";

                    if (bSVCache && (context != null) && (context.Application != null)) m_SVCache = (SVCache)context.Application["SVCache0"];
                    if (m_SVCache == null) m_SVCache = new SVCache();

                    sResponse = m_SVCache.SVCacheCommand(sResult);
                    context.Response.Write("_svcache0:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("__version", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    sResult = sResult.Substring("__version".Length);
                    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);

                    StringBuilder sbResponse = new StringBuilder();
                    sbResponse.Append("{");
                    sbResponse.Append("\"request\":" + sJSON.ToString());
                    sbResponse.Append(",\"version\":" + sVersion);
                    sbResponse.Append(",\"_XDOCVERSION_\":" + "\"5.0.0.171218\"");
                    sbResponse.Append(",\"whatsnew\":[");
                    sbResponse.Append(" \"" + "171001: new setting in web.config 'bak' (0,1)" + "\"");
                    sbResponse.Append(",\"" + "171001: new setting in web.config 'trace' (0,1,2)" + "\"");
                    sbResponse.Append(",\"" + "171001: in JLOOP set also parameter _XML " + "\"");
                    sbResponse.Append(",\"" + "171001: in JLOOP accept also non array jpath ending with [0] " + "\"");
                    sbResponse.Append(",\"" + "171001: support for XPATH command " + "\"");
                    sbResponse.Append(",\"" + "171101: file_copy and dir_copy for FUNC command " + "\"");
                    sbResponse.Append(",\"" + "171101: dir_merge for FUNC command " + "\"");
                    sbResponse.Append(",\"" + "171101: file_exists, dir_exists, file_fullname for FUNC command " + "\"");
                    sbResponse.Append(",\"" + "171101: bug fix DELETETEMPLATE command " + "\"");
                    sbResponse.Append(",\"" + "171101: support for WebSockets " + "\"");
                    sbResponse.Append(",\"" + "171101: new FUNC commands: ws_addticket, ws_removeticket, ws_removeuser, ws_sendticket, ws_senduser, ws_gettickets, ws_getusers" + "\"");
                    sbResponse.Append(",\"" + "171101: Provider=LDAP;domain=dom;username=user;password=psw" + "\"");
                    sbResponse.Append(",\"" + "171101: LDAP filter as query" + "\"");
                    sbResponse.Append(",\"" + "171101: 0xxx context session variables saved in application" + "\"");
                    sbResponse.Append(",\"" + "171109: fix for &null parameter in url" + "\"");
                    sbResponse.Append(",\"" + "171109: JDATA connection SPLIT_multicharseparator" + "\"");
                    sbResponse.Append(",\"" + "171117: Scheduler executes first job delayed after service started" + "\"");
                    sbResponse.Append(",\"" + "171117: JLOOP has a default pagesize of 100000 (not 1000 anymore)" + "\"");
                    sbResponse.Append(",\"" + "171117: JSONEscape encoding escapes also special double quotes  “ ”" + "\"");
                    sbResponse.Append(",\"" + "171120: new FUNC datetime_dayofweek and dayofyear" + "\"");
                    sbResponse.Append(",\"" + "171123: if supports SQL expressions" + "\"");
                    sbResponse.Append(",\"" + "171123: fix set error parameter on INCLUDE and INCLUDEONCE when ignore=1" + "\"");
                    sbResponse.Append(",\"" + "171128: FUNC jwt_decode token,psw,salt=1,verify=1" + "\"");
                    sbResponse.Append(",\"" + "171128: FUNC jwt_encode payload,psw,salt=1,alg=HS256" + "\"");
                    sbResponse.Append(",\"" + "171128: new FUNC jwt_token header,payload,psw,salt=1,alg=HS256" + "\"");
                    sbResponse.Append(",\"" + "171128: new FUNC salt key,usedate=false" + "\"");
                    sbResponse.Append(",\"" + "171128: web.config WS setting default 0 = ignores websokets" + "\"");
                    sbResponse.Append(",\"" + "171128: fix bug in SETFLD  tht did an append value" + "\"");
                    sbResponse.Append(",\"" + "171208: XDocScheduler supports multiple parallel tasks" + "\"");
                    sbResponse.Append(",\"" + "171208: s_Schedules json to define Schediles array" + "\"");
                    sbResponse.Append(",\"" + "171208: when version='3' because of backwards compatibility INCLUDE will not set error when missing" + "\"");
                    sbResponse.Append(",\"" + "171212: entry http://localhost:8080/<serviceName>?ID=1&Interval=0&Template=s&enabled=1" + "\"");
                    sbResponse.Append(",\"" + "171218: jloop.i.while loops int.MaxValue and jloop.i.while.1==1 loops max 100000" + "\"");
                    sbResponse.Append("]");
                    //int iRun = Runners();
                    //sbResponse.Append(",\"run\":" + iRun.ToString() );
                    //AKIAJBZZYPJWGYADQY4Q
                    //cmxZDCAN40fu/FQf5lbCxQlTnsa/2rlFkGhm2Fcd
                    sbResponse.Append("}");
                    JToken ob = JToken.Parse(sbResponse.ToString());
                    sResult = ob.ToString(Newtonsoft.Json.Formatting.Indented);
                    context.Response.Write(sResult);
                    context.Response.ContentType = "application/json; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                //if (sResult.IndexOf("_azure", StringComparison.CurrentCultureIgnoreCase) == 0)
                //{
                //    string sResponse = "";
                //    sResult = sResult.Substring("_azure".Length);
                //    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);
                //    if (sResult == "") sResult = "GET";

                //    sResponse = sJSON.ToString();
                //    context.Response.Write(sResponse);
                //    context.Response.ContentType = "application/json; charset=utf-8";// "text/xml; charset=utf-8";
                //    return;
                //}
            }
            string strContent = "";
            Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
            newHash.Add("Request", sJSON.ToString());
            newHash.Add("RemoveTab", "1");
            if ((context != null) && (context.Session != null)) m_SVCache = (SVCache)context.Session["SVCache"];
            if (m_SVCache == null) m_SVCache = new SVCache();
            m_SVCache.SetSV("_REQUEST_", "_", sJSON.ToString());

            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "start processing");
            strContent = ContextRunTemplate(context, sRootTemplate, newHash, ref m_SVCache);
            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "template processed");

            string sEncoding = "";
            if (m_SVCache != null)
            {
                string sHeaders = m_SVCache.GetSV("*", "Response_Headers", "");
                string[] aHeaders = MySplit(sHeaders, ';');
                foreach (string sHeader in aHeaders)
                    if (sHeader != "")
                    {
                        string[] aHeader = MySplit(sHeader + "=", '=');
                        context.Response.AddHeader(aHeader[0], aHeader[1]);
                    }
                string sStatus = m_SVCache.GetSV("Status", "Response_", "200 OK");
                context.Response.Status = sStatus;
                sEncoding = m_SVCache.GetSV("Encoding", "Response_", "");
                string sXDocBlocks_after = m_SVCache.GetSV("XDocBlocks", "Response_", ""); //-1=noblocks 0=extract subblocs 1=extract subblocks & take only blocks 
                int iXDocBlocks = 0;
                if (sXDocBlocks_after != "")
                    if (sXDocBlocks_after == "1")
                        iXDocBlocks = 1;
                    else if (sXDocBlocks_after == "-1")
                        iXDocBlocks = -1;
                    else
                        iXDocBlocks = 0;

                if (iXDocBlocks > -1)
                    strContent = ParseBlocks(strContent, iXDocBlocks);
                //////if ((hParameters["params_NOBLOCKS"] == null) || (hParameters["params_BLOCKS"] != null))
                //////{
                //////    if ((hParameters["params_BLOCK"] != null) && ((string)hParameters["params_BLOCK"] == "true"))
                //////        strContent = ParseBlocks(strContent, 1);
                //////    else
                //////        strContent = ParseBlocks(strContent, 0);
                //////}
                string sJSONResponse = m_SVCache.GetSV("JSONResponse", "Response_", ""); //default result
                if (sJSONResponse != "")
                {
                    if (sJSONResponse == "1") sJSONResponse = "result";
                    strContent = "{\"" + sJSONResponse + "\":\"" + Utils._JsonEscape(strContent) + "\"}";
                }
                string sXDocAppCache = m_SVCache.GetSV("XDocAppCache", "Response_", ""); //default result
                //if (sXDocAppCache == "1")
                //{
                //    if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
                //}
                if (sXDocAppCache == "0")
                {
                    if ((context != null) && (context.Application != null)) context.Application["AppCache"] = null;
                }
                string sXDocSVCache = m_SVCache.GetSV("XDocSVCache", "Response_", ""); //default result
                //if (sXDocSVCache == "1")
                //{
                //    //                if ((context != null) && (m_SVCache != null)) m_SVCache.WriteCookies(context.Response.Cookies);
                //    //                if ((context != null) && (context.Application != null)) context.Application["AppCache"] = m_AppCache;
                //    m_SVCache.SaveSVCache0();
                //    if ((context != null) && (context.Session != null)) context.Session["SVCache"] = m_SVCache;
                //    if ((context != null) && (context.Session != null)) context.Session["SessionID"] = m_SVCache.SessionID;
                //}
                if (sXDocSVCache == "0")
                {
                    //                if ((context != null) && (m_SVCache != null)) m_SVCache.WriteCookies(context.Response.Cookies);
                    //                if ((context != null) && (context.Application != null)) context.Application["AppCache"] = m_AppCache;
                    if ((context != null) && (context.Session != null)) context.Session["SVCache"] = null;
                    if ((context != null) && (context.Session != null)) context.Session["SessionID"] = null;
                }
            }

            //System.Diagnostics.Debug.WriteLine("end:" + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));

            if (sEncoding == "")
                context.Response.Write(strContent);
            else
            {
                System.Text.Encoding enc = null;
                try
                {
                    enc = System.Text.Encoding.GetEncoding(sEncoding);
                }
                catch (Exception)
                {
                    enc = System.Text.Encoding.Default;
                }
                byte[] arrContent = enc.GetBytes(strContent);
                context.Response.OutputStream.Write(arrContent, 0, arrContent.Length);
                context.Response.OutputStream.Flush();
            }
        }

    }
    public class HXDocService4 : IHttpHandler, IRequiresSessionState
    {
        private XDocService xdoc = new XDocService();
        public bool IsReusable
        {
            get { return false; }
        }
        public void ProcessRequest(HttpContext context)
        {
            xdoc.ProcessRequest(context);
        }
    }
    public class HXDocService4S : IHttpHandler
    {
        private XDocService xdoc = new XDocService();
        public bool IsReusable
        {
            get { return false; }
        }
        public void ProcessRequest(HttpContext context)
        {
            xdoc.ProcessRequest(context);
        }
    }
    public class HXDocService4R : IHttpHandler, IReadOnlySessionState
    {
        private XDocService xdoc = new XDocService();
        public bool IsReusable
        {
            get { return false; }
        }
        public void ProcessRequest(HttpContext context)
        {
            xdoc.ProcessRequest(context);
        }
    }
    public class HXDocService4A : HttpTaskAsyncHandler, IRequiresSessionState
    {
        static XDocService xdoc = new XDocService();
        async Task doit(HttpContext p_ctx)
        {

            XDocService xdoc4 = new XDocService();
            await Task.Run(() => xdoc.ProcessRequest(p_ctx));
        }
        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }
        public override Task ProcessRequestAsync(HttpContext p_ctx)
        {
            return doit(p_ctx);
        }
    }
    public class HXDocService4AS : HttpTaskAsyncHandler
    {
        static XDocService xdoc = new XDocService();
        async Task doit(HttpContext p_ctx)
        {

            XDocService xdoc4 = new XDocService();
            await Task.Run(() => xdoc.ProcessRequest(p_ctx));
        }
        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }
        public override Task ProcessRequestAsync(HttpContext p_ctx)
        {
            return doit(p_ctx);
        }
    }
    public class HXDocService4AR : HttpTaskAsyncHandler, IReadOnlySessionState
    {
        static XDocService xdoc = new XDocService();
        async Task doit(HttpContext p_ctx)
        {

            XDocService xdoc4 = new XDocService();
            await Task.Run(() => xdoc.ProcessRequest(p_ctx));
        }
        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }
        public override Task ProcessRequestAsync(HttpContext p_ctx)
        {
            return doit(p_ctx);
        }
    }


    public class XDocService4 : IHttpHandler, IRequiresSessionState
    {
        //        HttpContext m_context = null; // http://localhost:9622/_appcache/DEL
        public bool IsReusable
        {
            get { return false; }
        }
        bool bDebug = false;
        bool bAppCache = true;
        bool bSVCache = true;
        bool bAcceptTypes = false;
        bool bHeaders = false;
        bool bCookies = false;
        bool bFiles = false;
        bool bUserLanguages = false;
        bool bContent = false;
        bool bUser = false;
        bool bParams = false;
        bool bPathParams = true;
        bool bUri = false;
        bool bJWT = false;
        bool bClaims = false;
        bool bWS = false;
        string sJWTSecret = "Kubion";
        string sRootTemplate = "A";
        string sVersion = "";
        WSClients wsClients = null;

        private void LoadConfig()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["Debug"] != null) bDebug = (string)System.Configuration.ConfigurationManager.AppSettings["Debug"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Cache"] != null) bAppCache = (string)System.Configuration.ConfigurationManager.AppSettings["Cache"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Session"] != null) bSVCache = (string)System.Configuration.ConfigurationManager.AppSettings["Session"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] != null) bUri = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_AcceptTypes"] != null) bAcceptTypes = (string)System.Configuration.ConfigurationManager.AppSettings["Request_AcceptTypes"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Headers"] != null) bHeaders = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Headers"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Cookies"] != null) bCookies = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Cookies"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Files"] != null) bFiles = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Files"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_UserLanguages"] != null) bUserLanguages = (string)System.Configuration.ConfigurationManager.AppSettings["Request_UserLanguages"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Content"] != null) bContent = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Content"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_User"] != null) bUser = (string)System.Configuration.ConfigurationManager.AppSettings["Request_User"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Params"] != null) bParams = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Params"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_PathParams"] != null) bPathParams = (string)System.Configuration.ConfigurationManager.AppSettings["Request_PathParams"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] != null) bUri = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Uri"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_JWT"] != null) bJWT = (string)System.Configuration.ConfigurationManager.AppSettings["Request_JWT"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Claims"] != null) bClaims = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Claims"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["JWTSecret"] != null) sJWTSecret = (string)System.Configuration.ConfigurationManager.AppSettings["JWTSecret"];
            if (System.Configuration.ConfigurationManager.AppSettings["Signature"] != null) sJWTSecret = (string)System.Configuration.ConfigurationManager.AppSettings["Signature"];
            if (System.Configuration.ConfigurationManager.AppSettings["Request_Template"] != null) sRootTemplate = (string)System.Configuration.ConfigurationManager.AppSettings["Request_Template"];
            if (System.Configuration.ConfigurationManager.AppSettings["WS"] != null) bWS = (string)System.Configuration.ConfigurationManager.AppSettings["WS"] == "1";
            if (System.Configuration.ConfigurationManager.AppSettings["Version"] != null) sVersion = (string)System.Configuration.ConfigurationManager.AppSettings["Version"];
            if (sVersion == "") sVersion = "3";
            // 3 = old ReplaceParams and Settings, not tabs, JCMD
            // 4 = new ReplaceParams and Settings, smart tabs, JCMD
            // 5 = new ReplaceParams and Settings, smart tabs, JCMD4
        }


        #region XDoc Template

        private string ParseBlocks(string sText, int iBlocks)
        {//to do: remove "empty" style and script blocks
            sText = "[block_xdocmain]" + sText.Trim() + "[block]";

            int i = 0;
            int lvl = -1;
            string[] aLvlText = new string[20];
            string[] aLvlBlock = new string[20];

            int ni = sText.IndexOf("[block", 1);
            while (ni != -1)
            {
                string s = sText.Substring(i, ni - i);
                if (s.StartsWith("[block]"))
                {
                    if (lvl == 0) throw new Exception("invalid blocks construction: lvl-1");
                    aLvlText[lvl] += "[block]";
                    s = s.Substring("[block]".Length, s.Length - "[block]".Length);
                    i = ni;// +"[block]".Length - 1;
                    aLvlBlock[lvl - 1] += aLvlText[lvl] + aLvlBlock[lvl];
                    //aLvlText [lvl]="";
                    //aLvlBlock [lvl]="";
                    lvl--;
                    aLvlText[lvl] += s;
                }
                else
                {
                    lvl++;
                    if (lvl == 20) throw new Exception("invalid blocks construction lvl20");
                    aLvlText[lvl] = s;
                    aLvlBlock[lvl] = "";
                    i = ni;
                }
                ni = sText.IndexOf("[block", i + 1);
            }
            if (lvl != 0)
                throw new Exception("invalid blocks construction lvl0");
            aLvlText[0] += sText.Substring(i, sText.Length - i);
            aLvlText[0] = aLvlText[0].Substring("[block_xdocmain]".Length, aLvlText[0].Length - "[block_xdocmain]".Length - "[block]".Length);
            string sResult;
            if (iBlocks == 1)
            {
                sResult = aLvlBlock[0];
            }
            else
            {
                sResult = aLvlText[0];
                //if ((aLvlBlock[0].Trim() != "") && (aLvlBlock[0].Trim() != sSeed))
                if (aLvlBlock[0].Trim() != "")
                {
                    sResult += aLvlBlock[0].Trim();
                }
            }
            sResult = sResult.Replace("[block_script][block]", "");
            sResult = sResult.Replace("[block_style][block]", "");
            return sResult;
        }

        private string ContextCompileLibrary(HttpContext m_context, string strTemplatePath)
        {
            string sReturn = "";
            AppCache m_AppCache = null;
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, null);
            m_AppCache = xrun.AppCache;
            sReturn = m_AppCache.CompiledLibrary(strTemplatePath);
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            return sReturn;
        }
        private string ContextCompileTemplate(HttpContext m_context, string strTemplateName)
        {
            string sReturn = "";
            AppCache m_AppCache = null;
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, null);
            m_AppCache = xrun.AppCache;
            if (strTemplateName == "")
                sReturn = m_AppCache.CompileTemplates();
            else
                sReturn = m_AppCache.CompileTemplate(strTemplateName);
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            return sReturn;
        }
        private string ContextUncompileTemplate(HttpContext m_context, string strTemplateName)
        {
            string sReturn = "";
            AppCache m_AppCache = null;
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, null);
            m_AppCache = xrun.AppCache;
            if (strTemplateName == "")
                sReturn = m_AppCache.UncompileTemplates();
            else
                sReturn = m_AppCache.UncompileTemplate(strTemplateName);
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            return sReturn;
        }
        private string ContextExtractTemplate(HttpContext m_context)
        {
            string sReturn = "";
            AppCache m_AppCache = null;
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, null);
            m_AppCache = xrun.AppCache;
            sReturn = m_AppCache.ExtractTemplatesDB();
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            return sReturn;
        }
        public string RunTemplate(string strTemplateName)
        {
            AppCache m_AppCache = null;
            SVCache m_SVCache = null;
            return RunTemplate(strTemplateName, ref m_AppCache, ref m_SVCache);
        }
        public string RunTemplate(string strTemplateName, ref AppCache m_AppCache, ref SVCache m_SVCache)
        {
            if (m_AppCache == null) m_AppCache = new AppCache();
            if (m_SVCache == null) m_SVCache = new SVCache();
            CRuntime xrun = new CRuntime(m_AppCache, m_SVCache);
            //xrun.Clients = cClients;
////            xrun.Clients = wsClients;
            //XDocManager xm = new XDocManager();
            //xrun.Manager = null;
            StringBuilder sbResponse = new StringBuilder();
            string sExit = "";
            xrun.myHttpContext = null;
            xrun.sVersion = sVersion;
            string sResult = xrun.RunTemplate(strTemplateName, null, sbResponse, ref sExit);
            string sResponse = sbResponse.ToString();
            if (xrun.ExitValue != "") sExit = xrun.ExitValue;
            xrun.Dispose();
            if (sExit == "")
                return sResponse;
            else
                return sExit;
        }
        public string RunTemplate(HttpContext m_context, string strTemplateName, Hashtable hParameters, ref AppCache m_AppCache, ref SVCache m_SVCache)
        {
            if (m_AppCache == null) m_AppCache = new AppCache();
            CRuntime xrun = new CRuntime(m_AppCache, m_SVCache);
            //xrun.Clients = cClients;
////            xrun.Clients = wsClients;
            //XDocManager xm = new XDocManager();
            //xrun.Manager = xm;
            StringBuilder sbResponse = new StringBuilder();
            string sExit = "";
            string sResponse = "";
            xrun.myHttpContext = m_context;
            xrun.sVersion = sVersion;
            try
            {
                string sResult = xrun.RunTemplate(strTemplateName, hParameters, sbResponse, ref sExit);
                sResponse = sbResponse.ToString();

                //if ((hParameters["params_NOBLOCKS"] == null) || (hParameters["params_BLOCKS"] != null))
                //{
                //    if ((hParameters["params_BLOCK"] != null) && ((string)hParameters["params_BLOCK"]=="true"))
                //        sResponse = ParseBlocks(sResponse, 1);
                //    else
                //        sResponse = ParseBlocks(sResponse, 0);
                //}
                if (xrun.ExitValue != "") sExit = xrun.ExitValue;
            }
            catch (Exception ex)
            {
                sExit = ex.Message;
            }
            m_AppCache = xrun.AppCache;
            m_SVCache = xrun.SVCache;
            xrun.Dispose();
            if (sExit == "")
                return sResponse;
            else
                return sExit;
        }
        private string ContextRunTemplate(HttpContext m_context, string strTemplateName, Hashtable hParameters, ref SVCache m_SVCache)
        {
            AppCache m_AppCache = null;
            if (bSVCache && (m_context != null) && (m_context.Session != null)) m_SVCache = (SVCache)m_context.Session["SVCache"];
            if (bAppCache && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if ((m_AppCache==null) && (m_context.Request.Params["cache"] == "1") && (m_context != null) && (m_context.Application != null)) m_AppCache = (AppCache)m_context.Application["AppCache"];
            if (m_context.Request.Params["cache"] == "0") m_AppCache=null;
            if ((m_SVCache != null) && (m_context.Request.QueryString["callbackGuid"] == null) && (m_context.Request.QueryString["postbackGuid"] == null)) m_SVCache.ClearIncludeOnce();
            if (m_context != null) { if (m_SVCache == null) m_SVCache = new SVCache(); m_SVCache.FillCookies(m_context.Request.Cookies); }
            string sXDocBlocks = "";
            if (m_context.Request.Params["noblocks"] == "1") sXDocBlocks = "-1";
            if (m_context.Request.Params["noblocks"] == null)
                if (m_context.Request.Params["blocks"] == "true") sXDocBlocks = "1";
                else sXDocBlocks = "0";
            if(sXDocBlocks!="") m_SVCache.SetSV("XDocBlocks", "Response_", sXDocBlocks);
            //if ((hParameters["params_NOBLOCKS"] == null) || (hParameters["params_BLOCKS"] != null))
            //{
            //    if ((hParameters["params_BLOCK"] != null) && ((string)hParameters["params_BLOCK"]=="true"))
            //        sResponse = ParseBlocks(sResponse, 1);
            //    else
            //        sResponse = ParseBlocks(sResponse, 0);
            //}
            string sResult = RunTemplate(m_context, strTemplateName, hParameters, ref  m_AppCache, ref  m_SVCache);
            //if ((Cache != "0") && (m_context != null) && (m_context.Application != null)) m_context.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
            //if ((CacheSV != "0") && (m_context != null) && (m_context.Session != null)) m_context.Session["XDocCache"] = m_xdocCache;
            if ((m_context != null) && (m_SVCache != null)) m_SVCache.WriteCookies(m_context.Response.Cookies);
            if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
            m_SVCache.SaveSVCache0();
            if ((m_context != null) && (m_context.Session != null)) m_context.Session["SVCache"] = m_SVCache;
            if ((m_context != null) && (m_context.Session != null)) m_context.Session["SessionID"] = m_SVCache.SessionID;
            return sResult;
        }
        #endregion

        #region util
        private string myRequestRender(HttpRequest req, StringBuilder sJSON)
        {//URI = scheme "://" authority "/" path [ "?" query ] [ "#" fragment ]
            string sResult = "";
            string sApplicationPath = req.ApplicationPath;
            string sPath = req.Path;
            string sRelativePath = "";
            if (sPath.Length > sApplicationPath.Length)
            {
                sRelativePath = sPath.Substring(sApplicationPath.Length, sPath.Length - sApplicationPath.Length);
                sPath = sPath.Substring(0, sApplicationPath.Length);
            }
            if (sRelativePath.StartsWith("/")) sRelativePath = sRelativePath.Substring(1, sRelativePath.Length - 1);
            if (sApplicationPath.StartsWith("/")) sApplicationPath = sApplicationPath.Substring(1, sApplicationPath.Length - 1);
            if (sPath.StartsWith("/")) sPath = sPath.Substring(1, sPath.Length - 1);
            if (sRelativePath.ToLower().EndsWith(".nosession")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".nosession".Length);
            if (sRelativePath.ToLower().EndsWith(".rosession")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".rosession".Length);
            if (sRelativePath.ToLower().EndsWith(".session")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".session".Length);
            if (sRelativePath.ToLower().EndsWith(".asyncnosession")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".asyncnosession".Length);
            if (sRelativePath.ToLower().EndsWith(".asyncrosession")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".asyncrosession".Length);
            if (sRelativePath.ToLower().EndsWith(".async")) sRelativePath = sRelativePath.Substring(0, sRelativePath.Length - ".async".Length);
            sResult = sRelativePath; 

            //if (sRelativePath == "dummy")
            //    return "dummy";
            //if (sRelativePath.StartsWith("compile"))
            //    return sRelativePath;
            //if (sRelativePath.StartsWith("library"))
            //    return sRelativePath;
            if (sRelativePath.StartsWith("_v"))
            {
                sResult = sRelativePath.Substring(0, 3);
                sRelativePath = sRelativePath.Substring("_v0/".Length);
            }

            string postedData = myReadContent(req);
            try
            {
                NameValueCollection rp = req.Params;
            }
            catch (Exception) { };

            sJSON.Append("{\"request\":{");
            //json=result
            //noblock=1
            //uc=1
            sJSON.Append("\"Handler\":\"XDocServices.XDocService4\",");
            if (bParams) { sJSON.Append("\"Params\":"); myNameValueCollectionRender(sJSON, req.Params, req.ServerVariables); sJSON.Append(","); }
            if (bPathParams) { sJSON.Append("\"PathParams\":"); Utils.myPathParamsRender(sJSON, sRelativePath); sJSON.Append(","); }
            if (bAcceptTypes) { sJSON.Append("\"AcceptTypes\":"); myStringArrayRender(sJSON, req.AcceptTypes); sJSON.Append(","); }
            if (bHeaders) { sJSON.Append("\"Headers\":"); myNameValueCollectionRender(sJSON, req.Headers, null); sJSON.Append(","); }
            if (bCookies)
            {
                sJSON.Append("\"CookiesArray\":"); myCookieCollectionArrayRender(sJSON, req.Cookies); sJSON.Append(",");
                sJSON.Append("\"Cookies\":"); myCookieCollectionRender(sJSON, req.Cookies); sJSON.Append(",");
            }
            if (bFiles) { sJSON.Append("\"Files\":"); myFilesCollectionArrayRender(sJSON, req.Files); sJSON.Append(","); }
            if (bUserLanguages) { sJSON.Append("\"UserLanguages\":"); myStringArrayRender(sJSON, req.UserLanguages); sJSON.Append(","); }

            if (bUri)
            {
                sJSON.Append("\"RequestApplicationPath\":\""); sJSON.Append(Utils._JsonEscape(req.ApplicationPath)); sJSON.Append("\",");
                sJSON.Append("\"RequestPath\":\""); sJSON.Append(Utils._JsonEscape(req.Path)); sJSON.Append("\",");
                sJSON.Append("\"RequestWebsite\":\""); sJSON.Append(Utils._JsonEscape(sPath)); sJSON.Append("\",");
                string sBaseURL = req.Url.Scheme + "://" + req.Url.Host + ":" + req.Url.Port.ToString();
                if (sPath != "") sBaseURL += "/" + sPath;
                sJSON.Append("\"RequestBaseURL\":\""); sJSON.Append(Utils._JsonEscape(sBaseURL)); sJSON.Append("\",");
                //  #SPAR.BaseURL.%req_UriScheme%://%req_UriHost%:%req_UriPort%/%req_URIWebsite%#
                //  #IF(#PAR.req_URIWebsite#==)# #SPAR.BaseURL.%req_UriScheme%://%req_UriHost%:%req_UriPort%# #ENDIF#
                sJSON.Append("\"UriScheme\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Scheme)); sJSON.Append("\",");
                sJSON.Append("\"UriHost\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Host)); sJSON.Append("\",");
                sJSON.Append("\"UriPort\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Port.ToString())); sJSON.Append("\",");
                sJSON.Append("\"UriServer\":\""); sJSON.Append(Utils._JsonEscape(req.Url.Scheme + ":\\" + req.Url.Host + ":" + req.Url.Port.ToString())); sJSON.Append("\",");
                sJSON.Append("\"UriWebsite\":\""); sJSON.Append(Utils._JsonEscape(sApplicationPath)); sJSON.Append("\",");
                string sBaseURL1 = req.Url.Scheme + "://" + req.Url.Host + ":" + req.Url.Port.ToString();
                if (sApplicationPath != "") sBaseURL1 += "/" + sApplicationPath;
                sJSON.Append("\"UriBaseURL\":\""); sJSON.Append(Utils._JsonEscape(sBaseURL1)); sJSON.Append("\",");
                sJSON.Append("\"UriPath\":\""); sJSON.Append(Utils._JsonEscape(sRelativePath)); sJSON.Append("\",");
                myUriPathRender(sJSON, sRelativePath);
                sJSON.Append("\"UriQuery\":\""); sJSON.Append(Utils._JsonEscape(req.QueryString.ToString())); sJSON.Append("\",");
            }
            if (bContent)
            {
                sJSON.Append("\"ContentType\":\""); sJSON.Append(Utils._JsonEscape(req.ContentType)); sJSON.Append("\",");
                int iContentLength = req.ContentLength;
                sJSON.Append("\"ContentLength\":\""); sJSON.Append(Utils._JsonEscape(iContentLength.ToString())); sJSON.Append("\",");
                sJSON.Append("\"Content\":\""); sJSON.Append(Utils._JsonEscape(postedData)); sJSON.Append("\",");
                //////#IF(#PAR.req_ContentType#===^multipart/form-data;)##JPAR.Data.%req_Params%.#
                //////#ELSEIF(#PAR.req_ContentType#==application/x-www-form-urlencoded)##JPAR.Data.%req_Params%.#
                //////#ELSEIF(#PAR.req_ContentType#===^text/plain;)##JPAR.Data.%req_Content%.#
                //////#ENDIF#
                if (req.ContentType.StartsWith("multipart/form-data") || req.ContentType.StartsWith("application/x-www-form-urlencoded")) { sJSON.Append("\"Data\":"); myNameValueCollectionRender(sJSON, req.Params, req.ServerVariables); sJSON.Append(","); }
                else if (req.ContentType.StartsWith("text/plain")) { sJSON.Append("\"Data\":\""); sJSON.Append(Utils._JsonEscape(postedData)); sJSON.Append("\","); }
            }
            if (bUser)
            {
                sJSON.Append("\"UserHostAddress\":\""); sJSON.Append(Utils._JsonEscape(req.UserHostAddress)); sJSON.Append("\",");
                sJSON.Append("\"UserHostName\":\""); sJSON.Append(Utils._JsonEscape(req.UserHostName)); sJSON.Append("\",");
                sJSON.Append("\"UserAgent\":\""); sJSON.Append(Utils._JsonEscape(req.UserAgent)); sJSON.Append("\",");
                sJSON.Append("\"UserName\":\""); sJSON.Append(Utils._JsonEscape(req.LogonUserIdentity.Name)); sJSON.Append("\",");
            }
            if (bClaims)
            {
                IClaimsIdentity claimsPrincipal = System.Threading.Thread.CurrentPrincipal.Identity as IClaimsIdentity;
                if (claimsPrincipal != null)
                {
                    sJSON.Append("\"ClaimIsAuthenticated\":\""); sJSON.Append(Utils._JsonEscape(claimsPrincipal.IsAuthenticated)); sJSON.Append("\",");
                    sJSON.Append("\"ClaimName\":\""); sJSON.Append(Utils._JsonEscape(claimsPrincipal.Name)); sJSON.Append("\",");
                    sJSON.Append("\"ClaimLabel\":\""); sJSON.Append(Utils._JsonEscape(claimsPrincipal.Label)); sJSON.Append("\",");
                    if (claimsPrincipal != null && claimsPrincipal.IsAuthenticated)
                    {
                        sJSON.Append("\"ClaimsArray\":"); myClaimCollectionArrayRender(sJSON, claimsPrincipal.Claims); sJSON.Append(",");
                        sJSON.Append("\"Claims\":"); myClaimCollectionRender(sJSON, claimsPrincipal.Claims); sJSON.Append(",");
                    }
                }
                else
                {
                    sJSON.Append("\"ClaimIsAuthenticated\":null,");
                    sJSON.Append("\"ClaimName\":null,");
                    sJSON.Append("\"ClaimLabel\":null,");
                }
                //Microsoft.IdentityModel.Protocols.WSFederation.SignInRequestMessage signInRequest = FederatedAuthentication.WSFederationAuthenticationModule.CreateSignInRequest("passive", HttpContext.Current.Request.RawUrl, false);
                //string myurl = signInRequest.RequestUrl;
                //HttpContext.Current.Response.Redirect(myurl);

            }
            if (bJWT)
            {
                string sSecret = "";
                if (System.Configuration.ConfigurationManager.AppSettings["Signature"] != null) sSecret = (string)System.Configuration.ConfigurationManager.AppSettings["Signature"];
                string sJWTHeader = "", sJWTHeaderPayload = "{}";
                string sJWTCookie = "", sJWTCookiePayload = "{}";
                string s = req.Headers["Authorization"];
                if ((s != null) && s.StartsWith("Bearer "))
                {
                    try
                    {
                        sJWTHeader = s.Substring("Bearer ".Length);
                        sJWTHeaderPayload = JWT.JsonWebToken.JWTDecode(sJWTHeader, sSecret, false);
                    }
                    catch (Exception) { };
                }
                s = req.Cookies.Get("Authorization") != null ? req.Cookies.Get("Authorization").Value : null;
                if ((s != null) && s.StartsWith("Bearer "))
                {
                    try
                    {
                        sJWTCookie = s.Substring("Bearer ".Length);
                        sJWTCookiePayload = JWT.JsonWebToken.JWTDecode(sJWTCookie,sSecret,false); ;
                    }
                    catch (Exception) { };
                }
                sJSON.Append("\"JWTHeader\":\""); sJSON.Append(Utils._JsonEscape(sJWTHeader)); sJSON.Append("\",");
                sJSON.Append("\"JWTHeaderPayload\":"); sJSON.Append(sJWTHeaderPayload); sJSON.Append(",");
                //                sJSON.Append("\"JWTHeaderPayload\":\""); sJSON.Append(Utils._JsonEscape(sJWTHeaderPayload)); sJSON.Append("\",");
                //                sJSON.Append("\"JWTHeaderClaim\":"); sJSON.Append(Utils._JsonEscape(sJWTHeaderClaim)); sJSON.Append(",");
                sJSON.Append("\"JWTCookie\":\""); sJSON.Append(Utils._JsonEscape(sJWTCookie)); sJSON.Append("\",");
                sJSON.Append("\"JWTCookiePayload\":"); sJSON.Append(sJWTCookiePayload); sJSON.Append(",");
                //                sJSON.Append("\"JWTCookiePayload\":\""); sJSON.Append(Utils._JsonEscape(sJWTCookiePayload)); sJSON.Append("\",");
                //                sJSON.Append("\"JWTCookieClaim\":"); sJSON.Append(Utils._JsonEscape(sJWTCookieClaim)); sJSON.Append(",");
            }
            sJSON.Append("\"HttpMethod\":\""); sJSON.Append(Utils._JsonEscape(req.HttpMethod)); sJSON.Append("\",");
            sJSON.Append("\"HttpUrl\":\""); sJSON.Append(Utils._JsonEscape(req.Url.ToString())); sJSON.Append("\"");
            sJSON.Append("}}");

            return sResult;
        }
        public static int myUriPathRender(StringBuilder sJSON, string sPath)
        {
            int i = 0;
            string[] aPath = sPath.Split('/');
            string sIdsPath = "";
            sIdsPath = aPath[0];
            for (i = 0; i < aPath.Length; i++)
            {
                sJSON.Append("\"");
                sJSON.Append(Utils._JsonEscape("UriPath" + (i + 1).ToString()));
                sJSON.Append("\":\"");
                sJSON.Append(Utils._JsonEscape(aPath[i]));
                sJSON.Append("\",");
            }
            return i;
        }
        private string myReadContent(HttpRequest req)
        {
            string postedData = "";
            int iContentLength = req.ContentLength;
            //if (iContentLength < 5000)
            //{
            StreamReader reader = new StreamReader(req.InputStream, req.ContentEncoding);
            postedData = reader.ReadToEnd().Trim();
            return postedData;
            //}
            //else
            //    return "[[datasize exceeds limit]]";
        }
        private int myNameValueCollectionRender(StringBuilder sJSON, NameValueCollection col, NameValueCollection exclude)
        {
            int i = 0;
            try
            {
                if ((col == null) || (col.Keys == null))
                {
                    sJSON.Append("null");
                    return -1;
                }
                sJSON.Append("{");
                foreach (string key in col.Keys)
                {
                    if ((exclude == null) || (key == null) || (exclude[key] == null))
                    {
                        sJSON.Append("\"");
                        sJSON.Append(Utils._JsonEscape(key));
                        sJSON.Append("\":\"");
                        try
                        {
                            sJSON.Append(Utils._JsonEscape(col[key].ToString()));
                        }
                        catch (Exception e)
                        {
                            sJSON.Append(Utils._JsonEscape(e.Message));
                        }
                        sJSON.Append("\",");
                        i++;
                    }
                }
                sJSON.Append("\"Count\":\"");
                sJSON.Append(Utils._JsonEscape(i.ToString()));
                sJSON.Append("\"");
                sJSON.Append("}");
                return col.Count;
            }
            catch (Exception )
            { }
                    sJSON.Append("null");
                    return -1;
        }
        private int myStringArrayRender(StringBuilder sJSON, string[] col)
        {
            bool isFirst = true;
            sJSON.Append("[");
            if(col!=null)
            foreach (string key in col)
            {
                if (isFirst)
                    isFirst = false;
                else
                    sJSON.Append(",");
                sJSON.Append("\""); sJSON.Append(Utils._JsonEscape((key))); sJSON.Append("\"");
            }
            sJSON.Append("]");
            if (col != null)
                return col.GetLength(0);
            else
                return 0;
        }
        private int myCookieCollectionRender(StringBuilder sJSON, HttpCookieCollection col)
        {
            sJSON.Append("{");
            foreach (string skey in col)
            {
                HttpCookie key = col[skey];
                sJSON.Append("\"");
                sJSON.Append(Utils._JsonEscape(key.Name));
                sJSON.Append("\":\"");
                sJSON.Append(Utils._JsonEscape(key.Value));
                sJSON.Append("\",");
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(Utils._JsonEscape(col.Count.ToString()));
            sJSON.Append("\"");
            sJSON.Append("}");
            return col.Count;
        }
        private int myClaimCollectionRender(StringBuilder sJSON, ClaimCollection col)
        {
            sJSON.Append("{");
            foreach (Claim claim in col)
            {
                sJSON.Append("\"");
                sJSON.Append(Utils._JsonEscape(claim.ClaimType));
                sJSON.Append("\":\"");
                sJSON.Append(Utils._JsonEscape(claim.Value));
                sJSON.Append("\",");
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(Utils._JsonEscape(col.Count.ToString()));
            sJSON.Append("\"");
            sJSON.Append("}");
            return col.Count;
        }
        private int myClaimCollectionArrayRender(StringBuilder sJSON, ClaimCollection col)
        {
            bool isFirst = true;
            sJSON.Append("[");
            foreach (Claim claim in col)
            {
                if (isFirst)
                    isFirst = false;
                else
                    sJSON.Append(",");
                sJSON.Append("{");
                sJSON.Append("\"ClaimType\":\""); sJSON.Append(Utils._JsonEscape(claim.ClaimType)); sJSON.Append("\",");
                sJSON.Append("\"Value\":\""); sJSON.Append(Utils._JsonEscape(claim.Value)); sJSON.Append("\",");
                sJSON.Append("\"ValueType\":\""); sJSON.Append(Utils._JsonEscape(claim.ValueType)); sJSON.Append("\",");
                sJSON.Append("\"Subject\":\""); sJSON.Append(Utils._JsonEscape(claim.Subject.ToString())); sJSON.Append("\",");
                sJSON.Append("\"Issuer\":\""); sJSON.Append(Utils._JsonEscape(claim.Issuer)); sJSON.Append("\"");
                sJSON.Append("}");
            }
            sJSON.Append("]");
            return col.Count;
        }
        private int myCookieCollectionArrayRender(StringBuilder sJSON, HttpCookieCollection col)
        {
            bool isFirst = true;
            sJSON.Append("[");
            foreach (string skey in col)
            {
                HttpCookie key = col[skey];
                if (isFirst)
                    isFirst = false;
                else
                    sJSON.Append(",");
                sJSON.Append("{");
                sJSON.Append("\"Domain\":\""); sJSON.Append(Utils._JsonEscape(key.Domain)); sJSON.Append("\",");
                sJSON.Append("\"Name\":\""); sJSON.Append(Utils._JsonEscape(key.Name)); sJSON.Append("\",");
                sJSON.Append("\"Path\":\""); sJSON.Append(Utils._JsonEscape(key.Path)); sJSON.Append("\",");
                sJSON.Append("\"Value\":\""); sJSON.Append(Utils._JsonEscape(key.Value)); sJSON.Append("\",");
                sJSON.Append("\"Expires\":\""); sJSON.Append(Utils._JsonEscape(key.Expires.ToString())); sJSON.Append("\"");
                sJSON.Append("}");
            }
            sJSON.Append("]");
            return col.Count;
        }
        private int myFilesCollectionArrayRender(StringBuilder sJSON, HttpFileCollection col)
        {
            sJSON.Append("[");
            if (col.Count > 0)
            {
                bool isFirst = true;
                foreach (string key in col)
                {
                    HttpPostedFile _file = col[key];
                    if (isFirst)
                        isFirst = false;
                    else
                        sJSON.Append(",");
                    sJSON.Append("{");
                    // juni UPLOADS
                    // S_Files
                    //CRuntime runtime = new CRuntime();
                    //sJSON.Append("\"FileId\":\""); sJSON.Append(Utils._JsonEscape(mySaveUploadFile(_file, runtime))); sJSON.Append("\",");
                    sJSON.Append("\"FileName\":\""); sJSON.Append(Utils._JsonEscape(_file.FileName)); sJSON.Append("\",");
                    sJSON.Append("\"ContentType\":\""); sJSON.Append(Utils._JsonEscape(_file.ContentType)); sJSON.Append("\",");
                    sJSON.Append("\"ContentLength\":\""); sJSON.Append(Utils._JsonEscape(_file.ContentLength.ToString())); sJSON.Append("\",");
                    sJSON.Append("}");
                }
                //xm.Dispose();
            }
            sJSON.Append("]");
            return col.Count;
        }

        private string[] MySplit(string sVal, char cSeparator)
        {
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());
            }
            return aData;
        }
        // ^[ ]*(id|name)[ ]*$|^[ ]*\([ ]*(id|name)[ ]*\)[ ]*([ ]*(OR|AND)[ ]*\([ ]*(id|name)[ ]*\))*[ ]*$|^[ ]*\([ ]*\([ ]*(id|name)[ ]*\)[ ]*([ ]*(OR|AND)[ ]*\([ ]*(id|name)[ ]*\))*[ ]*\)[ ]*$
        //  (name) 
        //(id) 
        // name  
        //( name  ) OR (id)
        // ( (name) AND ( id ) OR (name)  OR  (name)   ) 
        //(id)AND (name) 
        #endregion

        public void ProcessRequest(HttpContext context)
        {
            SVCache m_SVCache = null;
            /*syntax:
             *
             * 
             * /library/<path>
             * 
             * /compile/<path>
             * 
             * /extract
             * 
             * /_appcache/[CFGGET|CFGDEL|GET|DEL|CLEAR]
             * 
             * /_svcache/[TEMPLATEONCE|TEMPLATEONCEDEL|TEMPLATEONCECLEAR|DEL|GET]
             * 
             * /__version
             * 
             */

            //System.Diagnostics.Debug.WriteLine("start" + "" + ": " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            LoadConfig();
            if (bWS)
            {
                if (context.IsWebSocketRequest)
                {
                    if (wsClients == null)
                        wsClients = (WSClients)context.Application["wsClients"];
                    if (wsClients == null)
                    {
                        wsClients = new WSClients();
                        context.Application["wsClients"] = wsClients;
                    }
                    if (wsClients != null)
                        context.AcceptWebSocketRequest(wsClients.Connect);
                    return;
                }
            }
            //m_context = context;
            context.Request.ValidateInput();
            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "input validated");
            StringBuilder sJSON = new StringBuilder();
            string sResult = myRequestRender(context.Request, sJSON);
            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "request builded");
            if (bDebug)
            {
                if (sResult.IndexOf("adfs", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    var signInRequest = FederatedAuthentication.WSFederationAuthenticationModule.CreateSignInRequest("passive", context.Request.RawUrl, false);
                    context.Response.Redirect(signInRequest.RequestUrl);
                }
                if (sResult.IndexOf("library", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (sResult.Length > "library/".Length)
                        sResult = sResult.Substring("library/".Length);
                    else
                        sResult = "";
                    string sResponse = ContextCompileLibrary(context, sResult);
                    context.Response.Write("LIBRARY:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("extract", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string sResponse = ContextExtractTemplate(context);
                    context.Response.Write("Extract:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("compile", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (sResult.Length > "compile/".Length)
                        sResult = sResult.Substring("compile/".Length);
                    else
                        sResult = "";
                    string sResponse = ContextCompileTemplate(context, sResult);
                    context.Response.Write("COMPILE:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("uncompile", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (sResult.Length > "uncompile/".Length)
                        sResult = sResult.Substring("uncompile/".Length);
                    else
                        sResult = "";
                    string sResponse = ContextUncompileTemplate(context, sResult);
                    context.Response.Write("UNCOMPILE:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("_appcache", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string sResponse = "";
                    sResult = sResult.Substring("_appcache".Length);
                    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);
                    if (sResult == "") sResult = "GET";

                    AppCache m_AppCache = null;
                    if (sResult.ToUpper() == "CLEAR") { m_AppCache = new AppCache(); context.Application["AppCache"] = m_AppCache; }
                    if (bAppCache && (context != null) && (context.Application != null)) m_AppCache = (AppCache)context.Application["AppCache"];
                    if (m_AppCache == null) m_AppCache = new AppCache();
                    m_AppCache.GetSchedules();
                    sResponse = m_AppCache.AppCacheCommand(sResult);
                    context.Response.Write("_appcache:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("_svcache", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string sResponse = "";
                    sResult = sResult.Substring("_svcache".Length);
                    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);
                    if (sResult == "") sResult = "GET";

                    if (bSVCache && (context != null) && (context.Session != null)) m_SVCache = (SVCache)context.Session["SVCache"];
                    if (m_SVCache == null) m_SVCache = new SVCache();

                    sResponse = m_SVCache.SVCacheCommand(sResult);
                    context.Response.Write("_svcache:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("_svcache0", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string sResponse = "";
                    sResult = sResult.Substring("_svcache0".Length);
                    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);
                    if (sResult == "") sResult = "GET";

                    if (bSVCache && (context != null) && (context.Application != null)) m_SVCache = (SVCache)context.Application["SVCache0"];
                    if (m_SVCache == null) m_SVCache = new SVCache();

                    sResponse = m_SVCache.SVCacheCommand(sResult);
                    context.Response.Write("_svcache0:\r\n" + sResponse);
                    context.Response.ContentType = "text/plain; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                if (sResult.IndexOf("__version", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    sResult = sResult.Substring("__version".Length);
                    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);

                    StringBuilder sbResponse = new StringBuilder();
                    sbResponse.Append("{");
                    sbResponse.Append("\"request\":" + sJSON.ToString());
                    sbResponse.Append(",\"version\":" + sVersion);
                    sbResponse.Append(",\"_XDOCVERSION_\":" + "\"5.0.0.171218\"");
                    sbResponse.Append(",\"whatsnew\":[");
                    sbResponse.Append(" \"" + "171001: new setting in web.config 'bak' (0,1)" + "\"");
                    sbResponse.Append(",\"" + "171001: new setting in web.config 'trace' (0,1,2)" + "\"");
                    sbResponse.Append(",\"" + "171001: in JLOOP set also parameter _XML " + "\"");
                    sbResponse.Append(",\"" + "171001: in JLOOP accept also non array jpath ending with [0] " + "\"");
                    sbResponse.Append(",\"" + "171001: support for XPATH command " + "\"");
                    sbResponse.Append(",\"" + "171101: file_copy and dir_copy for FUNC command " + "\"");
                    sbResponse.Append(",\"" + "171101: dir_merge for FUNC command " + "\"");
                    sbResponse.Append(",\"" + "171101: file_exists, dir_exists, file_fullname for FUNC command " + "\"");
                    sbResponse.Append(",\"" + "171101: bug fix DELETETEMPLATE command " + "\"");
                    sbResponse.Append(",\"" + "171101: support for WebSockets " + "\"");
                    sbResponse.Append(",\"" + "171101: new FUNC commands: ws_addticket, ws_removeticket, ws_removeuser, ws_sendticket, ws_senduser, ws_gettickets, ws_getusers" + "\"");
                    sbResponse.Append(",\"" + "171101: Provider=LDAP;domain=dom;username=user;password=psw" + "\"");
                    sbResponse.Append(",\"" + "171101: LDAP filter as query" + "\"");
                    sbResponse.Append(",\"" + "171101: 0xxx context session variables saved in application" + "\"");
                    sbResponse.Append(",\"" + "171109: fix for &null parameter in url" + "\"");
                    sbResponse.Append(",\"" + "171109: JDATA connection SPLIT_multicharseparator" + "\"");
                    sbResponse.Append(",\"" + "171117: Scheduler executes first job delayed after service started" + "\"");
                    sbResponse.Append(",\"" + "171117: JLOOP has a default pagesize of 100000 (not 1000 anymore)" + "\"");
                    sbResponse.Append(",\"" + "171117: JSONEscape encoding escapes also special double quotes  “ ”" + "\"");
                    sbResponse.Append(",\"" + "171120: new FUNC datetime_dayofweek and dayofyear" + "\"");
                    sbResponse.Append(",\"" + "171123: if supports SQL expressions" + "\"");
                    sbResponse.Append(",\"" + "171123: fix set error parameter on INCLUDE and INCLUDEONCE when ignore=1" + "\"");
                    sbResponse.Append(",\"" + "171128: FUNC jwt_decode token,psw,salt=1,verify=1" + "\"");
                    sbResponse.Append(",\"" + "171128: FUNC jwt_encode payload,psw,salt=1,alg=HS256" + "\"");
                    sbResponse.Append(",\"" + "171128: new FUNC jwt_token header,payload,psw,salt=1,alg=HS256" + "\"");
                    sbResponse.Append(",\"" + "171128: new FUNC salt key,usedate=false" + "\"");
                    sbResponse.Append(",\"" + "171128: web.config WS setting default 0 = ignores websokets" + "\"");
                    sbResponse.Append(",\"" + "171128: fix bug in SETFLD  tht did an append value" + "\"");
                    sbResponse.Append(",\"" + "171208: XDocScheduler supports multiple parallel tasks" + "\"");
                    sbResponse.Append(",\"" + "171208: s_Schedules json to define Schediles array" + "\"");
                    sbResponse.Append(",\"" + "171208: when version='3' because of backwards compatibility INCLUDE will not set error when missing" + "\"");
                    sbResponse.Append(",\"" + "171212: entry http://localhost:8080/<serviceName>?ID=1&Interval=0&Template=s&enabled=1" + "\"");
                    sbResponse.Append(",\"" + "171218: jloop.i.while loops int.MaxValue and jloop.i.while.1==1 loops max 100000" + "\"");
                    sbResponse.Append("]");
                    //int iRun = Runners();
                    //sbResponse.Append(",\"run\":" + iRun.ToString() );
                    //AKIAJBZZYPJWGYADQY4Q
                    //cmxZDCAN40fu/FQf5lbCxQlTnsa/2rlFkGhm2Fcd
                    sbResponse.Append("}");
                    JToken ob = JToken.Parse(sbResponse.ToString());
                    sResult = ob.ToString(Newtonsoft.Json.Formatting.Indented);
                    context.Response.Write(sResult);
                    context.Response.ContentType = "application/json; charset=utf-8";// "text/xml; charset=utf-8";
                    return;
                }
                //if (sResult.IndexOf("_azure", StringComparison.CurrentCultureIgnoreCase) == 0)
                //{
                //    string sResponse = "";
                //    sResult = sResult.Substring("_azure".Length);
                //    if (sResult.StartsWith("/")) sResult = sResult.Substring(1);
                //    if (sResult == "") sResult = "GET";

                //    sResponse = sJSON.ToString();
                //    context.Response.Write(sResponse);
                //    context.Response.ContentType = "application/json; charset=utf-8";// "text/xml; charset=utf-8";
                //    return;
                //}
            }
            string strContent = "";
            Hashtable newHash = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
            newHash.Add("Request", sJSON.ToString());
            newHash.Add("RemoveTab", "1");
            if ((context != null) && (context.Session != null)) m_SVCache = (SVCache)context.Session["SVCache"];
            if (m_SVCache == null) m_SVCache = new SVCache();
            m_SVCache.SetSV("_REQUEST_", "_", sJSON.ToString());

            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "start processing");
            strContent = ContextRunTemplate(context, sRootTemplate, newHash, ref m_SVCache);
            //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "template processed");

            string sEncoding = "";
            if (m_SVCache != null)
            {
                string sHeaders = m_SVCache.GetSV("*", "Response_Headers", "");
                string[] aHeaders = MySplit(sHeaders, ';');
                foreach (string sHeader in aHeaders)
                    if (sHeader != "")
                    {
                        string[] aHeader = MySplit(sHeader + "=", '=');
                        context.Response.AddHeader(aHeader[0], aHeader[1]);
                    }
                string sStatus = m_SVCache.GetSV("Status", "Response_", "200 OK");
                context.Response.Status = sStatus;
                sEncoding = m_SVCache.GetSV("Encoding", "Response_", "");
                string sXDocBlocks_after = m_SVCache.GetSV("XDocBlocks", "Response_", ""); //-1=noblocks 0=extract subblocs 1=extract subblocks & take only blocks 
                int iXDocBlocks = 0;
                if (sXDocBlocks_after != "")
                    if (sXDocBlocks_after == "1")
                        iXDocBlocks = 1;
                    else if (sXDocBlocks_after == "-1")
                        iXDocBlocks = -1;
                    else
                        iXDocBlocks = 0;

                if (iXDocBlocks > -1)
                    strContent = ParseBlocks(strContent, iXDocBlocks);
                //////if ((hParameters["params_NOBLOCKS"] == null) || (hParameters["params_BLOCKS"] != null))
                //////{
                //////    if ((hParameters["params_BLOCK"] != null) && ((string)hParameters["params_BLOCK"] == "true"))
                //////        strContent = ParseBlocks(strContent, 1);
                //////    else
                //////        strContent = ParseBlocks(strContent, 0);
                //////}
                string sJSONResponse = m_SVCache.GetSV("JSONResponse", "Response_", ""); //default result
                if (sJSONResponse != "")
                {
                    if (sJSONResponse == "1") sJSONResponse = "result";
                    strContent = "{\"" + sJSONResponse + "\":\"" + Utils._JsonEscape(strContent) + "\"}";
                }
                string sXDocAppCache = m_SVCache.GetSV("XDocAppCache", "Response_", ""); //default result
                //if (sXDocAppCache == "1")
                //{
                //    if ((m_context != null) && (m_context.Application != null)) m_context.Application["AppCache"] = m_AppCache;
                //}
                if (sXDocAppCache == "0")
                {
                    if ((context != null) && (context.Application != null)) context.Application["AppCache"] = null;
                }
                string sXDocSVCache = m_SVCache.GetSV("XDocSVCache", "Response_", ""); //default result
                //if (sXDocSVCache == "1")
                //{
                //    //                if ((context != null) && (m_SVCache != null)) m_SVCache.WriteCookies(context.Response.Cookies);
                //    //                if ((context != null) && (context.Application != null)) context.Application["AppCache"] = m_AppCache;
                //    m_SVCache.SaveSVCache0();
                //    if ((context != null) && (context.Session != null)) context.Session["SVCache"] = m_SVCache;
                //    if ((context != null) && (context.Session != null)) context.Session["SessionID"] = m_SVCache.SessionID;
                //}
                if (sXDocSVCache == "0")
                {
                    //                if ((context != null) && (m_SVCache != null)) m_SVCache.WriteCookies(context.Response.Cookies);
                    //                if ((context != null) && (context.Application != null)) context.Application["AppCache"] = m_AppCache;
                    if ((context != null) && (context.Session != null)) context.Session["SVCache"] = null;
                    if ((context != null) && (context.Session != null)) context.Session["SessionID"] = null;
                }
            }

            //System.Diagnostics.Debug.WriteLine("end:" + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));

            if (sEncoding == "")
                context.Response.Write(strContent);
            else
            {
                System.Text.Encoding enc = null;
                try
                {
                    enc = System.Text.Encoding.GetEncoding(sEncoding);
                }
                catch (Exception)
                {
                    enc = System.Text.Encoding.Default;
                }
                byte[] arrContent = enc.GetBytes(strContent);
                context.Response.OutputStream.Write(arrContent, 0, arrContent.Length);
                context.Response.OutputStream.Flush();
            }
        }

    }

    public class XDocService4Async : HttpTaskAsyncHandler, IRequiresSessionState
    {
        async Task doit(HttpContext p_ctx)
        {

            XDocService4 xdoc4 = new XDocService4();
            await Task.Run(() => xdoc4.ProcessRequest(p_ctx));
        }
        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public override Task ProcessRequestAsync(HttpContext p_ctx)
        {
            return doit(p_ctx);
        }

        static int _count = 0;
    }
    public class XDocService4AsyncR : HttpTaskAsyncHandler, IReadOnlySessionState
    {
        async Task doit(HttpContext p_ctx)
        {

            XDocService4R xdoc4 = new XDocService4R();
            await Task.Run(() => xdoc4.ProcessRequest(p_ctx));
        }
        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public override Task ProcessRequestAsync(HttpContext p_ctx)
        {
            return doit(p_ctx);
        }

        static int _count = 0;
    }
    public class XDocService4AsyncS : HttpTaskAsyncHandler
    {
        async Task doit(HttpContext p_ctx)
        {

            XDocService4S xdoc4 = new XDocService4S();
            await Task.Run(() => xdoc4.ProcessRequest(p_ctx));
        }
        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public override Task ProcessRequestAsync(HttpContext p_ctx)
        {
            return doit(p_ctx);
        }

        static int _count = 0;
    }

    //    public class SSEHandler : IHttpHandler
    //    {
    //        bool bWS = false;
    //        Clients cClients = null;

    //        public void ProcessRequest(HttpContext context)
    //        {
    //            if (System.Configuration.ConfigurationManager.AppSettings["WS"] != null) bWS = (string)System.Configuration.ConfigurationManager.AppSettings["WS"] == "1";
    //            if (bWS)
    //            {
    //                if (cClients == null)
    //                    cClients = (Clients)context.Application["Clients"];
    //                if (cClients == null)
    //                {
    //                    cClients = new Clients();
    //                    context.Application["Clients"] = cClients;
    //                }
    //            }
    //            if (cClients != null)
    //                cClients.ConnectSSE(context);
    ////            return;

    //            //context.Response.ContentType = "text/event-stream";
    //            //context.Response.AddHeader("Access-Control-Allow-Origin", "*");
    //            //var itt = 0; ;
    //            //while (true)
    //            //{
    //            //    if (!context.Response.IsClientConnected)
    //            //        break;

    //            //    /* **** UPDATE:  REMOVED THE BREAK. STREAMING BEGINS AFTER ABOUT THE 20TH OR 30TH LOOP ****/

    //            //    // if (itt++ > 10)
    //            //    //   break;

    //            //    context.Response.Write(string.Format("data: alive from XDoc {0}\n\n", DateTime.Now.ToLongTimeString()));
    //            //    context.Response.Flush();
    //            //    System.Threading.Thread.Sleep(5000);
    //            //    if (itt++ > 10) break;
    //            //}
    //            //context.Response.Write("data: end of stream\n\n");
    //            //// context.Response..ContentType = "text/plain";
    //            //context.Response.Flush();
    //            //context.Response.Close();

    //        }

    //        public void ProcessRequest1(HttpContext context)
    //        {
    //            HttpResponse Response = context.Response;
    //            DateTime startDate = DateTime.Now;
    //            //            Response.ContentType = "text/event-stream";
    //            Response.AddHeader("Content-Type", "text/event-stream");
    //            Response.AddHeader("Cache-Control", "no-cache");
    //            Response.AddHeader("Access-Control-Allow-Origin", "*");
    //            //while (startDate.AddMinutes(1) > DateTime.Now)
    //            //{
    //            //    Response.Write(string.Format("data: {0}\n\n", new Random().Next(1, 100)));
    //            //    Response.Flush();
    //            //    System.Threading.Thread.Sleep(5000);
    //            //}
    //            Response.Write("data: start 123\n\n");
    //            Response.Flush();
    //            System.Threading.Thread.Sleep(5000);
    //            if (!Response.IsClientConnected) return;

    //            Response.Write("id: 101\n");
    //            Response.Write("data: data 101\n\n");
    //            Response.Flush();
    //            System.Threading.Thread.Sleep(5000);
    //            if (!Response.IsClientConnected) return;

    //            Response.Write("id: 102\n");
    //            Response.Write("data: data102\n\n");
    //            Response.Flush();
    //            System.Threading.Thread.Sleep(5000);
    //            if (!Response.IsClientConnected) return;

    //            //Response.Write("event: close\n");
    //            //Response.Write("data: dataclose 123\n\n");
    //            //Response.Flush();
    //            //System.Threading.Thread.Sleep(5000);
    //            //if (!Response.IsClientConnected) return;

    //            Response.Write("event: foo\n");
    //        Response.Write("data: Message of type \"foo\"\n\n");
    //        Response.Flush();
    //            Response.Write("event: foo\n");
    //            Response.Write("data: Message2 of type \"foo\"\n\n");
    //            Response.Flush();
    //            System.Threading.Thread.Sleep(5000);
    //            if (!Response.IsClientConnected) return;

    //            Response.Write("event: foo1\n");
    //            Response.Write("data: Message of type \"foo1\"\n\n");
    //            Response.Flush();
    //            Response.Write("id: 103\n");
    //            Response.Write("data: data103\n\n");
    //            Response.Write("event: foo\n");
    //            Response.Write("data: Message of type \"foo\"\n\n");
    //            Response.Flush();
    //            Response.Flush();
    //            System.Threading.Thread.Sleep(5000);
    //            if (!Response.IsClientConnected) return;


    //            Response.Write("data: end of stream\n\n");
    //            Response.Flush();
    //            System.Threading.Thread.Sleep(5000);
    //            if (!Response.IsClientConnected) return;

    //            //Response.Write("id: CLOSE\n");
    //            //Response.Write("data: dataCLOSE\n\n");
    //            //Response.Flush();
    //            ////Response.Close();
    //            ////            Response.End();
    //            //System.Threading.Thread.Sleep(5000);
    //            //if (!Response.IsClientConnected) return;

    //            Response.Write("id: afterclose\n");
    //            Response.Write("data: dataafterclose \n\n");
    //            Response.Flush();

    //            Response.End();

    //        }

    //        public bool IsReusable
    //        {
    //            get
    //            {
    //                return false;
    //            }
    //        }

    //    }

}