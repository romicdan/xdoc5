﻿using System;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web;
using System.Text;
using System.Configuration;
//using XDataSourceModule;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Security.Cryptography;
using KubionDataNamespace;

namespace XHTMLMerge
{
    class CPersister
    {
        protected string m_XDocPath = "";
        protected string m_XBinPath = "";
        public CPersister()
        {
        }
        public CPersister(string sPath,string sBinPath)
        {
            m_XDocPath = sPath;
            m_XBinPath = sBinPath;
        }

        public void RemoveCompiledTemplates(string strTemplateName)
        {
            if (m_XBinPath != "")
            {
                if (File.Exists(m_XBinPath + strTemplateName + ".xbin4"))
                    File.Delete(m_XBinPath + strTemplateName + ".xbin4");
            }
            if (m_XDocPath != "")
            {
                if (File.Exists(m_XDocPath + strTemplateName + ".xdoc4"))
                    File.Delete(m_XDocPath + strTemplateName + ".xdoc4");
            }
        }
 
        public string SaveTemplateUnCompiled(string strTemplateName, string sUnCompiled)
        {
            string sResult = strTemplateName;
            if (m_XDocPath != "")
            {
                if (File.Exists(m_XDocPath + strTemplateName + ".xdoc4"))
                    File.Delete(m_XDocPath + strTemplateName + ".xdoc4");
                WriteAllText(m_XDocPath + strTemplateName + ".xdoc4", sUnCompiled);
            }
            return sResult;
        }
        public string SaveTemplateCompiled(string strTemplateName, string sCompiled, string sWarning)
        {
            string sResult = strTemplateName;
            if (m_XBinPath != "")
            {
                if (File.Exists(m_XBinPath + strTemplateName + ".xbin4"))
                    File.Delete(m_XBinPath + strTemplateName + ".xbin4");
                if (File.Exists(m_XBinPath + strTemplateName + ".xbin4.json"))
                    File.Delete(m_XBinPath + strTemplateName + ".xbin4.json");
                if (sCompiled == null)
                {
                    if ((sWarning != "") && (m_XDocPath != ""))
                        WriteAllText(m_XDocPath + strTemplateName + "_error.txt", sWarning);
                    return "";
                }
                try
                {
                    if (sCompiled != "error")
                    {
                        JToken ob = JToken.Parse(sCompiled);
                        sCompiled = ob.ToString(Newtonsoft.Json.Formatting.Indented);
                        WriteAllText(m_XBinPath + strTemplateName + ".xbin4.json", sCompiled);

                        string sCompiledEnc = new SimplerAES().Encrypt(sCompiled);
                        WriteAllText(m_XBinPath + strTemplateName + ".xbin4", sCompiledEnc);
                    }
                }
                catch(Exception ex3)
                {
                    sWarning = ex3.Message;
                }
                if (m_XDocPath != "")
                    if (File.Exists(m_XDocPath + strTemplateName + "_error.txt"))
                        File.Delete(m_XDocPath + strTemplateName + "_error.txt");
                if ((sWarning != "") && (m_XDocPath != ""))
                    WriteAllText(m_XDocPath + strTemplateName + "_error.txt", sWarning);
            }
            else
                return StoreCompiledTemplateDB(strTemplateName, sCompiled, sWarning);
            return sResult;
        }

        public string LoadTemplateCompiled(string strTemplateName, ref string templateWarning)
        {
            string sTemplateText = "error";
            if ((m_XDocPath != "") && (m_XBinPath != ""))
            {
                DateTime cdate = File.GetLastWriteTime(m_XBinPath + strTemplateName + ".xbin4");
                DateTime tdate = File.GetLastWriteTime(m_XDocPath + strTemplateName + ".xdoc4");
                if (cdate > tdate)
                    if (File.Exists(m_XBinPath + strTemplateName + ".xbin4"))
                    {
                        sTemplateText = File.ReadAllText(m_XBinPath + strTemplateName + ".xbin4");
                        sTemplateText = new SimplerAES().Decrypt(sTemplateText);
                    }
                if (File.Exists(m_XDocPath + strTemplateName + "_error.txt"))
                    templateWarning = File.ReadAllText(m_XDocPath + strTemplateName + "_error.txt");
            }
            else if (m_XBinPath != "")
            {
                if (File.Exists(m_XBinPath + strTemplateName + ".xbin4"))
                {
                    sTemplateText = File.ReadAllText(m_XBinPath + strTemplateName + ".xbin4");
                    sTemplateText = new SimplerAES().Decrypt(sTemplateText);
                }
            }
            else
                sTemplateText = LoadTemplateCompiledDB(strTemplateName, ref templateWarning);
            return sTemplateText;
        }

        public string SaveTemplateContent(string strTemplateName, string sContent)
        {
            string sTemplateText = "error";
            if (m_XDocPath != "")
            {
                if (File.Exists(m_XDocPath + strTemplateName + ".xdoc4"))
                    File.Delete(m_XDocPath + strTemplateName + ".xdoc4");
                WriteAllText(m_XDocPath + strTemplateName + ".xdoc4", sContent);
            }
            else
                sTemplateText = SaveTemplateContentDB(strTemplateName,sContent);
            return sTemplateText;
        }
        public string LoadTemplateContent(string strTemplateName)
        {
            string sTemplateText = "error";
            if (m_XDocPath != "")
            {
                if (File.Exists(m_XDocPath + strTemplateName + ".xdoc4"))
                {
                    sTemplateText = File.ReadAllText(m_XDocPath + strTemplateName + ".xdoc4");
                }
                else
                {
                    sTemplateText = LoadTemplateContentDB(strTemplateName);
                    //                    File.WriteAllText(m_XDocPath + templateName + ".xdoc4", sTemplateText);
                    WriteAllText(m_XDocPath + strTemplateName + ".xdoc4", sTemplateText);
                }
            }
            else
                sTemplateText = LoadTemplateContentDB(strTemplateName);
            return sTemplateText;
        }

        #region DB
        protected ClientData  m_clientData = null;

        private static string L_String(string strString)
        {
            if (strString == null) return "";
            else return strString.Replace("'", "''");
        }

        private string LoadTemplateContentDB(string templateName)
        {
            string sTemplateText = "error";
            //string sSQL = "SELECT templatename,templatecontent,templatecontentexpanded FROM S_Templates WHERE templatename='" + L_String(templateName) + "'";
            //DataTable dt = m_dataProvider.GetDataTable(sSQL);
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    DataRow row = dt.Rows[0];
            //    sTemplateText = ClientData.GetStringResult(row["templatecontentexpanded"], "");
            //    if ((sTemplateText == "{no_macros}") || (sTemplateText == ""))
            //        sTemplateText = ClientData.GetStringResult(row["templatecontent"], "");
            //}
            return sTemplateText;
        }
        private string SaveTemplateContentDB(string templateName, string sContent)
        {
            string sTemplateText = "error";
            //string sSQL = "SELECT templatename,templatecontent,templatecontentexpanded FROM S_Templates WHERE templatename='" + L_String(templateName) + "'";
            //DataTable dt = m_dataProvider.GetDataTable(sSQL);
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    DataRow row = dt.Rows[0];
            //    sTemplateText = ClientData.GetStringResult(row["templatecontentexpanded"], "");
            //    if ((sTemplateText == "{no_macros}") || (sTemplateText == ""))
            //        sTemplateText = ClientData.GetStringResult(row["templatecontent"], "");
            //}
            return sTemplateText;
        }
        private string LoadTemplateCompiledDB(string templateName, ref string templateWarning)
        {
            string sTemplateText = "error";
            //string sSQL = "SELECT templatename,compiled,warning FROM C_Templates WHERE templatename='" + L_String(templateName) + "' AND exists (SELECT templatename from s_templates where templatename='" + L_String(templateName) + "' and TemplateToRedirect like 'compiled%')";
            //DataTable dt = m_dataProvider.GetDataTable(sSQL);
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    DataRow row = dt.Rows[0];
            //    templateWarning = ClientData.GetStringResult(row["warning"], "");
            //    sTemplateText = ClientData.GetStringResult(row["compiled"], "");
            //}
            return sTemplateText;
        }
        private string StoreCompiledTemplateDB(string strTemplateName, string sCompiled, string sWarning)
        {
            string sResult = "";
//            string sSQL = "";
//            try
//            {
//                sSQL = "DELETE FROM C_Templates WHERE templatename='" + L_String(strTemplateName) + "'; INSERT INTO C_Templates (templatename,compiled,warning) VALUES('" + L_String(strTemplateName) + "','" + L_String(sCompiled) + "','" + L_String(sWarning) + "');  UPDATE S_Templates set TemplateToRedirect = 'compiled " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + "' WHERE templatename='" + L_String(strTemplateName) + "'";
//                sResult = m_dataProvider.ExecuteNonQuery(sSQL);
//            }
//            catch (Exception ex)
//            {
//                if (ex.Message.StartsWith("Invalid object name"))
//                {
//                    string sMessage1 = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[C_Templates]') AND type in (N'U')) 
//                                    CREATE TABLE [C_Templates](
//                                    	[TemplateName] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
//                                    	[Compiled] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
//                                    	[Warning] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
//                                     CONSTRAINT [PK_C_Templates] PRIMARY KEY CLUSTERED ([TemplateName] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";
//                    try
//                    {
//                        m_dataProvider.ExecuteNonQuery(sMessage1 + " " + sSQL);
//                    }
//                    catch (Exception)
//                    {
//                        throw;
//                    }
//                }
//            }
            return sResult;
        }
#endregion

        private static void WriteAllText(string filePath, string content)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(filePath);
            file.Directory.Create(); // If the directory already exists, this method does nothing.
            System.IO.File.WriteAllText(file.FullName, content);
        }

    }


 
    public class SimplerAES
    {
        private static byte[] key = { 123, 217, 19, 11, 24, 26, 85, 45, 114, 184, 27, 162, 37, 112, 222, 209, 241, 24, 175, 144, 173, 53, 196, 29, 24, 26, 17, 218, 131, 236, 53, 209 };
        private static byte[] vector = { 146, 64, 191, 111, 23, 3, 113, 119, 231, 121, 221, 112, 79, 32, 114, 156 };
        private ICryptoTransform encryptor, decryptor;
        private UTF8Encoding encoder;
        public SimplerAES() : this("") { }

        public SimplerAES(string password)
        {
            RijndaelManaged rm = new RijndaelManaged();
            if(password!="")
                key = new Rfc2898DeriveBytes(password, key).GetBytes(32);
            encryptor = rm.CreateEncryptor(key, vector);
            decryptor = rm.CreateDecryptor(key, vector);
            encoder = new UTF8Encoding();
        }

        public string Encrypt(string unencrypted)
        {
            return Convert.ToBase64String(Encrypt(encoder.GetBytes(unencrypted)));
        }

        public string Decrypt(string encrypted)
        {
            return encoder.GetString(Decrypt(Convert.FromBase64String(encrypted)));
        }

        public byte[] Encrypt(byte[] buffer)
        {
            return Transform(buffer, encryptor);
        }

        public byte[] Decrypt(byte[] buffer)
        {
            return Transform(buffer, decryptor);
        }

        protected byte[] Transform(byte[] buffer, ICryptoTransform transform)
        {
            MemoryStream stream = new MemoryStream();
            using (CryptoStream cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
            {
                cs.Write(buffer, 0, buffer.Length);
            }
            return stream.ToArray();
        }
    }
}

    


