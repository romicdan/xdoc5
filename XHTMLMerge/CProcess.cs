﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;
//using XDocuments;
using System.Globalization;
//using KubionLogNamespace;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.UI;
using System.Reflection;

namespace XHTMLMerge
{
    public class CProcess
    {
        internal CRuntime  m_runtime = null;
        internal string m_trace = "";
        internal bool noDash = false;
        internal string m_templateName = "";
        internal string sExitType = ""; // loop,template,request
        internal string sExitValue = "";
        internal bool m_UseGlobalParams = false;
        Hashtable m_defaultParams = CollectionsUtil.CreateCaseInsensitiveHashtable();
        internal Hashtable DefaultParams
        {
            get { return m_defaultParams; }
            set { m_defaultParams = value; }
        }
        Hashtable m_templateParams = CollectionsUtil.CreateCaseInsensitiveHashtable();
        internal Hashtable TemplateParams
        {
            get { return m_templateParams; }
            set { m_templateParams = value; }
        }
        Hashtable m_globalParams = CollectionsUtil.CreateCaseInsensitiveHashtable();
        internal Hashtable GlobalParams
        {
            get { return m_globalParams; }
            set { m_globalParams = value; }
        }
        Hashtable m_arrays = CollectionsUtil.CreateCaseInsensitiveHashtable();
        internal Hashtable Arrays
        {
            get { return m_arrays; }
            set { m_arrays = value; }
        }

        StringBuilder sbCond = new StringBuilder();
        StringBuilder sbParams = new StringBuilder();
        StringBuilder sbInclude = new StringBuilder();
        public string sVersion = "";

        //string sbs = "";

        public CProcess(CRuntime runtime, string templateName)
        {
            m_runtime = runtime;
            m_templateName = templateName;
            sVersion = runtime.sVersion;
            m_trace = m_runtime.m_trace;
        }
        //public string SRun(JCmd obj, Hashtable hParameters,  ref string sExit)
        //{
        //    string sResult = "";
        //    if (hParameters != null) TemplateParams = hParameters;
        //    TemplateParams["_TEMPLATENAME_"] = m_templateName;
        //    TemplateParams["_USERNAME_"] = m_runtime.UserName;
        //    TemplateParams["_OPEN_BRACKET_"] = "(";
        //    TemplateParams["_CLOSE_BRACKET_"] = ")";
        //    TemplateParams["_DASHSIGN_"] = "#";
        //    TemplateParams["_PERCENTSIGN_"] = "%";
        //    TemplateParams["_ATSIGN_"] = "@";
        //    TemplateParams["_DOTSIGN_"] = ".";
        //    TemplateParams["_COMMASIGN_"] = ",";
        //    TemplateParams["_CRLFSIGN_"] = "\r\n";
        //    TemplateParams["_TABSIGN_"] = "\t";

        //    sResult = obj.SProcessCommands(this);
        //    if (sExitType != "")
        //    {
        //        if (sExitType == "template")
        //        {
        //            sExit = sExitValue;// GetParameterValue("__exit_value", "");
        //        }
        //    }
        //    return sResult;
        //}
        private class UCMatch
        {
            public int Index = -1;
            public string Value = "";
            public bool IsUC = true;

            public int Length
            {
                get
                {
                    if (Value != null)
                        return Value.Length;
                    return 0;

                }
            }

            public UCMatch(string value, int index, bool isUC)
            {
                Index = index;
                Value = value;
                IsUC = isUC;
            }

            public override string ToString()
            {
                if (Value != null)
                    return Value;
                return "";
            }
        }
        SortedList<int, UCMatch> GetUCMatches(string originalText)
        {

            SortedList<int, UCMatch> ret = new SortedList<int, UCMatch>();
            if (originalText == null)
                return ret;

            string ucId = "<uc:";
            string ctrlId = "<ctrl:";
            string closingBr = "/>";

            string text = originalText.ToLower();

            int firstIndex = 0;

            while (firstIndex != -1)
            {
                int idx = text.IndexOf(ucId, firstIndex);
                if (idx != -1)
                {
                    int idx2 = text.IndexOf(closingBr, idx + 1);
                    if (idx2 == -1)
                        throw new Exception("User control tag is missing the closing angular bracket");
                    string tag = originalText.Substring(idx, idx2 - idx + closingBr.Length);
                    UCMatch match = new UCMatch(tag, idx, true);
                    ret[match.Index] = match;
                    firstIndex = idx + match.Length;
                }
                else
                    break;
            }

            firstIndex = 0;

            while (firstIndex != -1)
            {
                int idx = text.IndexOf(ctrlId, firstIndex);
                if (idx != -1)
                {
                    int idx2 = text.IndexOf(closingBr, idx + 1);
                    if (idx2 == -1)
                        throw new Exception("Web control tag is missing the closing angular bracket");
                    string tag = originalText.Substring(idx, idx2 - idx + closingBr.Length);
                    UCMatch match = new UCMatch(tag, idx, false);
                    ret[match.Index] = match;
                    firstIndex = idx + match.Length;
                }
                else
                    break;
            }

            return ret;

        }
        string GetWebControlName(string tag)
        {
            if (tag == null || tag == "")
                return "";
            string key = "ctrl:";
            int fio = tag.ToLower().IndexOf(key);
            if (fio != -1)
            {
                int lio = tag.IndexOf(" ", fio + 1);
                return tag.Substring(fio + key.Length, lio - fio - key.Length).TrimEnd().TrimStart();
            }
            return "";

        }
        StringBuilder ProcessUC2String(string docHtml)
        {
            StringBuilder sb = new StringBuilder();
            SortedList<int, UCMatch> allMatches = GetUCMatches(docHtml);

            //add first part of the template, before the first UC
            if (allMatches.Count > 0)
            {
                int firstOccurence = allMatches.Keys[0];
                if (allMatches[firstOccurence].Index > 0)
                {
                    string firstPart = docHtml.Substring(0, allMatches[firstOccurence].Index);
                    //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(firstPart));
                    sb.Append(firstPart);
                }
            }

            for (int k = 0; k < allMatches.Count; k++)
            {
                UCMatch m = allMatches[allMatches.Keys[k]];

                // add the middle template parts (between controls)
                if (k > 0)
                {
                    UCMatch previousMatch = allMatches[allMatches.Keys[k - 1]];
                    int start = previousMatch.Index + previousMatch.Length;
                    int end = m.Index - 1;
                    string templateText = docHtml.Substring(start, end - start + 1);
                    //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(templateText));
                    sb.Append(templateText);
                }

                if (m.IsUC) // this is an usercontrol
                {
                    //string tag = m.Value;

                    //string ucName = GetUCName(tag);
                    //Control uc = TemplateControl.LoadControl(ucName + ".ascx");

                    //string ucID = System.IO.Path.GetFileNameWithoutExtension(ucName);
                    //int count = 0;
                    ////bool found = true;
                    ////while (found)
                    ////{
                    ////if (DocumentBody.FindControl(ucID + "_" + count.ToString()) != null)
                    ////if (DocumentBody.FindControl(ucID + "_" + count.ToString()) != null)
                    ////{
                    ////    found = true;
                    ////    count++;
                    ////}
                    ////else
                    ////    found = false;
                    ////}
                    //ucID += "_" + count.ToString();
                    //uc.ID = ucID;

                    ////DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, uc);
                    //StringBuilder sb2 = new StringBuilder();
                    //StringWriter sw = new StringWriter(sb2);

                    //using (HtmlTextWriter writer = new HtmlTextWriter(sw))
                    //{
                    //    uc.RenderControl(writer);
                    //}
                    //sb.Append(sb2.ToString());


                    //Hashtable controlParameters = GetUCParameters(tag);

                    ////(uc as IDynamicUC).SetParameters(controlParameters);
                    ////(uc as IDynamicUC).DynamicUCEvent += new DynamicUCEventHandler(XDocUC_DynamicUCEvent);
                }
                else // this is a web control
                {
                    string typeName = GetWebControlName(m.Value);
                    if (typeName == "")
                        throw new Exception("The web control " + m.Value + " does not have a type name");

                    Hashtable tagAttributes = ParseUtils.TagAttributes(m.Value);

                    string path = ParseUtils.GetTagAttribute(tagAttributes, "assembly");
                    if (path == "")
                        throw new Exception("The web control " + m.Value + " does not have an assembly attribute");

                    path = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Bin"), path);

                    Assembly assmbl = Assembly.LoadFrom(path);
                    Control ctrl = assmbl.CreateInstance(typeName) as Control;

                    if (ctrl != null)
                    {
                        string ctrlID = tagAttributes["id"] as String;
                        if (ctrlID != "")
                        {
                            ctrl.ID = ctrlID;

                            IDictionaryEnumerator en = tagAttributes.GetEnumerator();
                            while (en.MoveNext())
                            {
                                if (en.Key.ToString().ToLower() == "assembly" || en.Key.ToString().ToLower() == "id")
                                    continue;
                                try
                                {
                                    PropertyInfo pi = assmbl.GetType(typeName).GetProperty(en.Key.ToString());
                                    pi.SetValue(ctrl, en.Value.ToString(), null);
                                }
                                catch { };

                            }

                            //DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, ctrl);

                            StringBuilder sb2 = new StringBuilder();
                            StringWriter sw = new StringWriter(sb2);

                            using (HtmlTextWriter writer = new HtmlTextWriter(sw))
                            {
                                ctrl.RenderControl(writer);
                            }
                            sb.Append(sb2.ToString());
                            ////MIMI
                            //m_ctrl = m.Value;
                        }
                    }
                }
            }

            // add the last template part

            if (allMatches.Count > 0)
            {
                int lastOccurence = allMatches.Keys[allMatches.Count - 1];
                if (allMatches[lastOccurence].Index > 0)
                {
                    int index = allMatches[lastOccurence].Index + allMatches[lastOccurence].Length;
                    if (index < docHtml.Length)
                    {
                        string lastPart = docHtml.Substring(index);
                        sb.Append(lastPart);
                    }
                }
            }
            else // there is no user control or web control
            {
                sb.Append(docHtml);
            }


            return sb;
        }

        /*
        * 170824       parameter sniffing :: :: SQLPar; OLEDBPar; ODBCPar; 
        *              unify KubionData
        *              HTTP response StatusDescription, "StatusCode" "StatusCodeInt"  in __header_out when XDocIgnore=1 in __headers_in
        */

        public void Run(JCmd obj, Hashtable hParameters, StringBuilder sb, ref string sExit)
        {
            DateTime dt1 = DateTime.Now;
            if (m_runtime.Runs <= 100)
            {
                try
                {
                    m_runtime.Runs++;
                    if (m_trace == "1")
                        m_runtime.Message(GetParameterValue("_SESSIONID_"), "Trace",  new String('\t', m_runtime.Runs) + "-start".PadRight(10) + "  Template:" + m_templateName + ";");
                    if (hParameters != null) TemplateParams = hParameters;
                    TemplateParams["_TEMPLATENAME_"] = m_templateName;
                    //                TemplateParams["_USERNAME_"] = m_runtime.UserName;
                    string sUserName = m_runtime.UserName;
                    TemplateParams["_FULLUSERNAME_"] = sUserName;
                    if (sUserName.IndexOf("\\") != -1)
                        sUserName = sUserName.Substring(sUserName.IndexOf("\\") + 1);
                    TemplateParams["_USERNAME_"] = sUserName;

                    try
                    {
                        HttpContext m_HttpContext = (HttpContext)m_runtime.myHttpContext;
                        if ((m_HttpContext != null) && (m_HttpContext.Session != null)) TemplateParams["_SESSIONID_"] = m_HttpContext.Session.SessionID;
                    }
                    catch (Exception) { };
                    TemplateParams["_OPEN_BRACKET_"] = "(";
                    TemplateParams["_CLOSE_BRACKET_"] = ")";
                    TemplateParams["_SINGLEQUOTE_"] = "'";
                    TemplateParams["_DOUBLEQUOTE_"] = "\"";
                    TemplateParams["_DASHSIGN_"] = "#";
                    TemplateParams["_PERCENTSIGN_"] = "%";
                    TemplateParams["_ATSIGN_"] = "@";
                    TemplateParams["_DOTSIGN_"] = ".";
                    TemplateParams["_COMMASIGN_"] = ",";
                    TemplateParams["_CRLFSIGN_"] = "\r\n";
                    TemplateParams["_TABSIGN_"] = "\t";
                    TemplateParams["_BACKSLASH_"] = "\\";
                    TemplateParams["_XDOCVERSION_"] = "5.0.0.171218";
                    noDash = (TemplateParams["noDash"] != null);
                    if (obj != null) obj.ProcessCommands(this, sb);
                    else throw (new Exception("null XDoc"));
                    string sUC = (string)TemplateParams["UC"];
                    if (sUC == null) sUC = "0";

                    if (sUC == "1" || sUC.ToLower() == "true")
                    {
                        string sContent = sb.ToString();
                        StringBuilder sb1 = ProcessUC2String(sContent);
                        sb.Clear();
                        sb.Append(sb1.ToString());
                    }
                    if (sExitType != "")
                    {
                        if (sExitType == "template")
                        {
                            sExit = sExitValue;// GetParameterValue("__exit_value", "");
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_runtime.Message(GetParameterValue("_SESSIONID_"), "Error", ex.Message);
                }
                finally
                {
                    if ((m_trace == "1") || (m_trace == "2"))
                        m_runtime.Message(GetParameterValue("_SESSIONID_"), "Trace",  new String('\t', m_runtime.Runs) + "-" + DateTime.Now.Subtract(dt1).TotalMilliseconds.ToString("F0").PadLeft(5) + "     " + " Template:" + m_templateName + ";");
                    m_runtime.Runs--;
                };
            }
            else
                sExit = "TemplatesOverflow";
        }
        public void Run4Inline(JCmd4 obj, string sTemplateName, StringBuilder sb, ref string sExit)
        {
            DateTime dt1 = DateTime.Now;
            if (m_runtime.Runs <= 100)
            {
                try
                {
                    m_runtime.Runs++;
                    if (m_trace == "1")
                        m_runtime.Message(GetParameterValue("_SESSIONID_"), "Trace", new String('\t', m_runtime.Runs) + "-start".PadRight(10) + "  Template:" + m_templateName + ";");
                    string oldTemplateName = (string)TemplateParams["_TEMPLATENAME_"];
                    TemplateParams["_TEMPLATENAME_"] = sTemplateName;

                    if (obj != null) obj.ProcessCommands(this, sb);
                    else throw (new Exception("null XDoc"));
                    string sUC = (string)TemplateParams["UC"];
                    if (sUC == null) sUC = "0";

                    if (sUC == "1" || sUC.ToLower() == "true")
                    {
                        string sContent = sb.ToString();
                        StringBuilder sb1 = ProcessUC2String(sContent);
                        sb.Clear();
                        sb.Append(sb1.ToString());
                    }
                    if (sExitType != "")
                    {
                        if (sExitType == "template")
                        {
                            sExit = sExitValue;// GetParameterValue("__exit_value", "");
                        }
                    }

                    TemplateParams["_TEMPLATENAME_"] = oldTemplateName;
                }
                catch (Exception ex)
                {
                    m_runtime.Message(GetParameterValue("_SESSIONID_"), "Error", ex.Message);
                }
                finally
                {
                    if ((m_trace == "1") || (m_trace == "2"))
                        m_runtime.Message(GetParameterValue("_SESSIONID_"), "Trace", new String('\t', m_runtime.Runs) + "-" + DateTime.Now.Subtract(dt1).TotalMilliseconds.ToString("F0").PadLeft(5) + "     " + " Template:" + m_templateName + ";");
                    m_runtime.Runs--;
                };
            }
            else
                sExit = "TemplatesOverflow";
        }

        public void Run4(JCmd4 obj, Hashtable hParameters, StringBuilder sb, ref string sExit)
        {
            DateTime dt1 = DateTime.Now;
            if (m_runtime.Runs <= 100)
            {
                try
                {
                    m_runtime.Runs++;
                    if (m_trace == "1")
                        m_runtime.Message(GetParameterValue("_SESSIONID_"), "Trace", new String('\t', m_runtime.Runs) + "-start".PadRight(10) + "  Template:" + m_templateName + ";");
                    if (hParameters != null) TemplateParams = hParameters;
                TemplateParams["_TEMPLATENAME_"] = m_templateName;
                //                TemplateParams["_USERNAME_"] = m_runtime.UserName;
                string sUserName = m_runtime.UserName;
                TemplateParams["_FULLUSERNAME_"] = sUserName;
                if (sUserName.IndexOf("\\") != -1)
                    sUserName = sUserName.Substring(sUserName.IndexOf("\\") + 1);
                TemplateParams["_USERNAME_"] = sUserName;
                try
                {
                    HttpContext m_HttpContext = (HttpContext)m_runtime.myHttpContext;
                    if ((m_HttpContext != null) && (m_HttpContext.Session != null)) TemplateParams["_SESSIONID_"] = m_HttpContext.Session.SessionID;
                }
                catch (Exception) { };
                TemplateParams["_OPEN_BRACKET_"] = "(";
                TemplateParams["_CLOSE_BRACKET_"] = ")";
                TemplateParams["_SINGLEQUOTE_"] = "'";
                TemplateParams["_DOUBLEQUOTE_"] = "\"";
                TemplateParams["_DASHSIGN_"] = "#";
                TemplateParams["_PERCENTSIGN_"] = "%";
                TemplateParams["_ATSIGN_"] = "@";
                TemplateParams["_DOTSIGN_"] = ".";
                TemplateParams["_COMMASIGN_"] = ",";
                TemplateParams["_CRLFSIGN_"] = "\r\n";
                TemplateParams["_TABSIGN_"] = "\t";
                TemplateParams["_BACKSLASH_"] = "\\";
                TemplateParams["_XDOCVERSION_"] = "5.0.0.171218";
                noDash = (TemplateParams["noDash"] != null);
                if (obj != null) obj.ProcessCommands(this, sb);
                else throw (new Exception("null XDoc"));
                string sUC = (string)TemplateParams["UC"];
                if (sUC == null) sUC = "0";

                if (sUC == "1" || sUC.ToLower() == "true")
                {
                    string sContent = sb.ToString();
                    StringBuilder sb1 = ProcessUC2String(sContent);
                    sb.Clear();
                    sb.Append(sb1.ToString());
                }
                if (sExitType != "")
                {
                    if (sExitType == "template")
                    {
                        sExit = sExitValue;// GetParameterValue("__exit_value", "");
                    }
                }
                }
                catch (Exception ex)
                {
                    m_runtime.Message(GetParameterValue("_SESSIONID_"), "Error", ex.Message);
                }
                finally
                {
                    if ((m_trace == "1") || (m_trace == "2"))
                        m_runtime.Message(GetParameterValue("_SESSIONID_"), "Trace", new String('\t', m_runtime.Runs) + "-" + DateTime.Now.Subtract(dt1).TotalMilliseconds.ToString("F0").PadLeft(5) + "     " + " Template:" + m_templateName + ";");
                    m_runtime.Runs--;
                };
            }
            else
                sExit = "TemplatesOverflow";
        }

        #region 2016
        int iInReplaceParameters3 = 0;
        internal string ReplaceParameters3(string sLine, bool bAll)
        {

            if (sLine == null) return "";
            if (sLine == "") return "";
            if (iInReplaceParameters3 > 10) return sLine;
            iInReplaceParameters3++;
            char cSep = '%';
            string[] aLine = sLine.Split(cSep);
            StringBuilder sbResponse = new StringBuilder(aLine[0]);
            for (int i = 1; i < aLine.GetLength(0); i++)
            {
                if (aLine[i].Contains(" ") || (aLine[i] == ""))
                {
                    sbResponse.Append(cSep);
                    sbResponse.Append(aLine[i]);
                    //                    if (i < aLine.GetLength(0) - 1) sbResponse.Append(cSep);
                }
                else
                {
                    string sVal = "";
                    if (aLine[i].Contains("."))
                    {
                        string[] aVar = ParseUtils.MySplit(aLine[i] + '.', '.');
                        sVal = GetParameterValue3(aVar[0], "missingvalue");
                        if (bAll && (sVal == "missingvalue")) sVal = "";
                        if (sVal != "missingvalue")
                        {
                            EncodeOption encOpt = GetEncodeOption(aVar[1]);
                            sVal = ApplyFormat(sVal, aVar[2]);
                            sVal = Utils.Encode(sVal, encOpt);
                        }
                    }
                    else
                    {
                        sVal = GetParameterValue3(aLine[i], "missingvalue");
                        if (bAll && (sVal == "missingvalue") && (i < aLine.GetLength(0) - 1)) sVal = "";
                    }
                    if (sVal != "missingvalue")
                    {
                        sbResponse.Append(sVal);
                    }
                    else
                    {
                        sbResponse.Append(cSep);
                        sbResponse.Append(aLine[i]);
                        if (i < aLine.GetLength(0) - 1) sbResponse.Append(cSep);
                    }
                    i++;
                    if (i == aLine.GetLength(0)) break;
                    sbResponse.Append(aLine[i]);
                }
            }
            iInReplaceParameters3--;
            return sbResponse.ToString();

        }
        internal string GetParameterValue3(string m_strParamName, string s_default)
        {
            //DateTime dt1 = DateTime.Now;
            string retVal = s_default;
            if (m_strParamName.ToUpper() == "NOW")
                retVal = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            else if (m_strParamName.ToUpper() == "UTCNOW")
                retVal = System.DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            else if (m_strParamName.ToUpper() == "TODAY")
                retVal = System.DateTime.Now.ToString("yyyy-MM-dd");
            else if (TemplateParams[m_strParamName] != null)
                retVal = (string)TemplateParams[m_strParamName].ToString();
            else if (DefaultParams[m_strParamName] != null)
                retVal = (string)DefaultParams[m_strParamName].ToString();
            else if (m_UseGlobalParams && (GlobalParams[m_strParamName] != null))
                retVal = (string)GlobalParams[m_strParamName].ToString();
            //DateTime dt2 = DateTime.Now;
            //m_runtime.AddTimespan("GetParVal", dt2.Subtract(dt1).TotalMilliseconds);
            return retVal;
        }

        int iInReplaceSettings3 = 0;
        internal string ReplaceSettings3(string sLine, string psContextName, bool bAll, bool bStrict)
        {
            if (iInReplaceSettings3 > 10) return sLine;
            iInReplaceSettings3++;
            char cSep = '@';
            string sResponse = "";
            string[] aLine = sLine.Split(cSep);
            for (int i = 0; i < aLine.GetLength(0); i++)
            {
                sResponse += aLine[i];
                i++;
                if (i < aLine.GetLength(0))
                {
                    string sVar = aLine[i] + "..";
                    if (sVar.Contains(">") || sVar.Contains(" ") || (i == aLine.GetLength(0) - 1))
                    {
                        sResponse += cSep;
                        i--;
                    }
                    else
                    {
                        string[] aVar = ParseUtils.MySplit(sVar, '.');

                        string sVal = m_runtime.SVCache.GetSV(aVar[0], psContextName, "missingvalue");
                        if (!bStrict && (sVal == "missingvalue")) sVal = m_runtime.SVCache.GetSV(aVar[0], "_", "missingvalue");
                        if (bAll && (sVal == "missingvalue")) sVal = "";

                        if (sVal != "missingvalue")
                        {
                            EncodeOption encOpt = Utils.GetEncodeOption(aVar[1]);
                            sVal = Utils.ApplyFormat(sVal, aVar[2]);
                            sVal = Utils.Encode(sVal, encOpt);
                            sResponse += sVal;
                        }
                        else
                        {
                            sResponse += cSep;
                            i--;
                        }
                    }
                }
            }
            iInReplaceSettings3--;
            return sResponse;
        }

        #endregion 2016
//-----------------------
        int iInReplaceParameters = 0;
        internal string ReplaceParameters(string sLine)
        {
            return ReplaceParameters(sLine, false);
        }
        internal string ReplaceParameters(string sLine, bool bAll)
        {
            if (sVersion == "3") return ReplaceParameters3(sLine, bAll);
            if (sLine == null) return "";
            if (sLine == "") return "";
            if (iInReplaceParameters > 10) return sLine;
            iInReplaceParameters++;
            char cSep = '%';
//            string[] aLine = sLine.Split(cSep);
            string[] aLine = Utils.MySplit( sLine,cSep);
            StringBuilder sbResponse = new StringBuilder(aLine[0]);
            for (int i = 1; i < aLine.GetLength(0); i++)
            {
                if (aLine[i].Contains(" ") || aLine[i].Contains("=") || aLine[i].Contains("&") || (aLine[i] == ""))
                {
                    sbResponse.Append(cSep);
                    sbResponse.Append(aLine[i]);
                    //                    if (i < aLine.GetLength(0) - 1) sbResponse.Append(cSep);
                }
                else
                {
                    string sVal = "";
                    if (aLine[i].Contains("."))
                    {
                        string[] aVar = ParseUtils.MySplit(aLine[i] + '.', '.');
                        sVal = GetParameterValue(aVar[0], "missingvalue");
                        if (bAll && (sVal == "missingvalue") && (i < aLine.GetLength(0) - 1)) sVal = "";
                        if (sVal != "missingvalue")
                        {
                            EncodeOption encOpt = GetEncodeOption(ReplaceParameters(aVar[1]));
                            sVal = ApplyFormat(sVal, ReplaceParameters(aVar[2]));
                            sVal = Utils.Encode(sVal, encOpt);
                        }
                    }
                    else
                    {
                        sVal = GetParameterValue(aLine[i], "missingvalue");
                        if (bAll && (sVal == "missingvalue") && (i < aLine.GetLength(0) - 1)) sVal = "";
                    }
                    if (sVal != "missingvalue")
                    {
                        sbResponse.Append(sVal);
                    }
                    else
                    {
                        sbResponse.Append(cSep);
                        i--;
                        //                        sbResponse.Append(aLine[i]);
                        //                        if (i < aLine.GetLength(0) - 1) sbResponse.Append(cSep);
                    }
                    i++;
                    if (i == aLine.GetLength(0)) break;
                    sbResponse.Append(aLine[i]);
                }
            }
            iInReplaceParameters--;
            return sbResponse.ToString();

        }
        //internal string ReplaceParameters2(string sLine, bool bAll)
        //{

        //    if (sLine == null) return "";
        //    if (sLine == "") return "";
        //    if (iInReplaceParameters > 10) return sLine;
        //    iInReplaceParameters++;
        //    char cSep = '%';
        //    string[] aLine = sLine.Split(cSep);
        //    StringBuilder sbResponse = new StringBuilder(aLine[0]);
        //    for (int i = 1; i < aLine.GetLength(0); i++)
        //    {
        //        if (aLine[i].Contains(" ") || (aLine[i] == ""))
        //        {
        //            sbResponse.Append(cSep);
        //            sbResponse.Append(aLine[i]);
        //            //                    if (i < aLine.GetLength(0) - 1) sbResponse.Append(cSep);
        //        }
        //        else
        //        {
        //            string sVal = "";
        //            if (aLine[i].Contains("."))
        //            {
        //                string[] aVar = ParseUtils.MySplit(aLine[i] + '.', '.');
        //                sVal = GetParameterValue(aVar[0], "missingvalue");
        //                if (bAll && (sVal == "missingvalue")) sVal = "";
        //                if (sVal != "missingvalue")
        //                {
        //                    EncodeOption encOpt = GetEncodeOption(aVar[1]);
        //                    sVal = ApplyFormat(sVal, aVar[2]);
        //                    sVal = Utils.Encode(sVal, encOpt);
        //                }
        //            }
        //            else
        //            {
        //                sVal = GetParameterValue(aLine[i], "missingvalue");
        //                if (bAll && (sVal == "missingvalue") && (i < aLine.GetLength(0) - 1)) sVal = "";
        //            }
        //            if (sVal != "missingvalue")
        //            {
        //                sbResponse.Append(sVal);
        //            }
        //            else
        //            {
        //                sbResponse.Append(cSep);
        //                sbResponse.Append(aLine[i]);
        //                if (i < aLine.GetLength(0) - 1) sbResponse.Append(cSep);
        //            }
        //            i++;
        //            if (i == aLine.GetLength(0)) break;
        //            sbResponse.Append(aLine[i]);
        //        }
        //    }
        //    iInReplaceParameters--;
        //    return sbResponse.ToString();

        //}
        //internal string ReplaceParameters1(string sLine, bool bAll)
        //{

        //    if (sLine == null) return "";
        //    if (iInReplaceParameters > 10) return sLine;
        //    iInReplaceParameters++;
        //    char cSep = '%';
        //    string sResponse = "";
        //    string[] aLine = sLine.Split(cSep);
        //    for (int i = 0; i < aLine.GetLength(0); i++)
        //    {
        //        sResponse += aLine[i];
        //        i++;
        //        if (i < aLine.GetLength(0))
        //        {
        //            string sVar = aLine[i] + "..";
        //            if (sVar.Contains(">") || sVar.Contains(" ") || (i == aLine.GetLength(0) - 1))
        //            {
        //                sResponse += cSep;
        //                i--;
        //            }
        //            else
        //            {
        //                //                        string[] aVar = sVar.Split('.');
        //                string[] aVar = ParseUtils.MySplit(sVar, '.');
        //                string sVal = GetParameterValue(aVar[0], "missingvalue");
        //                if (bAll && (sVal == "missingvalue")) sVal = "";
        //                if (sVal != "missingvalue")
        //                {
        //                    EncodeOption encOpt = Utils.GetEncodeOption(aVar[1]);
        //                    sVal = Utils.ApplyFormat(sVal, aVar[2]);
        //                    sVal = Utils.Encode(sVal, encOpt);
        //                    sResponse += sVal;
        //                }
        //                else
        //                {
        //                    sResponse += "%";
        //                    i--;
        //                }
        //            }
        //        }
        //    }
        //    iInReplaceParameters--;
        //    //if (aLine.GetLength(0) > 1) sResponse = ReplaceSettings(sResponse, sContextName);
        //    //            return sResponse.Trim();
        //    return sResponse;

        //}
        internal string GetParameterValue(string l_ParamName, string s_default, string l_Format, string l_Encode)
        {
            string sResult = "";
            sResult = GetParameterValue(l_ParamName, s_default);
            if (l_Format != "")
                sResult = ApplyFormat(sResult, l_Format);
            if (l_Encode != "None")
                sResult = Encode(sResult, l_Encode);
            return sResult;
        }
        //internal string GetParameterValue1(string l_ParamName, string s_default, string l_Format, string l_Encode)
        //{
        //    string sResult = "";
        //    sResult = GetParameterValue(l_ParamName, s_default);
        //    if (l_Format != "")
        //        sResult = Utils.ApplyFormat(sResult, l_Format);
        //    if (l_Encode != "None")
        //        sResult = Utils.Encode(sResult, l_Encode);
        //    return sResult;
        //}
        internal string GetParameterValue(string m_strParamName)
        {
            return GetParameterValue(m_strParamName, "");
        }
        internal string GetParameterValue(string m_strParamName, string s_default)
        {
            if (sVersion == "3") return GetParameterValue3(m_strParamName, s_default);
            //DateTime dt1 = DateTime.Now;
            string retVal = s_default;
            if (m_strParamName.ToUpper() == "NOW")
                retVal = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            else if (m_strParamName.ToUpper() == "UTCNOW")
                retVal = System.DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            else if (m_strParamName.ToUpper() == "TODAY")
                retVal = System.DateTime.Now.ToString("yyyy-MM-dd");
            else if (TemplateParams[m_strParamName] != null)
                retVal = (string)TemplateParams[m_strParamName].ToString();
            else if (DefaultParams[m_strParamName] != null)
                retVal = (string)DefaultParams[m_strParamName].ToString();
            else if (m_UseGlobalParams && (GlobalParams[m_strParamName] != null))
                retVal = (string)GlobalParams[m_strParamName].ToString();
            //DateTime dt2 = DateTime.Now;
            //m_runtime.AddTimespan("GetParVal", dt2.Subtract(dt1).TotalMilliseconds);
            return retVal;
        }
        internal string GetJsonParameters()
        {
//            StringBuilder sJSON = new StringBuilder();
//            sJSON.Append("{");
            return Utils.Hashtable2Json(TemplateParams, "_");
  //          sJSON.Append("\""); sJSON.Append(Utils._JsonEscape(key)); sJSON.Append("\":\""); sJSON.Append(Utils._JsonEscape(hash[key].ToString())); sJSON.Append("\"");

 //           sJSON.Append("}");
//            return sJSON.ToString();
        }

        int iInReplaceSettings = 0;
        internal string ReplaceSettings(string sLine, string psContextName, bool bAll, bool bStrict)
        {
            if (sVersion == "3") return ReplaceSettings3(sLine, psContextName, bAll, bStrict);
            if (iInReplaceSettings > 10) return sLine;
            iInReplaceSettings++;
            char cSep = '@';
            string sResponse = "";
            string[] aLine = sLine.Split(cSep);
            for (int i = 0; i < aLine.GetLength(0); i++)
            {
                sResponse += aLine[i];
                i++;
                if (i < aLine.GetLength(0))
                {
                    string sVar = aLine[i] + "..";
                    if (sVar.Contains(">") || sVar.Contains(" ") || sVar.Contains("=") || sVar.Contains("&") || (i == aLine.GetLength(0) - 1))
                    {
                        sResponse += cSep;
                        i--;
                    }
                    else
                    {
                        string[] aVar = ParseUtils.MySplit(sVar, '.');
                        string sVal = m_runtime.SVCache.GetSV(aVar[0], psContextName, "missingvalue");
                        if (!bStrict && ((sVal == "missingvalue") || (sVal == sLine))) sVal = m_runtime.SVCache.GetSV(aVar[0], "_", "missingvalue");
                        if (bAll && (sVal == "missingvalue")) sVal = "";
                        if (sVal != "missingvalue")
                        {
                            sVal = ReplaceSettings(sVal, psContextName, bAll, bStrict);
                            EncodeOption encOpt = Utils.GetEncodeOption(aVar[1]);
                            sVal = Utils.ApplyFormat(sVal, aVar[2]);
                            sVal = Utils.Encode(sVal, encOpt);
                            sResponse += sVal;
                        }
                        else
                        {
                            sResponse += cSep;
                            i--;
                        }
                    }
                }
            }
            iInReplaceSettings--;
            if ((aLine.GetLength(0) > 1) && sResponse != sLine) sResponse = ReplaceSettings(sResponse, psContextName, bAll, bStrict);
            return sResponse;
        }

        //private string Process_CFG(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_CfgClass, l_CfgName, l_CfgDefault, l_Encode;

        //    l_CfgClass = ReplaceParameters((string)obj.Context);
        //    l_CfgName = ReplaceParameters((string)obj.Name);
        //    l_CfgDefault = ReplaceParameters((string)obj.Value);
        //    l_Encode = ReplaceParameters((string)obj.Value2);

        //    sResult = m_runtime.AppCache.GetCfg(l_CfgClass + "__" + l_CfgName, l_CfgDefault);
        //    sResult = ReplaceParameters(sResult);
        //    sResult = Utils.Encode(sResult, l_Encode);
        //    if (sb != null) sb.Append(sResult);
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_DEFPAR(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_strParamName, l_strValue;
        //    l_strParamName = ReplaceParameters((string)obj.ParamName);
        //    l_strValue = ReplaceParameters((string)obj.Value);
        //    if (l_strParamName != "") DefaultParams[l_strParamName] = l_strValue;
        //    if (sb != null) sb.Append(sResult);
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_SPAR(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_strParamName, l_strValue;
        //    l_strParamName = ReplaceParameters((string)obj.ParamName);
        //    if ((string)obj.Opt2 == "1")
        //        l_strValue = ReplaceParameters((string)obj.Value, true);
        //    else
        //        l_strValue = ReplaceParameters((string)obj.Value);
        //    if ((string)obj.Opt1 == "1") l_strValue = GetParameterValue(l_strValue);
        //    if (l_strParamName != "") DefaultParams[l_strParamName] = l_strValue;
        //    if (l_strParamName == "")
        //        sResult = l_strValue;
        //    else
        //        TemplateParams[l_strParamName] = l_strValue;
        //    if (sb != null) sb.Append(sResult);
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_TEXT(JCmd obj, StringBuilder sb)
        //{
        //    //start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end

        //    //Regex regexMacro = new Regex(@"\#MACRO\..*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)\#", RegexOptions.IgnoreCase);
        //    //MatchCollection mc = regexMacro.Matches(s_text);
        //    //for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //    //{
        //    //    Match m = mc[matchIndex];
        //    //    string sResult = "";
        //    //    s_text = s_text.Replace(m.Value, sResult);
        //    //}

        //    string sResult = "";
        //    string sText = obj.Value;
        //    string sTextU = sText.ToUpper();
        //    int i1 = sTextU.IndexOf("#STARTBLOCK#");
        //    int j1 = sTextU.IndexOf("#ENDBLOCK#");
        //    if ((i1 >= 0) && (j1 >= 0) && (i1 < j1))
        //        sText = sText.Substring(i1 + "#STARTBLOCK#".Length, j1 - i1 - "#STARTBLOCK#".Length);
        //    i1 = sTextU.IndexOf("#STARTCOMMENT#");
        //    j1 = sTextU.IndexOf("#ENDCOMMENT#");
        //    if ((i1 >= 0) && (j1 >= 0) && (i1 < j1))
        //        sText = sText.Substring(0, i1) + sText.Substring(j1 + "#ENDCOMMENT#".Length);
        //    sResult = sText;
        //    if (sb != null) sb.Append(sResult);
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_IIF(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_condition, l_trueExpr, l_falseExpr;
        //    l_condition = ReplaceParameters((string)obj.Value);
        //    l_trueExpr = ReplaceParameters((string)obj.Value2);
        //    l_falseExpr = ReplaceParameters((string)obj.Value3);
        //    bool condResult = EvalCondition(l_condition, m_runtime.DataProvider);
        //    if (condResult == true)
        //        sResult = l_trueExpr;
        //    else
        //        sResult = l_falseExpr;
        //    if (sb != null) sb.Append(sResult);
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_IF(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    sbCond.Clear();
        //    foreach (JCmd command in obj.Params)
        //    {
        //        ProcessObject(command, sbCond);
        //    }
        //    string l_condition = sbCond.ToString();
        //    l_condition = ReplaceParameters(l_condition);
        //    bool condResult = EvalCondition(l_condition, m_runtime.DataProvider);
        //    if (condResult == true)
        //        sResult += ProcessCommands(obj, sb);
        //    else if (condResult == false && obj.ElseIf != null)
        //    {
        //        sResult += ProcessObject(obj.ElseIf, sb);
        //    }
        //    return "";//            return sResult;
        //}
        //private string Process_PAR(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_ParamName, l_Format, l_Encode;
        //    l_ParamName = ReplaceParameters((string)obj.ParamName);
        //    l_Format = ReplaceParameters((string)obj.Value3);
        //    l_Encode = ReplaceParameters((string)obj.Value2);

        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";
        //    try
        //    {
        //        sResult = GetParameterValue(l_ParamName);
        //        if (l_Format != "")
        //            sResult = Utils.ApplyFormat(sResult, l_Format);
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing PAR command";
        //        sErrorVerbose = "Error executing PAR command for parameter " + l_ParamName + " in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    sResult = Utils.Encode(sResult, l_Encode);
        //    if (sb != null) sb.Append(sResult);
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_JLOOP(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_PageSize, l_PageNr, l_ErrorParamName;
        //    l_ParamName = ReplaceParameters((string)obj.ParamName);
        //    l_Source = ReplaceParameters((string)obj.Name);
        //    l_JPath = ReplaceParameters((string)obj.Value);
        //    l_PageSize = ReplaceParameters((string)obj.Value2);
        //    l_PageNr = ReplaceParameters((string)obj.Value3);
        //    l_ErrorParamName = ReplaceParameters((string)obj.Context);
        //    int i_PageSize = 10;
        //    int i_PageNr = 1;
        //    int.TryParse(l_PageSize, out i_PageSize);
        //    int.TryParse(l_PageNr, out i_PageNr);
        //    int start = (i_PageNr - 1) * i_PageSize;
        //    int end = i_PageNr * i_PageSize;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string val;
        //    //System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();
        //    try
        //    {
        //        JArray ja;
        //        if (l_JPath == "")
        //            ja = JArray.Parse(l_Source);
        //        else
        //            ja = (JArray)(JObject.Parse(l_Source)).SelectToken(l_JPath);
        //        for (int i = start; i < end && i < ja.Count; i++)
        //        {
        //            JToken j = ja[i];
        //            val = j.ToString();
        //            TemplateParams[l_ParamName] = val;
        //            TemplateParams[l_ParamName + "__count"] = ja.Count.ToString();
        //            TemplateParams[l_ParamName + "__fetchID"] = (i - start).ToString();
        //            TemplateParams[l_ParamName + "__fetchID1"] = (i - start + 1).ToString();
        //            TemplateParams[l_ParamName + "__oddEven"] = (i - start) % 2 == 1 ? "1" : "0";
        //            TemplateParams[l_ParamName + "__firstRow"] = (i == start) ? "1" : "0";
        //            TemplateParams[l_ParamName + "__lastRow"] = ((i == end - 1) || (i == ja.Count - 1)) ? "1" : "0";
        //            foreach (JProperty jp in j.Children<JProperty>())
        //                if (jp.Value.Type == JTokenType.Date)
        //                {
        //                    DateTime d = (DateTime)jp.Value;
        //                    TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                }
        //                else
        //                    TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value;
        //            sResult += ProcessCommands(obj, sb);
        //            foreach (JProperty jp in j.Children<JProperty>())
        //                TemplateParams.Remove(l_ParamName + "_" + jp.Name);
        //            string sExitType = GetParameterValue("__exit_type", "none");
        //            if (sExitType != "none")
        //            {
        //                if (sExitType == "loop")
        //                {
        //                    TemplateParams.Remove("__exit_type");
        //                    sErrorVerbose = sError = GetParameterValue("__exit_value", "");
        //                    TemplateParams.Remove("__exit_value");
        //                }
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JLOOP command";
        //        sErrorVerbose = "Error executing JLOOP command for parameter " + l_ParamName + " in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //        //sbRetVal.Clear();
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_JPAR(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;

        //    l_ParamName = ReplaceParameters((string)obj.ParamName);
        //    l_Source = ReplaceParameters((string)obj.Name);
        //    l_JPath = ReplaceParameters((string)obj.Value);
        //    l_ErrorParamName = "ERROR";

        //    //if (obj.isParSource == "1") l_Source = (string)TemplateParams[l_Source];
        //    string val = "";
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    try
        //    {
        //        JObject o = JObject.Parse(l_Source);
        //        if (l_ParamName == "")
        //        {
        //            sResult = o.SelectToken(l_JPath).ToString();
        //            if (sb != null) sb.Append(sResult);
        //        }
        //        else
        //        {
        //            sResult = "";
        //            JToken jt = o.SelectToken(l_JPath);
        //            val = jt.ToString();
        //            if (jt is JArray)
        //                TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

        //            TemplateParams[l_ParamName] = val;
        //            foreach (JProperty jp in jt.Children<JProperty>())
        //            {
        //                if (jp.Value.Type == JTokenType.Date)
        //                {
        //                    DateTime d = (DateTime)jp.Value;
        //                    TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                }
        //                else
        //                    TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JPAR command";
        //        sErrorVerbose = "Error executing JPAR command for parameter " + l_ParamName + " in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_JKEYS(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
        //    l_ParamName = ReplaceParameters((string)obj.ParamName);
        //    l_Source = ReplaceParameters((string)obj.Name);
        //    l_JPath = ReplaceParameters((string)obj.Value);
        //    l_ErrorParamName = "ERROR";

        //    //if (obj.isParSource == "1") l_Source = (string)TemplateParams[l_Source];
        //    string val = "";
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    try
        //    {
        //        JObject o = JObject.Parse(l_Source);
        //        if (l_ParamName == "")
        //        {
        //            sResult = o.SelectToken(l_JPath).ToString();
        //            if (sb != null) sb.Append(sResult);
        //        }
        //        else
        //        {
        //            sResult = "";
        //            JToken jt = o.SelectToken(l_JPath);
        //            val = jt.ToString();
        //            if (jt is JArray)
        //                TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

        //            TemplateParams[l_ParamName] = val;
        //            foreach (JProperty jp in jt.Children<JProperty>())
        //            {
        //                TemplateParams[l_ParamName + "__key"] = jp.Name;
        //                if (jp.Value.Type == JTokenType.Date)
        //                {
        //                    DateTime d = (DateTime)jp.Value;
        //                    TemplateParams[l_ParamName + "__value"] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                }
        //                else
        //                    TemplateParams[l_ParamName + "__value"] = jp.Value;
        //                sResult += ProcessCommands(obj, sb);
        //                TemplateParams.Remove(l_ParamName + "__key");
        //                TemplateParams.Remove(l_ParamName + "__value");
        //                string sExitType = GetParameterValue("__exit_type", "none");
        //                if (sExitType != "none")
        //                {
        //                    if (sExitType == "loop")
        //                    {
        //                        TemplateParams.Remove("__exit_type");
        //                        sErrorVerbose = sError = GetParameterValue("__exit_value", "");
        //                        TemplateParams.Remove("__exit_value");
        //                    }
        //                    break;
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JKEYS command";
        //        sErrorVerbose = "Error executing JKEYS command for parameter " + l_ParamName + " in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_JDATA(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_ParamName, l_Select, l_Conn, l_ErrorParamName;
        //    l_ParamName = ReplaceParameters((string)obj.ParamName);
        //    l_Select = ReplaceParameters((string)obj.Name);
        //    l_Conn = ReplaceParameters((string)obj.Value);
        //    l_ErrorParamName = ReplaceParameters((string)obj.Context);

        //    string sError = "";
        //    string sErrorVerbose = "";
        //    try
        //    {
        //        if (l_Conn.StartsWith("SPLITXX"))
        //            sResult = JSON_SplitXX(l_Select, l_Conn[7], l_Conn[8], l_Conn.Substring(9));
        //        else
        //            sResult = m_runtime.GetResponse(l_Select, l_Conn);
        //        if (l_ParamName == "")
        //        {
        //            if (sb != null) sb.Append(sResult);
        //        }
        //        else
        //        {
        //            TemplateParams[l_ParamName] = sResult;
        //            sResult = "";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JDATA command";
        //        sErrorVerbose = "Error executing JDATA command for parameter " + l_ParamName + " in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //sbs += sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_FUNC(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_ErrorParamName = "ERROR";
        //    string strFormat = obj.Context;
        //    string l_ParamName = "";
        //    string l_Operation = obj.Name;
        //    string l_Value1 = "", l_Value2 = "", l_Value3 = "", l_Value4 = "", l_Value5 = "", l_Value6 = "";
        //    StringBuilder sJSON;
        //    double doubleVal, dVal;
        //    DateTime datetimeVal, datetimeVal2;
        //    TimeSpan timespanVal2;
        //    int i_Value = 0, iVal2 = 0, iVal3 = 0, iVal4 = 0, iVal5 = 0, iVal6 = 0;

        //    l_ParamName = ReplaceParameters((string)obj.ParamName);
        //    l_Value1 = ReplaceParameters((string)obj.Value);
        //    l_Value2 = ReplaceParameters((string)obj.Value2);
        //    l_Value3 = ReplaceParameters((string)obj.Value3);
        //    l_Value4 = ReplaceParameters((string)obj.ID);
        //    l_Value5 = ReplaceParameters((string)obj.Opt1);
        //    l_Value6 = ReplaceParameters((string)obj.Opt2);

        //    CultureInfo ci_in = CultureInfo.InvariantCulture;
        //    CultureInfo ci = CultureInfo.InvariantCulture;
        //    strFormat = Utils.ParseFormat(strFormat, out  ci_in, out ci);

        //    string sError = "";
        //    string sErrorVerbose = "";
        //    try
        //    {
        //        switch (l_Operation.ToLower())
        //        {
        //            case "guid":
        //                sResult = Guid.NewGuid().ToString();
        //                break;
        //            case "plus":
        //                if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
        //                doubleVal = doubleVal + dVal;
        //                if (l_Value3 != "")
        //                {
        //                    if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal + dVal;
        //                }
        //                if (l_Value4 != "")
        //                {
        //                    if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal + dVal;
        //                }
        //                if (l_Value5 != "")
        //                {
        //                    if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal + dVal;
        //                }
        //                if (l_Value6 != "")
        //                {
        //                    if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal + dVal;
        //                }
        //                sResult = doubleVal.ToString(strFormat, ci);
        //                break;
        //            case "minus":
        //                if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
        //                doubleVal = doubleVal - dVal;
        //                if (l_Value3 != "")
        //                {
        //                    if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal - dVal;
        //                }
        //                if (l_Value4 != "")
        //                {
        //                    if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal - dVal;
        //                }
        //                if (l_Value5 != "")
        //                {
        //                    if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal - dVal;
        //                }
        //                if (l_Value6 != "")
        //                {
        //                    if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal - dVal;
        //                }
        //                sResult = doubleVal.ToString(strFormat, ci);
        //                break;
        //            case "mult":
        //                if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
        //                doubleVal = doubleVal * dVal;
        //                if (l_Value3 != "")
        //                {
        //                    if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal * dVal;
        //                }
        //                if (l_Value4 != "")
        //                {
        //                    if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal * dVal;
        //                }
        //                if (l_Value5 != "")
        //                {
        //                    if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal * dVal;
        //                }
        //                if (l_Value6 != "")
        //                {
        //                    if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal * dVal;
        //                }
        //                sResult = doubleVal.ToString(strFormat, ci);
        //                break;
        //            case "div":
        //                if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
        //                doubleVal = doubleVal / dVal;
        //                if (l_Value3 != "")
        //                {
        //                    if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal / dVal;
        //                }
        //                if (l_Value4 != "")
        //                {
        //                    if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal / dVal;
        //                }
        //                if (l_Value5 != "")
        //                {
        //                    if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal / dVal;
        //                }
        //                if (l_Value6 != "")
        //                {
        //                    if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                        throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as double;");
        //                    doubleVal = doubleVal / dVal;
        //                }
        //                sResult = doubleVal.ToString(strFormat, ci);
        //                break;
        //            case "mod":
        //                if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
        //                doubleVal = doubleVal % dVal;
        //                sResult = doubleVal.ToString(strFormat, ci);
        //                break;
        //            case "string_concat":
        //                sResult = l_Value1 + l_Value2 + l_Value3 + l_Value4 + l_Value5 + l_Value6;
        //                break;
        //            case "string_length":
        //                doubleVal = l_Value1.Length;
        //                sResult = doubleVal.ToString(strFormat, ci);
        //                break;
        //            case "string_trim":
        //                l_Value1 = l_Value1.Trim();
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_trimend":
        //                l_Value1 = l_Value1.TrimEnd();
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_trimstart":
        //                l_Value1 = l_Value1.TrimStart();
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_toupper":
        //                l_Value1 = l_Value1.ToUpper();
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_tolower":
        //                l_Value1 = l_Value1.ToLower();
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_substring":
        //                if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                if (l_Value3 != "")
        //                {
        //                    if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
        //                        throw new Exception("Parameter Value2: " + l_Value3 + " cannot evaluate as int;");
        //                    l_Value1 = l_Value1.Substring(iVal2, iVal3);
        //                }
        //                else
        //                    l_Value1 = l_Value1.Substring(iVal2);
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_startswith":
        //                l_Value1 = (l_Value1.StartsWith(l_Value2)) ? "1" : "0";
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_endswith":
        //                l_Value1 = (l_Value1.EndsWith(l_Value2)) ? "1" : "0";
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_replace":
        //                l_Value1 = l_Value1.Replace(l_Value2, l_Value3);
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_contains":
        //                l_Value1 = (l_Value1.Contains(l_Value2)) ? "1" : "0";
        //                sResult = l_Value1;//.ToString(strFormat, ci);
        //                break;
        //            case "string_indexof":
        //                i_Value = l_Value1.IndexOf(l_Value2);
        //                if (l_Value3 != "")
        //                {
        //                    if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
        //                        throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as int;");
        //                    i_Value = l_Value1.IndexOf(l_Value2, iVal3);
        //                }
        //                if (l_Value4 != "")
        //                {
        //                    if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
        //                        throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as int;");
        //                    i_Value = l_Value1.IndexOf(l_Value2, iVal3, iVal4);
        //                }
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_day":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
        //                i_Value = datetimeVal.Day;
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_month":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                i_Value = datetimeVal.Month;
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_year":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                i_Value = datetimeVal.Year;
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_hour":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                i_Value = datetimeVal.Hour;
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_minute":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                i_Value = datetimeVal.Minute;
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_second":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                i_Value = datetimeVal.Second;
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_millisecond":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                i_Value = datetimeVal.Millisecond;
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_ticks":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                sResult = datetimeVal.Ticks.ToString(strFormat, ci);
        //                break;
        //            case "datetime_subtract":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!TimeSpan.TryParse(l_Value2, out timespanVal2))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as timespan;");
        //                datetimeVal = datetimeVal.Subtract(timespanVal2);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_add":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!TimeSpan.TryParse(l_Value2, out timespanVal2))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as timespan;");
        //                datetimeVal = datetimeVal.Add(timespanVal2);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_subtractduration":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                try
        //                {
        //                    timespanVal2 = System.Xml.XmlConvert.ToTimeSpan(l_Value2);
        //                }
        //                catch (Exception e)
        //                {
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as duration;");
        //                }
        //                datetimeVal = datetimeVal.Subtract(timespanVal2);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_addduration":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                try
        //                {
        //                    timespanVal2 = System.Xml.XmlConvert.ToTimeSpan(l_Value2);
        //                }
        //                catch (Exception e)
        //                {
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as duration;");
        //                }
        //                datetimeVal = datetimeVal.Add(timespanVal2);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_addmilliseconds":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                datetimeVal = datetimeVal.AddMilliseconds(dVal);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_addseconds":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                datetimeVal = datetimeVal.AddSeconds(dVal);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_addminutes":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                datetimeVal = datetimeVal.AddMinutes(dVal);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_addhours":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                datetimeVal = datetimeVal.AddHours(dVal);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_adddays":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                datetimeVal = datetimeVal.AddDays(dVal);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_addmonths":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                datetimeVal = datetimeVal.AddMonths(iVal2);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_addyears":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                datetimeVal = datetimeVal.AddYears(iVal2);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_addworkminutes":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
        //                    throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
        //                if (l_Value3 == "") l_Value3 = "0";
        //                if (l_Value4 == "") l_Value4 = "0";
        //                if (l_Value5 == "") l_Value5 = "0";
        //                if (l_Value6 == "") l_Value6 = "0";
        //                if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
        //                    throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as int;");
        //                if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
        //                    throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as int;");
        //                if (!int.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out iVal5))
        //                    throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as int;");
        //                if (!int.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out iVal6))
        //                    throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as int;");
        //                datetimeVal = Utils.DateTime_AddWorkMinutes(datetimeVal, iVal2, iVal3, iVal4, iVal5, iVal6);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "datetime_diffworkminutes":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!DateTime.TryParse(l_Value2, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal2))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (l_Value3 == "") l_Value3 = "0";
        //                if (l_Value4 == "") l_Value4 = "0";
        //                if (l_Value5 == "") l_Value5 = "0";
        //                if (l_Value6 == "") l_Value6 = "0";
        //                if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
        //                    throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as int;");
        //                if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
        //                    throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as int;");
        //                if (!int.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out iVal5))
        //                    throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as int;");
        //                if (!int.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out iVal6))
        //                    throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as int;");
        //                i_Value = Utils.DateTime_CalcWorkMinutes(datetimeVal, datetimeVal2, iVal3, iVal4, iVal5, iVal6);
        //                sResult = i_Value.ToString(strFormat, ci);
        //                break;
        //            case "datetime_diffdatetime":
        //                if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                if (!DateTime.TryParse(l_Value2, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal2))
        //                    throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
        //                timespanVal2 = datetimeVal.Subtract(datetimeVal2);
        //                datetimeVal = new DateTime(timespanVal2.Ticks);
        //                sResult = datetimeVal.ToString(strFormat, ci);
        //                break;
        //            case "json_path":
        //                sJSON = new StringBuilder();
        //                Utils.myPathParamsRender(sJSON, l_Value1);
        //                sResult = sJSON.ToString();
        //                sJSON.Clear();
        //                sJSON = null;
        //                break;
        //            case "json_query":
        //                sJSON = new StringBuilder();
        //                Utils.myQueryRender(sJSON, l_Value1);
        //                sResult = sJSON.ToString();
        //                sJSON.Clear();
        //                sJSON = null;
        //                break;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing FUNC " + l_Operation + " command";
        //        sErrorVerbose = "Error executing FUNC " + l_Operation + " command for parameter " + l_ParamName + " in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }


        //    if (l_ParamName == "")
        //    {
        //        if (sb != null) sb.Append(sResult);
        //    }
        //    else
        //    {
        //        TemplateParams[l_ParamName] = sResult;
        //        sResult = "";
        //    }
        //    //sbs+=retVal;
        //    return "";//            return sResult;

        //}
        //private string Process_FILE1(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    //string l_InType = "";
        //    //string l_InVal = "";
        //    //string l_OutType = "";
        //    //string l_OutVal = "";
        //    //string m_FileID = "";

        //    //l_InType = ReplaceParameters((string)obj.InType).ToUpper();
        //    //l_InVal = ReplaceParameters((string)obj.InVal);
        //    //l_OutType = ReplaceParameters((string)obj.OutType).ToUpper();
        //    //l_OutVal = ReplaceParameters((string)obj.OutVal);
        //    //l_OutVal = Utils.Encode(l_OutVal, EncodeOption.XDOCDecode);

        //    //string sError = "";
        //    //string sErrorVerbose = "";
        //    //string l_ErrorParamName = "ERROR";
        //    //try
        //    //{
        //    //    switch (l_InType)
        //    //    {
        //    //        case "UPLOAD":
        //    //            ReadUploadFile(l_InVal);
        //    //            break;
        //    //        case "NFS":
        //    //            ReadNFSFile(l_InVal);
        //    //            break;
        //    //        case "URI":
        //    //            ReadURIFile(l_InVal);
        //    //            break;
        //    //        case "HTTP":
        //    //            ReadHTTPFile(l_InVal);
        //    //            break;
        //    //        case "TEMP":
        //    //            ReadNFSFile(HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_InVal);
        //    //            //ReadURIFile(".\\Attachments\\" + l_InVal);
        //    //            break;
        //    //        case "DB":
        //    //            ReadDBFile(l_InVal);
        //    //            break;
        //    //        case "BLOB":
        //    //            ReadBlobFile(l_InVal);
        //    //            break;
        //    //        case "PAR":
        //    //            ReadPARFile(l_InVal);
        //    //            break;
        //    //        case "PAR64":
        //    //            ReadPAR64File(l_InVal);
        //    //            break;
        //    //        case "DEL":
        //    //            m_FileName = "";
        //    //            m_FileExtension = "";
        //    //            m_FileContent = null;
        //    //            break;
        //    //        default:
        //    //            throw new Exception ("Invalid source type");
        //    //            break;
        //    //    }
        //    //    if (sError == "")
        //    //    {
        //    //        SetPar("FileName", m_FileName);
        //    //        SetPar("FileExtension", m_FileExtension);
        //    //        SetPar("FileContentType", m_FileContentType);


        //    //        if (l_OutType == "") { l_OutType = "PAR"; l_OutVal = "File"; }
        //    //        switch (l_OutType)
        //    //        {
        //    //            case "RESPONSE":
        //    //                string sInline = "1";
        //    //                sInline = GetPar("FileResponseInline");
        //    //                if (sInline != "0") sInline = "1";
        //    //                WriteResponseFile(l_OutVal, sInline);
        //    //                break;
        //    //            case "NFS":
        //    //                WriteNFSFile(l_OutVal);
        //    //                break;
        //    //            case "URI":
        //    //                WriteURIFile(l_OutVal);
        //    //                break;
        //    //            case "HTTP":
        //    //                WriteHTTPFile(l_OutVal);
        //    //                break;
        //    //            case "TEMP":
        //    //                WriteNFSFile(HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_OutVal);
        //    //                SetPar("FilePath", HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_OutVal);
        //    //                break;
        //    //            case "DB":
        //    //                WriteDBFile(l_OutVal);
        //    //                break;
        //    //            case "BLOB":
        //    //                WriteBlobFile(l_OutVal);
        //    //                break;
        //    //            case "PAR":
        //    //                WritePARFile(l_OutVal);
        //    //                break;
        //    //            case "PAR64":
        //    //                WritePAR64File(l_OutVal);
        //    //                break;
        //    //            default:
        //    //                throw new Exception("Invalid target type");
        //    //                break;
        //    //        }
        //    //        SetPar("FileID", m_FileID);
        //    //        SetPar("FileError", "");
        //    //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    sError = "Error executing FILE command";
        //    //    sErrorVerbose = "Error executing FILE command in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    //}
        //    //if (l_ErrorParamName != "" && sError != "")
        //    //{
        //    //    TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //    //    TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    //}
        //    sResult = "[[" + "FILE" + "]]";
        //    if (sb != null) sb.Append(sResult);
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_SVSET(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_VarName, l_ContextName, l_ID, l_Val;

        //    l_VarName = ReplaceParameters((string)obj.Name);
        //    l_ContextName = ReplaceParameters((string)obj.Context);
        //    l_ID = ReplaceParameters((string)obj.ID);
        //    l_Val = ReplaceParameters((string)obj.Value);
        //    l_Val = Utils.Encode(l_Val, EncodeOption.XDOCDecode);
        //    if (l_ID == "ID")
        //        l_ID = GetParameterValue("ID", "");
        //    l_ContextName = l_ContextName + "_" + l_ID;

        //    if ((string)obj.Opt1 == "1")
        //    {
        //        string sPar = m_runtime.SVCache.GetSV(l_VarName, l_ContextName, "");
        //        if (sPar != "") return sResult;
        //    }
        //    m_runtime.SVCache.SetSV(l_VarName, l_ContextName, l_Val);
        //    return "";//            return sResult;
        //}
        //private string Process_SVGET(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_VarName, l_ContextName, l_ID, l_DefaultVal, l_ParamName, l_What, l_With;
        //    l_VarName = ReplaceParameters((string)obj.Name);
        //    l_ContextName = ReplaceParameters((string)obj.Context);
        //    l_ID = ReplaceParameters((string)obj.ID);
        //    l_DefaultVal = ReplaceParameters((string)obj.Value);
        //    l_ParamName = ReplaceParameters((string)obj.ParamName);
        //    l_What = ReplaceParameters((string)obj.Value2);
        //    l_With = ReplaceParameters((string)obj.Value3);
        //    if (l_ID == "ID")
        //        l_ID = GetParameterValue("ID", "");
        //    l_ContextName = l_ContextName + "_" + l_ID;

        //    bool bStrict = false;
        //    if ((string)obj.Opt2 == "1") bStrict = true;

        //    sResult = m_runtime.SVCache.GetSV(l_VarName, l_ContextName, l_DefaultVal);
        //    sResult = ReplaceSettings(sResult, l_ContextName, ((string)obj.Opt1 == "1"), bStrict);
        //    sResult = ReplaceParameters(sResult, ((string)obj.Opt1 == "1"));

        //    if (sResult == "missing_setting") sResult = l_DefaultVal;
        //    if (l_What != "") sResult = sResult.Replace(l_What, l_With);
        //    //if ((string)obj.ReplaceAll == "1")
        //    //{
        //    //    sVal = ReplaceSettings(sVal, "_", true,bStrict );
        //    //    sVal = ReplaceParameters(sVal, true);
        //    //}
        //    if (l_ParamName == "")
        //    {
        //        if (sb != null) sb.Append(sResult);
        //    }
        //    else
        //    {
        //        TemplateParams[l_ParamName] = sResult;
        //        sResult = "";
        //    }
        //    //sbs+=sVal;
        //    return "";//            return sResult;
        //}
        //private string Process_SVDEL(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_VarName, l_ContextName, l_ID, l_Val;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";

        //    l_VarName = ReplaceParameters((string)obj.Name);
        //    l_ContextName = ReplaceParameters((string)obj.Context);
        //    l_ID = ReplaceParameters((string)obj.ID);
        //    if (l_ID == "ID")
        //        l_ID = GetParameterValue("ID", "");
        //    l_ContextName = l_ContextName + "_" + l_ID;
        //    try
        //    {

        //        if ((string)obj.Opt1 == "1")
        //            m_runtime.SVCache.DelSV("", "*", true);
        //        else
        //        {
        //            if ((string)obj.Opt2 == "1") m_runtime.SVCache.DelSV(l_VarName, l_ContextName, true);
        //            else m_runtime.SVCache.DelSV(l_VarName, l_ContextName, false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing SVDEL command";
        //        sErrorVerbose = "Error executing SVDEL command in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //        throw new Exception(sErrorVerbose);
        //    }
        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return "";//            return sResult;
        //}
        //private string Process_MSG(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_Text, l_Level;

        //    l_Text = ReplaceParameters((string)obj.Value);
        //    l_Level = ReplaceParameters((string)obj.Name);

        //    m_runtime.Message(GetParameterValue("SESSIONID"), l_Level, l_Text);
        //    System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "MSG" + l_Level + "-" + l_Text);
        //    return "";//            return sResult;
        //}
        //private string Process_INCLUDEONCE(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Ignore;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";

        //    l_TemplateName = ReplaceParameters((string)obj.Name);
        //    l_Ignore = ReplaceParameters((string)obj.Opt1);
        //    if (!(m_runtime.SVCache.SetIncludeOnce(l_TemplateName))) return "";
        //    try
        //    {
        //        string sWarning = "";
        //        JCmd o = m_runtime.AppCache.RequestTemplateTree(l_TemplateName, ref sWarning);
        //        CProcess p = new CProcess(m_runtime, l_TemplateName);

        //        string sExit = "";
        //        sResult = p.Run(o, null, sbInclude, ref sExit);
        //        sErrorVerbose = sError = sExit;
        //        sResult = sbInclude.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing INCLUDEONCE command";
        //        sErrorVerbose = "Error executing INCLUDEONCE command for parameter " + l_TemplateName + " in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //        if (l_Ignore != "1") throw new Exception(sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }

        //    if (sb != null) sb.Append(sResult);
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_INCLUDE(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Ignore, l_NoParse, l_ParamName;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";

        //    //l_TemplateName = ReplaceParameters((string)obj.TemplateName);
        //    l_ParamName = ReplaceParameters((string)obj.ParamName);

        //    sbParams.Clear();
        //    foreach (JCmd command in obj.Params)
        //    {
        //        ProcessObject(command, sbParams);
        //    }
        //    string sParameters = sbParams.ToString();
        //    Hashtable hParameters = Utils.HashtableFromQueryString(sParameters);
        //    l_TemplateName = (string)hParameters["TemplateName"];
        //    l_NoParse = (string)hParameters["NoParse"];
        //    l_Ignore = (string)hParameters["Ignore"];

        //    try
        //    {
        //        if (l_NoParse == "1")
        //            sResult = m_runtime.AppCache.RequestTemplateText(l_TemplateName);
        //        else
        //        {
        //            string sWarning = "";
        //            JCmd o = m_runtime.AppCache.RequestTemplateTree(l_TemplateName, ref sWarning);
        //            sbInclude.Clear();
        //            CProcess p = new CProcess(m_runtime, l_TemplateName);
        //            string sExit = "";
        //            sResult = p.Run(o, hParameters, sbInclude, ref sExit);
        //            sErrorVerbose = sError = sExit;
        //            sResult = sbInclude.ToString();
        //        }
        //        if (l_ParamName == "")
        //        {
        //            if (sb != null) sb.Append(sResult);
        //        }
        //        else
        //        {
        //            TemplateParams[l_ParamName] = sResult;
        //            sResult = "";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing INCLUDE command";
        //        sErrorVerbose = "Error executing INCLUDE command for template " + l_TemplateName + " in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //        if (l_Ignore != "1") throw new Exception(sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //sbs+=sResult;
        //    return "";//            return sResult;
        //}
        //private string Process_EXIT(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_Text, l_Level;

        //    l_Text = ReplaceParameters((string)obj.Value);
        //    l_Level = ReplaceParameters((string)obj.Name);
        //    TemplateParams["__exit_type"] = l_Level;
        //    TemplateParams["__exit_value"] = l_Text;
        //    if (l_Level.ToLower() == "request")
        //    {
        //        m_runtime.ExitRequest = true;
        //        m_runtime.ExitValue = l_Text;
        //    }
        //    return "";//            return sResult;
        //}
        //private string Process_THROW(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_Text;

        //    l_Text = ReplaceParameters((string)obj.Value);
        //    TemplateParams["__exit_type"] = "request";
        //    TemplateParams["__exit_value"] = l_Text;
        //    m_runtime.ExitRequest = true;
        //    m_runtime.ExitValue = l_Text;
        //    return "";//            return sResult;
        //}
        //private string Process_IMPORT(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Conn; int l_ConnID;
        //    string l_SubTemplateName, l_Ignore, l_NoParse;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";


        //    sbParams.Clear();
        //    foreach (JCmd command in obj.Params)
        //    {
        //        ProcessObject(command, sbParams);
        //    }
        //    string sParameters = sbParams.ToString();
        //    Hashtable hParameters = Utils.HashtableFromQueryString(sParameters);
        //    l_SubTemplateName = (string)hParameters["TemplateName"];
        //    l_NoParse = (string)hParameters["NoParse"];
        //    l_Ignore = (string)hParameters["Ignore"];

        //    try
        //    {
        //        if (l_NoParse == "1")
        //            sResult = m_runtime.AppCache.RequestTemplateText(l_SubTemplateName);
        //        else
        //        {
        //            string sWarning = "";
        //            JCmd o = m_runtime.AppCache.RequestTemplateTree(l_SubTemplateName, ref sWarning);
        //            sbInclude.Clear();
        //            CProcess p = new CProcess(m_runtime, l_SubTemplateName);
        //            string sExit = "";
        //            sResult = p.Run(o, hParameters, sbInclude, ref sExit);
        //            sErrorVerbose = sError = sExit;
        //            sResult = sbInclude.ToString();
        //        }
        //        sResult = sResult.Replace("%d_export", "%d");
        //        sResult = sResult.Replace("%dash_export", "%dash");
        //        sResult = sResult.Replace("%macro_export", "%macro");
        //        sResult = sResult.Replace("%percent_export", "%percent");
        //        sResult = sResult.Replace("_percent_export", "_percent");
        //        l_Conn = ReplaceParameters((string)obj.Context);
        //        l_ConnID = int.Parse(obj.ID);
        //        if ((string)obj.Opt1 == "1")
        //            l_TemplateName = "ImportSet";
        //        else
        //            l_TemplateName = ReplaceParameters((string)obj.Name);

        //        if (l_ConnID == -99)
        //            sResult = m_runtime.Manager.ImportTemplate(l_Conn, l_TemplateName, sResult);
        //        else
        //            sResult = m_runtime.Manager.ImportTemplate(l_ConnID, l_TemplateName, sResult);
        //        if (sb != null) sb.Append(sResult);
        //        //sbs+=sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing IMPORT command";
        //        sErrorVerbose = "Error executing IMPORT command  in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //        if (l_Ignore != "1") throw new Exception(sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return "";//            return sResult;
        //}
        //private string Process_EXPORT(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Conn; int l_ConnID;
        //    string l_Encode;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";

        //    l_Conn = ReplaceParameters((string)obj.Context);
        //    l_ConnID = int.Parse(obj.ID);
        //    l_TemplateName = ReplaceParameters((string)obj.Name);
        //    l_Encode = ReplaceParameters((string)obj.Value2);

        //    try
        //    {
        //        if (l_ConnID == -99)
        //            sResult = m_runtime.Manager.ExportTemplate(l_Conn, l_TemplateName);
        //        else
        //            sResult = m_runtime.Manager.ExportTemplate(l_ConnID, l_TemplateName);
        //        sResult = Utils.Encode(sResult, l_Encode); ;
        //        sResult = sResult.Replace("%d", "%d_export");
        //        sResult = sResult.Replace("%dash", "%dash_export");
        //        sResult = sResult.Replace("%macro", "%macro_export");
        //        sResult = sResult.Replace("%percent", "%percent_export");
        //        sResult = sResult.Replace("_percent", "_percent_export");
        //        if (sb != null) sb.Append(sResult);
        //        //sbs+=sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing EXPORT command";
        //        sErrorVerbose = "Error executing EXPORT command  in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return "";//            return sResult;
        //}
        //private string Process_DELETE(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Conn; int l_ConnID;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";

        //    l_Conn = ReplaceParameters((string)obj.Context);
        //    l_ConnID = int.Parse(obj.ID);
        //    l_TemplateName = ReplaceParameters((string)obj.Name);

        //    try
        //    {
        //        if (l_ConnID == -99)
        //            sResult = m_runtime.Manager.RemoveTemplate(l_Conn, l_TemplateName);
        //        else
        //            sResult = m_runtime.Manager.RemoveTemplate(l_ConnID, l_TemplateName);
        //        if (sb != null) sb.Append(sResult);
        //        //sbs+=sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing DELETE command";
        //        sErrorVerbose = "Error executing DELETE command  in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return "";//            return sResult;
        //}
        //private string Process_TRANSFER(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_SourceTemplateName, l_SourceConn; int l_SourceConnID;
        //    string l_TargetTemplateName, l_TargetConn; int l_TargetConnID;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";
        //    l_SourceConn = ReplaceParameters((string)obj.Context);
        //    l_SourceConnID = int.Parse(obj.ID);
        //    l_SourceTemplateName = ReplaceParameters((string)obj.Name);
        //    l_TargetConn = ReplaceParameters((string)obj.Value2);
        //    l_TargetConnID = int.Parse(obj.Value3);
        //    l_TargetTemplateName = ReplaceParameters((string)obj.Value);

        //    try
        //    {
        //        if ((string)obj.Opt1 == "1")
        //        {
        //            string postfix = "";
        //            if ((l_SourceConn == l_TargetConn) && (l_SourceConnID == l_TargetConnID)) postfix = "_Copy";
        //            l_TargetTemplateName = "ImportSet";
        //            if (l_SourceConnID == -99)
        //                sResult = m_runtime.Manager.ExportTemplatesLike(l_SourceConn, l_SourceTemplateName, postfix);
        //            else
        //                sResult = m_runtime.Manager.ExportTemplatesLike(l_SourceConnID, l_SourceTemplateName, postfix);
        //        }
        //        else
        //        {
        //            if (l_SourceConnID == -99)
        //                sResult = m_runtime.Manager.ExportTemplate(l_SourceConn, l_SourceTemplateName);
        //            else
        //                sResult = m_runtime.Manager.ExportTemplate(l_SourceConnID, l_SourceTemplateName);
        //        }
        //        if (l_TargetConnID == -99)
        //            sResult = m_runtime.Manager.ImportTemplate(l_TargetConn, l_TargetTemplateName, sResult);
        //        else
        //            sResult = m_runtime.Manager.ImportTemplate(l_TargetConnID, l_TargetTemplateName, sResult);
        //        if (sb != null) sb.Append(sResult);
        //        //sbs+=sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing TRANSFER command";
        //        sErrorVerbose = "Error executing TRANSFER command  in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return "";//            return sResult;
        //}
        //private string Process_FILE(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_InType, l_InVal;
        //    string l_OutType, l_OutVal;
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string l_ErrorParamName = "ERROR";

        //    l_InType = ReplaceParameters((string)obj.Name);
        //    l_InVal = ReplaceParameters((string)obj.Value);
        //    l_OutType = ReplaceParameters((string)obj.Context);
        //    l_OutVal = ReplaceParameters((string)obj.ID);

        //    try
        //    {
        //        sResult = "[[FILE." + l_InType + l_InVal + l_OutType + l_OutVal + "]]";
        //        if (sb != null) sb.Append(sResult);
        //        //sbs+=sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing FILE command";
        //        sErrorVerbose = "Error executing FILE command  in template " + m_templateName + " at line + " + obj.Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        TemplateParams[l_ErrorParamName] = (TemplateParams[l_ErrorParamName] == null ? "" : TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        TemplateParams[l_ErrorParamName + "Verbose"] = (TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return "";//            return sResult;
        //}

        //private string ProcessCommands(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    foreach (JCmd command in obj.Commands)
        //    {
        //        sResult += ProcessObject(command, sb);
        //        string sExitType = GetParameterValue("__exit_type", "none");
        //        if (sExitType != "none")
        //        {
        //            break;
        //        }
        //        if (m_runtime.ExitRequest) break;

        //    }
        //    return "";//            return sResult;
        //}
        //private string ProcessObject(JCmd obj, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string sType = obj.Type;
        //    sType = sType.ToUpper();
        //    //System.Diagnostics.Debug.WriteLine( sType );
        //    switch (sType)
        //    {
        //        case "TEXT":
        //            sResult += Process_TEXT(obj, sb);
        //            break;
        //        case "DEFPAR":
        //            sResult += Process_DEFPAR(obj, sb);
        //            break;
        //        case "SPAR":
        //            sResult += Process_SPAR(obj, sb);
        //            break;
        //        case "PAR":
        //            sResult += Process_PAR(obj, sb);
        //            break;
        //        case "IIF":
        //            sResult += Process_IIF(obj, sb);
        //            break;
        //        case "IF":
        //            sResult += Process_IF(obj, sb);
        //            break;
        //        case "JLOOP":
        //            sResult += Process_JLOOP(obj, sb);
        //            break;
        //        case "JKEYS":
        //            sResult += Process_JKEYS(obj, sb);
        //            break;
        //        case "JPAR":
        //            sResult += Process_JPAR(obj, sb);
        //            break;
        //        case "JDATA":
        //            sResult += Process_JDATA(obj, sb);
        //            break;
        //        case "CFG":
        //            sResult += Process_CFG(obj, sb);
        //            break;
        //        case "FUNC":
        //            sResult += Process_FUNC(obj, sb);
        //            break;
        //        case "MSG":
        //            sResult += Process_MSG(obj, sb);
        //            break;
        //        case "SVSET":
        //            sResult += Process_SVSET(obj, sb);
        //            break;
        //        case "SVGET":
        //            sResult += Process_SVGET(obj, sb);
        //            break;
        //        case "SVDEL":
        //            sResult += Process_SVDEL(obj, sb);
        //            break;
        //        case "INCLUDE":
        //            sResult += Process_INCLUDE(obj, sb);
        //            break;
        //        case "INCLUDEONCE":
        //            sResult += Process_INCLUDEONCE(obj, sb);
        //            break;
        //        case "EXIT":
        //            sResult += Process_EXIT(obj, sb);
        //            break;
        //        case "THROW":
        //            sResult += Process_THROW(obj, sb);
        //            break;
        //        case "IMPORT":
        //            sResult += Process_IMPORT(obj, sb);
        //            break;
        //        case "EXPORT":
        //            sResult += Process_EXPORT(obj, sb);
        //            break;
        //        case "DELETE":
        //            sResult += Process_DELETE(obj, sb);
        //            break;
        //        case "TRANSFER":
        //            sResult += Process_TRANSFER(obj, sb);
        //            break;
        //        case "FILE":
        //            sResult += Process_FILE(obj, sb);
        //            break;
        //        case "USEPARAMETERS":
        //            m_UseGlobalParams = true;
        //            break;
        //        default:
        //            sResult = "[[" + sType + "]]";
        //            if (sb != null) sb.Append(sResult);
        //            //sbs+=sResult;
        //            break;
        //    }
        //    return "";//            return sResult;
        //}

        #region static
        //internal static bool RegExMatch(string sRegEx, string sValue, bool bIgnoreCase)
        //{
        //    Regex objPattern;
        //    if (bIgnoreCase)
        //        objPattern = new Regex(sRegEx, RegexOptions.IgnoreCase);
        //    else
        //        objPattern = new Regex(sRegEx);

        //    return objPattern.IsMatch(sValue);
        //}
        //internal static bool EvalCondition(string condition, XProviderData xDataProvider)
        //{
        //    bool condResult = false;

        //    if (condition == "")
        //        condResult = true;
        //    else if (condition.Contains("===="))
        //    {
        //        int i = condition.IndexOf("====");
        //        if (RegExMatch(condition.Substring(i + 4).Trim(), condition.Substring(0, i).Trim(), false))
        //            condResult = true;
        //    }
        //    else if (condition.Contains("!==="))
        //    {
        //        int i = condition.IndexOf("!===");
        //        if (!RegExMatch(condition.Substring(i + 4).Trim(), condition.Substring(0, i).Trim(), false))
        //            condResult = true;
        //    }
        //    else if (condition.Contains("==="))
        //    {
        //        int i = condition.IndexOf("===");
        //        if (RegExMatch(condition.Substring(i + 3).Trim(), condition.Substring(0, i).Trim(), true))
        //            condResult = true;
        //    }
        //    else if (condition.Contains("!=="))
        //    {
        //        int i = condition.IndexOf("!==");
        //        if (!RegExMatch(condition.Substring(i + 3).Trim(), condition.Substring(0, i).Trim(), true))
        //            condResult = true;
        //    }
        //    else if (condition.Contains("=="))
        //    {
        //        int i = condition.IndexOf("==");
        //        if (condition.Substring(i + 2).Trim() == condition.Substring(0, i).Trim())
        //            condResult = true;
        //    }
        //    else if (condition.Contains("!="))
        //    {
        //        int i = condition.IndexOf("!=");
        //        if (condition.Substring(i + 2).Trim() != condition.Substring(0, i).Trim())
        //            condResult = true;
        //    }
        //    else if (condition.Contains(">>>"))
        //    {
        //        int i = condition.IndexOf(">>>");
        //        double do1, do2;
        //        if (double.TryParse(condition.Substring(0, i).Trim(), out do1))
        //            if (double.TryParse(condition.Substring(i + 3).Trim(), out do2))
        //                if (do1 > do2)
        //                    condResult = true;
        //    }
        //    else if (condition.Contains(">>"))
        //    {
        //        int i = condition.IndexOf(">>");
        //        if (condition.Substring(0, i).Trim().CompareTo(condition.Substring(i + 2).Trim()) > 0)
        //            condResult = true;
        //    }
        //    else
        //    {
        //        DataTable dt = xDataProvider.GetDataTable("SELECT 1 WHERE " + condition);
        //        if (dt.Rows.Count > 0)
        //            condResult = true;
        //    }
        //    return condResult;
        //}
        //internal static string JSON_SplitXX(string sData, char cRowSep, char cColSep, string sHeader)
        //{
        //    string[] sRows;                     //array of rows
        //    string[] sCols;                     //array of rows
        //    string[] sColNames;                 //array of column names
        //    int iCols;                          //number of columns
        //    int iRows;                          //number of rows
        //    StringBuilder sb = new StringBuilder();

        //    sb.Append("[");

        //    if (cColSep == ' ') cColSep = '|';
        //    if (cRowSep == ' ') cRowSep = '|';

        //    sRows = Utils.MySplit(sData, cRowSep);
        //    sColNames = Utils.MySplit(sHeader, cColSep);
        //    iCols = sColNames.GetLength(0);
        //    iRows = sRows.GetLength(0);

        //    for (int i = 0; i < iRows; i++)
        //    {
        //        if (i > 0) sb.Append(",");
        //        sb.Append("{");
        //        sCols = Utils.MySplit(sRows[i], cColSep);
        //        for (int j = 0; (j < iCols) && (j < sCols.Length); j++)
        //        {
        //            if (j > 0) sb.Append(",");
        //            sb.Append("\""); sb.Append(sColNames[j]); sb.Append("\":\""); sb.Append(sCols[j]); sb.Append("\"");
        //        }
        //        sb.Append("}");
        //    }
        //    sb.Append("]");

        //    return sb.ToString();
        //}
        #endregion
        private  string ParseFormat(string strFormat, out CultureInfo ci_in, out CultureInfo ci)
        {
            ci_in = CultureInfo.InvariantCulture;
            ci = CultureInfo.InvariantCulture;
            if (strFormat != "")
            {
                strFormat = strFormat.Replace("%dash%", "#");
                if (strFormat.Contains("|"))
                {
                    string strFormat1 = strFormat.Substring(strFormat.IndexOf("|") + 1);
                    if (strFormat1 == "NL") strFormat1 = "nl-NL";
                    if (strFormat1 == "US") strFormat1 = "en-US";
                    try { ci_in = CultureInfo.CreateSpecificCulture(strFormat1); }
                    catch (Exception ) { };
                    strFormat = strFormat.Substring(0, strFormat.IndexOf("|"));
                }
                if (strFormat.StartsWith("NL"))
                {
                    strFormat = strFormat.Substring(2);
                    ci = CultureInfo.CreateSpecificCulture("nl-NL");
                }
                if (strFormat.StartsWith("US"))
                {
                    strFormat = strFormat.Substring(2);
                    ci = CultureInfo.CreateSpecificCulture("en-US");
                }
            }
            return strFormat;
        }
        private  string ApplyFormat(string oVal, string strFormat)
        {
            string retVal = oVal;
            CultureInfo ci_in = CultureInfo.InvariantCulture;
            CultureInfo ci = CultureInfo.InvariantCulture;
            bool b_isU = false;

            if (strFormat != "")
            {
                strFormat = ParseFormat(strFormat, out ci_in, out ci);
                if (strFormat.StartsWith("U"))
                {
                    b_isU = true;
                    strFormat = strFormat.Substring(1);
                }
                if (strFormat.StartsWith(">")) { retVal = oVal.ToString().ToUpper(); }
                else if (strFormat.StartsWith("<")) { retVal = oVal.ToString().ToLower(); }
                else
                {
                    double dVal;
                    DateTime dtVal;
                    if (double.TryParse(oVal.ToString(), System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        retVal = dVal.ToString(strFormat, ci);
                    else if (DateTime.TryParse(oVal.ToString(), ci_in, System.Globalization.DateTimeStyles.None, out dtVal))
                        retVal = dtVal.ToString(strFormat, ci);
                    else
                        retVal = oVal.ToString();
                }
                if (b_isU)
                {
                    retVal = retVal.Replace(',', '.');
                }
            }
            return retVal;
        }
        private  string Encode(string s, EncodeOption encOption)
        {
            string retVal = s;

            switch (encOption)
            {
                case EncodeOption.Encode:
                    retVal = XDocHtmlEncode(XDocUrlEncode(retVal));
                    //retVal = HttpUtility.HtmlEncode(XDocUrlEncode(retVal));
                    break;
                case EncodeOption.Decode:
                    retVal = XDocUrlDecode(XDocHtmlDecode(retVal));
                    //retVal = XDocUrlDecode(HttpUtility.HtmlDecode(retVal));
                    break;
                case EncodeOption.HTMLDecode:
                    retVal = XDocHtmlDecode(retVal);
                    //retVal = HttpUtility.HtmlDecode(retVal);
                    break;
                case EncodeOption.HTMLEncode:
                    retVal = XDocHtmlEncode(retVal);
                    //retVal = HttpUtility.HtmlEncode(retVal);
                    break;
                case EncodeOption.URLDecode:
                    retVal = XDocUrlDecode(retVal);
                    break;
                case EncodeOption.URLEncode:
                    retVal = XDocUrlEncode(retVal);
                    break;
                case EncodeOption.EndCDATA:
                    retVal = retVal.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
                    retVal = retVal.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
                    retVal = retVal.Replace("]]>", Utils.CT_ENDCDATA);
                    break;
                case EncodeOption.SQLEscape:
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.PAREscape:
                    retVal = retVal.Replace(".", "\\.");
                    retVal = retVal.Replace("#", "%23");
                    retVal = retVal.Replace("(", "%28");
                    retVal = retVal.Replace(")", "%29");
                    break;
                case EncodeOption.MACROEscape:
                    retVal = retVal.Replace(",", "\\,");
                    retVal = retVal.Replace(")", "\\)");
                    break;
                case EncodeOption.XDOCEncode:
                    retVal = retVal.Replace("#", "%23");
                    retVal = retVal.Replace("(", "%28");
                    retVal = retVal.Replace(")", "%29");
                    break;
                case EncodeOption.XDOCDecode:
                    retVal = retVal.Replace("%23", "#");
                    retVal = retVal.Replace("%28", "(");
                    retVal = retVal.Replace("%29", ")");
                    break;
                case EncodeOption.EncodeSQLEscape:
                    retVal = XDocHtmlEncode(XDocUrlEncode(retVal));
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlEncode(XDocUrlEncode(retVal));
                    break;
                case EncodeOption.DecodeSQLEscape:
                    retVal = XDocUrlDecode(XDocHtmlDecode(retVal));
                    retVal = retVal.Replace("'", "''");
                    //retVal = XDocUrlDecode(HttpUtility.HtmlDecode(retVal));
                    break;
                case EncodeOption.HTMLDecodeSQLEscape:
                    retVal = XDocHtmlDecode(retVal);
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlDecode(retVal);
                    break;
                case EncodeOption.HTMLEncodeSQLEscape:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlEncode(retVal);
                    break;
                case EncodeOption.URLDecodeSQLEscape:
                    retVal = XDocUrlDecode(retVal);
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.URLEncodeSQLEscape:
                    retVal = XDocUrlEncode(retVal);
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.JSEscape:
                    retVal = retVal.Replace("\\", "\\\\");
                    retVal = retVal.Replace("'", "\\'");
                    retVal = retVal.Replace("\"", "\\\"");
                    retVal = retVal.Replace("`", "\\`");
                    retVal = retVal.Replace("\r", "\\r");
                    retVal = retVal.Replace("\n", "\\n");
                    break;
                case EncodeOption.HTMLEncodeJSEscape:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("\\", "\\\\");
                    retVal = retVal.Replace("'", "\\'");
                    retVal = retVal.Replace("\"", "\\\"");
                    retVal = retVal.Replace("`", "\\`");
                    break;
                case EncodeOption.Text2HTML:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("&#13;&#10;", "<br>");
                    break;
                case EncodeOption.Base64Encode8:
                    byte[] binaryData8 = Encoding.UTF8.GetBytes(retVal);
                    retVal = Convert.ToBase64String(binaryData8, 0, binaryData8.Length);
                    break;
                case EncodeOption.Base64Encode:
                    byte[] binaryData = Encoding.Default.GetBytes(retVal);
                    retVal = Convert.ToBase64String(binaryData, 0, binaryData.Length);
                    break;
                case EncodeOption.Base64Decode:
                    byte[] binaryDataD = Convert.FromBase64String(retVal);
                    //retVal = Encoding.UTF8.GetString(binaryDataD, 0, binaryDataD.Length);
                    retVal = System.Text.Encoding.Default.GetString(binaryDataD);
                    break;
                case EncodeOption.JSONEscape:
                    retVal = Utils._JsonEscape(retVal);
                    break;
                case EncodeOption.None:
                    break;
                default:
                    break;
            }

            return retVal;
        }

        private  string Encode(string s, string encOption)
        {
            return Encode(s, GetEncodeOption(encOption));
        }
        private  EncodeOption GetEncodeOption(string s)
        {
            EncodeOption encOpt = EncodeOption.None;
            foreach (string enumName in Enum.GetNames(typeof(EncodeOption)))
            {
                if (s.ToUpper().Trim() == enumName.ToUpper())
                {
                    encOpt = (EncodeOption)Enum.Parse(typeof(EncodeOption), enumName, false);
                    break;
                }
            }
            return encOpt;
        }
        private  string XDocHtmlDecode(string retVal)
        {
            retVal = HttpUtility.HtmlDecode(retVal);
            return retVal;
        }
        private  string XDocHtmlEncode(string retVal)
        {
            retVal = HttpUtility.HtmlEncode(retVal);
            retVal = retVal.Replace("\r\n", "&#13;&#10;");
            retVal = retVal.Replace("\r", "&#13;&#10;");
            retVal = retVal.Replace("\n", "&#13;&#10;");
            return retVal;
        }

        private  string XDocUrlDecode(string retVal)
        {
            string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
            if (sEncoding == null) sEncoding = "";
            if (sEncoding == "") sEncoding = "utf-8";
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
            if (enc != null)
                retVal = HttpUtility.UrlDecode(retVal, enc);
            else
                retVal = HttpUtility.UrlDecode(retVal, System.Text.Encoding.Default);

            return retVal;
        }
        private  string XDocUrlEncode(string retVal)
        {
            string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
            if (sEncoding == null) sEncoding = "";
            if (sEncoding == "") sEncoding = "utf-8";
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
            if (enc != null)
                retVal = HttpUtility.UrlEncode(retVal, enc);
            else
                retVal = HttpUtility.UrlEncode(retVal, System.Text.Encoding.Default);
            retVal = retVal.Replace("+", "%20");
            retVal = retVal.Replace("!", "%21");
            retVal = retVal.Replace("(", "%28");
            retVal = retVal.Replace(")", "%29");
            retVal = retVal.Replace("'", "%27");

            return retVal;
        }

    }

}
