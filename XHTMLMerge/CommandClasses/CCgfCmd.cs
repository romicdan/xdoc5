//using KubionLogNamespace;
using System;
using System.Globalization;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CCgfCmd : CCmd
    {
        string m_CfgClass = "";
        string m_CfgName = "";
        string m_CfgDefault = "";
        string m_ParamName = "";
        EncodeOption m_encodeOption = EncodeOption.None;
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"CFG\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_CfgClass)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_CfgName)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_CfgDefault)); sJSON.Append("\"");
            sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_encodeOption.ToString())); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParamName)); sJSON.Append("\"");
            sJSON.Append("}");
        }
        public string CfgClass
        {
            get { return m_CfgClass; }
            set { m_CfgClass = value; }
        }
        public string CfgName
        {
            get { return m_CfgName; }
            set { m_CfgName = value; }
        }
        public string CfgDefault
        {
            get { return m_CfgDefault; }
            set { m_CfgDefault = value; }
        }
        public string ParamName
        {
            get { return m_ParamName; }
            set { m_ParamName = value; }
        }

        public EncodeOption EncodeOption
        {
            get { return m_encodeOption; }
            set { m_encodeOption = value; }
        }

        public CCgfCmd()
            : base()
        {
            m_enType = CommandType.CFGCommand ;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_CfgClass, l_CfgName, l_CfgDefault;

        //    l_CfgClass = m_parser.ReplaceParameters(m_CfgClass);
        //    l_CfgName = m_parser.ReplaceParameters(m_CfgName);
        //    l_CfgDefault = m_parser.ReplaceParameters(m_CfgDefault);

        //    string sResult = m_parser.GetCfg(l_CfgClass + "__" + l_CfgName, l_CfgDefault);
        //    sResult = m_parser.ReplaceParameters(sResult);
        //    sResult = Utils.Encode(sResult, m_encodeOption);

        //    return sResult;
        //}

    }
}


