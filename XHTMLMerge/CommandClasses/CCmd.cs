//using KubionLogNamespace;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
//using XDataSourceModule;


namespace XHTMLMerge
{
    [Serializable]
    public class CCmd
	{
		#region Protected members

        protected int m_cmdID;
        protected int 
            m_nLine = -1,
			m_nStartIndex = -1,
			m_nEndIndex = -1;
		protected bool m_bIsBlockCommand;
		protected CommandType m_enType;
		protected CmdCollection m_vChildCmds;
        [NonSerialized]
        protected CParser orig_m_parser = null;
        protected string newLine = Environment.NewLine;

		#endregion Protected members

		#region Public properties

        public int cmdID
        {
            get { return m_cmdID; }
            set { m_cmdID = value; }
        }

        public int Line
        {
            get { return m_nLine; }
            set { m_nLine = value; }
        }
        public int StartIndex
        {
            get { return m_nStartIndex; }
            set { m_nStartIndex = value; }
        }

		public int EndIndex
		{
			get	{ return m_nEndIndex; }
			set	{ m_nEndIndex = value; }
		}

		public CommandType Type
		{
			get { return m_enType; }
			set {
				m_enType = value; 
			}
		}

        [System.Xml.Serialization.XmlIgnore]
        public CParser Parser
		{
			get { return orig_m_parser; }
			set { orig_m_parser = value; }
		}


		public CmdCollection ChildCommands
		{
			get { return m_vChildCmds; }
			set { m_vChildCmds = value; }
		}

		public bool IsBlockComand
		{
			get { return m_bIsBlockCommand; }
		}

		#endregion Public properties

        public CCmd(): this(-1)
		{
		}

        public CCmd(int i)
        {
            m_cmdID = i;
            m_vChildCmds = new CmdCollection();
        }


        public virtual string FakeExecute(CParser m_parser)
        {
            if (m_vChildCmds.Count > 0)
            {
                //System.Diagnostics.Debug.WriteLine(this.m_enType);
                //string retVal = "";
                System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();
                foreach (CCmd cmd in m_vChildCmds)
                {
                    //System.Diagnostics.Debug.WriteLine(cmd.Type );
                    //retVal += cmd.Execute(m_parser);
                    sbRetVal.Append(cmd.FakeExecute(m_parser));
//                    sbRetVal.Append(m_vChildCmds.Count.ToString ());
                }
                //return retVal;
//                return m_enType.ToString() + sbRetVal.ToString();
                return  sbRetVal.ToString();
            }
            else
//            else return m_enType.ToString() + "";
             return  "";
        }
        public virtual string Execute(CParser m_parser)
        {
            if (m_vChildCmds.Count > 0)
            {
                //System.Diagnostics.Debug.WriteLine(this.m_enType);
                //string retVal = "";
                System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();
                foreach (CCmd cmd in m_vChildCmds)
                {
                    //System.Diagnostics.Debug.WriteLine(cmd.Type );
                    //retVal += cmd.Execute(m_parser);
                    sbRetVal.Append(cmd.Execute(m_parser));
                }
                //return retVal;
                return sbRetVal.ToString();
            }
            else return "";
        }
        public virtual void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\""); sJSON.Append(Utils._JsonEscape(m_enType.ToString())); sJSON.Append("\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            GetJsonChilds(sJSON, sbWarning);
            sJSON.Append("}");
        }
        public virtual void GetJsonChilds(StringBuilder sJSON, StringBuilder sbWarning)
        {
            //int i = 0;
            //int ii = m_vChildCmds.Count;
            if (m_vChildCmds.Count > 0)
            {
                sJSON.Append(",\"Commands\":[");
                bool isFirst = true;
                foreach (CCmd cmd in m_vChildCmds)
                {
                    if (isFirst) isFirst = false; else sJSON.Append(",");
                    cmd.GetJson(sJSON, sbWarning);
                    //System.Diagnostics.Debug.WriteLine("" + i++ + "/" + ii + sJSON.ToString());
                }
                sJSON.Append("]");
            }
        }

        internal static bool RegExMatch(string sRegEx, string sValue, bool bIgnoreCase)
        {
            Regex objPattern;
            if (bIgnoreCase)
                objPattern = new Regex(sRegEx, RegexOptions.IgnoreCase);
            else
                objPattern = new Regex(sRegEx);

            return objPattern.IsMatch(sValue);
        }
        internal bool EvalCondition4E(string op1, string op2)
        {
            return RegExMatch(op2, op1, false);
        }
        internal bool EvalConditionN3E(string op1, string op2)
        {
            return !RegExMatch(op2, op1, false);
        }
        internal bool EvalCondition3E(string op1, string op2)
        {
            return RegExMatch(op2, op1, true);
        }
        internal bool EvalConditionN2E(string op1, string op2)
        {
            return !RegExMatch(op2, op1, true);
        }
        internal bool EvalCondition2E(string op1, string op2)
        {
            return (op2 == op1);
        }
        internal bool EvalConditionN1E(string op1, string op2)
        {
            return (op2 != op1);
        }
        internal bool EvalCondition3G(string op1, string op2)
        {
            double do1, do2;
            return (double.TryParse(op1, out do1)) && (double.TryParse(op2, out do2)) && (do1 > do2);
        }
        internal bool EvalCondition2G(string op1, string op2)
        {
            return (op1.CompareTo(op2) > 0);
        }
        internal bool EvalCondition3L(string op1, string op2)
        {
            double do1, do2;
            return (double.TryParse(op1, out do1)) && (double.TryParse(op2, out do2)) && (do1 < do2);
        }
        internal bool EvalCondition2L(string op1, string op2)
        {
            return (op1.CompareTo(op2) < 0);
        }
        internal bool EvalConditionTrue(string op1, string op2)
        {
            return (true);
        }
        //internal bool EvalConditionSQL(string op1, XProviderData xDataProvider)
        //{
        //    DataTable dt = xDataProvider.GetDataTable("SELECT 1 WHERE " + op1);
        //    return (dt.Rows.Count > 0);
        //}


	}
}
