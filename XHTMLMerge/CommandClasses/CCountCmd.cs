using System;
using System.Diagnostics;

namespace XHTMLMerge
{
    [Serializable]
    public class CCountCmd : CCmd
	{
        # region Protected members

        protected int m_TemplateID = -1;
        protected string m_strQueryName = "";
		protected CCmd[] m_Parameters = null;
        //protected CContext m_context = null;
		//protected XDataSourceModule.IXDataSource m_evaluator = null;
        private CCmd m_pageSizeCmd = null;

		#endregion Protected members

		#region Public properties

        public int TemplateID
        {
            get { return m_TemplateID; }
            set { m_TemplateID = value; }
        }

        public CCmd PageSizeCmd
        {
            get { return m_pageSizeCmd; }
            set { m_pageSizeCmd = value; }
        }


		public string QueryName
		{
			get { return m_strQueryName; }
			set { m_strQueryName = value; }
		}

		public CCmd[] Parameters
		{
			get { return m_Parameters; }
			set { m_Parameters = value; }
		}

        //public CContext Context
        //{
        //    get { return m_context; }
        //    set { m_context = value; }
        //}

        //public XDataSourceModule.IXDataSource Evaluator
        //{
        //    get { return m_evaluator; }
        //    set { m_evaluator = value; }
        //}


		#endregion Public properties

        public CCountCmd()
            : base()
		{
			this.m_enType = CommandType.CountCommand;		
			m_bIsBlockCommand = false;
		}



  //      public override string Execute(CParser m_parser)
  //      {
  //          string sError = "";
  //          string sErrorVerbose = "";
  //          string sParameters = "";
  //          double dDuration = 0;
  //          string retVal = "";
  //          int count=-1;
  //          string key;

  //          Array queryParameters = GetParamArray(m_parser);
  //          try
  //          {

  //              //m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_evaluator_RequestHierarchy);
  //              m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy);

  //              count = m_parser.Evaluator.EvaluateSource(m_TemplateID, m_strQueryName, queryParameters, out sError, out sErrorVerbose, out sParameters, out dDuration, out key);
  //              m_parser.Context.Keys[this] = key;

  //              if (count == -1)
  //              {
  //                  return "-1";
  //                  //throw (new Exception(error));
  //              }
                
  //              int pageSize = -1;

  //              if (m_pageSizeCmd != null)
  //              {
  //                  string strPgSize = m_pageSizeCmd.Execute(m_parser);
  //                  if (!int.TryParse(strPgSize, out pageSize))
  //                      pageSize = -1;
  //              }

  //              retVal = count.ToString();
  //              if (pageSize != -1)
  //              {
  //                  int ret = count / pageSize;
  //                  if (count % pageSize != 0)
  //                      ret++;
  //                  retVal = ret.ToString();
  //              }

  //          }
  //          catch (Exception ex)
  //          {
  //              Trace.WriteLine(ex);
  //              sError = ex.Message;
  //              sErrorVerbose = sError;
  //          }
  //          string sErrorParameter = GetErrorParameter(queryParameters);
  //          string l_ErrorParName = m_parser.ReplaceParameters(sErrorParameter);
  //          if (sError != "")
  //          {
  //              retVal = "";
  //              //sErrorVerbose = "Error executing COUNT command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + sParameters + "; " + newLine + sErrorVerbose;
  //              sErrorVerbose = "Error executing COUNT command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
  //              m_parser.Message("QueryError", sErrorVerbose);
  //          }
  //          else
  //          {
  //              sErrorVerbose = "";
  //          }
  //          // set par 
  //          if (l_ErrorParName != "")
  //          {
  //              m_parser.ParamDefaults[l_ErrorParName] = sError;
  //              m_parser.TemplateParams[l_ErrorParName] = sError;
  //              m_parser.ParamDefaults[l_ErrorParName + "Verbose"] = sErrorVerbose;
  //              m_parser.TemplateParams[l_ErrorParName + "Verbose"] = sErrorVerbose;
  //          }
            
  //          if (m_parser.ParserQueryLog == "1") m_parser.QueryLog(m_strQueryName, sParameters, count, sError, sErrorVerbose,dDuration);

  //          return retVal;

		//}

        private string GetErrorParameter(Array m_parameters)
        {
            string sError = "Error";

            for (int index = 0; index < m_parameters.GetLength(0); index++)
            {
                if (m_parameters.GetValue(index) != null)
                {
                    string sValue = m_parameters.GetValue(index).ToString();
                    if (sValue.Contains("="))
                    {
                        string sParName = sValue.Substring(0, sValue.IndexOf("="));
                        string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
                        if (sParName.ToUpper() == "ERROR") sError = sValue1;
                    }
                }
            }
            return sError;
        }
        public Array GetParamArray(CParser m_parser, bool bDefQry)
        {
            Array arrParams = Array.CreateInstance(typeof(string), m_Parameters.GetLength(0) + 1);
            for (int i = 0; i < m_Parameters.GetLength(0); i++)
            {
                CCmd cmd = m_Parameters[i];
                string strValue = cmd.Execute(m_parser);
                if (strValue.ToUpper().Contains("#PAR."))
                    strValue = m_parser.Parameters2Values(strValue);
                arrParams.SetValue(strValue, i);
            }
            if (bDefQry)
                arrParams.SetValue("DEFQRY=1", m_Parameters.GetLength(0));
            else
                arrParams.SetValue("DEFQRY=0", m_Parameters.GetLength(0));
            return arrParams;
        }
 
        public Array GetParamArray(CParser m_parser)
		{
			Array arrParams = Array.CreateInstance(typeof(string), m_Parameters.GetLength(0));
			for(int i = 0; i < m_Parameters.GetLength(0); i++)
			{
				CCmd cmd = m_Parameters[i];
                string strValue = cmd.Execute(m_parser);
				arrParams.SetValue(strValue, i);
			}
			return arrParams;
		}

        //void m_evaluator_RequestHierarchy(XDataSourceModule.SourceResult sourceResult, string parameters)
        //{
        //    l_parser.RaiseRequestHierarchy(sourceResult, parameters);
        //}

	}
}