//using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CDELCmd : CCmd
    {
        bool m_DeleteAll = false;
        bool m_DeleteFrom = false;
        string m_VarName = "";
        string m_ContextName = "";
        string m_ID = "ID";
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"SVDEL\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_VarName)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_ContextName)); sJSON.Append("\"");
            sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_ID)); sJSON.Append("\"");
            sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_DeleteAll)); sJSON.Append("\"");
            sJSON.Append(",\"Opt2\":\""); sJSON.Append(Utils._JsonEscape(m_DeleteFrom)); sJSON.Append("\"");
            sJSON.Append("}");
        }
        

        public bool DeleteAll
        {
            get { return m_DeleteAll; }
            set { m_DeleteAll = value; }
        }
        public bool DeleteFrom
        {
            get { return m_DeleteFrom; }
            set { m_DeleteFrom = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public CDELCmd()
            : base()
        {
            m_enType = CommandType.DELCommand ;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_VarName, l_ContextName, l_ID;

        //    if (m_DeleteAll)
        //        m_parser.DelAll();
        //    else
        //    {
        //        l_VarName = m_parser.ReplaceParameters(m_VarName);
        //        l_ContextName = m_parser.ReplaceParameters(m_ContextName);
        //        l_ID = m_parser.ReplaceParameters(m_ID);

        //        if (m_DeleteFrom) m_parser.DelFromSV(l_VarName, l_ContextName, l_ID);
        //        else m_parser.DelSV(l_VarName, l_ContextName, l_ID);
        //    }
        //    return "";
        //}

    }
}

