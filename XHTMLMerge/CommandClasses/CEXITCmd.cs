﻿//using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CEXITCmd : CCmd
    {
        protected string m_strMsgLevel = "";
        protected string m_strMsgText = "";

        public string MsgLevel
        {
            get { return m_strMsgLevel; }
            set { m_strMsgLevel = value; }
        }
        public string MsgText
        {
            get { return m_strMsgText; }
            set { m_strMsgText = value; }
        }
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"EXIT\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_strMsgLevel)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_strMsgText)); sJSON.Append("\"");
            sJSON.Append("}");
        }

        public CEXITCmd()
            : base()
        {
            m_enType = CommandType.EXITCommand ;
            this.m_bIsBlockCommand = false;
        }

        //public override string Execute(CParser m_parser)
        //{
        //    string l_strMsgText, l_strMsgLevel;
        //    l_strMsgLevel = m_parser.ReplaceParameters(m_strMsgLevel);
        //    l_strMsgText = m_parser.ReplaceParameters(m_strMsgText);

        //    //m_parser.Message(l_strMsgLevel, l_strMsgText);
        //    //System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "EXIT" + l_strMsgLevel + "-" + l_strMsgText);

        //    return "";
        //}


    }
}




