using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Reflection;
using System.Web;
using System.Globalization;
//using XDataSourceModule;
//using KubionLogNamespace;


namespace XHTMLMerge
{
    [Serializable]
    public class CFLDCmd : CCmd
	{

		# region Protected members

		protected string 
			m_strQueryName = "",
			m_strFieldName = "",
            m_strFormat = "";

        //protected CContext m_context = null;
		//protected XDataSourceModule.IXDataSource m_evaluator = null;
		protected Array m_parameters = null;
		protected CCmd m_parent = null;
		protected bool m_binaryField = false;
        protected bool m_decodeHTML = false;
        protected bool m_encodeHTML = false;
        protected bool m_isFetchID = false;
        protected bool m_isFetchID1 = false;
        protected bool m_oddEven = false;
        protected bool m_isCount = false;
        protected bool m_isFirstRow = false;
        protected bool m_isLastRow = false;


        protected EncodeOption m_encodeOption = EncodeOption.None;

        private CCmd m_pageSizeCmd = null;


        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            string m_Val = "%" + m_strQueryName + "_";
            if (m_isFetchID) m_Val += "_";
            if (m_isFetchID1) m_Val += "_";
            if (m_isCount) m_Val += "_";
            if (m_isFirstRow) m_Val += "_";
            if (m_isLastRow) m_Val += "_";
            if (m_oddEven) m_Val += "_";
            m_Val += m_strFieldName;
            //if (m_encodeOption != EncodeOption.None) m_Val += "\\." + m_encodeOption.ToString();
            //if (m_strFormat != "")
            //{
            //    if (m_encodeOption == EncodeOption.None) m_Val += "\\.";
            //    m_Val += "\\." + m_strFormat;
            //}
            if (m_encodeOption != EncodeOption.None) m_Val += "." + m_encodeOption.ToString();
            if (m_strFormat != "")
            {
                if (m_encodeOption == EncodeOption.None) m_Val += ".";
                m_Val += "." + m_strFormat;
            }
            m_Val += "%";
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"SPAR\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape("")); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_Val)); sJSON.Append("\"");
            sJSON.Append(",\"Opt2\":\""); sJSON.Append(Utils._JsonEscape("1")); sJSON.Append("\"");
            sJSON.Append("}");
        }

		#endregion Protected members


		#region Public properties

        public CCmd PageSizeCmd
        {
            get { return m_pageSizeCmd; }
            set { m_pageSizeCmd = value; }
        }


        public bool IsFetchID
        {
            get { return m_isFetchID; }
            set { m_isFetchID = value; }
        }
        public bool IsFetchID1
        {
            get { return m_isFetchID1; }
            set { m_isFetchID1 = value; }
        }
        public bool IsCount
        {
            get { return m_isCount; }
            set { m_isCount = value; }
        }
        public bool IsFirstRow
        {
            get { return m_isFirstRow; }
            set { m_isFirstRow = value; }
        }
        public bool IsLastRow
        {
            get { return m_isLastRow; }
            set { m_isLastRow = value; }
        }
        public bool OddEven
        {
            get { return m_oddEven; }
            set { m_oddEven = value; }
        }
        public bool BinaryField
        {
            get { return m_binaryField; }
            set { m_binaryField = value; }
        }
        public EncodeOption EncodeOption
        {
            get { return m_encodeOption; }
            set { m_encodeOption = value; }
        }

		public string QueryName
		{
			get { return m_strQueryName; }
			set { m_strQueryName = value; }
		}
		public string FieldName
		{
			get { return m_strFieldName; }
			set { m_strFieldName = value; }
		}

        public string Format
        {
            get { return m_strFormat; }
            set { m_strFormat = value; }
        }

        //public CContext Context
        //{
        //    get { return m_context; }
        //    set { m_context = value; }
        //}

        //public XDataSourceModule.IXDataSource Evaluator
        //{
        //    get { return m_evaluator; }
        //    set { m_evaluator = value; }
        //}
		public Array Parameters
		{
			get {return m_parameters;}
			set {m_parameters = value;}
		}
		public CCmd Parent
		{
			get { return m_parent; }
			set { m_parent = value; }
		}
        public bool EncodeHTML
        {
            get { return m_encodeHTML; }
            set 
            { 
                m_encodeHTML = value;
                if (m_encodeHTML == true)
                    m_decodeHTML = false;
            }
        }
        public bool DecodeHTML
        {
            get { return m_decodeHTML; }
            set 
            { 
                m_decodeHTML = value;
                if (m_decodeHTML == true)
                    m_encodeHTML = false;
            }
        }

        //public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        //{
        //    sJSON.Append("{");
        //    sJSON.Append("\"Type\":\"FLD\"");
        //    sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
        //    sJSON.Append(",\"QueryName\":\""); sJSON.Append(Utils._JsonEscape(m_strQueryName)); sJSON.Append("\"");
        //    sJSON.Append(",\"Parameters\":[");

        //    if (m_pageSizeCmd != null) { sJSON.Append(",\"PageSize\":"); m_pageSizeCmd.GetJson(sJSON,sbWarning); }
        //    sJSON.Append(",\"Format\":\""); sJSON.Append(Utils._JsonEscape(m_strFormat)); sJSON.Append("\"");
        //    sJSON.Append(",\"EncodeOption\":\""); sJSON.Append(Utils._JsonEscape(m_encodeOption.ToString ())); sJSON.Append("\"");
        //    sJSON.Append(",\"Parameters\":[");
        //    bool isFirst = true;
        //    foreach (CCmd cCmd in m_parameters)
        //    {
        //        if (isFirst) isFirst = false; else sJSON.Append(",");
        //        cCmd.GetJson(sJSON,sbWarning);
        //    }
        //    sJSON.Append("]");
        //    sJSON.Append(",\"isFetchID\":\""); sJSON.Append(Utils._JsonEscape(m_isFetchID)); sJSON.Append("\"");
        //    sJSON.Append(",\"isFetchID1\":\""); sJSON.Append(Utils._JsonEscape(m_isFetchID1)); sJSON.Append("\"");
        //    sJSON.Append(",\"oddEven\":\""); sJSON.Append(Utils._JsonEscape(m_oddEven)); sJSON.Append("\"");
        //    sJSON.Append(",\"isCount\":\""); sJSON.Append(Utils._JsonEscape(m_isCount)); sJSON.Append("\"");
        //    sJSON.Append(",\"isFirstRow\":\""); sJSON.Append(Utils._JsonEscape(m_isFirstRow)); sJSON.Append("\"");
        //    sJSON.Append(",\"isLastRow\":\""); sJSON.Append(Utils._JsonEscape(m_isLastRow)); sJSON.Append("\"");
        //    sJSON.Append(",\"binaryField\":\""); sJSON.Append(Utils._JsonEscape(m_binaryField)); sJSON.Append("\"");
        //    sJSON.Append("}");
        //    sbWarning.Append("Command FLD at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        //}

		#endregion Public properties


		public CFLDCmd():base()
		{
			m_enType = CommandType.FLDCommand;
			m_bIsBlockCommand = false;
		}
//result.GetFieldValue(fieldSource, Index, out Value);
        //public override string Execute(CParser m_parser)
        //{
        //    string l_strFieldName = m_parser.ReplaceParameters(m_strFieldName);

        //    SourceResult parentResult = null;
        //    int queryIndex = m_parser.Context.OGetQueryIndex(m_parent); 
        //    //int queryIndex = m_context.GetQueryIndex(m_strQueryName);
        //    object oVal = "";
        //    string retVal = "";
        //    try
        //    {
        //        int parentResultsCount = -1;
        //        if (m_parent is CREPCmd)
        //        {
        //            parentResult = ((CREPCmd)m_parent).GetResult(m_parser);
        //            parentResultsCount = ((CREPCmd)m_parent).ResultsCount;
        //        }
        //        else if (m_parent is CXPATHCmd)
        //        {
        //            parentResult = ((CXPATHCmd)m_parent).GetResult(m_parser);
        //            parentResultsCount = ((CXPATHCmd)m_parent).ResultsCount;
        //        }
        //        else if (m_parent is CQRYCmd)
        //        {
        //            parentResult = ((CQRYCmd)m_parent).GetResult(m_parser);
        //            parentResultsCount = ((CQRYCmd)m_parent).ResultsCount;
        //        }

        //        string error = "";

        //        if (m_isFetchID)
        //            retVal = queryIndex.ToString();
        //        else if (m_isFetchID1)
        //            retVal = (queryIndex + 1).ToString();
        //        else if (m_isFirstRow)
        //            retVal = ((queryIndex == 0) && (parentResultsCount > 0) ? 1 : 0).ToString();
        //        else if (m_isLastRow)
        //            retVal = ((queryIndex == parentResultsCount - 1) ? 1 : 0).ToString();
        //        else if (m_oddEven)
        //            retVal = (queryIndex % 2).ToString();
        //        else if (m_isCount)
        //        {
        //            int retResCount = parentResultsCount;
        //            if (m_pageSizeCmd != null)
        //            {
        //                string temp = m_pageSizeCmd.Execute(m_parser);
        //                int pageSize = -1;
        //                if (int.TryParse(temp.Trim(), out pageSize))
        //                {
        //                    retResCount = parentResultsCount / pageSize;
        //                    if (parentResultsCount % pageSize != 0)
        //                        retResCount++;
        //                }
        //            }
        //            retVal = retResCount.ToString();
        //        }
        //        else
        //        {
        //            parentResult.GetFieldValue(l_strFieldName, queryIndex, out oVal);

        //            if (oVal != null && !m_binaryField)
        //                retVal = m_parser.ApplyFormat(oVal.ToString(), m_strFormat);

        //            if (m_binaryField)
        //                retVal = Encoding.UTF8.GetString((byte[])oVal);

        //            retVal = Utils.Encode(retVal, m_encodeOption);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Trace.WriteLine(ex);
        //        throw new Exception("Error executing FLD command for query " + m_strQueryName + " and field "
        //            + m_strFieldName + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + ex.Message);
        //    }
        //    return retVal;
        //}

//        public override string Execute(CParser m_parser)
//        {
//            int queryIndex = m_context.GetQueryIndex(m_strQueryName);
//            object oVal = "";
//            string retVal = "";
//            Array parameters = null;
//            try
//            {
//                int parentResultsCount = -1;
//                if (m_parent is CREPCmd)
//                {
//                    parameters = ((CREPCmd)m_parent).GetParamArray(m_parser );
//                    parentResultsCount = ((CREPCmd)m_parent).ResultsCount;
//                }
//                else if (m_parent is CXPATHCmd)
//                {
//                    parameters = ((CXPATHCmd)m_parent).GetParamArray(m_parser);
////                    parameters = ((CXPATHCmd)m_parent).GetOriginalParamArray(m_parser);//.GetParamArray(m_parser);
//                    parentResultsCount = ((CXPATHCmd)m_parent).ResultsCount;
//                }
//                else if (m_parent is CQRYCmd)
//                {
//                    parameters = ((CQRYCmd)m_parent).GetParamArray(m_parser);
//                    parentResultsCount = ((CQRYCmd)m_parent).ResultsCount;
//                }

//                string error = "";



//                if (m_isFetchID)
//                    retVal = queryIndex.ToString();
//                else if (m_isFetchID1)
//                    retVal = (queryIndex+1).ToString();
//                else if (m_isFirstRow)
//                    retVal = ((queryIndex == 0) && (parentResultsCount > 0) ? 1 : 0).ToString();
//                else if (m_isLastRow)
//                    retVal = ((queryIndex == parentResultsCount -1)?1:0).ToString();
//                else if (m_oddEven)
//                    retVal = (queryIndex % 2).ToString();
//                else if (m_isCount)
//                {
//                    int retResCount = parentResultsCount;
//                    if (m_pageSizeCmd != null)
//                    {
//                        string temp = m_pageSizeCmd.Execute(m_parser);
//                        int pageSize = -1;
//                        if (int.TryParse(temp.Trim(), out pageSize))
//                        {
//                            retResCount = parentResultsCount / pageSize;
//                            if (parentResultsCount % pageSize != 0)
//                                retResCount++;
//                        }
//                    }
//                    retVal = retResCount.ToString();
//                }
//                else
//                {
//                    if (!m_parser.Evaluator.GetValue(m_strFieldName, m_strQueryName, parameters, queryIndex, out oVal, out error))
//                    {
//                        string strError = "Cannot evaluate field " + m_strQueryName + "." + m_strFieldName;
//                        bool hasParams = parameters.GetLength(0) > 0;
//                        if (hasParams)
//                            strError += "(";
//                        foreach (string s in parameters)
//                            strError += s + ",";
//                        if (strError.EndsWith(","))
//                            strError = strError.Substring(0, strError.Length - 1);
//                        if (hasParams)
//                            strError += ")";
//                        strError += " for evaluator index=" + queryIndex.ToString() + "; " + newLine + error;

//                        //Trace.WriteLine(strError);
//                        //KubionLogNamespace.KubionLog.WriteLine(strError);

//                        if (Parent.Type == CommandType.XPATHCommand )
//                        {
//                            strError = "";
//                            return "";
//                        }
//                        else if (Parent.Type == CommandType.QRYCommand)
//                        {
//                            strError = "";
//                            return "";
//                        }
//                        else if (Parent.Type == CommandType.REPCommand)
//                        {
//                            KubionLogNamespace.KubionLog.WriteLine(strError);
//                            strError = "";
//                            return "";
//                        }
//                        else
//                            throw (new Exception(strError));
//                    }

//                    if (oVal != null && !m_binaryField)
//                    {//#fld.nu.nu.@d#
//                        string strFormat = m_strFormat;
//                        retVal = m_parser.ApplyFormat(oVal.ToString (), strFormat);
//                        ////if (strFormat.StartsWith("@"))
//                        ////{
//                        ////    CCmd cmd = new CPARCmd();
//                        ////    ((CPARCmd)cmd).ParameterName = strFormat.Substring(1);
//                        ////    cmd.Parser = m_parser;
//                        ////    strFormat = cmd.Execute();
//                        ////}

//                        ////bool b_isU = false;
//                        ////strFormat = strFormat.Replace("%dash%", "#");
//                        ////CultureInfo ci = CultureInfo.InvariantCulture;
//                        ////if (strFormat.StartsWith("NL"))
//                        ////{
//                        ////    strFormat = strFormat.Substring(2);
//                        ////    ci = CultureInfo.CreateSpecificCulture("nl-NL");
//                        ////}
//                        ////if (strFormat.StartsWith("US"))
//                        ////{
//                        ////    strFormat = strFormat.Substring(2);
//                        ////    ci = CultureInfo.CreateSpecificCulture("en-US");
//                        ////}
//                        ////if (strFormat.StartsWith("U"))
//                        ////{
//                        ////    b_isU = true;
//                        ////    strFormat = strFormat.Substring(1);
//                        ////}
//                        ////if (strFormat != "")
//                        ////{
//                        ////    retVal = m_parser.ApplyFormat(oVal.ToString (), strFormat);
//                            ////if (oVal is String)
//                            ////{
//                            ////    DateTime oValDT ;
//                            ////    int oValI;
//                            ////    float oValF; 
//                            ////    double oValD;
//                            ////    decimal oValDe;

//                            ////    if (DateTime.TryParse(oVal.ToString (), out oValDT))
//                            ////    {
//                            ////        Type t = oValDT.GetType();
//                            ////        MethodInfo mi = t.GetMethod("ToString", new Type[] { typeof(string), typeof(CultureInfo) });
//                            ////        try
//                            ////        {
//                            ////            if (mi != null)
//                            ////                retVal = (string)mi.Invoke(oValDT, new object[] { strFormat, ci });
//                            ////        }
//                            ////        catch
//                            ////        {
//                            ////            MethodInfo mi1 = t.GetMethod("ToString", new Type[] { typeof(string) });
//                            ////            if (mi1 != null)
//                            ////                retVal = (string)mi1.Invoke(oValDT, new object[] { strFormat });
//                            ////        }
//                            ////    }
//                            ////    else
//                            ////    if (int.TryParse(oVal.ToString (), out oValI))
//                            ////    {
//                            ////        Type t = oValI.GetType();
//                            ////        MethodInfo mi = t.GetMethod("ToString", new Type[] { typeof(string), typeof(CultureInfo) });
//                            ////        try
//                            ////        {
//                            ////            if (mi != null)
//                            ////                retVal = (string)mi.Invoke(oValI, new object[] { strFormat, ci });
//                            ////        }
//                            ////        catch
//                            ////        {
//                            ////            MethodInfo mi1 = t.GetMethod("ToString", new Type[] { typeof(string) });
//                            ////            if (mi1 != null)
//                            ////                retVal = (string)mi1.Invoke(oValI, new object[] { strFormat });
//                            ////        }
//                            ////    }
//                            ////    else
//                            ////    if (float.TryParse(oVal.ToString (), out oValF))
//                            ////    {
//                            ////        Type t = oValF.GetType();
//                            ////        MethodInfo mi = t.GetMethod("ToString", new Type[] { typeof(string), typeof(CultureInfo) });
//                            ////        try
//                            ////        {
//                            ////            if (mi != null)
//                            ////                retVal = (string)mi.Invoke(oValF, new object[] { strFormat, ci });
//                            ////        }
//                            ////        catch
//                            ////        {
//                            ////            MethodInfo mi1 = t.GetMethod("ToString", new Type[] { typeof(string) });
//                            ////            if (mi1 != null)
//                            ////                retVal = (string)mi1.Invoke(oValF, new object[] { strFormat });
//                            ////        }
//                            ////    }
//                            ////    else
//                            ////    if (decimal.TryParse(oVal.ToString (), out oValDe))
//                            ////    {
//                            ////        Type t = oValDe.GetType();
//                            ////        MethodInfo mi = t.GetMethod("ToString", new Type[] { typeof(string), typeof(CultureInfo) });
//                            ////        try
//                            ////        {
//                            ////            if (mi != null)
//                            ////                retVal = (string)mi.Invoke(oValDe, new object[] { strFormat, ci });
//                            ////        }
//                            ////        catch
//                            ////        {
//                            ////            MethodInfo mi1 = t.GetMethod("ToString", new Type[] { typeof(string) });
//                            ////            if (mi1 != null)
//                            ////                retVal = (string)mi1.Invoke(oValDe, new object[] { strFormat });
//                            ////        }
//                            ////    }

//                        ////    }
//                        ////    else
//                        ////    if (oVal is DateTime || oVal is int || oVal is double || oVal is float || oVal is decimal)
//                        ////    {
//                        ////        Type t = oVal.GetType();
//                        ////        MethodInfo mi = t.GetMethod("ToString", new Type[] { typeof(string),typeof (CultureInfo) });
//                        ////        try
//                        ////        {
//                        ////            if (mi != null)
//                        ////                retVal = (string)mi.Invoke(oVal, new object[] { strFormat,ci  });
//                        ////        }
//                        ////        catch
//                        ////        {
//                        ////            MethodInfo mi1 = t.GetMethod("ToString", new Type[] { typeof(string) });
//                        ////            if (mi1 != null)
//                        ////                retVal = (string)mi1.Invoke(oVal, new object[] { strFormat });
//                        ////        }
//                        ////    }
//                        ////    else
//                        ////        retVal = oVal.ToString();

//                        ////}
//                        ////else
//                        ////    retVal = oVal.ToString();
//                        ////if (b_isU)
//                        ////{
//                        ////    retVal  = retVal .Replace (',','.');
//                        ////}
//                    }

//                    if (m_binaryField)
//                        retVal = Encoding.UTF8.GetString((byte[])oVal);

//                    retVal = Utils.Encode(retVal, m_encodeOption);
//                }
//            }
//            catch (Exception ex)
//            {
//                Trace.WriteLine(ex);
//                throw new Exception("Error executing FLD command for query " + m_strQueryName + " and field " 
//                    + m_strFieldName + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + ex.Message);
//            }
//            return retVal;		
//        }

	}
}