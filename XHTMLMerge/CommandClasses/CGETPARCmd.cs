////using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CGETPARCmd : CCmd
    {
        protected string m_ParName = "";
        protected  string m_VarName = "";
        protected  string m_ContextName = "";
        protected  string m_ID = "ID";
        protected string m_What = "";
        protected string m_With = "";
        protected string m_DefaultVal = "";
        protected string m_Strict = "";

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"SVGET\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParName)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_VarName)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_ContextName)); sJSON.Append("\"");
            sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_ID)); sJSON.Append("\"");
            sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_What)); sJSON.Append("\"");
            sJSON.Append(",\"Value3\":\""); sJSON.Append(Utils._JsonEscape(m_With)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_DefaultVal)); sJSON.Append("\"");
            sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_ReplaceAllParams)); sJSON.Append("\"");
            sJSON.Append(",\"Opt2\":\""); sJSON.Append(Utils._JsonEscape(m_Strict)); sJSON.Append("\"");
            sJSON.Append("}");
        }

        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }
        public string DefaultVal
        {
            get { return m_DefaultVal; }
            set { m_DefaultVal = value; }
        }
        public string What
        {
            get { return m_What; }
            set { m_What = value; }
        }
        public string With
        {
            get { return m_With; }
            set { m_With = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        protected  string m_ReplaceAllParams = "";
        public string ReplaceAllParams
        {
            get { return m_ReplaceAllParams; }
            set { m_ReplaceAllParams = value; }
        }
        public string Strict
        {
            get { return m_Strict; }
            set { m_Strict = value; }
        }

        public CGETPARCmd()
            : base()
        {
            m_enType = CommandType.GETPARCommand;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_VarName, l_ContextName, l_ID, l_DefaultVal, l_ParName,l_What, l_With;

        //    l_VarName = m_parser.ReplaceParameters(m_VarName);
        //    l_ContextName = m_parser.ReplaceParameters(m_ContextName);
        //    l_ID = m_parser.ReplaceParameters(m_ID);
        //    l_DefaultVal = m_parser.ReplaceParameters(m_DefaultVal);
        //    l_ParName = m_parser.ReplaceParameters(m_ParName);
        //    l_What = m_parser.ReplaceParameters(m_What);
        //    l_With = m_parser.ReplaceParameters(m_With);

        //    bool bStrict = false;
        //    if (m_Strict == "1") bStrict = true; 
        //    string sPar = m_parser.GetSV(l_VarName, l_ContextName,l_ID,l_DefaultVal,bStrict   );
        //    if (sPar == "missing_setting") sPar = l_DefaultVal;
        //    if (l_What != "") sPar = sPar.Replace(l_What, l_With);
        //    if (m_ReplaceAllParams == "1") 
        //    {
        //        sPar = m_parser.ReplaceSettings (sPar,"","", true);
        //        sPar = m_parser.ReplaceParameters(sPar, true);
        //    }
        //    if (l_ParName != "")
        //    {
        //        m_parser.ParamDefaults[l_ParName] = sPar;
        //        m_parser.TemplateParams[l_ParName] = sPar;
        //        return "";
        //    }
        //    else
        //        return sPar;
        //}

    }
}

