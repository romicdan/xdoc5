using System;
using System.Diagnostics;
//using XDataSourceModule;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
//using KubionLogNamespace;

namespace XHTMLMerge
{
    [Serializable]
    public class CJLOOPCmd : CCmd
    {
        # region Protected members

        protected string m_ParamName = "";
        protected string m_Source = "";
        protected string m_JPath = "";
        protected string m_PageSize = "";
        protected string m_PageNr = "1";
        protected string m_ErrorParamName = "ERROR";
        //protected string m_isParSource = "0";
        protected string m_Condition = "";
        protected string m_Opt1 = "";
        protected string m_Opt2 = "";

        #endregion Protected members

        #region Public properties

        public string ParameterName
        {
            get { return m_ParamName; }
            set { m_ParamName = value; }
        }
        public string Source
        {
            get { return m_Source; }
            set { m_Source = value; }
        }
        public string JPath
        {
            get { return m_JPath; }
            set { m_JPath = value; }
        }
        public string PageSize
        {
            get { return m_PageSize; }
            set { m_PageSize = value; }
        }
        public string PageNr
        {
            get { return m_PageNr; }
            set { m_PageNr = value; }
        }
        //public string isParSource
        //{
        //    get { return m_isParSource; }
        //    set { m_isParSource = value; }
        //}
        public string ErrorParamName
        {
            get { return m_ErrorParamName; }
            set { m_ErrorParamName = value; }
        }
        public string Condition
        {
            get { return m_Condition; }
            set
            {
                m_Condition = value; SetEvalCondition(m_Condition);
            }
        }


        #endregion Public properties

        public CJLOOPCmd(): base()
        {
            this.m_enType = CommandType.JLOOPCommand ;
            m_bIsBlockCommand = true;
        }

        delegate bool delegateEvalCondition(string op1, string op2);
        private delegateEvalCondition myEvalCondition = null;
        private void SetEvalCondition(string condition)
        {
            if (condition == "")
                myEvalCondition = EvalConditionTrue;
            else if (condition.Contains("===="))
            {
                int i = condition.IndexOf("====");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 4).Trim();
                myEvalCondition = EvalCondition4E;
            }
            else if (condition.Contains("!==="))
            {
                int i = condition.IndexOf("!===");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 4).Trim();
                myEvalCondition = EvalConditionN3E;
            }
            else if (condition.Contains("==="))
            {
                int i = condition.IndexOf("===");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 3).Trim();
                myEvalCondition = EvalCondition3E;
            }
            else if (condition.Contains("!=="))
            {
                int i = condition.IndexOf("!==");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 3).Trim();
                myEvalCondition = EvalConditionN2E;
            }
            else if (condition.Contains("=="))
            {
                int i = condition.IndexOf("==");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 2).Trim();
                myEvalCondition = EvalCondition2E;
            }
            else if (condition.Contains("!="))
            {
                int i = condition.IndexOf("!=");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 2).Trim();
                myEvalCondition = EvalConditionN1E;
            }
            else if (condition.Contains(">>>"))
            {
                int i = condition.IndexOf(">>>");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 3).Trim();
                myEvalCondition = EvalCondition3G;
            }
            else if (condition.Contains(">>"))
            {
                int i = condition.IndexOf(">>");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 2).Trim();
                myEvalCondition = EvalCondition2G;
            }
            else if (condition.Contains("<<<"))
            {
                int i = condition.IndexOf("<<<");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 3).Trim();
                myEvalCondition = EvalCondition3L;
            }
            else if (condition.Contains("<<"))
            {
                int i = condition.IndexOf("<<");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 2).Trim();
                myEvalCondition = EvalCondition2L;
            }
            else
            {
                m_Condition = "SQL";
                myEvalCondition = EvalConditionTrue;
            }

        }


        //public override string Execute(CParser m_parser)
        //{
        //    //DateTime dt1 = DateTime.Now;
        //    //DateTime dt2 = DateTime.Now;
        //    string l_ParamName, l_Source, l_JPath, l_PageSize, l_PageNr, l_ErrorParamName;
        //    l_ParamName = m_parser.ReplaceParameters(m_ParamName);
        //    l_PageSize = m_parser.ReplaceParameters(m_PageSize);
        //    l_PageNr = m_parser.ReplaceParameters(m_PageNr);
        //    l_ErrorParamName = m_parser.ReplaceParameters(m_ErrorParamName);

        //    //if(m_isParSource=="1") l_Source =(string ) m_parser.TemplateParams[l_Source ];

        //    int i_PageSize = 10;
        //    int i_PageNr = 1;
        //    int.TryParse(l_PageSize, out i_PageSize);
        //    int.TryParse(l_PageNr, out i_PageNr);
        //    int start = (i_PageNr - 1) * i_PageSize;
        //    int end = i_PageNr * i_PageSize;

        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string val;
        //    System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();

        //    try
        //    {
        //        bool _isFirst = true;
        //        JArray ja=null;
        //        string sArrayName = "";
        //        if (l_ErrorParamName.Contains("|"))
        //        {
        //            sArrayName = l_ErrorParamName.Substring(l_ErrorParamName.IndexOf("|") + 1);
        //            l_ErrorParamName = l_ErrorParamName.Substring(0, l_ErrorParamName.IndexOf("|"));
        //        }
        //        if (sArrayName != "")
        //            ja = (JArray)m_parser.Arrays[sArrayName];
        //        if (ja == null)
        //        {
        //            l_Source = m_parser.ReplaceParameters(m_Source);
        //            l_JPath = m_parser.ReplaceParameters(m_JPath);
        //            if (l_JPath == "")
        //                ja = JArray.Parse(l_Source);
        //            else
        //                ja = (JArray)(JObject.Parse(l_Source)).SelectToken(l_JPath);
        //            if (sArrayName != "")
        //                m_parser.Arrays[sArrayName] = ja;
        //        }

        //        for (int i = start; i < end && i < ja.Count; i++)
        //        {
        //            JToken j = ja[i];
        //            val = j.ToString();
        //            m_parser.TemplateParams[l_ParamName] = val;
        //            m_parser.TemplateParams[l_ParamName + "__count"] = ja.Count.ToString();
        //            m_parser.TemplateParams[l_ParamName + "__fetchID"] = (i - start).ToString();
        //            m_parser.TemplateParams[l_ParamName + "__fetchID1"] = (i - start + 1).ToString();
        //            m_parser.TemplateParams[l_ParamName + "__oddEven"] = (i - start) % 2 == 1 ? "1" : "0";
        //            m_parser.TemplateParams[l_ParamName + "__isFirst"] = (_isFirst) ? "1" : "0";
        //            m_parser.TemplateParams[l_ParamName + "__firstRow"] = (i == start) ? "1" : "0";
        //            m_parser.TemplateParams[l_ParamName + "__lastRow"] = ((i == end - 1) || (i == ja.Count - 1)) ? "1" : "0";
        //            foreach (JProperty jp in j.Children<JProperty>())
        //                if (jp.Value.Type == JTokenType.Date)
        //                {
        //                    DateTime d = (DateTime)jp.Value;
        //                    m_parser.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                }
        //                else
        //                    m_parser.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString();
        //            //dt2 = DateTime.Now;
        //            //m_parser.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        //            bool bLoop = true;
        //            if (m_Condition != "")
        //            {
        //                //DateTime dt3 = DateTime.Now;
        //                //DateTime dt4;
        //                //string l_condition = process.ReplaceParameters(m_ID);
        //                //bLoop = EvalCondition(l_condition, process.m_runtime.DataProvider);
        //                string l_Opt1 = m_parser.ReplaceParameters(m_Opt1);
        //                string l_Opt2 = m_parser.ReplaceParameters(m_Opt2);
        //                if (m_Condition == "SQL")
        //                    bLoop = EvalConditionSQL(l_Opt1, m_parser.DataProvider);
        //                else
        //                    bLoop = myEvalCondition(l_Opt1, l_Opt2);
        //                //dt4 = DateTime.Now;
        //                //process.m_runtime.AddTimespan("LOOP_connd", dt4.Subtract(dt3).TotalMilliseconds);
        //            }
        //            //if (m_Condition != "")
        //            //{
        //            //    string l_Condition = m_parser.ReplaceParameters(m_Condition);
        //            //    bLoop = CParser.EvalCondition(l_Condition, m_parser.DataProvider);
        //            //}
        //            if (bLoop)
        //            {
        //                _isFirst = false;
        //                string output = base.Execute(m_parser);
        //                sbRetVal.Append(output);
        //            }
        //            //dt1 = DateTime.Now;
        //            foreach (JProperty jp in j.Children<JProperty>())
        //                m_parser.TemplateParams.Remove(l_ParamName + "_" + jp.Name);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JLOOP command";
        //        sErrorVerbose = ex.Message;
        //        sErrorVerbose = "Error executing JLOOP command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
        //        sbRetVal.Clear(); 
        //    }

        //    if (l_ErrorParamName != "")
        //    {
        //        m_parser.ParamDefaults[l_ErrorParamName] = sError;
        //        m_parser.TemplateParams[l_ErrorParamName] = sError;
        //        m_parser.ParamDefaults[l_ErrorParamName + "Verbose"] = sErrorVerbose;
        //        m_parser.TemplateParams[l_ErrorParamName + "Verbose"] = sErrorVerbose;
        //    }
        //    //dt2 = DateTime.Now;
        //    //m_parser.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        //    return sbRetVal.ToString();
        //}
        
        //public SourceResult GetResult(CParser m_parser)
        //{
        //    return (SourceResult)m_parser.Context.Results[this];
        //    //return m_Result;
        //}
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"JLOOP\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParamName)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_Source)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_JPath)); sJSON.Append("\"");
            sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_PageSize)); sJSON.Append("\"");
            sJSON.Append(",\"Value3\":\""); sJSON.Append(Utils._JsonEscape(m_PageNr)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_ErrorParamName)); sJSON.Append("\"");
            if ((m_Source.ToLower() == "while") && (m_Condition==""))
            {
                sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_JPath)); sJSON.Append("\"");
            }
            else
            {
                sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_Condition)); sJSON.Append("\"");
            }
            //sJSON.Append(",\"ParSource\":\""); sJSON.Append(Utils._JsonEscape(m_isParSource)); sJSON.Append("\"");
            base.GetJsonChilds(sJSON,sbWarning);
            sJSON.Append("}");
        }

    }
}