using System;
using System.Collections;
using System.Diagnostics;
using System.Web;
using System.Globalization;
using System.Reflection;
//using KubionLogNamespace;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CPARCmd : CCmd
	{
		#region Protected members

		protected string m_strParamName = "";
        protected string m_strFormat = "";

        protected EncodeOption m_encodeOption = EncodeOption.None;
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"PAR\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_strParamName)); sJSON.Append("\"");
            sJSON.Append(",\"Value3\":\""); sJSON.Append(Utils._JsonEscape(m_strFormat)); sJSON.Append("\"");
            sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_encodeOption.ToString())); sJSON.Append("\"");
            sJSON.Append("}");
        }
		
		#endregion Protected members

		#region Public properties

        public EncodeOption EncodeOption
        {
            get { return m_encodeOption; }
            set { m_encodeOption = value; }
        }

		public string ParameterName
		{
			get { return m_strParamName; }
			set { m_strParamName = value; }
		}

        public string Format
        {
            get { return m_strFormat; }
            set { m_strFormat = value; }
        }

		#endregion Public properties


		public CPARCmd():base()
		{
			m_enType = CommandType.PARCommand;
			m_bIsBlockCommand = false;
		}

  //      public override string Execute(CParser m_parser)
  //      {
  //          //DateTime dt1 = DateTime.Now;
  //          object oVal = null;
  //          string strFormat = m_strFormat;
  //          string retVal = "";
  //          try
  //          {
  //              if (m_strParamName.ToUpper() == "TEMPLATEID")
  //                  retVal = m_parser.TemplateID.ToString();
  //              else
  //              {
  //                  oVal = m_parser.GetParameterValue(m_strParamName);

  //                  if (strFormat == "")
  //                      retVal = oVal.ToString();
  //                  else
  //                  {
  //                      retVal = m_parser.ApplyFormat(oVal.ToString (), strFormat); 
  //                      ////if (strFormat.StartsWith("@"))
  //                      ////{
  //                      ////    CCmd cmd = new CPARCmd();
  //                      ////    ((CPARCmd)cmd).ParameterName = strFormat.Substring(1);
  //                      ////    cmd.Parser = m_parser;
  //                      ////    strFormat = cmd.Execute();
  //                      ////}
  //                      ////bool b_isU = false;
  //                      ////strFormat = strFormat.Replace("%dash%", "#");
  //                      ////CultureInfo ci = CultureInfo.InvariantCulture;
  //                      ////if (strFormat.StartsWith("NL"))
  //                      ////{
  //                      ////    strFormat = strFormat.Substring(2);
  //                      ////    ci = CultureInfo.CreateSpecificCulture("nl-NL");
  //                      ////}
  //                      ////if (strFormat.StartsWith("US"))
  //                      ////{
  //                      ////    strFormat = strFormat.Substring(2);
  //                      ////    ci = CultureInfo.CreateSpecificCulture("en-US");
  //                      ////}
  //                      ////if (strFormat.StartsWith("U"))
  //                      ////{
  //                      ////    b_isU = true;
  //                      ////    strFormat = strFormat.Substring(1);
  //                      ////}
  //                      ////if (strFormat != "")
  //                      ////{
  //                      ////    try
  //                      ////    {
  //                      ////        double dVal = Convert.ToDouble(oVal.ToString());
  //                      ////        retVal = dVal.ToString(strFormat, ci);
  //                      ////    }
  //                      ////    catch
  //                      ////    {
  //                      ////        try
  //                      ////        {
  //                      ////            DateTime dVal = Convert.ToDateTime(oVal.ToString());
  //                      ////            retVal = dVal.ToString(strFormat, ci);
  //                      ////        }
  //                      ////        catch
  //                      ////        {
  //                      ////            retVal = oVal.ToString();
  //                      ////        }
  //                      ////    }
  //                      ////    //if (oVal is DateTime || oVal is int || oVal is double || oVal is float || oVal is decimal)
  //                      ////    //{
  //                      ////    //    Type t = oVal.GetType();
  //                      ////    //    MethodInfo mi = t.GetMethod("ToString", new Type[] { typeof(string), typeof(CultureInfo) });
  //                      ////    //    try
  //                      ////    //    {
  //                      ////    //        if (mi != null)
  //                      ////    //            retVal = (string)mi.Invoke(oVal, new object[] { strFormat, ci });
  //                      ////    //    }
  //                      ////    //    catch
  //                      ////    //    {
  //                      ////    //        MethodInfo mi1 = t.GetMethod("ToString", new Type[] { typeof(string) });
  //                      ////    //        if (mi1 != null)
  //                      ////    //            retVal = (string)mi1.Invoke(oVal, new object[] { strFormat });
  //                      ////    //    }
  //                      ////    //}
  //                      ////    //else
  //                      ////    //    retVal = oVal.ToString();

  //                      ////}
  //                      ////else
  //                      ////    retVal = oVal.ToString();
  //                      ////if (b_isU)
  //                      ////{
  //                      ////    retVal = retVal.Replace(',', '.');
  //                      ////}

  //                      //////try
  //                      //////{
  //                      //////    double val = Convert.ToDouble(retVal.ToString());
  //                      //////    retString = val.ToString(strFormat);
  //                      //////}
  //                      //////catch
  //                      //////{
  //                      //////    try
  //                      //////    {
  //                      //////        DateTime dt = Convert.ToDateTime(retVal.ToString());
  //                      //////        retString = dt.ToString(strFormat);
  //                      //////    }
  //                      //////    catch
  //                      //////    {
  //                      //////        retString = retVal.ToString();
  //                      //////    }
  //                      //////}
  //                  }

  //              }
  //          }
  //          catch(Exception ex)
  //          {
  //              Trace.WriteLine(ex);
  //              throw new Exception("PAR command for parameter " + m_strParamName + " at line + " + m_parser.GetLine(m_nStartIndex).ToString() + " cannot execute;" + newLine + ex.Message);
  //          }

  //          retVal = Utils.Encode(retVal, m_encodeOption);
  //          //DateTime dt2 = DateTime.Now;
  //          //m_parser.AddTimespan("PAR", dt2.Subtract(dt1).TotalMilliseconds);

  //          return retVal;
		//}


	}
}
