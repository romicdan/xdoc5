////using KubionLogNamespace;
using System;
using System.Diagnostics;
using System.Text;
//using XDataSourceModule;

namespace XHTMLMerge
{
    [Serializable]
    public class CQRYCmd : CCmd
	{

		# region Protected members

        protected int m_TemplateID = -1;
        protected string m_strQueryName = "";
		protected CCmd[] m_Parameters = null;
        //protected CContext m_context = null;
		//protected XDataSourceModule.IXDataSource m_evaluator = null;
        protected int m_resultsCount = -1;
        [NonSerialized]
        //protected SourceResult m_Result = null;
        protected bool m_DataTable = false;

        private string GetAParameter(string sParameter, string sDefault)
        {
            string sResult = sDefault;
            foreach(CCmd cmd in m_Parameters)
            {
                string sValue = ((CTextCmd)cmd).Text;
                if (sValue.Contains("="))
                {
                    string sParName = sValue.Substring(0, sValue.IndexOf("="));
                    string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
                    if (sParName.ToUpper() == sParameter.ToUpper()) sResult = sValue1;
                }
            }
            if (sResult.StartsWith("#PAR."))
                sResult = "%" + sResult.Substring("#PAR.".Length,sResult.Length-1-"#PAR.".Length) + "%";
            return sResult;
        }
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
//#QRY.getWFFirstStep(Query=#PAR.WFFirstStepQuery#,Conn=REST_DMS)#
//#JDATA.jgetWFFirstStep.%WFFirstStepQuery%.REST_DMS#
//#JPAR.getWFFirstStep.%jgetWFFirstStep%.result[0]#
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"JDATA\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_TemplateID.ToString())); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape("J"+m_strQueryName)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(GetAParameter("Query",""))); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(GetAParameter("Conn", ""))); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(GetAParameter("Error", "Error"))); sJSON.Append("\"");
            sJSON.Append("}");
            sJSON.Append(",{");
            sJSON.Append("\"Type\":\"JPAR\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_TemplateID.ToString())); sJSON.Append("\"");
            sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_strQueryName)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape("%J" + m_strQueryName+"%")); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape("result[0]")); sJSON.Append("\"");
            sJSON.Append("}");

            foreach (CCmd cmd in m_vChildCmds)
            {
                sJSON.Append(",");
                cmd.GetJson(sJSON, sbWarning);
            }
            sbWarning.Append("Command QRY at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");

            //sJSON.Append("{");
            //sJSON.Append("\"Type\":\"QRY\"");
            //sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            //sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_TemplateID.ToString())); sJSON.Append("\"");
            //sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_strQueryName)); sJSON.Append("\"");
            //sJSON.Append(",\"Params\":[");
            //bool isFirst = true;
            //foreach (CCmd cCmd in this.m_Parameters)
            //{
            //    if (isFirst) isFirst = false; else sJSON.Append(",");
            //    cCmd.GetJson(sJSON,sbWarning);
            //}
            //sJSON.Append("]");
            //base.GetJsonChilds(sJSON, sbWarning);
            //sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_DataTable)); sJSON.Append("\"");
            //sJSON.Append("}");
            //sbWarning.Append("Command QRY at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        }


		#endregion Protected members

		#region Public properties

        public int ResultsCount
        {
            get { return m_resultsCount; }
            set { m_resultsCount = value; }
        }

        public int TemplateID
        {
            get { return m_TemplateID; }
            set { m_TemplateID = value; }
        }
        
        public string QueryName
		{
			get { return m_strQueryName; }
			set { m_strQueryName = value; }
		}

		public CCmd[] Parameters
		{
			get { return m_Parameters; }
			set { m_Parameters = value; }
		}	

        //public CContext Context
        //{
        //    get { return m_context; }
        //    set { m_context = value; }
        //}

        //public XDataSourceModule.IXDataSource Evaluator
        //{
        //    get { return m_evaluator; }
        //    set { m_evaluator = value; }
        //}

        public bool DataTable
        {
            get { return m_DataTable; }
            set { m_DataTable = value; if (m_DataTable) m_bIsBlockCommand = false; }
        }

		#endregion Public properties


		public CQRYCmd():base()
		{
			m_enType = CommandType.QRYCommand;
			m_bIsBlockCommand = true;
		}

        private string GetErrorParameter(Array m_parameters)
        {
            string sError = "Error";

            for (int index = 0; index < m_parameters.GetLength(0); index++)
            {
                if (m_parameters.GetValue(index) != null)
                {
                    string sValue = m_parameters.GetValue(index).ToString();
                    if (sValue.Contains("="))
                    {
                        string sParName = sValue.Substring(0, sValue.IndexOf("="));
                        string sValue1 = sValue.Substring(sValue.IndexOf("=") + 1);
                        if (sParName.ToUpper() == "ERROR") sError = sValue1;
                    }
                }
            }
            return sError;
        }

        //public override string Execute(CParser m_parser)
        //{
        //    string retVal = "";
        //    string sError = "";
        //    string sErrorVerbose = "";
        //    string sParameters = "";
        //    double dDuration = 0;
        //    string key="";

        //    bool bDefQry = (m_parser.Queries[m_strQueryName] != null);
        //    Array queryParameters = GetParamArray(m_parser, bDefQry);
        //    try
        //    {
        //        string error = "";
        //        string errorverbose = "";
        //        //m_context.ResetQIndex(m_strQueryName);
        //        //m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_evaluator_RequestHierarchy);
        //        m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy);

        //        m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out sError, out sErrorVerbose, out sParameters, out dDuration , out key);
        //        m_parser.Context.Keys[this] = key;
        //        m_parser.Context.Results[this] = m_Result;
        //        m_parser.Context.OResetQIndex(this);
        //    }
        //    catch (Exception ex)
        //    {
        //        Trace.WriteLine(ex);
        //        sError = ex.Message;
        //        sErrorVerbose = sError;
        //    }
        //    string sErrorParameter = GetErrorParameter(queryParameters);
        //    string l_ErrorParName = m_parser.ReplaceParameters(sErrorParameter);
        //    if (sError != "")
        //    {
        //        retVal = "";
        //        //sErrorVerbose = "Error executing COUNT command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + sParameters + "; " + newLine + sErrorVerbose;
        //        sErrorVerbose = "Error executing QRY command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
        //        m_parser.Message("QueryError", sErrorVerbose);
        //    }
        //    else
        //    {
        //        sErrorVerbose = "";
        //    }
        //    // set par 
        //    if (l_ErrorParName != "")
        //    {
        //        m_parser.ParamDefaults[l_ErrorParName] = sError;
        //        m_parser.TemplateParams[l_ErrorParName] = sError;
        //        m_parser.ParamDefaults[l_ErrorParName + "Verbose"] = sErrorVerbose;
        //        m_parser.TemplateParams[l_ErrorParName + "Verbose"] = sErrorVerbose;
        //    }

        //    m_resultsCount = 0;
        //    if (m_Result != null) m_resultsCount = m_Result.GetCount();
        //    if (m_parser.ParserQueryLog == "1") m_parser.QueryLog(m_strQueryName, sParameters, m_resultsCount, sError, sErrorVerbose, dDuration);

        //    if (m_DataTable)
        //    {
        //        if (m_Result != null) retVal = m_Result.GetJSON(0,  0);
        //        m_parser.TemplateParams[m_strQueryName + "Data"] = retVal;
        //        return "";
        //    }

        //    retVal = base.Execute(m_parser);
        //    return retVal;
        //}

        //public override string Execute(CParser m_parser)
        //{
        //    string retVal = "";
        //    string sError = "";

        //    bool bDefQry = (m_parser.Queries[m_strQueryName] != null);
        //    Array queryParameters = GetParamArray(m_parser,bDefQry );
        //    try
        //    {
        //        string error = "";
        //        m_context.ResetQIndex(m_strQueryName);
        //        //m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_evaluator_RequestHierarchy);
        //        m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy );

        //        m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out error);
        //        if (error != "") sError = error;
        //    }
        //    catch (Exception ex)
        //    {
        //        retVal = "";
        //        Trace.WriteLine(ex);
        //        string parameters = "";
        //        Array queryParams = GetParamArray(m_parser, bDefQry);
        //        if (queryParameters != null && queryParameters.GetLength(0) != 0)
        //        {
        //            parameters = " with parameters (";
        //            for (int i = 0; i < queryParameters.Length; i++)
        //            {
        //                parameters += (string)queryParameters.GetValue(i);
        //                if (i != queryParameters.Length - 1)
        //                    parameters += ",";
        //            }
        //            parameters += ")";
        //        }
        //        m_parser.Message(1, "Error executing QRY command at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + parameters + "; " + newLine + ex.Message);
        //        sError = ex.Message;
        //    }
        //    if(sError != "")
        //    {
        //        string sErrorParameter = GetErrorParameter(queryParameters);
        //        string l_ParName = m_parser.ReplaceParameters(sErrorParameter);
        //        // set par 
        //        if (l_ParName != "")
        //        {
        //            m_parser.ParamDefaults[l_ParName] = sError ;
        //            m_parser.TemplateParams[l_ParName] = sError ;
        //        }

        //    }
        //    m_resultsCount = 0;
        //    if (m_Result != null) m_resultsCount = m_Result.GetCount();
        //    if (m_parser.ParserQueryLog == "1")
        //    {
        //        string sParameters = "";
        //        Array queryParams = GetParamArray(m_parser, bDefQry);
        //        if (queryParameters != null && queryParameters.GetLength(0) != 0)
        //        {
        //            for (int i = 0; i < queryParameters.Length; i++)
        //                sParameters += (string)queryParameters.GetValue(i) + newLine;
        //        }
        //        m_parser.QueryLog(m_strQueryName, sParameters, m_resultsCount, sError);
        //    }

        //    retVal = base.Execute(m_parser);
        //    return retVal;
        //}
        //public override string Execute(CParser m_parser)
        //{
        //    string retVal = "";

        //    Array queryParameters = GetParamArray(m_parser);
        //    try
        //    {
        //        string error = "";
        //        m_context.ResetQIndex(m_strQueryName);
        //        //m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_evaluator_RequestHierarchy);
        //        m_parser.Evaluator.RequestHierarchy += new XDataSourceModule.SourceResult.RequestHierarcyEventHandler(m_parser.parser_RequestHierarchy);

        //        m_Result = m_parser.Evaluator.EvaluateSourceResult(m_TemplateID, m_strQueryName, queryParameters, out error);
        //        m_resultsCount = m_Result.GetCount();
        //        //int count = m_parser.Evaluator.EvaluateSource(m_TemplateID, m_strQueryName, queryParameters, out error);
        //        ////if (count == -1)
        //        ////{
        //        ////    throw (new Exception(error));
        //        ////}
        //        //// catchqry

        //        ////if (count == 0)
        //        //    //return "";
        //        //m_resultsCount = count;

        //        retVal = base.Execute(m_parser);
        //    }
        //    catch (Exception ex)
        //    {
        //        retVal = "";
        //        Trace.WriteLine(ex);
        //        string parameters = "";
        //        Array queryParams = GetParamArray(m_parser);
        //        if (queryParameters != null && queryParameters.GetLength(0) != 0)
        //        {
        //            parameters = " with parameters (";
        //            for (int i = 0; i < queryParameters.Length; i++)
        //            {
        //                parameters += (string)queryParameters.GetValue(i);
        //                if (i != queryParameters.Length - 1)
        //                    parameters += ",";
        //            }
        //            parameters += ")";
        //        }

        //        throw new Exception("Error executing QRY command at line " + m_parser.GetLine(m_nStartIndex).ToString() + " with query " + m_strQueryName + parameters + "; " + newLine + ex.Message);
        //    }
        //    return retVal;
        //}

        //public SourceResult GetResult(CParser m_parser)
        //{
        //    return (SourceResult)m_parser.Context.Results[this];
        //    //return m_Result;
        //}
		public Array GetParamArray(CParser m_parser, bool bDefQry)
		{
			Array arrParams = Array.CreateInstance(typeof(string), m_Parameters.GetLength(0)+1);
			for(int i = 0; i < m_Parameters.GetLength(0); i++)
			{
				CCmd cmd = m_Parameters[i];
                string strValue = cmd.Execute(m_parser);
                if (strValue.ToUpper() .Contains ("#PAR.") )
                    strValue = m_parser.Parameters2Values(strValue);
				arrParams.SetValue(strValue, i);
			}
            if (bDefQry)
                arrParams.SetValue("DEFQRY=1", m_Parameters.GetLength(0));
            else
                arrParams.SetValue("DEFQRY=0", m_Parameters.GetLength(0));
            return arrParams;
		}

        //void m_evaluator_RequestHierarchy(XDataSourceModule.SourceResult sourceResult, string parameters)
        //{
        //    l_parser.RaiseRequestHierarchy(sourceResult, parameters);
        //}

	}
}