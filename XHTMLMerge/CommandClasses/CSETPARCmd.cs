////using KubionLogNamespace;
using System;
using System.Globalization;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CSETPARCmd : CCmd
	{
        protected string m_ParName = "";
        protected string m_VarName = "";
        protected string m_ContextName = "";
        protected string m_ID = "ID";
        protected bool m_OnlyNull = false;
        protected string m_Format = "";
        protected string m_EncodeOption = "";
        
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            string m_Val = "%" + m_ParName;
            //if (m_EncodeOption != "") m_Val += "\\." + m_EncodeOption;
            //if (m_Format != "")
            //{
            //    if (m_EncodeOption == "") m_Val += "\\.";
            //    m_Val += "\\." + m_Format;
            //}
            //m_Val += "%";
            if (m_EncodeOption != "") m_Val += "." + m_EncodeOption;
            if (m_Format != "")
            {
                if (m_EncodeOption == "") m_Val += ".";
                m_Format = m_Format.Replace("\\.", ".");
                m_Format =  m_Format.Replace(".", "\\.");
                m_Val += "." + m_Format;
            }
            m_Val += "%";
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"SVSET\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_Val)); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_VarName)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_ContextName)); sJSON.Append("\"");
            sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_ID)); sJSON.Append("\"");
            sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_OnlyNull)); sJSON.Append("\"");
            sJSON.Append("}");
            //sJSON.Append("{");
            //sJSON.Append("\"Type\":\"SETPAR\"");
            //sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            //sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParName)); sJSON.Append("\"");
            //sJSON.Append(",\"VarName\":\""); sJSON.Append(Utils._JsonEscape(m_VarName)); sJSON.Append("\"");
            //sJSON.Append(",\"ContextName\":\""); sJSON.Append(Utils._JsonEscape(m_ContextName)); sJSON.Append("\"");
            //sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_ID)); sJSON.Append("\"");
            //sJSON.Append(",\"OnlyNull\":\""); sJSON.Append(Utils._JsonEscape(m_OnlyNull)); sJSON.Append("\"");
            //sJSON.Append(",\"Format\":\""); sJSON.Append(Utils._JsonEscape(m_Format)); sJSON.Append("\"");
            //sJSON.Append(",\"EncodeOption\":\""); sJSON.Append(Utils._JsonEscape(m_EncodeOption.ToString())); sJSON.Append("\"");
            //sJSON.Append("}");
            //sbWarning.Append("Command SETPAR at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        }

        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }
        public bool OnlyNull
        {
            get { return m_OnlyNull; }
            set { m_OnlyNull = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string Format
        {
            get { return m_Format; }
            set { m_Format = value; }
        }
        public string EncodeOption
        {
            get { return m_EncodeOption; }
            set { m_EncodeOption = value; }
        }

        public CSETPARCmd(): base()
		{
			m_enType = CommandType.SETPARCommand ;
			this.m_bIsBlockCommand = false;
		}


        //public override string Execute(CParser m_parser)
        //{
        //    string l_VarName, l_ContextName, l_ID, l_ParName, l_EncodeOption, l_Format;

        //    l_ParName = m_parser.ReplaceParameters(m_ParName);
        //    l_EncodeOption = m_parser.ReplaceParameters(m_EncodeOption);
        //    l_Format = m_parser.ReplaceParameters(m_Format);
        //    l_VarName = m_parser.ReplaceParameters(m_VarName);
        //    l_ContextName = m_parser.ReplaceParameters(m_ContextName);
        //    l_ID = m_parser.ReplaceParameters(m_ID);

        //    if (OnlyNull)
        //    {
        //        string sPar = m_parser.GetSV(l_VarName, l_ContextName, l_ID, "");
        //        if (sPar != "") return "";
        //    }

        //    string retVal = "";
        //    //if (m_parser.TemplateParams[l_ParName] != null) retVal = (string)m_parser.TemplateParams[l_ParName];
        //    //else if (m_parser.ParamDefaults[l_ParName] != null) retVal = (string)m_parser.ParamDefaults[l_ParName];
        //    //else retVal = m_parser.GetParameterValue(l_ParName);
        //    retVal =  m_parser.GetParameterValue (l_ParName );

        //    retVal = m_parser.ApplyFormat(retVal, l_Format);

        //    //if (l_Format != "")
        //    //{

        //    //    if (l_Format.StartsWith("@"))
        //    //    {
        //    //        CCmd cmd = new CPARCmd();
        //    //        ((CPARCmd)cmd).ParameterName = l_Format.Substring(1);
        //    //        cmd.Parser = m_parser;
        //    //        l_Format = cmd.Execute();
        //    //    }
        //    //    bool b_isU = false;
        //    //    l_Format = l_Format.Replace("%dash%", "#");
        //    //    CultureInfo ci = CultureInfo.InvariantCulture;
        //    //    if (l_Format.StartsWith("NL"))
        //    //    {
        //    //        l_Format = l_Format.Substring(2);
        //    //        ci = CultureInfo.CreateSpecificCulture("nl-NL");
        //    //    }
        //    //    if (l_Format.StartsWith("US"))
        //    //    {
        //    //        l_Format = l_Format.Substring(2);
        //    //        ci = CultureInfo.CreateSpecificCulture("en-US");
        //    //    }
        //    //    if (l_Format.StartsWith("U"))
        //    //    {
        //    //        b_isU = true;
        //    //        l_Format = l_Format.Substring(1);
        //    //    }
        //    //    if (l_Format != "")
        //    //    {
        //    //        try
        //    //        {
        //    //            double dVal = Convert.ToDouble(retVal);
        //    //            retVal = dVal.ToString(l_Format, ci);
        //    //        }
        //    //        catch
        //    //        {
        //    //            try
        //    //            {
        //    //                DateTime dVal = Convert.ToDateTime(retVal);
        //    //                retVal = dVal.ToString(l_Format, ci);
        //    //            }
        //    //            catch
        //    //            {
        //    //            }
        //    //        }

        //    //    }
        //    //    if (b_isU)
        //    //    {
        //    //        retVal = retVal.Replace(',', '.');
        //    //    }
        //    //}
        //    EncodeOption t_encodeOption = CParser.GetEncodeOption(l_EncodeOption);
        //    retVal = Utils.Encode(retVal, t_encodeOption);

        //    m_parser.SetSV(l_VarName, l_ContextName, retVal, l_ID);
        //    return "";
        //}

	}
}

