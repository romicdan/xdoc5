////using KubionLogNamespace;
using Newtonsoft.Json;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CTextCmd : CCmd
	{
        string m_text = "";
        //bool m_forcedText = false;
        const string
            START_BLOCK = "STARTBLOCK",
            END_BLOCK = "ENDBLOCK",
            START_COMMENT = "STARTCOMMENT",
            END_COMMENT = "ENDCOMMENT";
        //public string _JsonEscape1(string aText)
        //{
        //    DateTime dt1 = DateTime.Now;
        //    string result = "";
        //    result = JsonConvert.ToString(aText);
        //    DateTime dt2 = DateTime.Now;
        //    string s = "_JsonEscape1 (" + aText.Length.ToString() + ") = " + dt2.Subtract(dt1).TotalMilliseconds.ToString();
        //    return result;
        //}

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            if (m_text != "")
            {
//                m_text = m_text.Replace("\t", "");
                while (m_text.EndsWith("\t")) m_text = m_text.Substring(0, m_text.Length - 1);
                if (m_text.StartsWith("\r\n"))
                    m_text = m_text.Substring(2);
                //                m_text = m_text.Replace("\n", "");
                sJSON.Append("{");
                sJSON.Append("\"Type\":\"Text\"");
                sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
                sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape (m_text)); sJSON.Append("\"");
                sJSON.Append("}");
            }
            else
                sJSON.Append("{}");
        }

        public string Text
        {
            get { return m_text; }
            set { m_text = value;
            if (!RemoveStart(ref m_text, "\r\n"))
                RemoveStart(ref m_text, "\n");
            }
        }

		public CTextCmd():base()
		{
			m_enType = CommandType.TEXTCommand;
			this.m_bIsBlockCommand = false;
		}

        public void ForceText(string text)
        {
            //m_forcedText = true;
            m_text = text;
        }

  //      public override string Execute(CParser m_parser)
  //      {
  //          string retVal = "";
  //          //DateTime dt1 = DateTime.Now;
  //          if (m_forcedText == true)
  //              if ((m_text.ToUpper().StartsWith("#" + START_BLOCK + "#")) && (m_text.ToUpper().EndsWith("#" + END_BLOCK + "#")))
  //                  retVal = m_text.Substring(("#" + START_BLOCK + "#").Length, m_text.Length - ("#" + START_BLOCK + "#" + "#" + END_BLOCK + "#").Length);
  //              else
  //                  if ((m_text.ToUpper().StartsWith("#" + START_COMMENT + "#")) && (m_text.ToUpper().EndsWith("#" + END_COMMENT + "#")))
  //                      retVal = "";
  //                  else
  //                      retVal = m_text;
  //          else
  //              retVal = m_parser.TemplateText.Substring(StartIndex, EndIndex - StartIndex);

  //          if (!RemoveStart(ref retVal, "\r\n"))
  //              RemoveStart(ref retVal, "\n");

  //          if (m_parser.RemoveNewLine)
  //          {
  //              retVal = retVal.Replace("\r\n", " ");
  //              retVal = retVal.Replace("\n", " ");
  //          }

  //          if (m_parser.RemoveTab)
  //          {
  //              retVal = retVal.Replace("\t", "");
  //          }
  //          if (retVal != m_text)
  //              System.Diagnostics.Debug.WriteLine(retVal);
  //          //DateTime dt2 = DateTime.Now;
  //          //m_parser.AddTimespan(dt2.Subtract(dt1).TotalMilliseconds);
  //          return retVal;
		//}

        bool RemoveStart(ref string text, string newLine)
        {
            if (text.StartsWith(newLine))
            {
                text = text.Substring(newLine.Length);
                return true;
            }

            return false;
        }
	}
}
