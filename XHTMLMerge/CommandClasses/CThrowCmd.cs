////using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CThrowCmd : CCmd
    {
        string m_text = "";
        public string Text
        {
            get { return m_text; }
            set { m_text = value; }
        }
        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"THROW\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_text)); sJSON.Append("\"");
            sJSON.Append("}");
            //sbWarning.Append("Command THROW at line "); sbWarning.Append(Line.ToString()); sbWarning.Append(" is obsolete.\r\n");
        }
        public CThrowCmd()
            : base()
        {
            m_enType = CommandType.THROWCommand ;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    string l_text = m_parser.ReplaceParameters(m_text);

        //    string retVal = "";
        //    retVal = l_text;

        //    throw new Exception(retVal );

        //    return "";
        //}

    }
}

