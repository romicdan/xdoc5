////using KubionLogNamespace;
using System;
using System.Text;

namespace XHTMLMerge
{
    [Serializable]
    public class CTransferCmd : CCmd
    {
        int m_SourceConnID = -99;
        string m_SourceConn = "";
        string m_SourceTemplateName = "";
        int m_TargetConnID = -99;
        string m_TargetConn = "";
        string m_TargetTemplateName = "";
        bool m_TransferLike = false;

        public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        {
            sJSON.Append("{");
            sJSON.Append("\"Type\":\"TRANSFER\"");
            sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
            sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_SourceTemplateName)); sJSON.Append("\"");
            sJSON.Append(",\"ID\":\""); sJSON.Append(Utils._JsonEscape(m_SourceConnID)); sJSON.Append("\"");
            sJSON.Append(",\"Context\":\""); sJSON.Append(Utils._JsonEscape(m_SourceConn)); sJSON.Append("\"");
            sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_TargetTemplateName)); sJSON.Append("\"");
            sJSON.Append(",\"Value3\":\""); sJSON.Append(Utils._JsonEscape(m_TargetConnID)); sJSON.Append("\"");
            sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_TargetConn)); sJSON.Append("\"");
            sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_TransferLike)); sJSON.Append("\"");
            sJSON.Append("}");
        }

        public int SourceConnID
        {
            get { return m_SourceConnID; }
            set { m_SourceConnID = value; }
        }
        public string SourceConn
        {
            get { return m_SourceConn; }
            set { m_SourceConn = value; }
        }
        public string SourceTemplateName
        {
            get { return m_SourceTemplateName; }
            set { m_SourceTemplateName = value; }
        }
        public int TargetConnID
        {
            get { return m_TargetConnID; }
            set { m_TargetConnID = value; }
        }
        public string TargetConn
        {
            get { return m_TargetConn; }
            set { m_TargetConn = value; }
        }
        public string TargetTemplateName
        {
            get { return m_TargetTemplateName; }
            set { m_TargetTemplateName = value; }
        }
        public bool TransferLike
        {
            get { return m_TransferLike; }
            set { m_TransferLike = value; }
        }

        public CTransferCmd()
            : base()
        {
            m_enType = CommandType.TRANSFERCommand ;
            this.m_bIsBlockCommand = false;
        }


        //public override string Execute(CParser m_parser)
        //{
        //    //string l_TemplateName = m_parser.ReplaceParameters(m_TemplateName);
        //    string l_SourceTemplateName, l_SourceConn, l_TargetTemplateName, l_TargetConn;
        //    l_SourceTemplateName = m_parser.ReplaceParameters(SourceTemplateName);
        //    l_SourceConn = m_parser.ReplaceParameters(SourceConn);
        //    l_TargetTemplateName = m_parser.ReplaceParameters(TargetTemplateName);
        //    l_TargetConn = m_parser.ReplaceParameters(TargetConn);
        //    string sResult = "";
        //    if (TransferLike)
        //    {
        //        string postfix = "";
        //        if ((l_SourceConn == l_TargetConn) && (SourceConnID == TargetConnID)) postfix = "_Copy";
        //        l_TargetTemplateName = "ImportSet";
        //        if (SourceConnID == -99)
        //            sResult = m_parser.Manager.ExportTemplatesLike(l_SourceConn, l_SourceTemplateName,postfix);
        //        else
        //            sResult = m_parser.Manager.ExportTemplatesLike(SourceConnID, l_SourceTemplateName,postfix);
        //    }
        //    else
        //    {
        //        if (l_TargetTemplateName == "") l_TargetTemplateName = l_SourceTemplateName;
        //        if ((l_SourceConn == l_TargetConn) && (SourceConnID == TargetConnID) && (l_SourceTemplateName == l_TargetTemplateName)) l_TargetTemplateName += "_Copy";
        //        if (SourceConnID == -99)
        //            sResult = m_parser.Manager.ExportTemplate(l_SourceConn, l_SourceTemplateName);
        //        else
        //            sResult = m_parser.Manager.ExportTemplate(SourceConnID, l_SourceTemplateName);
        //    }
        //    if (TargetConnID == -99)
        //        sResult = m_parser.Manager.ImportTemplate(l_TargetConn, l_TargetTemplateName, sResult);
        //    else
        //        sResult = m_parser.Manager.ImportTemplate(TargetConnID, l_TargetTemplateName, sResult);

        //    return sResult;
        //}

    }
}


