﻿////using KubionLogNamespace;
// System.Diagnostics.Debug.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "message" );
//  \\10.200.200.9\e$\web_project\IRIS4\Scripts_Testen
//  http://10.200.200.9/IRIS_4_Test_Client/Admin/DGGraph
//
using KubionDataNamespace;
using Microsoft.IdentityModel.Claims;
using Microsoft.IdentityModel.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
//using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace XHTMLMerge
{
    [Serializable]
    public class JCmd
    {
        private string m_XFilesPath = "";
        protected string m_Type = "";
        public string Type
        {
            get { return m_Type; }
            set { m_Type = value; SetProcess(m_Type.ToUpper () ); }
        }
        protected string m_Line = "";
        public string Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        protected string m_ParamName = "";
        public string ParamName
        {
            get { return m_ParamName; }
            set { m_ParamName = value; }
        }
        protected string m_Name = "";
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        protected string m_Value = "";
        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; if ((m_Type == "IF") && (m_Name == "IFIP"))
                SetEvalCondition(m_Value); }
        }
        protected string m_Context = "";
        public string Context
        {
            get { return m_Context; }
            set { m_Context = value; }
        }
        protected string m_ID = "";
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; if ((m_Type == "JLOOP") || (m_Type == "IIF")) SetEvalCondition(m_ID); }
        }
        protected string m_Opt1 = "";
        public string Opt1
        {
            get { return m_Opt1; }
            set { m_Opt1 = value; }
        }
        protected string m_Opt2 = "";
        public string Opt2
        {
            get { return m_Opt2; }
            set { m_Opt2 = value; }
        }
        protected string m_Value2 = "";
        public string Value2
        {
            get { return m_Value2; }
            set { m_Value2 = value; }
        }
        protected string m_Value3 = "";
        public string Value3
        {
            get { return m_Value3; }
            set { m_Value3 = value; }
        }
        //protected JCmdCollection  m_Params =  null; 
        protected IList<JCmd> m_Params = null; //new List<JCmd>();
        public IList<JCmd> Params
        {
            //get { return m_Params; }
            set
            {
                //m_Params = new JCmdCollection();
                //foreach (JCmd cmd in value)
                //    m_Params.Add(cmd);
                m_Params = value;
            }
        }
        //protected JCmdCollection m_Commands = null;
        protected IList<JCmd> m_Commands = null; //new List<JCmd>();
        public IList<JCmd> Commands
        {
            get { return m_Commands; }
            set
            {
                //m_Commands = new JCmdCollection();
                //foreach (JCmd cmd in value)
                //    m_Commands.Add(cmd);
                m_Commands = value;
            }
        }
        //protected JCmd[] m_Params = null;
        //public JCmd[] Params
        //{
        //    get { return m_Params; }
        //    set { m_Params = value; }
        //}
        //protected JCmd[] m_Commands = null;
        //public JCmd[] Commands
        //{
        //    get { return m_Commands; }
        //    set { m_Commands = value; }
        //}
        protected JCmd  m_ElseIf = null;
        public JCmd ElseIf
        {
            get { return m_ElseIf; }
            set { m_ElseIf = value; }
        }

        delegate void delegateProcessObject(CProcess process, StringBuilder sb);
        private delegateProcessObject ProcessObject = null;
        //delegate string delegateSProcessObject(CProcess process);
        //private delegateSProcessObject SProcessObject = null;
        private void SetProcess(string sType)
        {
            switch (sType)
            {
                case "TEXT":
                    ProcessObject = Process_TEXT;
                    //SProcessObject = SProcess_TEXT;
                    break;
                case "DEFPAR":
                    ProcessObject = Process_DEFPAR;
                    //SProcessObject = SProcess_DEFPAR;
                    break;
                case "SPAR":
                    ProcessObject = Process_SPAR;
                    //SProcessObject = SProcess_SPAR;
                    break;
                case "PAR":
                    ProcessObject = Process_PAR;
                    //SProcessObject = SProcess_PAR;
                    break;
                case "IIF":
                    ProcessObject = Process_IIF;
                    //SProcessObject = SProcess_IIF;
                    break;
                case "IF":
                    ProcessObject = Process_IF;
                    //SProcessObject = SProcess_IF;
                    break;
                case "XPATH":
                    ProcessObject = Process_XPATH;
                    //SProcessObject = Process_XPATH;
                    break;
                case "JLOOP":
                    ProcessObject = Process_JLOOP;
                    //SProcessObject = SProcess_JLOOP;
                    break;
                case "JSORT":
                    ProcessObject = Process_JSORT;
                    //SProcessObject = SProcess_JSORT;
                    break;
                case "JKEYS":
                    ProcessObject = Process_JKEYS;
                    //SProcessObject = SProcess_JKEYS;
                    break;
                case "JSET":
                    ProcessObject = Process_JSET;
                    //SProcessObject = SProcess_JSET;
                    break;
                case "JPAR":
                    ProcessObject = Process_JPAR;
                    //SProcessObject = SProcess_JPAR;
                    break;
                case "JDATA":
                    ProcessObject = Process_JDATA;
                    //SProcessObject = SProcess_JDATA;
                    break;
                case "JARRAY":
                    ProcessObject = Process_JARRAY;
                    //SProcessObject = SProcess_JARRAY;
                    break;
                case "CFG":
                    ProcessObject = Process_CFG;
                    //SProcessObject = SProcess_CFG;
                    break;
                case "FUNC":
                    ProcessObject = Process_FUNC;
                    //SProcessObject = SProcess_FUNC;
                    break;
                case "MSG":
                    ProcessObject = Process_MSG;
                    //SProcessObject = SProcess_MSG;
                    break;
                case "SVSET":
                    ProcessObject = Process_SVSET;
                    //SProcessObject = SProcess_SVSET;
                    break;
                case "SVGET":
                    ProcessObject = Process_SVGET;
                    //SProcessObject = SProcess_SVGET;
                    break;
                case "SVDEL":
                    ProcessObject = Process_SVDEL;
                    //SProcessObject = SProcess_SVDEL;
                    break;
                case "INCLUDE":
                    ProcessObject = Process_INCLUDE;
                    //SProcessObject = SProcess_INCLUDE;
                    break;
                case "INCLUDEONCE":
                    ProcessObject = Process_INCLUDEONCE;
                    //SProcessObject = SProcess_INCLUDEONCE;
                    break;
                case "EXIT":
                    ProcessObject = Process_EXIT;
                    //SProcessObject = SProcess_EXIT;
                    break;
                case "THROW":
                    ProcessObject = Process_THROW;
                    //SProcessObject = SProcess_THROW;
                    break;
                case "IMPORT":
                    ProcessObject = Process_IMPORT;
                    //SProcessObject = SProcess_IMPORT;
                    break;
                case "EXPORT":
                    ProcessObject = Process_EXPORT;
                    //SProcessObject = SProcess_EXPORT;
                    break;
                case "DELETE":
                    ProcessObject = Process_DELETE;
                    //SProcessObject = SProcess_DELETE;
                    break;
                case "TRANSFER":
                    ProcessObject = Process_TRANSFER;
                    //SProcessObject = SProcess_TRANSFER;
                    break;
                case "FILE":
                    ProcessObject = Process_FILE;
                    //SProcessObject = SProcess_FILE;
                    break;
                case "USEPARAMETERS":
                    ProcessObject = Process_USEPARAMETERS;
                    //SProcessObject = SProcess_USEPARAMETERS;
                    break;
                //case "QRY":
                //    ProcessObject = Process_QRY;
                //    break;
                //case "REP":
                //    ProcessObject = Process_QRY;
                //    break;
                default:
                    ProcessObject = Process_default;
                    //SProcessObject = SProcess_default;
                    break;
            }

        }
        delegate bool delegateEvalCondition(string op1, string op2);
        private delegateEvalCondition myEvalCondition = null;
        private void SetEvalCondition(string condition)
        {
            if (condition == "")
                myEvalCondition = EvalConditionTrue;
            else if (condition.Contains("===="))
            {
                int i = condition.IndexOf("====");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 4).Trim();
                myEvalCondition = EvalCondition4E;
            }
            else if (condition.Contains("!==="))
            {
                int i = condition.IndexOf("!===");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 4).Trim();
                myEvalCondition = EvalConditionN3E;
            }
            else if (condition.Contains("==="))
            {
                int i = condition.IndexOf("===");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 3).Trim();
                myEvalCondition = EvalCondition3E;
            }
            else if (condition.Contains("!=="))
            {
                int i = condition.IndexOf("!==");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 3).Trim();
                myEvalCondition = EvalConditionN2E;
            }
            else if (condition.Contains("=="))
            {
                int i = condition.IndexOf("==");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 2).Trim();
                myEvalCondition = EvalCondition2E;
            }
            else if (condition.Contains("!="))
            {
                int i = condition.IndexOf("!=");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 2).Trim();
                myEvalCondition = EvalConditionN1E;
            }
            else if (condition.Contains(">>>"))
            {
                int i = condition.IndexOf(">>>");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 3).Trim();
                myEvalCondition = EvalCondition3G;
            }
            else if (condition.Contains(">>"))
            {
                int i = condition.IndexOf(">>");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 2).Trim();
                myEvalCondition = EvalCondition2G;
            }
            else if (condition.Contains("<<<"))
            {
                int i = condition.IndexOf("<<<");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 3).Trim();
                myEvalCondition = EvalCondition3L;
            }
            else if (condition.Contains("<<"))
            {
                int i = condition.IndexOf("<<");
                m_Opt1 = condition.Substring(0, i).Trim();
                m_Opt2 = condition.Substring(i + 2).Trim();
                myEvalCondition = EvalCondition2L;
            }
            else
            {
                m_ID = "SQL";
                myEvalCondition = EvalConditionTrue;
            }

        }

        //private JToken SelectJSONToken(JToken o, string l_JPath)
        //{
        //    JToken jt;
        //    if (l_JPath == "")
        //        jt = o;
        //    else
        //        jt = o.SelectToken(l_JPath);
        //    if (jt == null) throw new Exception ("JPath not found");
        //    return jt;
        //}
 //       private JToken SelectJSONToken(string l_Source, string l_JPath, string sDefault)
 //       {
 //           JToken jt;
 //           if (l_JPath == "")
 //               jt = JToken.Parse(l_Source);
 //           else
 ////               jt = (JObject.Parse(l_Source)).SelectToken(l_JPath);
 //               jt = (JToken.Parse(l_Source)).SelectToken(l_JPath);
 //           if (jt == null)
 //           {
 //               jt = JToken.Parse(sDefault);
 //           }
 //           return jt;
 //       }
        private JArray SelectJSONArray(string l_Source, string l_JPath)
        {
            //JToken jt;
            //if (l_JPath == "")
            //    jt = JArray.Parse(l_Source);
            //else
            //    jt = (JObject.Parse(l_Source)).SelectToken(l_JPath);
            //if (jt is JArray)
            //    return (JArray)jt;
            //else
            //    return new JArray(jt);
            JToken jt;
            if (l_JPath == "")
                jt = JToken.Parse(l_Source);
            else
                jt = (JToken.Parse(l_Source)).SelectToken(l_JPath);
            //                jt = (JObject.Parse(l_Source)).SelectToken(l_JPath);
            if (jt is JArray)
                return (JArray)jt;
            else
                return new JArray(jt);
            //                    if (l_JPath == "")
            //                    {
            //                        JToken jt = JToken.Parse(l_Source);
            //                        if (jt is JArray)
            //                            ja = (JArray)jt;
            //                        else
            //                            ja = new JArray(jt);
            //                    }
            //                        //ja = JArray.Parse(l_Source);
            //                    else
            //                    {
            //                        JToken jt = (JObject.Parse(l_Source)).SelectToken(l_JPath);
            //                        if (jt is JArray)
            //                            ja = (JArray)jt;
            //                        else
            //                            ja = new JArray(jt);
            ////                        ja = (JArray)(JObject.Parse(l_Source)).SelectToken(l_JPath);
            //                    }
        }

        private JArray JSONArrayParseSelect(string l_Source, string l_JPath, string sDefault)
        {
            JToken jt = JSONSelect(JSONParse(l_Source, sDefault), l_JPath);
            if (jt is JArray)
                return (JArray)jt;
            else
                return new JArray(jt);
        }
        private JToken JSONParseSelect(string l_Source, string l_JPath, string sDefault)
        {
            return JSONSelect(JSONParse(l_Source, sDefault), l_JPath);
        }
        private JToken JSONParse(string l_Source, string sDefault)
        {
            JToken jt = null;
            try
            {
                jt = JToken.Parse(l_Source);
            }
            catch (Exception ex)
            {
                if (sDefault == null)
                    throw ex;
                jt = JToken.Parse(sDefault);
            };
            return jt;
            //JToken jt = null;
            //try
            //{
            //    jt = JToken.Parse(l_Source);
            //} catch(Exception){};
            //if (jt == null)
            //    jt = JToken.Parse(sDefault);
            //return jt;
        }
        private JToken JSONSelect(JToken o, string l_JPath)
        {
            JToken jt;
            if (l_JPath == "")
                jt = o;
            else
                jt = o.SelectToken(l_JPath);
            if (jt == null) throw new Exception("JPath not found");
            return jt;
        }

        #region string
        //private string SProcess_default(CProcess process)
        //{
        //    string sResult = "";
        //    sResult = "[[" + m_Type + "]]";
        //    return sResult;
        //}
        //private string SProcess_CFG(CProcess process)
        //{
        //    string sResult = "";
        //    string l_CfgClass, l_CfgName, l_CfgDefault, l_Encode;

        //    l_CfgClass = process.ReplaceParameters(m_Context);
        //    l_CfgName = process.ReplaceParameters(m_Name);
        //    l_CfgDefault = process.ReplaceParameters(m_Value);
        //    l_Encode = process.ReplaceParameters(m_Value2);

        //    sResult = process.m_runtime.AppCache.GetCfg(l_CfgClass + "__" + l_CfgName, l_CfgDefault);
        //    sResult = process.ReplaceParameters(sResult);
        //    sResult = Utils.Encode(sResult, l_Encode);
        //    return sResult;
        //}
        //private string SProcess_DEFPAR(CProcess process)
        //{
        //    string sResult = "";
        //    string l_strParamName, l_strValue;
        //    l_strParamName = process.ReplaceParameters(m_ParamName);
        //    l_strValue = process.ReplaceParameters(m_Value);
        //    if (l_strParamName != "") process.DefaultParams[l_strParamName] = l_strValue;
        //    return "";//            return sResult;
        //}
        //private string SProcess_SPAR(CProcess process)
        //{
        //    string sResult = "";
        //    string l_strParamName, l_strValue;
            
        //    l_strParamName = process.ReplaceParameters(m_ParamName);
        //    if (m_Opt2 == "1")
        //        l_strValue = process.ReplaceParameters(m_Value, true);
        //    else
        //        l_strValue = process.ReplaceParameters(m_Value);
        //    if (m_Opt1 == "1") l_strValue = process.GetParameterValue(l_strValue);
        //    //if (l_strParamName != "") process.DefaultParams[l_strParamName] = l_strValue;
        //    if (l_strParamName == "")
        //    {
        //        sResult = l_strValue;
        //    }
        //    else
        //        process.TemplateParams[l_strParamName] = l_strValue;
        //    return sResult;
        //}
        //private string SProcess_TEXT(CProcess process)
        //{
        //    //start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end

        //    //Regex regexMacro = new Regex(@"\#MACRO\..*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)\#", RegexOptions.IgnoreCase);
        //    //MatchCollection mc = regexMacro.Matches(s_text);
        //    //for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //    //{
        //    //    Match m = mc[matchIndex];
        //    //    string sResult = "";
        //    //    s_text = s_text.Replace(m.Value, sResult);
        //    //}

        //    string sResult = "";
        //    string sText = m_Value;
        //    string sTextU = sText.ToUpper();
        //    int i1 = sTextU.IndexOf("#STARTBLOCK#");
        //    int j1 = sTextU.IndexOf("#ENDBLOCK#");
        //    if ((i1 >= 0) && (j1 >= 0) && (i1 < j1))
        //        sText = sText.Substring(i1 + "#STARTBLOCK#".Length, j1 - i1 - "#STARTBLOCK#".Length);
        //    i1 = sTextU.IndexOf("#STARTCOMMENT#");
        //    j1 = sTextU.IndexOf("#ENDCOMMENT#");
        //    if ((i1 >= 0) && (j1 >= 0) && (i1 < j1))
        //        sText = sText.Substring(0, i1) + sText.Substring(j1 + "#ENDCOMMENT#".Length);
        //    sResult = sText;
        //    return sResult;
        //}
        //private string SProcess_IIF(CProcess process)
        //{
        //    string sResult = "";
        //    string l_condition, l_trueExpr, l_falseExpr, l_ParamName;
        //    l_condition = process.ReplaceParameters(m_Value);
        //    bool condResult = EvalCondition(l_condition, process.m_runtime.DataProvider);
        //    if (condResult == true)
        //    {
        //        l_trueExpr = process.ReplaceParameters(m_Value2);
        //        sResult = l_trueExpr;
        //    }
        //    else
        //    {
        //        l_falseExpr = process.ReplaceParameters(m_Value3);
        //        sResult = l_falseExpr;
        //    }
        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    if (l_ParamName != "")
        //    {
        //        process.TemplateParams[l_ParamName] = sResult;
        //        sResult = "";
        //    }
        //    return sResult;
        //}
        //private string SProcess_IF(CProcess process)
        //{
        //    //DateTime dt1 = DateTime.Now;
        //    string sResult = "";
        //    string l_condition = "";
        //    if (m_Name == "IFI")
        //        l_condition = m_Value;
        //    else
        //    {
        //        //process.m_runtime.AddTimespan("IF_CondS_PAR", 1);
        //        if ((m_Params != null) && (m_Params.Count > 0))
        //            foreach (JCmd command in m_Params)
        //            {
        //                l_condition += command.SProcessObject(process);
        //            }
        //    }
        //    l_condition = process.ReplaceParameters(l_condition);
        //    //DateTime dt2 = DateTime.Now;
        //    //process.m_runtime.AddTimespan("IF_Cond", dt2.Subtract(dt1).TotalMilliseconds);
        //    bool condResult = EvalCondition(l_condition, process.m_runtime.DataProvider);
        //    if (condResult == true)
        //        sResult += SProcessCommands(process);
        //    else if (condResult == false && m_ElseIf != null)
        //    {
        //        sResult += m_ElseIf.SProcessObject(process);
        //    }
        //    return sResult;
        //}
        //private string SProcess_PAR(CProcess process)
        //{
        //    //DateTime dt1 = DateTime.Now;
        //    string sResult = "";
        //    string l_ParamName, l_Format, l_Encode;
        //    //l_ParamName = process.ReplaceParameters(m_ParamName);
        //    //l_Format = process.ReplaceParameters(m_Value3);
        //    //l_Encode = process.ReplaceParameters(m_Value2);
        //    l_ParamName = m_ParamName;
        //    l_Format = m_Value3;
        //    l_Encode = m_Value2;

        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";
        //    try
        //    {
        //        //sResult = process.GetParameterValue(l_ParamName);
        //        //if (l_Format != "")
        //        //    sResult = Utils.ApplyFormat(sResult, l_Format);
        //        //sResult = Utils.Encode(sResult, l_Encode);
        //        sResult = process.GetParameterValue(l_ParamName, "", l_Format, l_Encode);
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing PAR command";
        //        sErrorVerbose = "Error executing PAR command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //DateTime dt2 = DateTime.Now;
        //    //process.m_runtime.AddTimespan(m_Type, dt2.Subtract(dt1).TotalMilliseconds);
        //                return sResult;
        //}
        //private string SProcess_JLOOP(CProcess process)
        //{
        //    //DateTime dt1 = DateTime.Now;
        //    //DateTime dt2;
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_PageSize, l_PageNr, l_ErrorParamName;
            
        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Source = process.ReplaceParameters(m_Name);
        //    l_JPath = process.ReplaceParameters(m_Value);
        //    l_PageSize = process.ReplaceParameters(m_Value2);
        //    l_PageNr = process.ReplaceParameters(m_Value3);
        //    l_ErrorParamName = process.ReplaceParameters(m_Context);
        //    int i_PageSize = 10;
        //    int i_PageNr = 1;
        //    int.TryParse(l_PageSize, out i_PageSize);
        //    int.TryParse(l_PageNr, out i_PageNr);
        //    int start = (i_PageNr - 1) * i_PageSize;
        //    int end = i_PageNr * i_PageSize;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string val;
        //    //System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();
        //    try
        //    {
        //        bool _isFirst = true;
        //        JArray ja=null;
        //        string sArrayName = "";
        //        if (l_ErrorParamName.Contains("|"))
        //        {
        //            sArrayName = l_ErrorParamName.Substring(l_ErrorParamName.IndexOf("|") + 1);
        //            l_ErrorParamName = l_ErrorParamName.Substring(0, l_ErrorParamName.IndexOf("|"));
        //        }
        //        if (sArrayName != "") 
        //            ja = (JArray)process.Arrays[sArrayName];
        //        if (ja == null)
        //        {
        //            ja = SelectJSONArray(l_Source, l_JPath);
        //            //if (l_JPath == "")
        //            //    ja = JArray.Parse(l_Source);
        //            //else
        //            //    ja = (JArray)(JObject.Parse(l_Source)).SelectToken(l_JPath);
        //            if (sArrayName != "") 
        //                process.Arrays[sArrayName] = ja;
        //        }

        //        for (int i = start; i < end && i < ja.Count; i++)
        //        {
        //            JToken j = ja[i];
        //            val = j.ToString();
        //            process.TemplateParams[l_ParamName] = val;
        //            process.TemplateParams[l_ParamName + "__count"] = ja.Count.ToString();
        //            process.TemplateParams[l_ParamName + "__fetchID"] = (i - start).ToString();
        //            process.TemplateParams[l_ParamName + "__fetchID1"] = (i - start + 1).ToString();
        //            process.TemplateParams[l_ParamName + "__oddEven"] = (i - start) % 2 == 1 ? "1" : "0";
        //            process.TemplateParams[l_ParamName + "__isFirst"] = (_isFirst) ? "1" : "0";
        //            process.TemplateParams[l_ParamName + "__firstRow"] = (i == start) ? "1" : "0";
        //            process.TemplateParams[l_ParamName + "__lastRow"] = ((i == end - 1) || (i == ja.Count - 1)) ? "1" : "0";
        //            //if (l_Source.StartsWith("arr_"))
        //            //    foreach (string sKey in aKeys)
        //            //        if (j[sKey].Type == JTokenType.Date)
        //            //        {
        //            //            DateTime d = (DateTime)j[sKey];
        //            //            process.TemplateParams[l_ParamName + "_" + sKey] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //            //        }
        //            //        else
        //            //            process.TemplateParams[l_ParamName + "_" + sKey] = j[sKey].ToString ();
        //            //else
        //                foreach (JProperty jp in j.Children<JProperty>())
        //                    if (jp.Value.Type == JTokenType.Date)
        //                    {
        //                        DateTime d = (DateTime)jp.Value;
        //                        process.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                    }
        //                    else
        //                        process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString ();
        //            //dt2 = DateTime.Now;
        //            //process.m_runtime.AddTimespan("LOOP_for",dt2.Subtract(dt1).TotalMilliseconds);
        //                bool bLoop = true;
        //                if (m_ID != "")
        //                {
        //                    //DateTime dt3 = DateTime.Now;
        //                    //DateTime dt4;
        //                    //string l_condition = process.ReplaceParameters(m_ID);
        //                    //bLoop = EvalCondition(l_condition, process.m_runtime.DataProvider);
        //                    string l_Opt1 = process.ReplaceParameters(m_Opt1);
        //                    string l_Opt2 = process.ReplaceParameters(m_Opt2);
        //                    if (m_ID == "SQL")
        //                        bLoop = EvalConditionSQL(l_Opt1, process.m_runtime.DataProvider);
        //                    else
        //                        bLoop = myEvalCondition(l_Opt1, l_Opt2);
        //                    //dt4 = DateTime.Now;
        //                    //process.m_runtime.AddTimespan("LOOP_connd", dt4.Subtract(dt3).TotalMilliseconds);
        //                }
        //                if (bLoop == true)
        //                {
        //                    _isFirst = false;
        //                    sResult += SProcessCommands(process);
        //                }
        //            //dt1 = DateTime.Now;
        //            //if (l_Source.StartsWith("arr_"))
        //            //    foreach (string sKey in aKeys)
        //            //        process.TemplateParams.Remove(l_ParamName + "_" + sKey);
        //            //else
        //                foreach (JProperty jp in j.Children<JProperty>())
        //                    process.TemplateParams.Remove(l_ParamName + "_" + jp.Name);
        //            string sExitType = process.sExitType;// GetParameterValue("__exit_type", "none");
        //            if (sExitType != "")
        //            {
        //                if (sExitType == "loop")
        //                {
        //                    process.sExitType ="";//.TemplateParams.Remove("__exit_type");
        //                    sErrorVerbose = sError = process.sExitValue; //.GetParameterValue("__exit_value", "");
        //                    process.sExitValue ="";//.TemplateParams.Remove("__exit_value");
        //                }
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JLOOP command";
        //        sErrorVerbose = "Error executing JLOOP command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //dt2 = DateTime.Now;
        //    //process.m_runtime.AddTimespan("LOOP_for",dt2.Subtract(dt1).TotalMilliseconds);
        //               return sResult;
        //}
        //private string SProcess_JSET(CProcess process)
        //{
        //    string sResult = "";
        //    sResult = "JSET";
        //    return sResult;
        //}
        //private string SProcess_JPAR(CProcess process)
        //{
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
            

        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Source = process.ReplaceParameters(m_Name);
        //    l_JPath = process.ReplaceParameters(m_Value);
        //    l_ErrorParamName = "ERROR";

        //    //if (isParSource == "1") l_Source = (string)process.TemplateParams[l_Source];
        //    string val = "";
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    try
        //    {
        //        JObject o = JObject.Parse(l_Source);
        //        if (l_ParamName == "")
        //        {
        //            sResult = o.SelectToken(l_JPath).ToString();
        //        }
        //        else
        //        {
        //            sResult = "";
        //            JToken jt = o.SelectToken(l_JPath);
        //            val = jt.ToString();
        //            if (jt is JArray)
        //                process.TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();
        //            process.TemplateParams[l_ParamName] = val;
        //            foreach (JProperty jp in jt.Children<JProperty>())
        //            {
        //                if (jp.Value.Type == JTokenType.Date)
        //                {
        //                    DateTime d = (DateTime)jp.Value;
        //                    process.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                }
        //                else
        //                    process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString ();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JPAR command";
        //        sErrorVerbose = "Error executing JPAR command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return sResult;
        //}
        //private string SProcess_JKEYS(CProcess process)
        //{
        //    //DateTime dt1 = DateTime.Now;
        //    //DateTime dt2;
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Source = process.ReplaceParameters(m_Name);
        //    l_JPath = process.ReplaceParameters(m_Value);
        //    l_ErrorParamName = "ERROR";

        //    string val = "";
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    try
        //    {
        //        JObject o = JObject.Parse(l_Source);
        //        if (l_ParamName == "")
        //        {
        //            sResult = o.SelectToken(l_JPath).ToString();
        //        }
        //        else
        //        {
        //            sResult = "";
        //            JToken jt = o.SelectToken(l_JPath);
        //            val = jt.ToString();
        //            if (jt is JArray)
        //                process.TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

        //            process.TemplateParams[l_ParamName] = val;
        //            foreach (JProperty jp in jt.Children<JProperty>())
        //            {
        //                process.TemplateParams[l_ParamName + "__key"] = jp.Name;
        //                if (jp.Value.Type == JTokenType.Date)
        //                {
        //                    DateTime d = (DateTime)jp.Value;
        //                    process.TemplateParams[l_ParamName + "__value"] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                }
        //                else
        //                    process.TemplateParams[l_ParamName + "__value"] = jp.Value.ToString ();
        //                //dt2 = DateTime.Now;
        //                //process.m_runtime.AddTimespan("JKEYS_for", dt2.Subtract(dt1).TotalMilliseconds);
        //                sResult += SProcessCommands(process);
        //                //dt1 = DateTime.Now;
        //                process.TemplateParams.Remove(l_ParamName + "__key");
        //                process.TemplateParams.Remove(l_ParamName + "__value");
        //                string sExitType = process.sExitType ;//.GetParameterValue("__exit_type", "none");
        //                if (sExitType != "")
        //                {
        //                    if (sExitType == "loop")
        //                    {
        //                        process.sExitType ="";//.TemplateParams.Remove("__exit_type");
        //                        sErrorVerbose = sError = process.sExitValue;//.GetParameterValue("__exit_value", "");
        //                        process.sExitValue ="";//.TemplateParams.Remove("__exit_value");
        //                    }
        //                    break;
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JKEYS command";
        //        sErrorVerbose = "Error executing JKEYS command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //dt2 = DateTime.Now;
        //    //process.m_runtime.AddTimespan("JKEYS_for", dt2.Subtract(dt1).TotalMilliseconds);
        //    return sResult;
        //}
        //private string SProcess_JDATA(CProcess process)
        //{
        //    string sResult = "";
        //    string l_ParamName, l_Select, l_Conn, l_ErrorParamName;
            
        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Select = process.ReplaceParameters(m_Name);
        //    l_Conn = process.ReplaceParameters(m_Value);
        //    l_ErrorParamName = process.ReplaceParameters(m_Context);

        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    try
        //    {
        //        if (l_Conn.StartsWith("SPLITXX"))
        //            sResult = JSON_SplitXX(l_Select, l_Conn[7], l_Conn[8], l_Conn.Substring(9));
        //        else
        //            sResult = process.m_runtime.GetResponse(l_Select, l_Conn);
        //        if (l_ParamName != "")
        //        {
        //            process.TemplateParams[l_ParamName] = sResult;
        //            sResult = "";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JDATA command";
        //        sErrorVerbose = "Error executing JDATA command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return sResult;
        //}
        //private string SProcess_JSORT(CProcess process)
        //{
        //    //DateTime dt1 = DateTime.Now;
        //    //DateTime dt2;
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;

        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Source = process.ReplaceParameters(m_Name);
        //    l_JPath = process.ReplaceParameters(m_Value);
        //    l_ErrorParamName = process.ReplaceParameters(m_Context);
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    try
        //    {
        //        JArray ja;
        //        if (l_JPath == "")
        //            ja = JArray.Parse(l_Source);
        //        else
        //            ja = (JArray)(JObject.Parse(l_Source)).SelectToken(l_JPath);
        //        process.Arrays["arr_" + l_ParamName] = ja;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JSORT command";
        //        sErrorVerbose = "Error executing JSORT command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
        //        //sbRetVal.Clear();
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //dt2 = DateTime.Now;
        //    //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        //    return "";
        //}
        //private string SProcess_JARRAY(CProcess process)
        //{
        //    //DateTime dt1 = DateTime.Now;
        //    //DateTime dt2;
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;

        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Source = process.ReplaceParameters(m_Name);
        //    l_JPath = process.ReplaceParameters(m_Value);
        //    l_ErrorParamName = process.ReplaceParameters(m_Context);
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    try
        //    {
        //        JArray ja;
        //        if (l_JPath == "")
        //            ja = JArray.Parse(l_Source);
        //        else
        //            ja = (JArray)(JObject.Parse(l_Source)).SelectToken(l_JPath);
        //        process.Arrays["arr_"+l_ParamName] = ja;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JARRAY command";
        //        sErrorVerbose = "Error executing JARRAY command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
        //        //sbRetVal.Clear();
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //dt2 = DateTime.Now;
        //    //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        //    return "";
        //}
        //private string SProcess_FUNC(CProcess process)
        //{
        //    string sResult = "";
        //    string l_ErrorParamName = "ERROR";
        //    string l_ParamName = "";
        //    string l_Operation = Name;
        //    string l_Value1 = "", l_Value2 = "", l_Value3 = "", l_Value4 = "", l_Value5 = "", l_Value6 = "";

        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Value1 = process.ReplaceParameters(m_Value);
        //    l_Value2 = process.ReplaceParameters(m_Value2);
        //    l_Value3 = process.ReplaceParameters(m_Value3);
        //    l_Value4 = process.ReplaceParameters(m_ID);
        //    l_Value5 = process.ReplaceParameters(m_Opt1);
        //    l_Value6 = process.ReplaceParameters(m_Opt2);


        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    try
        //    {
        //        sResult = doFUNC(l_Operation.ToLower (),l_Value1 ,l_Value2 ,l_Value3 ,l_Value4, l_Value5,l_Value6,m_Context   );
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing FUNC " + l_Operation + " command";
        //        sErrorVerbose = "Error executing FUNC " + l_Operation + " command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }


        //    if (l_ParamName != "")
        //    {
        //        process.TemplateParams[l_ParamName] = sResult;
        //        sResult = "";
        //    }
        //    return sResult;
        //}
        //private string SProcess_FILE1(CProcess process)
        //{
        //    string sResult = "";
        //    //string l_InType = "";
        //    //string l_InVal = "";
        //    //string l_OutType = "";
        //    //string l_OutVal = "";
        //    //string m_FileID = "";

        //    //l_InType = process.ReplaceParameters(InType).ToUpper();
        //    //l_InVal = process.ReplaceParameters(InVal);
        //    //l_OutType = process.ReplaceParameters(OutType).ToUpper();
        //    //l_OutVal = process.ReplaceParameters(OutVal);
        //    //l_OutVal = Utils.Encode(l_OutVal, EncodeOption.XDOCDecode);

        //    //string sError = "";
        //    //string sErrorVerbose = "", sErrorMessage = "";
        //    //string l_ErrorParamName = "ERROR";
        //    //try
        //    //{
        //    //    switch (l_InType)
        //    //    {
        //    //        case "UPLOAD":
        //    //            ReadUploadFile(l_InVal);
        //    //            break;
        //    //        case "NFS":
        //    //            ReadNFSFile(l_InVal);
        //    //            break;
        //    //        case "URI":
        //    //            ReadURIFile(l_InVal);
        //    //            break;
        //    //        case "HTTP":
        //    //            ReadHTTPFile(l_InVal);
        //    //            break;
        //    //        case "TEMP":
        //    //            ReadNFSFile(HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_InVal);
        //    //            //ReadURIFile(".\\Attachments\\" + l_InVal);
        //    //            break;
        //    //        case "DB":
        //    //            ReadDBFile(l_InVal);
        //    //            break;
        //    //        case "BLOB":
        //    //            ReadBlobFile(l_InVal);
        //    //            break;
        //    //        case "PAR":
        //    //            ReadPARFile(l_InVal);
        //    //            break;
        //    //        case "PAR64":
        //    //            ReadPAR64File(l_InVal);
        //    //            break;
        //    //        case "DEL":
        //    //            m_FileName = "";
        //    //            m_FileExtension = "";
        //    //            m_FileContent = null;
        //    //            break;
        //    //        default:
        //    //            throw new Exception ("Invalid source type");
        //    //            break;
        //    //    }
        //    //    if (sError == "")
        //    //    {
        //    //        SetPar("FileName", m_FileName);
        //    //        SetPar("FileExtension", m_FileExtension);
        //    //        SetPar("FileContentType", m_FileContentType);


        //    //        if (l_OutType == "") { l_OutType = "PAR"; l_OutVal = "File"; }
        //    //        switch (l_OutType)
        //    //        {
        //    //            case "RESPONSE":
        //    //                string sInline = "1";
        //    //                sInline = GetPar("FileResponseInline");
        //    //                if (sInline != "0") sInline = "1";
        //    //                WriteResponseFile(l_OutVal, sInline);
        //    //                break;
        //    //            case "NFS":
        //    //                WriteNFSFile(l_OutVal);
        //    //                break;
        //    //            case "URI":
        //    //                WriteURIFile(l_OutVal);
        //    //                break;
        //    //            case "HTTP":
        //    //                WriteHTTPFile(l_OutVal);
        //    //                break;
        //    //            case "TEMP":
        //    //                WriteNFSFile(HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_OutVal);
        //    //                SetPar("FilePath", HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_OutVal);
        //    //                break;
        //    //            case "DB":
        //    //                WriteDBFile(l_OutVal);
        //    //                break;
        //    //            case "BLOB":
        //    //                WriteBlobFile(l_OutVal);
        //    //                break;
        //    //            case "PAR":
        //    //                WritePARFile(l_OutVal);
        //    //                break;
        //    //            case "PAR64":
        //    //                WritePAR64File(l_OutVal);
        //    //                break;
        //    //            default:
        //    //                throw new Exception("Invalid target type");
        //    //                break;
        //    //        }
        //    //        SetPar("FileID", m_FileID);
        //    //        SetPar("FileError", "");
        //    //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    sError = "Error executing FILE command";
        //    //    sErrorVerbose = "Error executing FILE command in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    //}
        //    //if (l_ErrorParamName != "" && sError != "")
        //    //{
        //    //    process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //    //    process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    //}
        //    sResult = "[[" + "FILE" + "]]";
        //    return sResult;
        //}
        //private string SProcess_SVSET(CProcess process)
        //{
        //    string sResult = "";
        //    string l_VarName, l_ContextName, l_ID, l_Val;

        //    l_VarName = process.ReplaceParameters(m_Name);
        //    l_ContextName = process.ReplaceParameters(m_Context);
        //    l_ID = process.ReplaceParameters(m_ID);
        //    l_Val = process.ReplaceParameters(m_Value);
        //    l_Val = Utils.Encode(l_Val, EncodeOption.XDOCDecode);
        //    if (l_ID == "ID")
        //        l_ID = process.GetParameterValue("ID", "");
        //    l_ContextName = l_ContextName + "_" + l_ID;

        //    if (Opt1 == "1")
        //    {
        //        string sPar = process.m_runtime.SVCache.GetSV(l_VarName, l_ContextName, "");
        //        if (sPar != "") return sResult;
        //    }
        //    process.m_runtime.SVCache.SetSV(l_VarName, l_ContextName, l_Val);
        //    return "";//            return sResult;
        //}
        //private string SProcess_SVGET(CProcess process)
        //{
        //    string sResult = "";
        //    string l_VarName, l_ContextName, l_ID, l_DefaultVal, l_ParamName, l_What, l_With;
            
        //    l_VarName = process.ReplaceParameters(m_Name);
        //    l_ContextName = process.ReplaceParameters(m_Context);
        //    l_ID = process.ReplaceParameters(m_ID);
        //    l_DefaultVal = process.ReplaceParameters(m_Value);
        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_What = process.ReplaceParameters(m_Value2);
        //    l_With = process.ReplaceParameters(m_Value3);
        //    if (l_ID == "ID")
        //        l_ID = process.GetParameterValue("ID", "");
        //    l_ContextName = l_ContextName + "_" + l_ID;

        //    bool bStrict = false;
        //    if (Opt2 == "1") bStrict = true;

        //    sResult = process.m_runtime.SVCache.GetSV(l_VarName, l_ContextName, l_DefaultVal);
        //    sResult = process.ReplaceSettings(sResult, l_ContextName, (Opt1 == "1"), bStrict);
        //    sResult = process.ReplaceParameters(sResult, (m_Opt1 == "1"));

        //    if (sResult == "missing_setting") sResult = l_DefaultVal;
        //    if (l_What != "") sResult = sResult.Replace(l_What, l_With);
        //    //if (ReplaceAll == "1")
        //    //{
        //    //    sVal = ReplaceSettings(sVal, "_", true,bStrict );
        //    //    sVal = process.ReplaceParameters(sVal, true);
        //    //}
        //    if (l_ParamName != "")
        //    {
        //        process.TemplateParams[l_ParamName] = sResult;
        //        sResult = "";
        //    }
        //    return sResult;
        //}
        //private string SProcess_SVDEL(CProcess process)
        //{
        //    string sResult = "";
        //    string l_VarName, l_ContextName, l_ID, l_Val;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";

            
        //    l_VarName = process.ReplaceParameters(m_Name);
        //    l_ContextName = process.ReplaceParameters(m_Context);
        //    l_ID = process.ReplaceParameters(m_ID);
        //    if (l_ID == "ID")
        //        l_ID = process.GetParameterValue("ID", "");
        //    l_ContextName = l_ContextName + "_" + l_ID;
        //    try
        //    {

        //        if (Opt1 == "1")
        //            process.m_runtime.SVCache.DelSV("", "*", true);
        //        else
        //        {
        //            if (Opt2 == "1") process.m_runtime.SVCache.DelSV(l_VarName, l_ContextName, true);
        //            else process.m_runtime.SVCache.DelSV(l_VarName, l_ContextName, false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing SVDEL command";
        //        sErrorVerbose = "Error executing SVDEL command in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //        throw new Exception(sErrorVerbose);
        //    }
        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return "";//            return sResult;
        //}
        //private string SProcess_MSG(CProcess process)
        //{
        //    string sResult = "";
        //    string l_Text, l_Level;

            
        //    l_Text = process.ReplaceParameters(m_Value);
        //    l_Level = process.ReplaceParameters(m_Name);

        //    process.m_runtime.Message(process.GetParameterValue("SESSIONID"), l_Level, l_Text);
        //    System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "MSG" + l_Level + "-" + l_Text);
        //    return "";//            return sResult;
        //}
        //private string SProcess_INCLUDEONCE(CProcess process)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Ignore;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";

        //    l_TemplateName = process.ReplaceParameters(m_Name);
        //    l_Ignore = m_Opt1;
        //    if (!(process.m_runtime.SVCache.SetIncludeOnce(l_TemplateName))) return "";
        //    try
        //    {
        //        string sWarning = "";
        //        JCmd o = process.m_runtime.AppCache.RequestTemplateTree(l_TemplateName, ref sWarning);
        //        CProcess p = new CProcess(process.m_runtime, l_TemplateName);

        //        string sExit = "";
        //        sResult = p.SRun(o, null, ref sExit);
        //        sErrorVerbose = sError = sExit;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing INCLUDEONCE command";
        //        sErrorVerbose = "Error executing INCLUDEONCE command for parameter " + l_TemplateName + " in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //        if (l_Ignore != "1") throw new Exception(sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }

        //    return sResult;
        //}
        //private string SProcess_INCLUDE(CProcess process)
        //{
        //    //DateTime dt1 = DateTime.Now;
        //    string sResult = "";
        //    string l_TemplateName, l_Ignore, l_NoParse, l_ParamName;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";
        //    //

        //    //l_TemplateName = process.ReplaceParameters(TemplateName);
        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    string sParameters = "";
        //    if ((m_Params != null) && (m_Params.Count > 0))
        //        foreach (JCmd command in m_Params)
        //    {
        //        sParameters+=command.SProcessObject(process);
        //    }
        //    Hashtable hParameters = Utils.HashtableFromQueryString(sParameters);
        //    l_TemplateName = (string)hParameters["TemplateName"];
        //    l_NoParse = (string)hParameters["NoParse"];
        //    l_Ignore = (string)hParameters["Ignore"];
        //    hParameters.Remove("TemplateName");
        //    hParameters.Remove("NoParse");
        //    hParameters.Remove("Ignore");

        //    try
        //    {
        //        if (l_NoParse == "1")
        //            sResult = process.m_runtime.AppCache.RequestTemplateText(l_TemplateName);
        //        else
        //        {
        //            string sWarning = "";
        //            JCmd o = process.m_runtime.AppCache.RequestTemplateTree(l_TemplateName, ref sWarning);
        //            CProcess p = new CProcess(process.m_runtime, l_TemplateName);
        //            string sExit = "";
        //            sResult = p.SRun(o, hParameters, ref sExit);
        //            sErrorVerbose = sError = sExit;
        //        }
        //        if (l_ParamName != "")
        //        {
        //            process.TemplateParams[l_ParamName] = sResult;
        //            sResult = "";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing INCLUDE command";
        //        sErrorVerbose = "Error executing INCLUDE command for template " + l_TemplateName + " in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //        if (l_Ignore != "1") throw new Exception(sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    //DateTime dt2 = DateTime.Now;
        //    //process.m_runtime.AddTimespan(m_Type, dt2.Subtract(dt1).TotalMilliseconds);
        //    return sResult;
        //}
        //private string SProcess_EXIT(CProcess process)
        //{
        //    string sResult = "";
        //    string l_Text, l_Level;

            
        //    l_Text = process.ReplaceParameters(m_Value);
        //    l_Level = process.ReplaceParameters(m_Name);
        //    process.sExitType =l_Level; //.TemplateParams["__exit_type"] = l_Level;
        //    process.sExitValue = l_Text;// .TemplateParams["__exit_value"] = l_Text;
        //    if (l_Level.ToLower() == "request")
        //    {
        //        process.m_runtime.ExitRequest = true;
        //        process.m_runtime.ExitValue = l_Text;
        //    }
        //    return "";//            return sResult;
        //}
        //private string SProcess_THROW(CProcess process)
        //{
        //    string sResult = "";
        //    string l_Text;

            
        //    l_Text = process.ReplaceParameters(m_Value);
        //    process.sExitType = "request"; //.TemplateParams["__exit_type"] = "request";
        //    process.sExitValue = l_Text;// .TemplateParams["__exit_value"] = l_Text;
        //    process.m_runtime.ExitRequest = true;
        //    process.m_runtime.ExitValue = l_Text;
        //    return "";//            return sResult;
        //}
        //private string SProcess_IMPORT(CProcess process)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Conn; int l_ConnID;
        //    string l_SubTemplateName, l_Ignore, l_NoParse;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";

        //    string sParameters = "";
        //    if ((m_Params != null) && (m_Params.Count > 0))
        //        foreach (JCmd command in m_Params)
        //    {
        //        sParameters+=command.SProcessObject(process);
        //    }
        //    Hashtable hParameters = Utils.HashtableFromQueryString(sParameters);
        //    l_SubTemplateName = (string)hParameters["TemplateName"];
        //    l_NoParse = (string)hParameters["NoParse"];
        //    l_Ignore = (string)hParameters["Ignore"];
        //    hParameters.Remove("TemplateName");
        //    hParameters.Remove("NoParse");
        //    hParameters.Remove("Ignore");

        //    try
        //    {
        //        if (l_NoParse == "1")
        //            sResult = process.m_runtime.AppCache.RequestTemplateText(l_SubTemplateName);
        //        else
        //        {
        //            string sWarning = "";
        //            JCmd o = process.m_runtime.AppCache.RequestTemplateTree(l_SubTemplateName, ref sWarning);
        //            StringBuilder sbInclude = new StringBuilder();
        //            sbInclude.Clear();
        //            CProcess p = new CProcess(process.m_runtime, l_SubTemplateName);
        //            string sExit = "";
        //            sResult = p.SRun(o, hParameters,  ref sExit);
        //            sErrorVerbose = sError = sExit;
        //            sResult = sbInclude.ToString();
        //        }
        //        sResult = sResult.Replace("%d_export", "%d");
        //        sResult = sResult.Replace("%dash_export", "%dash");
        //        sResult = sResult.Replace("%macro_export", "%macro");
        //        sResult = sResult.Replace("%percent_export", "%percent");
        //        sResult = sResult.Replace("_percent_export", "_percent");
        //        l_Conn = process.ReplaceParameters(m_Context);
        //        l_ConnID = int.Parse(ID);
        //        if (Opt1 == "1")
        //            l_TemplateName = "ImportSet";
        //        else
        //            l_TemplateName = process.ReplaceParameters(m_Name);

        //        if (l_ConnID == -99)
        //            sResult = process.m_runtime.Manager.ImportTemplate(l_Conn, l_TemplateName, sResult);
        //        else
        //            sResult = process.m_runtime.Manager.ImportTemplate(l_ConnID, l_TemplateName, sResult);
        //        process.m_runtime.AppCache.RemoveTemplates(l_TemplateName, sResult);
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing IMPORT command";
        //        sErrorVerbose = "Error executing IMPORT command  in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //        if (l_Ignore != "1") throw new Exception(sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return sResult;
        //}
        //private string SProcess_EXPORT(CProcess process)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Conn; int l_ConnID;
        //    string l_Encode;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";

            
        //    l_Conn = process.ReplaceParameters(m_Context);
        //    l_ConnID = int.Parse(ID);
        //    l_TemplateName = process.ReplaceParameters(m_Name);
        //    l_Encode = process.ReplaceParameters(m_Value2);

        //    try
        //    {
        //        if (l_ConnID == -99)
        //            sResult = process.m_runtime.Manager.ExportTemplate(l_Conn, l_TemplateName);
        //        else
        //            sResult = process.m_runtime.Manager.ExportTemplate(l_ConnID, l_TemplateName);
        //        sResult = Utils.Encode(sResult, l_Encode); ;
        //        sResult = sResult.Replace("%d", "%d_export");
        //        sResult = sResult.Replace("%dash", "%dash_export");
        //        sResult = sResult.Replace("%macro", "%macro_export");
        //        sResult = sResult.Replace("%percent", "%percent_export");
        //        sResult = sResult.Replace("_percent", "_percent_export");
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing EXPORT command";
        //        sErrorVerbose = "Error executing EXPORT command  in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return sResult;
        //}
        //private string SProcess_DELETE(CProcess process)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Conn; int l_ConnID;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";

            
        //    l_Conn = process.ReplaceParameters(m_Context);
        //    l_ConnID = int.Parse(ID);
        //    l_TemplateName = process.ReplaceParameters(m_Name);

        //    try
        //    {
        //        if (l_ConnID == -99)
        //            sResult = process.m_runtime.Manager.RemoveTemplate(l_Conn, l_TemplateName);
        //        else
        //            sResult = process.m_runtime.Manager.RemoveTemplate(l_ConnID, l_TemplateName);
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing DELETE command";
        //        sErrorVerbose = "Error executing DELETE command  in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return sResult;
        //}
        //private string SProcess_TRANSFER(CProcess process)
        //{
        //    string sResult = "";
        //    string l_SourceTemplateName, l_SourceConn; int l_SourceConnID;
        //    string l_TargetTemplateName, l_TargetConn; int l_TargetConnID;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";
            
        //    l_SourceConn = process.ReplaceParameters(m_Context);
        //    l_SourceConnID = int.Parse(ID);
        //    l_SourceTemplateName = process.ReplaceParameters(m_Name);
        //    l_TargetConn = process.ReplaceParameters(m_Value2);
        //    l_TargetConnID = int.Parse(Value3);
        //    l_TargetTemplateName = process.ReplaceParameters(m_Value);

        //    try
        //    {
        //        if (Opt1 == "1")
        //        {
        //            string postfix = "";
        //            if ((l_SourceConn == l_TargetConn) && (l_SourceConnID == l_TargetConnID)) postfix = "_Copy";
        //            l_TargetTemplateName = "ImportSet";
        //            if (l_SourceConnID == -99)
        //                sResult = process.m_runtime.Manager.ExportTemplatesLike(l_SourceConn, l_SourceTemplateName, postfix);
        //            else
        //                sResult = process.m_runtime.Manager.ExportTemplatesLike(l_SourceConnID, l_SourceTemplateName, postfix);
        //        }
        //        else
        //        {
        //            if (l_TargetTemplateName == "") l_TargetTemplateName = l_SourceTemplateName;
        //            if ((l_SourceConn == l_TargetConn) && (l_SourceConnID == l_TargetConnID) && (l_SourceTemplateName == l_TargetTemplateName)) l_TargetTemplateName += "_Copy";
        //            if (l_SourceConnID == -99)
        //                sResult = process.m_runtime.Manager.ExportTemplate(l_SourceConn, l_SourceTemplateName);
        //            else
        //                sResult = process.m_runtime.Manager.ExportTemplate(l_SourceConnID, l_SourceTemplateName);
        //        }
        //        if (l_TargetConnID == -99)
        //            sResult = process.m_runtime.Manager.ImportTemplate(l_TargetConn, l_TargetTemplateName, sResult);
        //        else
        //            sResult = process.m_runtime.Manager.ImportTemplate(l_TargetConnID, l_TargetTemplateName, sResult);
        //        process.m_runtime.AppCache.RemoveTemplates(l_TargetTemplateName, sResult);
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing TRANSFER command";
        //        sErrorVerbose = "Error executing TRANSFER command  in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return sResult;
        //}
        //private string SProcess_FILE(CProcess process)
        //{
        //    string sResult = "";
        //    string l_InType, l_InVal;
        //    string l_OutType, l_OutVal;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";

            
        //    l_InType = process.ReplaceParameters(m_Name);
        //    l_InVal = process.ReplaceParameters(m_Value);
        //    l_OutType = process.ReplaceParameters(m_Context);
        //    l_OutVal = process.ReplaceParameters(m_ID);

        //    try
        //    {
        //        sResult = "[[FILE." + l_InType + l_InVal + l_OutType + l_OutVal + "]]";
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing FILE command";
        //        sErrorVerbose = "Error executing FILE command  in template " + process.m_templateName + " at line + " + m_Line +           ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //    return sResult;
        //}
        //private string SProcess_USEPARAMETERS(CProcess process)
        //{
        //    string sResult = "";
        //    process.m_UseGlobalParams = true;
        //    return "";//            return sResult;
        //}
        //internal string SProcessCommands(CProcess process)
        //{
        //    string sResult = "";
        //    if ((m_Commands != null) && (m_Commands.Count > 0))
        //    {
        //        foreach (JCmd command in m_Commands)
        //        {
        //            sResult += command.SProcessObject(process);
        //            string sExitType = process.sExitType;// .GetParameterValue("__exit_type", "none");
        //            if (sExitType != "")
        //            {
        //                break;
        //            }
        //            if (process.m_runtime.ExitRequest) break;

        //        }
        //    }
        //    return sResult;
        //}

#endregion
        #region StringBuilder

        private void Process_default(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            //sResult = "[[" + m_Type + "]]";
            //if (sb != null) sb.Append(sResult);
            StringBuilder sb1 = new StringBuilder();
            DisplayCommand(sb1);
            System.Diagnostics.Debug.WriteLine("deprecated command:\n" + sb1.ToString());
        }
        private void Process_CFG(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_CfgClass, l_CfgName, l_CfgDefault, l_Encode;
            string l_ParamName = "";
            l_CfgClass = process.ReplaceParameters(m_Context);
            l_CfgName = process.ReplaceParameters(m_Name);
            l_CfgDefault = process.ReplaceParameters(m_Value);
            l_Encode = process.ReplaceParameters(m_Value2);
            l_ParamName = process.ReplaceParameters(m_ParamName);

            sResult = process.m_runtime.AppCache.GetCfg(l_CfgClass + "__" + l_CfgName, l_CfgDefault);
            sResult = process.ReplaceParameters(sResult);
            if (l_Encode!="") sResult = Utils.Encode(sResult, l_Encode);

            if (l_ParamName == "")
            {
                if (sb != null) sb.Append(sResult);
            }
            else
            {
                process.TemplateParams[l_ParamName] = sResult;
                sResult = "";
            }
        }
        private void Process_DEFPAR(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            string l_strParamName, l_strValue;
            
            l_strParamName = process.ReplaceParameters(m_ParamName);
            l_strValue = process.ReplaceParameters(m_Value);
            if (l_strParamName != "") process.DefaultParams[l_strParamName] = l_strValue;
        }
        private void Process_SPAR(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_strParamName, l_Value;
            
            l_strParamName = process.ReplaceParameters(m_ParamName);
            if (m_Opt2 == "1")
            {
                l_Value = process.ReplaceParameters(m_Value, true);
                l_Value = process.ReplaceParameters(l_Value, true);
                l_Value = process.ReplaceParameters(l_Value, true);
            }
            else
                l_Value = process.ReplaceParameters(m_Value);
            if (m_Opt1 == "1")
                l_Value = process.GetParameterValue(l_Value);
            if (l_strParamName == "")
            {
                sResult = l_Value;
                if (sb != null) sb.Append(sResult);
            }
            else
                process.TemplateParams[l_strParamName] = l_Value;
        }
        private void Process_TEXT(CProcess process, StringBuilder sb)
        {
            //start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])+?((start(?:(?!start)[^.])*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end)(?:(?!start)[^.])*?)*?end

            //Regex regexMacro = new Regex(@"\#MACRO\..*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)\#", RegexOptions.IgnoreCase);
            //MatchCollection mc = regexMacro.Matches(s_text);
            //for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            //{
            //    Match m = mc[matchIndex];
            //    string sResult = "";
            //    s_text = s_text.Replace(m.Value, sResult);
            //}

            string sResult = "";
            string sText = m_Value;
            string sTextU = sText.ToUpper();
            int i1 = sTextU.IndexOf("#STARTBLOCK#");
            int j1 = sTextU.IndexOf("#ENDBLOCK#");
            if ((i1 >= 0) && (j1 >= 0) && (i1 < j1))
                sText = sText.Substring(i1 + "#STARTBLOCK#".Length, j1 - i1 - "#STARTBLOCK#".Length);
            i1 = sTextU.IndexOf("#STARTCOMMENT#");
            j1 = sTextU.IndexOf("#ENDCOMMENT#");
            if ((i1 >= 0) && (j1 >= 0) && (i1 < j1))
                sText = sText.Substring(0, i1) + sText.Substring(j1 + "#ENDCOMMENT#".Length);
            sResult = sText;
            if (!process.noDash)
            {
                sResult = sResult.Replace("%d%", "#");
                sResult = sResult.Replace("%dash%", "#");
                sResult = sResult.Replace("%macro%", "@@");
                sResult = sResult.Replace("%at%", "@");
                sResult = sResult.Replace("%percent%", "%");
                sResult = sResult.Replace("_percent_", "%");
            }
            if (process.sVersion == "3") sResult = sResult.Replace("\t", "");
            while (sResult.EndsWith("\t"))
                sResult = sResult.Substring(0, sResult.Length - 1);
            if ((sResult!="\r\n")&&(sResult.StartsWith("\r\n")))
                sResult = sResult.Substring(2);
            if (sb != null) sb.Append(sResult);
        }
        private void Process_IIF(CProcess process, StringBuilder sb)
        {
            //DateTime dt1 = DateTime.Now;
            string sResult = "";
            string  l_trueExpr, l_falseExpr, l_ParamName;
            bool condResult = true;
            if (m_ID != "")
            {
                //DateTime dt3 = DateTime.Now;
                //DateTime dt4;
                //string l_condition = process.ReplaceParameters(m_ID);
                //bLoop = EvalCondition(l_condition, process.m_runtime.DataProvider);
                //string l_Opt1 = process.ReplaceParameters(m_Opt1);
                //string l_Opt2 = process.ReplaceParameters(m_Opt2);
                string l_Opt1 = process.ReplaceParameters(m_Opt1,true);
                string l_Opt2 = process.ReplaceParameters(m_Opt2,true);
                //DateTime dt2 = DateTime.Now;
                //process.m_runtime.AddTimespan("IIF_CondS", dt2.Subtract(dt1).TotalMilliseconds);
                if (m_ID == "SQL")
                    condResult = EvalConditionSQL(l_Opt1);
                else
                    condResult = myEvalCondition(l_Opt1, l_Opt2);
                //DateTime dt3 = DateTime.Now;
                //process.m_runtime.AddTimespan("IIF_EvalCondS", dt3.Subtract(dt2).TotalMilliseconds);
            }

            //l_condition = process.ReplaceParameters(m_Value);
            //bool condResult = EvalCondition(l_condition, process.m_runtime.DataProvider);
            if (condResult == true)
            {
                l_trueExpr = process.ReplaceParameters(m_Value2);
                sResult = l_trueExpr;
            }
            else
            {
                l_falseExpr = process.ReplaceParameters(m_Value3);
                sResult = l_falseExpr;
            }
            l_ParamName = process.ReplaceParameters(m_ParamName);
            if (l_ParamName == "")
            {
                if (sb != null) sb.Append(sResult);
            }
            else
            {
                process.TemplateParams[l_ParamName] = sResult;
            }
            //DateTime dt4 = DateTime.Now;
            //process.m_runtime.AddTimespan("IIF", dt4.Subtract(dt1).TotalMilliseconds);
        }
        private void Process_IF(CProcess process, StringBuilder sb)
        {
            //DateTime dt1 = DateTime.Now;
            //string sResult = "";
            string l_condition = "";
            bool condResult=true;

            if (m_Name == "IFIP")
            {
                //string l_Opt1 = process.ReplaceParameters(m_Opt1);
                //string l_Opt2 = process.ReplaceParameters(m_Opt2);
                string l_Opt1 = process.ReplaceParameters(m_Opt1, true);
                string l_Opt2 = process.ReplaceParameters(m_Opt2, true);
                //DateTime dt2 = DateTime.Now;
                //process.m_runtime.AddTimespan("IIF_CondS", dt2.Subtract(dt1).TotalMilliseconds);
                    if (m_ID == "SQL")
                    condResult = EvalConditionSQL(l_Opt1);
                else
                    condResult = myEvalCondition(l_Opt1, l_Opt2);
            }
            else
            {
                if (m_Name == "IFI")
                    l_condition = m_Value;
                else
                {
                    StringBuilder sbCond = new StringBuilder();
                    sbCond.Clear();
                    if ((m_Params != null) && (m_Params.Count > 0))
                        foreach (JCmd command in m_Params)
                        {
                            command.ProcessObject(process, sbCond);
                        }
                    l_condition = sbCond.ToString();
                }
                l_condition = process.ReplaceParameters(l_condition);
//                l_condition = process.ReplaceParameters(l_condition,true);

                //DateTime dt2 = DateTime.Now;
                //process.m_runtime.AddTimespan("IF_CondS", dt2.Subtract(dt1).TotalMilliseconds);

                condResult = EvalCondition(l_condition);

                //DateTime dt3 = DateTime.Now;
                //process.m_runtime.AddTimespan("IF_EvalCondS", dt3.Subtract(dt2).TotalMilliseconds);
            }
            if (condResult == true)
                ProcessCommands(process, sb);
            else if (condResult == false && m_ElseIf != null)
            {
                m_ElseIf.ProcessObject(process, sb);
            }
            //DateTime dt4 = DateTime.Now;
            //process.m_runtime.AddTimespan("IF", dt4.Subtract(dt1).TotalMilliseconds);
        }
        private void Process_PAR(CProcess process, StringBuilder sb)
        {
            //DateTime dt1 = DateTime.Now;
            string sResult = "";
            string l_ParamName, l_Format, l_Encode;
            //l_ParamName = process.ReplaceParameters(m_ParamName);
            //l_Format = process.ReplaceParameters(m_Value3);
            //l_Encode = process.ReplaceParameters(m_Value2);
            l_ParamName = m_ParamName;
            l_Format = m_Value3;
            l_Encode = m_Value2;

            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";
            try
            {
                //sResult = process.GetParameterValue(l_ParamName);
                //if (l_Format != "")
                //    sResult = Utils.ApplyFormat(sResult, l_Format);
                //sResult = Utils.Encode(sResult, l_Encode);
                sResult = process.GetParameterValue(l_ParamName, "", l_Format, l_Encode);
            }
            catch (Exception ex)
            {
                sError = "Error executing PAR command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing PAR command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            if (sb != null) sb.Append(sResult);
            //DateTime dt2 = DateTime.Now;
            //process.m_runtime.AddTimespan(m_Type, dt2.Subtract(dt1).TotalMilliseconds);
        }
        private void Process_QRY(CProcess process, StringBuilder sb)
        {
            DisplayCommand(sb);
            //string sResult = "";
            //sResult = "{";
            //sResult += "\"Line\":\"" + Line + "\"";
            //sResult += ",\"Type\":\"" + Type + "\"";
            //sResult += ",\"ParamName\":\"" + ParamName + "\"";
            //sResult += ",\"Name\":\"" + Name + "\"";
            //sResult += ",\"Value\":\"" + Value + "\"";
            //sResult += ",\"Context\":\"" + Context + "\"";
            //sResult += ",\"ID\":\"" + ID + "\"";
            //sResult += ",\"Opt1\":\"" + Opt1 + "\"";
            //sResult += ",\"Opt2\":\"" + Opt2 + "\"";
            //sResult += ",\"Value2\":\"" + Value2 + "\"";
            //sResult += ",\"Value3\":\"" + Value3 + "\"";
            //sResult += ",\"Value2\":\"" + Value2 + "\"";
            //sResult += "}";
            //if (sb != null) sb.Append(sResult);
        }
        private void Process_XPATH(CProcess process, StringBuilder sb)
        {
            //XPath_GetDataTable

            //#XPATH.qname(source,path,1/0)#
            //#FUNC.qname_source.xml2json..source#
            //#FUNC.qname_path.regex_replace.\/.\\.#
            //#JLOOP.qname.qname_source.qname_path[0]#
            //            or
            //#JLOOP.qname.qname_source.qname_path#

            string sResult = "";
            string l_ParamName, l_Source, l_JPath, l_PageSize, l_PageNr, l_ErrorParamName;

            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_PageSize = process.ReplaceParameters(m_Value2);
            l_PageNr = process.ReplaceParameters(m_Value3);
            l_ErrorParamName = process.ReplaceParameters(m_Context);

            int i_PageSize = 10;
            int i_PageNr = 1;
            int.TryParse(l_PageSize, out i_PageSize);
            int.TryParse(l_PageNr, out i_PageNr);
            if (i_PageSize == 0) i_PageSize = 100001;
            if (i_PageNr == 0) i_PageNr = 1;
            int start = (i_PageNr - 1) * i_PageSize;
            int end = i_PageNr * i_PageSize;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            int jaCount = i_PageSize;// 100001;
            bool bWhile = false;
            JToken jt = null;
            //System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();
            try
            {
                bool _isFirst = true;
                JArray ja = null;

                l_Source = "[]";
                l_JPath = "";
                if ((m_Params != null) && (m_Params.Count > 2))
                {
                    //#FUNC.qname_source.xml2json..source#
                    //#FUNC.qname_path.regex_replace.\/.\\.#
                    StringBuilder sbParams = new StringBuilder();
                    sbParams.Clear();
                    m_Params[0].ProcessObject(process, sbParams);
                    l_Source = sbParams.ToString();
                    l_Source = doFUNC("xml2json", l_Source, "", "", "", "", "", "", process);
                    sbParams.Clear();
                    m_Params[1].ProcessObject(process, sbParams);
                    l_JPath = sbParams.ToString();
                    l_JPath = l_JPath.Replace("/", ".");
                    sbParams.Clear();
                    m_Params[2].ProcessObject(process, sbParams);
                    if (sbParams.ToString() == "1")
                        l_JPath = l_JPath + "[0]";
                }
                ja = SelectJSONArray(l_Source, l_JPath);
                jaCount = ja.Count;
                for (int i = start; (i < end && i < jaCount) || (jaCount == -1); i++)
                {
                    process.TemplateParams[l_ParamName + "__count"] = jaCount;
                    process.TemplateParams[l_ParamName + "__fetchID"] = (i - start).ToString();
                    process.TemplateParams[l_ParamName + "__fetchID1"] = (i - start + 1).ToString();
                    process.TemplateParams[l_ParamName + "__oddEven"] = (i - start) % 2 == 1 ? "1" : "0";
                    process.TemplateParams[l_ParamName + "__isFirst"] = (_isFirst) ? "1" : "0";
                    process.TemplateParams[l_ParamName + "__firstRow"] = (i == start) ? "1" : "0";
                    process.TemplateParams[l_ParamName + "__lastRow"] = ((i == end - 1) || (i == jaCount - 1)) ? "1" : "0";
                    if (bWhile)
                    {
                        process.TemplateParams[l_ParamName] = i.ToString();
                    }
                    else
                    {
                        jt = ja[i];
                        process.TemplateParams[l_ParamName] = jt.ToString();
                        process.TemplateParams[l_ParamName + "_XML"] = jt.ToString();
                        foreach (JProperty jp in jt.Children<JProperty>())
                            if (jp.Value.Type == JTokenType.Null)
                            {
                                process.TemplateParams[l_ParamName + "_" + jp.Name] = "<null>";
                            }
                            else if (jp.Value.Type == JTokenType.Date)
                            {
                                DateTime d = (DateTime)jp.Value;
                                process.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                            }
                            else if (jp.Value.Type == JTokenType.Array)
                                process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString(Newtonsoft.Json.Formatting.None);
                            else
                                process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString();
                    }
                    bool bLoop = true;
                    if (bLoop == true)
                    {
                        _isFirst = false;
                        ProcessCommands(process, sb);
                    }
                    if (!bWhile)
                    {
                        foreach (JProperty jp in jt.Children<JProperty>())
                            process.TemplateParams.Remove(l_ParamName + "_" + jp.Name);
                        process.TemplateParams.Remove(l_ParamName + "_XML");
                    }

                    string sExitType = process.sExitType;// GetParameterValue("__exit_type", "none");
                    if (sExitType != "")
                    {
                        if (sExitType == "loop")
                        {
                            process.sExitType = "";//.TemplateParams.Remove("__exit_type");
                            sErrorVerbose = sError = process.sExitValue; //.GetParameterValue("__exit_value", "");
                            process.sExitValue = "";//.TemplateParams.Remove("__exit_value");
                        }
                        break;
                    }

                }
            }
            catch (Exception ex)
            {
                sError = "Error executing XPATH command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing XPATH command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
                //sbRetVal.Clear();
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            //dt2 = DateTime.Now;
            //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        }
        private void Process_JLOOPfe(CProcess process, StringBuilder sb)
        {
            //foreach (JToken j in ja)
            //{
            //    DataRow dr = ret.NewRow();
            //    foreach (string f in fa)
            //    {
            //        if (f != "") if (j.SelectToken(f) != null) dr[f] = j.SelectToken(f).ToString();
            //    }
            //    dr["JSON"] = j.ToString();
            //    ret.Rows.Add(dr);
            //}
            // #JARRAY.arr.fields.json.path.err.size.nr#
            // #LOOP.i.cond.arr.fields.err,size,nr#


            //DateTime dt1 = DateTime.Now;
            //DateTime dt2;
            //string sResult = "";
            string l_ParamName, l_Source, l_JPath, l_PageSize, l_PageNr, l_ErrorParamName;

            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_PageSize = process.ReplaceParameters(m_Value2);
            l_PageNr = process.ReplaceParameters(m_Value3);
            l_ErrorParamName = process.ReplaceParameters(m_Context);

            int _i = 0;
            int i_PageSize = 10;
            int i_PageNr = 1;
            int.TryParse(l_PageSize, out i_PageSize);
            int.TryParse(l_PageNr, out i_PageNr);
            if (i_PageSize == 0) i_PageSize = 100001;
            if (i_PageNr == 0) i_PageNr = 1;
            int start = (i_PageNr - 1) * i_PageSize;
            int end = i_PageNr * i_PageSize;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            int jaCount = 100001;
            bool bWhile = false;
            //JToken jt = null;
            //System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();
            try
            {
                bool _isFirst = true;
                JArray ja = null;
                string sArrayName = "";
                if (l_ErrorParamName.Contains("|"))
                {
                    sArrayName = l_ErrorParamName.Substring(l_ErrorParamName.IndexOf("|") + 1);
                    l_ErrorParamName = l_ErrorParamName.Substring(0, l_ErrorParamName.IndexOf("|"));
                }
                if (sArrayName != "")
                {
                    ja = (JArray)process.Arrays[sArrayName];
                    if (ja != null) jaCount = ja.Count;
                }
                if (ja == null)
                {
                    l_Source = process.ReplaceParameters(m_Name);
                    l_JPath = process.ReplaceParameters(m_Value);
                    if (l_Source.ToLower() == "while")
                    {
                        bWhile = true;
                    }
                    else
                    {
                        ja = SelectJSONArray(l_Source, l_JPath);
                        if (sArrayName != "")
                            process.Arrays[sArrayName] = ja;
                        jaCount = ja.Count;
                    }
                }
                //DateTime dta = DateTime.Now;
                //process.m_runtime.AddTimespan("IF_ParseArray", dta.Subtract(dt1).TotalMilliseconds);
                //JToken[] jta = null;
                //Array jaa;
                //ja.CopyTo(jta, start);
                int i = start;
                foreach (JToken jt in ja)
                {//                for (int i = start; i < end && i < jaCount; i++)
                    if (i >= start)
                    {
                        process.TemplateParams[l_ParamName + "__count"] = jaCount;
                        process.TemplateParams[l_ParamName + "__fetchID"] = (i - start).ToString();
                        process.TemplateParams[l_ParamName + "__fetchID1"] = (i - start + 1).ToString();
                        process.TemplateParams[l_ParamName + "__oddEven"] = (i - start) % 2 == 1 ? "1" : "0";
                        process.TemplateParams[l_ParamName + "__isFirst"] = (_isFirst) ? "1" : "0";
                        process.TemplateParams[l_ParamName + "__firstRow"] = (i == start) ? "1" : "0";
                        process.TemplateParams[l_ParamName + "__lastRow"] = ((i == end - 1) || (i == jaCount - 1)) ? "1" : "0";
                        //DateTime dtp = DateTime.Now;
                        if (bWhile)
                        {
                            process.TemplateParams[l_ParamName] = i.ToString();
                        }
                        else
                        {
                            //                        jt = ja[i];
                            process.TemplateParams[l_ParamName] = jt.ToString();
                            foreach (JProperty jp in jt.Children<JProperty>())
                                if (jp.Value.Type == JTokenType.Null)
                                {
                                    process.TemplateParams[l_ParamName + "_" + jp.Name] = "<null>";
                                }
                                else if (jp.Value.Type == JTokenType.Date)
                                {
                                    DateTime d = (DateTime)jp.Value;
                                    process.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                }
                                else if (jp.Value.Type == JTokenType.Array)
                                    process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString(Newtonsoft.Json.Formatting.None);
                                else
                                    process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString();
                        }
                        //dt2 = DateTime.Now;
                        //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
                        //process.m_runtime.AddTimespan("LOOP_propset", dt2.Subtract(dtp).TotalMilliseconds);
                        bool bLoop = true;
                        if (m_ID != "")
                        {
                            //DateTime dt3 = DateTime.Now;
                            //DateTime dt4;
                            //string l_condition = process.ReplaceParameters(m_ID);
                            //bLoop = EvalCondition(l_condition, process.m_runtime.DataProvider);
                            //string l_Opt1 = process.ReplaceParameters(m_Opt1);
                            //string l_Opt2 = process.ReplaceParameters(m_Opt2);
                            string l_Opt1 = process.ReplaceParameters(m_Opt1, true);
                            string l_Opt2 = process.ReplaceParameters(m_Opt2, true);
                            if (m_ID == "SQL")
                                bLoop = EvalConditionSQL(l_Opt1);
                            else
                                bLoop = myEvalCondition(l_Opt1, l_Opt2);
                            //dt4 = DateTime.Now;
                            //process.m_runtime.AddTimespan("LOOP_connd", dt4.Subtract(dt3).TotalMilliseconds);
                        }
                        if (bLoop == true)
                        {
                            _isFirst = false;
                            ProcessCommands(process, sb);
                            _i++;
                        }
                        //dt1 = DateTime.Now;
                        if (!bWhile)
                            foreach (JProperty jp in jt.Children<JProperty>())
                                process.TemplateParams.Remove(l_ParamName + "_" + jp.Name);
                        //dtp = DateTime.Now;
                        //process.m_runtime.AddTimespan("LOOP_propclean", dtp.Subtract(dt1).TotalMilliseconds);

                        string sExitType = process.sExitType;// GetParameterValue("__exit_type", "none");
                        if (sExitType != "")
                        {
                            if (sExitType == "loop")
                            {
                                process.sExitType = "";//.TemplateParams.Remove("__exit_type");
                                sErrorVerbose = sError = process.sExitValue; //.GetParameterValue("__exit_value", "");
                                process.sExitValue = "";//.TemplateParams.Remove("__exit_value");
                            }
                            break;
                        }
                        //i++;
                        if (_i >= end) break;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JLOOP command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing JLOOP command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
                //sbRetVal.Clear();
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            //dt2 = DateTime.Now;
            //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        }

        private void Process_JLOOP(CProcess process, StringBuilder sb)
        {
            //foreach (JToken j in ja)
            //{
            //    DataRow dr = ret.NewRow();
            //    foreach (string f in fa)
            //    {
            //        if (f != "") if (j.SelectToken(f) != null) dr[f] = j.SelectToken(f).ToString();
            //    }
            //    dr["JSON"] = j.ToString();
            //    ret.Rows.Add(dr);
            //}
            // #JARRAY.arr.fields.json.path.err.size.nr#
            // #LOOP.i.cond.arr.fields.err,size,nr#


            //DateTime dt1 = DateTime.Now;
            //DateTime dt2;
            //string sResult = "";
            string l_ParamName, l_Source, l_JPath, l_PageSize, l_PageNr, l_ErrorParamName;
            
            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_PageSize = process.ReplaceParameters(m_Value2);
            l_PageNr = process.ReplaceParameters(m_Value3);
            l_ErrorParamName = process.ReplaceParameters(m_Context);

            int _i = 0;
            int i_PageSize = 10;
            int i_PageNr = 1;
            int.TryParse(l_PageSize, out i_PageSize);
            int.TryParse(l_PageNr, out i_PageNr);
            if (i_PageSize == 0) i_PageSize = 100001;
            if (i_PageNr == 0) i_PageNr = 1;
            int start = (i_PageNr - 1) * i_PageSize;
            int end = i_PageNr * i_PageSize;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            int jaCount = 100001;
            bool bWhile = false;
            JToken jt=null;
            //System.Text.StringBuilder sbRetVal = new System.Text.StringBuilder();
            try
            {
                bool _isFirst = true;
                JArray ja = null;
                string sArrayName = "";
                if (l_ErrorParamName.Contains("|"))
                {
                    sArrayName = l_ErrorParamName.Substring(l_ErrorParamName.IndexOf("|") + 1);
                    l_ErrorParamName = l_ErrorParamName.Substring(0, l_ErrorParamName.IndexOf("|"));
                }
                if (sArrayName != "")
                {
                    ja = (JArray)process.Arrays[sArrayName];
                    if (ja!= null) jaCount = ja.Count;
                }
                if (ja == null)
                {
                    l_Source = process.ReplaceParameters(m_Name);
                    l_JPath = process.ReplaceParameters(m_Value);
                    if (l_Source.ToLower() == "while")
                    {
                        bWhile = true;
                        if (m_ID == "")
                        {
                            jaCount = int.MaxValue;
                            end = int.MaxValue;
                        }
                    }
                    else
                    {
                        ja = SelectJSONArray(l_Source, l_JPath);
                        if (sArrayName != "")
                            process.Arrays[sArrayName] = ja;
                        jaCount = ja.Count;
                    }
                }
                //DateTime dta = DateTime.Now;
                //process.m_runtime.AddTimespan("IF_ParseArray", dta.Subtract(dt1).TotalMilliseconds);
                for (int i = start; _i < end && i < jaCount; i++)
                {
                    process.TemplateParams[l_ParamName + "__count"] = jaCount;
                    process.TemplateParams[l_ParamName + "__fetchID"] = (i - start).ToString();
                    process.TemplateParams[l_ParamName + "__fetchID1"] = (i - start + 1).ToString();
                    process.TemplateParams[l_ParamName + "__oddEven"] = (i - start) % 2 == 1 ? "1" : "0";
                    process.TemplateParams[l_ParamName + "__isFirst"] = (_isFirst) ? "1" : "0";
                    process.TemplateParams[l_ParamName + "__firstRow"] = (i == start) ? "1" : "0";
                    process.TemplateParams[l_ParamName + "__lastRow"] = ((i == end - 1) || (i == jaCount - 1)) ? "1" : "0";
                    //DateTime dtp = DateTime.Now;
                    if (bWhile)
                    {
                        process.TemplateParams[l_ParamName] = i.ToString();
                    }
                    else
                    {
                        jt = ja[i];
                        process.TemplateParams[l_ParamName] = jt.ToString();
                        foreach (JProperty jp in jt.Children<JProperty>())
                            if (jp.Value.Type == JTokenType.Null)
                            {
                                process.TemplateParams[l_ParamName + "_" + jp.Name] = "<null>";
                            }
                            else if (jp.Value.Type == JTokenType.Date)
                            {
                                DateTime d = (DateTime)jp.Value;
                                process.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                            }
                            else if (jp.Value.Type == JTokenType.Array)
                                process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString(Newtonsoft.Json.Formatting.None);
                            else
                                process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString();
                    }
                    //dt2 = DateTime.Now;
                    //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
                    //process.m_runtime.AddTimespan("LOOP_propset", dt2.Subtract(dtp).TotalMilliseconds);
                    bool bLoop = true;
                    if (m_ID != "")
                    {
                        //DateTime dt3 = DateTime.Now;
                        //DateTime dt4;
                        //string l_condition = process.ReplaceParameters(m_ID);
                        //bLoop = EvalCondition(l_condition, process.m_runtime.DataProvider);
                        //string l_Opt1 = process.ReplaceParameters(m_Opt1);
                        //string l_Opt2 = process.ReplaceParameters(m_Opt2);
                        string l_Opt1 = process.ReplaceParameters(m_Opt1,true);
                        string l_Opt2 = process.ReplaceParameters(m_Opt2,true);
                        if (m_ID == "SQL")
                            bLoop = EvalConditionSQL(l_Opt1);
                        else
                            bLoop = myEvalCondition(l_Opt1, l_Opt2);
                        //dt4 = DateTime.Now;
                        //process.m_runtime.AddTimespan("LOOP_connd", dt4.Subtract(dt3).TotalMilliseconds);
                    }
                    if (bLoop == true)
                    {
                        _isFirst = false;
                        ProcessCommands(process, sb);
                        _i++;
                    }
                    //dt1 = DateTime.Now;
                    if (!bWhile)
                        foreach (JProperty jp in jt.Children<JProperty>())
                            process.TemplateParams.Remove(l_ParamName + "_" + jp.Name);
                    //dtp = DateTime.Now;
                    //process.m_runtime.AddTimespan("LOOP_propclean", dtp.Subtract(dt1).TotalMilliseconds);

                    string sExitType = process.sExitType;// GetParameterValue("__exit_type", "none");
                    if (sExitType != "")
                    {
                        if (sExitType == "loop")
                        {
                            process.sExitType = "";//.TemplateParams.Remove("__exit_type");
                            sErrorVerbose = sError = process.sExitValue; //.GetParameterValue("__exit_value", "");
                            process.sExitValue = "";//.TemplateParams.Remove("__exit_value");
                        }
                        break;
                    }

                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JLOOP command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing JLOOP command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
                //sbRetVal.Clear();
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            //dt2 = DateTime.Now;
            //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        }
        private void Process_JSORT(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_ParamName, l_Source, l_Sort, l_ErrorParamName;

            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_Source = process.ReplaceParameters(m_Name);
            l_Sort = process.ReplaceParameters(m_Value);
            l_ErrorParamName = process.ReplaceParameters(m_Context);
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            try
            {
                DataTable dt = (DataTable)JsonConvert.DeserializeObject<DataTable>(l_Source);
                DataView dv = dt.DefaultView;
                dv.Sort = l_Sort;
                DataTable sortedDT = dv.ToTable();
                sResult = Newtonsoft.Json.JsonConvert.SerializeObject(sortedDT);
                if (l_ParamName == "")
                {
                    if (sb != null) sb.Append(sResult);
                }
                else
                {
                    process.TemplateParams[l_ParamName] = sResult;
                    sResult = "";
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JSORT command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing JSORT command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
                //sbRetVal.Clear();
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            //dt2 = DateTime.Now;
            //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        }

        private void Process_JSET(CProcess process, StringBuilder sb)
        {
            if (m_Opt1 == "SORT")
            {
                Process_JSORT(process, sb);
                return;
            }
            //string sResult = "";
            string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
            string l_JOP, l_JObj, l_JAttr;


            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_Source = process.ReplaceParameters(m_Name);
            l_JPath = process.ReplaceParameters(m_Value);
            l_JOP = process.ReplaceParameters(m_Opt1);
            l_JObj = process.ReplaceParameters(m_Value2);
            l_JAttr = process.ReplaceParameters(m_Value3);
            l_ErrorParamName = "ERROR";

            string val = "";
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            try
            {
                if (l_JOP == "APP")
                {
                    if (l_JPath == "")
                    {
                        JArray jt = SelectJSONArray(l_Source, l_JPath);
                        int index = -1;
                        if (int.TryParse(l_JAttr, out index))
                            (jt as JArray).Insert(index, JToken.Parse(l_JObj));
                        else
                            (jt as JArray).Add(JToken.Parse(l_JObj));

                        val = jt.ToString();
                    }
                    else
                    {
                        //JObject o = JObject.Parse(l_Source);
                        JToken o = JToken.Parse(l_Source);
                        JToken jt = o.SelectToken(l_JPath);
                        int index = -1;
                        if (int.TryParse(l_JAttr, out index))
                            (jt as JArray).Insert(index, JToken.Parse(l_JObj));
                        else
                            (jt as JArray).Add(JToken.Parse(l_JObj));

                        val = o.ToString();
                    }
                }
                else
                {
                    //JObject o = JObject.Parse(l_Source);
                    //JToken jt = o.SelectToken(l_JPath);
                    //JToken o = SelectJSONToken(l_Source, "", "");
                    //JToken jt = SelectJSONToken(l_Source, l_JPath, "");
                    JToken o = JSONParse(l_Source,  "");
                    JToken jt = JSONSelect(o, l_JPath);

                    if (l_JOP == "DEL")
                    {
                        if (jt != null)
                            if (jt.Parent is JArray)
                                (jt.Parent as JArray).Remove(jt);
                            else
                                jt.Parent.Remove();
                        val = o.ToString();
                    }
                    else if (l_JOP == "UPD")
                    {
                        jt.Replace(JToken.Parse(l_JObj));
                        val = o.ToString();
                    }
                    else if (l_JOP == "INS")
                    {
                        try
                        {
                            (jt as JObject).Add(l_JAttr, JToken.Parse(l_JObj));
                        }
                        catch (Exception addex)
                        {
                            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "JINS error" + "-" + addex.Message);
                            //process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
                        }
                        val = o.ToString();
                    }
                }

                if (l_ParamName == "")
                {
                    if (sb != null) sb.Append(val);
                }
                else
                {
                    //sResult = "";
                    process.TemplateParams[l_ParamName] = val;
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing J" + l_JOP + " command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing J" + l_JOP + " command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }
        //private void Process_JSET(CProcess process, StringBuilder sb)
        //{
        //    if (m_Opt1 == "SORT")
        //    {
        //        Process_JSORT(process, sb);
        //        return;
        //    }
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
        //    string l_JOP, l_JObj, l_JAttr;


        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Source = process.ReplaceParameters(m_Name);
        //    l_JPath = process.ReplaceParameters(m_Value);
        //    l_JOP = process.ReplaceParameters(m_Opt1);
        //    l_JObj = process.ReplaceParameters(m_Value2);
        //    l_JAttr = process.ReplaceParameters(m_Value3);
        //    l_ErrorParamName = "ERROR";

        //    string val = "";
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    try
        //    {
        //        if (l_JOP == "APP")
        //        {
        //            if (l_JPath == "")
        //            {
        //                JArray jt = SelectJSONArray(l_Source, l_JPath);
        //                int index = -1;
        //                if (int.TryParse(l_JAttr, out index))
        //                    (jt as JArray).Insert(index, JToken.Parse(l_JObj));
        //                else
        //                    (jt as JArray).Add(JToken.Parse(l_JObj));

        //                val = jt.ToString();
        //            }
        //            else
        //            {
        //                //JObject o = JObject.Parse(l_Source);
        //                JToken o = JToken.Parse(l_Source);
        //                JToken jt = o.SelectToken(l_JPath);
        //                int index = -1;
        //                if (int.TryParse(l_JAttr, out index))
        //                    (jt as JArray).Insert(index, JToken.Parse(l_JObj));
        //                else
        //                    (jt as JArray).Add(JToken.Parse(l_JObj));

        //                val = o.ToString();
        //            }
        //        }
        //        else if (l_JOP == "DEL1")
        //        {
        //            if (l_JPath == "")
        //            {
        //                JArray jt = SelectJSONArray(l_Source, l_JPath);
        //                int index = -1;
        //                if (int.TryParse(l_JAttr, out index))
        //                    (jt as JArray).Insert(index, JToken.Parse(l_JObj));
        //                else
        //                    (jt as JArray).Add(JToken.Parse(l_JObj));

        //                val = jt.ToString();
        //            }
        //            else
        //            {
        //                //JObject o = JObject.Parse(l_Source);
        //                JToken o = JToken.Parse(l_Source);
        //                JToken jt = o.SelectToken(l_JPath);
        //                int index = -1;
        //                if (int.TryParse(l_JAttr, out index))
        //                    (jt as JArray).Insert(index, JToken.Parse(l_JObj));
        //                else
        //                    (jt as JArray).Add(JToken.Parse(l_JObj));

        //                val = o.ToString();
        //            }
        //        }
        //        else
        //        {
        //            //JObject o = JObject.Parse(l_Source);
        //            //JToken jt = o.SelectToken(l_JPath);
        //            JToken o = SelectJSONToken(l_Source, "", "");
        //            //JToken jt = SelectJSONToken(l_Source, l_JPath, "");

        //            JToken jt = SelectJSONToken(o, l_JPath);

        //            if (l_JOP == "DEL")
        //            {
        //                if (jt != null)
        //                    if (jt.Parent is JArray)
        //                        (jt.Parent as JArray).Remove(jt);
        //                    else
        //                        jt.Parent.Remove();
        //                val = o.ToString();
        //            }
        //            else if (l_JOP == "UPD")
        //            {
        //                jt.Replace(JToken.Parse(l_JObj));
        //                val = o.ToString();
        //            }
        //            else if (l_JOP == "INS")
        //            {
        //                try
        //                {
        //                    (jt as JObject).Add(l_JAttr, JToken.Parse(l_JObj));
        //                }
        //                catch (Exception addex)
        //                {
        //                    System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "JINS error" + "-" + addex.Message);
        //                    //process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
        //                }
        //                val = o.ToString();
        //            }
        //        }

        //        if (l_ParamName == "")
        //        {
        //            if (sb != null) sb.Append(val);
        //        }
        //        else
        //        {
        //            sResult = "";
        //            process.TemplateParams[l_ParamName] = val;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing J" + l_JOP + " command";
        //        sErrorVerbose = "Error executing J" + l_JOP + " command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
        //        process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //}
        private void Process_JPAR(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
            string sDefault = null;

            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_Source = process.ReplaceParameters(m_Name);
            l_JPath = process.ReplaceParameters(m_Value);
            l_ErrorParamName = "ERROR";

            if (l_ParamName.Contains("|"))
            {
                sDefault = "";
                sDefault = l_ParamName.Substring(l_ParamName.IndexOf("|") + 1);
                l_ParamName = l_ParamName.Substring(0, l_ParamName.IndexOf("|"));
            }

            //if (isParSource == "1") l_Source = (string)process.TemplateParams[l_Source];
            string val = "";
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            JToken jt = null;
            try
            {
                //jt = SelectJSONToken(l_Source, l_JPath, sDefault);
                jt = JSONParseSelect(l_Source, l_JPath, sDefault);
                if (l_ParamName == "")
                {
                    sResult = jt.ToString(); 
                    if (sb != null) sb.Append(sResult);
                }
                else
                {
                    sResult = "";
                    val = jt.ToString();
                    //if (jt.Type == JTokenType.Date)
                    //{
                    //    DateTime d = (DateTime) jt.ToObject<DateTime>();//.ToString();
                    //     val = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    //}
                    //else
                    //    val = jt.ToString();
                    if (jt is JArray)
                        process.TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

                    process.TemplateParams[l_ParamName] = val;
                    foreach (JProperty jp in jt.Children<JProperty>())
                        if (jp.Value.Type == JTokenType.Null)
                        {
                            process.TemplateParams[l_ParamName + "_" + jp.Name] = "<null>";
                        }
                        else if (jp.Value.Type == JTokenType.Date)
                        {
                            DateTime d = (DateTime)jp.Value;
                            process.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        }
                        else if (jp.Value.Type == JTokenType.Array)
                            process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString(Newtonsoft.Json.Formatting.None);
                        else
                            process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JPAR command";
                if ((jt == null) && (sDefault == null))
                {
                    sErrorMessage = ex.Message;
                    sErrorVerbose = "Error executing JPAR command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                }
                else
                {
                    sErrorMessage = "jPath not found";
                    sErrorVerbose = "Error executing JPAR command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + "jPath not found";
                }//process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }
        //private void Process_JPAR(CProcess process, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
        //    string sDefault = null;

        //    l_ParamName = process.ReplaceParameters(m_ParamName);
        //    l_Source = process.ReplaceParameters(m_Name);
        //    l_JPath = process.ReplaceParameters(m_Value);
        //    l_ErrorParamName = "ERROR";
            
        //    //if (isParSource == "1") l_Source = (string)process.TemplateParams[l_Source];
        //    string val = "";
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    try
        //    {
        //        JObject o = JObject.Parse(l_Source);
        //        if (l_ParamName == "")
        //        {
        //            sResult = o.SelectToken(l_JPath).ToString();
        //            if (sb != null) sb.Append(sResult);
        //        }
        //        else
        //        {
        //            sResult = "";
        //            JToken jt = o.SelectToken(l_JPath);

        //            val = jt.ToString();
        //                //if (jt.Type == JTokenType.Date)
        //                //{
        //                //    DateTime d = (DateTime) jt.ToObject<DateTime>();//.ToString();
        //                //     val = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                //}
        //                //else
        //                //    val = jt.ToString();
        //            if (jt is JArray)
        //                process.TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

        //            process.TemplateParams[l_ParamName] = val;
        //            foreach (JProperty jp in jt.Children<JProperty>())
        //            {
        //                if (jp.Value.Type == JTokenType.Date)
        //                {
        //                    DateTime d = (DateTime)jp.Value;
        //                    process.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //                }
        //                else
        //                    process.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing JPAR command";
        //        sErrorVerbose = "Error executing JPAR command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //    }
        //}
        private void Process_JKEYS(CProcess process, StringBuilder sb)
        {
            //DateTime dt1 = DateTime.Now;
            //DateTime dt2;
            string sResult = "";
            string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
            
            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_Source = process.ReplaceParameters(m_Name);
            l_JPath = process.ReplaceParameters(m_Value);
            l_ErrorParamName = "ERROR";

            //if (isParSource == "1") l_Source = (string)process.TemplateParams[l_Source];
            string val = "";
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            try
            {
                //JObject o = JObject.Parse(l_Source);
                JToken o = JToken.Parse(l_Source);
                if (l_ParamName == "")
                {
                    sResult = o.SelectToken(l_JPath).ToString();
                    if (sb != null) sb.Append(sResult);
                }
                else
                {
                    sResult = "";
                    JToken jt = o.SelectToken(l_JPath);
                    val = jt.ToString();
                    if (jt is JArray)
                        process.TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

                    process.TemplateParams[l_ParamName] = val;
                    foreach (JProperty jp in jt.Children<JProperty>())
                    {
                        process.TemplateParams[l_ParamName + "__key"] = jp.Name;
                        if (jp.Value.Type == JTokenType.Date)
                        {
                            DateTime d = (DateTime)jp.Value;
                            process.TemplateParams[l_ParamName + "__value"] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        }
                        else if (jp.Value.Type == JTokenType.Array)
                            process.TemplateParams[l_ParamName + "__value"] = jp.Value.ToString(Newtonsoft.Json.Formatting.None);
                        else
                            process.TemplateParams[l_ParamName + "__value"] = jp.Value.ToString();

                        //dt2 = DateTime.Now;
                        //process.m_runtime.AddTimespan("JKEYS_for", dt2.Subtract(dt1).TotalMilliseconds);
                        ProcessCommands(process, sb);
                        //dt1 = DateTime.Now;
                        process.TemplateParams.Remove(l_ParamName + "__key");
                        process.TemplateParams.Remove(l_ParamName + "__value");
                        string sExitType = process.sExitType ;//.GetParameterValue("__exit_type", "none");
                        if (sExitType != "")
                        {
                            if (sExitType == "loop")
                            {
                                process.sExitType ="";//.TemplateParams.Remove("__exit_type");
                                sErrorVerbose = sError = process.sExitValue;//.GetParameterValue("__exit_value", "");
                                process.sExitValue ="";//.TemplateParams.Remove("__exit_value");
                            }
                            break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JKEYS command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing JKEYS command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            //dt2 = DateTime.Now;
            //process.m_runtime.AddTimespan("JKEYS_for", dt2.Subtract(dt1).TotalMilliseconds);
        }
        private void Process_JDATA(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_ParamName, l_Select, l_Conn, l_ErrorParamName;
            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_Select = process.ReplaceParameters(m_Name);
            l_Conn = process.ReplaceParameters(m_Value);
            l_ErrorParamName = process.ReplaceParameters(m_Context);
            string strCookies = "", strHeaders="";

            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            try
            {
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "JDATA start" + "-" + l_ParamName);
                if(m_ID=="SPLITXX")
                {
                    string l1="", l2="&", l3="=", l4="";
                    if (m_Params != null)
                    {
                        if (m_Params.Count > 0) l1 = process.ReplaceParameters(m_Params[0].m_Value);
                        if (m_Params.Count > 1) l2 = process.ReplaceParameters(m_Params[1].m_Value);
                        if (m_Params.Count > 2) l3 = process.ReplaceParameters(m_Params[2].m_Value);
                        if (m_Params.Count > 3) l4 = process.ReplaceParameters(m_Params[3].m_Value);

                        sResult = JSON_SplitXX(l1,l2[0],l3[0],l4);
                        sResult = "{\"result\":" + sResult + "}";
                    }
                }
                else if(m_ID=="FOR")
                {
                    string s1 = "1";
                    string s2 = "1";
                    if (m_Params != null)
                    {
                        if (m_Params.Count > 0) s1 = process.ReplaceParameters(m_Params[0].m_Value);
                        if (m_Params.Count > 1) s2 = process.ReplaceParameters(m_Params[1].m_Value);
                    }
                    string sFor = "";
                    int iRows = 0;
                    int iCols = 0;
                    int.TryParse(s1, out iRows);
                    int.TryParse(s2, out iCols);
                    for (int iR = 0; iR < iRows; iR++)
                        for (int iC = 0; iC < iCols; iC++)
                        {
                            if (iR + iC > 0) sFor += "|";
                            sFor += (iR * iCols + iC + 1).ToString() + "," + (iR * iCols + iC ).ToString() + "," + (iR + 1).ToString() + "," + iR.ToString() + "," + (iC + 1).ToString() + "," + iC.ToString();
                        }
                    sResult = JSON_SplitXX(sFor, '|', ',', "ID,ID0,R,R0,C,C0");
                    sResult = "{\"result\":" + sResult + "}";
                }
                else if (l_Conn.StartsWith("SPLITXX"))
                    sResult = JSON_SplitXX(l_Select, l_Conn[7], l_Conn[8], l_Conn.Substring(9));
                else if (l_Conn=="SPLIT_")
                    sResult = JSON_Split(l_Select, l_Conn[5]);
                else if (l_Conn.StartsWith("SPLIT_"))
                    sResult = JSON_Split(l_Select, l_Conn.Substring(6));
                else if (l_Conn.StartsWith("SPLIT"))
                    sResult = JSON_Split(l_Select, l_Conn[5]);
                else if (l_Conn.StartsWith("FOR"))
                {
                    string sFor1 = "0";
                    int iRows1 = 0;
                    int.TryParse(l_Select, out iRows1);
                    for (int iR = 0; iR < iRows1 - 1; iR++)
                        sFor1 += "," + (iR + 1).ToString();
                    sResult = "[" + sFor1 + "]";
                }
                else
                {
                    strCookies = process.GetParameterValue(l_ParamName + "__cookies_in");
                    strHeaders = process.GetParameterValue(l_ParamName + "__headers_in");
                    process.TemplateParams[l_ParamName + "__cookies_in"] = "";
                    process.TemplateParams[l_ParamName + "__headers_in"] = "";
                    if (strCookies == "") strCookies = "[]";
                    if (strHeaders == "") strHeaders = "[]";
                    strCookies = "{\"cookies\":" + strCookies + ",\"headers\":" + strHeaders + "}";
                    sResult = process.m_runtime.GetResponse(l_Select, l_Conn, ref strCookies);
                }
//System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "JDATA end" + "-" + l_ParamName);
                JToken jCH= JSONParse(strCookies, "{\"cookies\":[],\"headers\":[]}");
                strHeaders = JSONSelect(jCH, "headers").ToString(Newtonsoft.Json.Formatting.None); 
                strCookies = JSONSelect(jCH, "cookies").ToString(Newtonsoft.Json.Formatting.None); 
                process.TemplateParams[l_ParamName + "__cookies_out"] = strCookies;
                process.TemplateParams[l_ParamName + "__headers_out"] = strHeaders;

                if (l_ParamName == "")
                {
                    if (sb != null) sb.Append(sResult);
                }
                else
                {
                    process.TemplateParams[l_ParamName] = sResult;
                    sResult = "";
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JDATA command";
                sErrorMessage = ex.Message;
                //"Posted data (decoded):"
                if (sErrorMessage.IndexOf("Posted data (decoded):")>0) sErrorMessage = sErrorMessage.Substring(0, sErrorMessage.IndexOf("Posted data (decoded):"));
                sErrorVerbose = "Error executing JDATA command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }
        public object Deserialize(string jsonText, Type valueType)
        {
            try
            {
                Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

                json.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
                json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
                json.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

                StringReader sr = new StringReader(jsonText);

                Newtonsoft.Json.JsonTextReader reader = new JsonTextReader(sr);
                object result = json.Deserialize(reader, valueType);
                reader.Close();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void Process_JARRAY(CProcess process, StringBuilder sb)
        {
            //DateTime dt1 = DateTime.Now;
            //DateTime dt2;
            //string sResult = "";
            string l_ParamName, l_Source, l_JPath, l_ErrorParamName;

            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_Source = process.ReplaceParameters(m_Name);
            l_JPath = process.ReplaceParameters(m_Value);
            l_ErrorParamName = process.ReplaceParameters(m_Context);
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            try
            {
                JArray ja;
                ja = SelectJSONArray(l_Source, l_JPath);

                //if (l_JPath == "")
                //    ja = JArray.Parse(l_Source);
                //else
                //    ja = (JArray)(JObject.Parse(l_Source)).SelectToken(l_JPath);
                process.Arrays["arr_"+l_ParamName] = ja; 
            }
            catch (Exception ex)
            {
                sError = "Error executing JARRAY command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing JARRAY command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
                //sbRetVal.Clear();
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            //dt2 = DateTime.Now;
            //process.m_runtime.AddTimespan("LOOP_for", dt2.Subtract(dt1).TotalMilliseconds);
        }
        private void Process_FUNC(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_ErrorParamName = "ERROR";
            string l_ParamName = "";
            string l_Operation = Name;
            string l_Format = m_Context;
            string l_Value1 = "", l_Value2 = "", l_Value3 = "", l_Value4 = "", l_Value5 = "", l_Value6 = "";

            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_Value1 = process.ReplaceParameters(m_Value);
            l_Value2 = process.ReplaceParameters(m_Value2);
            l_Value3 = process.ReplaceParameters(m_Value3);
            l_Value4 = process.ReplaceParameters(m_ID);
            l_Value5 = process.ReplaceParameters(m_Opt1);
            l_Value6 = process.ReplaceParameters(m_Opt2);
            l_Format = process.ReplaceParameters(m_Context);


            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            try
            {
                sResult = doFUNC(l_Operation.ToLower(), l_Value1, l_Value2, l_Value3, l_Value4, l_Value5, l_Value6, l_Format, process);
            }
            catch (Exception ex)
            {
                sError = "Error executing FUNC " + l_Operation + " command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing FUNC " + l_Operation + " command for parameter " + l_ParamName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }


            if (l_ParamName == "")
            {
                if (sb != null) sb.Append(sResult);
            }
            else
            {
                process.TemplateParams[l_ParamName] = sResult;
                sResult = "";
            }
        }
        private void Process_SVSET(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            string l_VarName, l_ContextName, l_ID, l_Value;
            //Opt1=onlynull, Opt2=isappend, Value2=appendchar

            l_VarName = process.ReplaceParameters(m_Name);
            l_ContextName = process.ReplaceParameters(m_Context);
            l_ID = process.ReplaceParameters(m_ID);
            l_Value = process.ReplaceParameters(m_Value);
            if (m_Opt2 == "1")
            {
                l_Value = process.ReplaceParameters(m_Value, true);
                l_Value = process.ReplaceParameters(l_Value, true);
                l_Value = process.ReplaceParameters(l_Value, true);
            }
            else
                l_Value = process.ReplaceParameters(m_Value);
            l_Value = Utils.Encode(l_Value, EncodeOption.XDOCDecode);
            if (l_ID == "ID")
                l_ID = process.GetParameterValue("ID", "");
            l_ContextName = l_ContextName + "_" + l_ID;


            if ((Opt1 == "1") || (Opt2 == "1"))
            {
                string sPar = process.m_runtime.SVCache.GetSV(l_VarName, l_ContextName, "");
                if ((Opt1 == "1") && (sPar != "") && (sPar != "<null>")) return;
                if (Opt2 == "1") l_Value = sPar + (sPar == "" ? "" : Value3) + l_Value;
            }
            
            //if (Opt1 == "1")
            //{
            //    string sPar = process.m_runtime.SVCache.GetSV(l_VarName, l_ContextName, "");
            //    if (sPar != "") return;
            //}
            process.m_runtime.SVCache.SetSV(l_VarName, l_ContextName, l_Value);
        }
        private void Process_SVGET(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_VarName, l_ContextName, l_ID, l_DefaultVal, l_ParamName, l_What, l_With;
            
            l_VarName = process.ReplaceParameters(m_Name);
            l_ContextName = process.ReplaceParameters(m_Context);
            l_ID = process.ReplaceParameters(m_ID);
            l_DefaultVal = process.ReplaceParameters(m_Value);
            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_What = process.ReplaceParameters(m_Value2);
            l_With = process.ReplaceParameters(m_Value3);
            if (l_ID == "ID")
                l_ID = process.GetParameterValue("ID", "");
            l_ContextName = l_ContextName + "_" + l_ID;

            bool bStrict = false;
            if (Opt2 == "1") bStrict = true;
            if (l_DefaultVal != "")
                sResult = process.m_runtime.SVCache.GetSV(l_VarName, l_ContextName, l_DefaultVal);
            else
            {

                sResult = process.m_runtime.SVCache.GetSV(l_VarName, l_ContextName, "missingvalue");
                //if (sResult == "missingvalue")
                //    sResult = process.m_runtime.SVCache.GetSV(l_VarName, "_", l_DefaultVal);
                if ((sResult == "missingvalue") && (Opt1 == "1"))
                    sResult = process.m_runtime.SVCache.GetSV(l_VarName, "_", l_DefaultVal);
                //else
                //    sResult = l_DefaultVal;

            }

            sResult = process.ReplaceParameters(sResult, (m_Opt1 == "1"));
            sResult = process.ReplaceSettings(sResult, l_ContextName, (Opt1 == "1"), bStrict);
            if (sResult == "missing_setting") sResult = l_DefaultVal;
            if (sResult == "missingvalue") sResult = l_DefaultVal;
            if (l_What != "") sResult = sResult.Replace(l_What, l_With);
            //if (ReplaceAll == "1")
            //{
            //    sVal = ReplaceSettings(sVal, "_", true,bStrict );
            //    sVal = process.ReplaceParameters(sVal, true);
            //}
            if (l_ParamName == "")
            {
                if (sb != null) sb.Append(sResult);
            }
            else
            {
                process.TemplateParams[l_ParamName] = sResult;
                sResult = "";
            }
        }
        private void Process_SVDEL(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            string l_VarName, l_ContextName, l_ID;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";

            
            l_VarName = process.ReplaceParameters(m_Name);
            l_ContextName = process.ReplaceParameters(m_Context);
            l_ID = process.ReplaceParameters(m_ID);
            if (l_ID == "ID")
                l_ID = process.GetParameterValue("ID", "");
            l_ContextName = l_ContextName + "_" + l_ID;
            try
            {

                if (Opt1 == "1")
                    process.m_runtime.SVCache.DelSV("", "*", true);
                else
                {
                    if (Opt2 == "1") process.m_runtime.SVCache.DelSV(l_VarName, l_ContextName, true);
                    else process.m_runtime.SVCache.DelSV(l_VarName, l_ContextName, false);
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing SVDEL command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing SVDEL command in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
//                throw new Exception(sErrorVerbose);
            }
            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }
        private void Process_MSG(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            string l_Text, l_Level;

            //string s = "2015-01-19 19:37:25";
            //DateTime d1 = DateTime.Parse(s);
            //DateTime d2 = (DateTime)s;
            //DataTable dt = process.m_runtime.DataProvider.GetDataTable("select top 1 starttime from apilog");
            //d1 = (DateTime)dt.Rows [0][0];
            l_Text = process.ReplaceParameters(m_Value);
            l_Level = process.ReplaceParameters(m_Name);

            process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), l_Level, l_Text);
            System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "MSG" + l_Level + "-" + l_Text);
        }
        private void Process_INCLUDEONCE(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_TemplateName, l_Ignore;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";

            StringBuilder sbInclude = new StringBuilder();
            l_TemplateName = process.ReplaceParameters(m_Name);
            l_Ignore = m_Opt1;
            if (!(process.m_runtime.SVCache.SetIncludeOnce(l_TemplateName))) return ;
            try
            {
                string sWarning = "";
                JCmd o = process.m_runtime.AppCache.RequestTemplateTree(l_TemplateName, ref sWarning);
                if (o == null) throw new Exception("Template not found or error");
                CProcess p = new CProcess(process.m_runtime, l_TemplateName);

                string sExit = "";
                p.Run(o, null, sbInclude, ref sExit);
                sErrorVerbose = sError = sExit;
                sResult = sbInclude.ToString();
            }
            catch (Exception ex)
            {
                sError = "Error executing INCLUDEONCE command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing INCLUDEONCE command for parameter " + l_TemplateName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
//                if (l_Ignore != "1") throw new Exception(sErrorVerbose);
                if (l_Ignore != "1")
                    if (process.sVersion != "3")
                        throw new Exception(sErrorVerbose);

            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }

            if (sb != null) sb.Append(sResult);
        }
        private void Process_INCLUDE(CProcess process, StringBuilder sb)
        {
            //DateTime dt1 = DateTime.Now;
            string sResult = "";
            string l_TemplateName, l_Ignore, l_NoParse, l_ParamName;
            string l_ExecTemplateName;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";
            //

            //l_TemplateName = process.ReplaceParameters(TemplateName);
            l_ParamName = process.ReplaceParameters(m_ParamName);
            l_ExecTemplateName = process.ReplaceParameters(m_Value);
            string sParameters = "";

            if (l_ExecTemplateName != "")
            {
                sParameters = "TemplateName=" + l_ExecTemplateName + "&" + process.ReplaceParameters(m_Value2);
            }
            else
            {
                StringBuilder sbParams = new StringBuilder();
                sbParams.Clear();
                if ((m_Params != null) && (m_Params.Count > 0))
                    foreach (JCmd command in m_Params)
                    {
                        command.ProcessObject(process, sbParams);
                    }
                sParameters = sbParams.ToString();
            }
            Hashtable hParameters = Utils.HashtableFromQueryString(sParameters);
            l_TemplateName = (string)hParameters["TemplateName"];
            l_NoParse = (string)hParameters["NoParse"];
            l_Ignore = (string)hParameters["Ignore"];
            string l_Inline = (string)hParameters["Inline"];
            hParameters.Remove("Inline");
            hParameters.Remove("TemplateName");
            hParameters.Remove("NoParse");
            hParameters.Remove("Ignore");
            try
            {
                if (l_NoParse == "1")
                    sResult = process.m_runtime.AppCache.RequestTemplateText(l_TemplateName);
                else
                {
                    if (l_Inline == "1")
                    {
                        string sWarning = "";
                        JCmd4 o = process.m_runtime.AppCache.RequestTemplateTree4(l_TemplateName, ref sWarning);
                        if (o == null) throw new Exception("Template not found or error");
                        StringBuilder sbInclude = new StringBuilder();
                        sbInclude.Clear();
                        string sExit = "";

                        process.Run4Inline(o, l_TemplateName, sbInclude, ref sExit);
                        sErrorVerbose = sError = sExit;
                        sResult = sbInclude.ToString();
                    }
                    else
                    {
                        string sWarning = "";
                        JCmd o = process.m_runtime.AppCache.RequestTemplateTree(l_TemplateName, ref sWarning);
                        if (o == null) throw new Exception("Template not found or error");
                        StringBuilder sbInclude = new StringBuilder();
                        sbInclude.Clear();
                        CProcess p = new CProcess(process.m_runtime, l_TemplateName);
                        string sExit = "";
                        p.Run(o, hParameters, sbInclude, ref sExit);
                        sErrorVerbose = sError = sExit;
                        sResult = sbInclude.ToString();
                    }
                }
                if (l_ParamName == "")
                {
                    if (sb != null) sb.Append(sResult);
                }
                else
                {
                    process.TemplateParams[l_ParamName] = sResult;
                    sResult = "";
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing INCLUDE command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing INCLUDE command for template " + l_TemplateName + " in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                if (ex.Message != "Thread was being aborted.")
                    process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
                if (ex.Message != "Thread was being aborted.")
                    //                    if (l_Ignore != "1") throw new Exception(sErrorVerbose);
                    if (l_Ignore != "1")
                        if (process.sVersion != "3")
                            throw new Exception(sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            //DateTime dt2 = DateTime.Now;
            //process.m_runtime.AddTimespan(m_Type, dt2.Subtract(dt1).TotalMilliseconds);
        }
        private void Process_EXIT(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            string l_Text, l_Level;

            
            l_Text = process.ReplaceParameters(m_Value);
            l_Level = process.ReplaceParameters(m_Name);
            process.sExitType =l_Level; //.TemplateParams["__exit_type"] = l_Level;
            process.sExitValue = l_Text;// .TemplateParams["__exit_value"] = l_Text;
            if (l_Level.ToLower() == "request")
            {
                process.m_runtime.ExitRequest = true;
                process.m_runtime.ExitValue = l_Text;
            }
        }
        private void Process_THROW(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            string l_Text;

            
            l_Text = process.ReplaceParameters(m_Value);
            process.sExitType = "request"; //.TemplateParams["__exit_type"] = "request";
            process.sExitValue = l_Text;// .TemplateParams["__exit_value"] = l_Text;
            process.m_runtime.ExitRequest = true;
            process.m_runtime.ExitValue = l_Text;
        }

        private void Process_IMPORT(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_TemplateName, l_Conn; int l_ConnID;
            string l_SubTemplateName, l_Ignore, l_NoParse;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";
            string l_ExecTemplateName;

            l_ExecTemplateName = process.ReplaceParameters(m_Value);
            string sParameters = "";

            if (l_ExecTemplateName != "")
            {
                sParameters = "TemplateName=" + l_ExecTemplateName + "&" + process.ReplaceParameters(m_Value2);
            }
            else
            {

                StringBuilder sbParams = new StringBuilder();
                sbParams.Clear();
                if ((m_Params != null) && (m_Params.Count > 0))
                    foreach (JCmd command in m_Params)
                    {
                        command.ProcessObject(process, sbParams);
                    }
                 sParameters = sbParams.ToString();
            }
            Hashtable hParameters = Utils.HashtableFromQueryString(sParameters);
            l_SubTemplateName = (string)hParameters["TemplateName"];
            l_NoParse = (string)hParameters["NoParse"];
            l_Ignore = (string)hParameters["Ignore"];
            hParameters.Remove("TemplateName");
            hParameters.Remove("NoParse");
            hParameters.Remove("Ignore");

            try
            {
                if (l_NoParse == "1")
                    sResult = process.m_runtime.AppCache.RequestTemplateText(l_SubTemplateName);
                else
                {
                    string sWarning = "";
                    JCmd o = process.m_runtime.AppCache.RequestTemplateTree(l_SubTemplateName, ref sWarning);
                    StringBuilder sbInclude = new StringBuilder();
                    sbInclude.Clear();
                    CProcess p = new CProcess(process.m_runtime, l_SubTemplateName);
                    string sExit = "";
                    p.Run(o, hParameters, sbInclude, ref sExit);
                    sErrorVerbose = sError = sExit;
                    sResult = sbInclude.ToString();
                }
                sResult = sResult.Replace("%d_export", "%d");
                sResult = sResult.Replace("%dash_export", "%dash");
                sResult = sResult.Replace("%macro_export", "%macro");
                sResult = sResult.Replace("%percent_export", "%percent");
                sResult = sResult.Replace("_percent_export", "_percent");
                l_Conn = process.ReplaceParameters(m_Context);
                l_ConnID = int.Parse(ID);
                if (Opt1 == "1")
                    l_TemplateName = "ImportSet";
                else
                    l_TemplateName = process.ReplaceParameters(m_Name);

                sResult = process.m_runtime.AppCache.ImportTemplates(l_TemplateName, sResult);

                //////if (l_ConnID == -99)
                //////    sResult = process.m_runtime.Manager.ImportTemplate(l_Conn, l_TemplateName, sResult);
                //////else
                //////    sResult = process.m_runtime.Manager.ImportTemplate(l_ConnID, l_TemplateName, sResult);
                //////process.m_runtime.AppCache.RemoveTemplates(l_TemplateName, sResult);
                if (sb != null) sb.Append(sResult);
                //sbs+=sResult;
            }
            catch (Exception ex)
            {
                sError = "Error executing IMPORT command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing IMPORT command  in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
                if (l_Ignore != "1")
                    if (process.sVersion != "3")
                        throw new Exception(sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }
        private void Process_EXPORT(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_TemplateName, l_Conn; int l_ConnID;
            string l_Encode;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";


            l_Conn = process.ReplaceParameters(m_Context);
            l_ConnID = int.Parse(ID);
            l_TemplateName = process.ReplaceParameters(m_Name);
            l_Encode = process.ReplaceParameters(m_Value2);

            try
            {
                sResult = process.m_runtime.AppCache.ExportTemplates(l_TemplateName);

                //////if (l_ConnID == -99)
                //////    sResult = process.m_runtime.Manager.ImportTemplate(l_Conn, l_TemplateName, sResult);
                //////else
                //////    sResult = process.m_runtime.Manager.ImportTemplate(l_ConnID, l_TemplateName, sResult);
                sResult = Utils.Encode(sResult, l_Encode); ;
                sResult = sResult.Replace("%d", "%d_export");
                sResult = sResult.Replace("%dash", "%dash_export");
                sResult = sResult.Replace("%macro", "%macro_export");
                sResult = sResult.Replace("%percent", "%percent_export");
                sResult = sResult.Replace("_percent", "_percent_export");
                if (sb != null) sb.Append(sResult);
                //sbs+=sResult;
            }
            catch (Exception ex)
            {
                sError = "Error executing EXPORT command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing EXPORT command  in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }        //private void Process_IMPORT_1(CProcess process, StringBuilder sb)
                 //{
                 //    string sResult = "";
                 //    string l_TemplateName, l_Conn; int l_ConnID;
                 //    string l_SubTemplateName, l_Ignore, l_NoParse;
                 //    string sError = "";
                 //    string sErrorVerbose = "", sErrorMessage = "";
                 //    string l_ErrorParamName = "ERROR";



        //    StringBuilder sbParams = new StringBuilder();
        //    sbParams.Clear();
        //    if ((m_Params != null) && (m_Params.Count > 0))
        //        foreach (JCmd command in m_Params)
        //    {
        //        command.ProcessObject(process, sbParams);
        //    }
        //    string sParameters = sbParams.ToString();
        //    Hashtable hParameters = Utils.HashtableFromQueryString(sParameters);
        //    l_SubTemplateName = (string)hParameters["TemplateName"];
        //    l_NoParse = (string)hParameters["NoParse"];
        //    l_Ignore = (string)hParameters["Ignore"];
        //    hParameters.Remove("TemplateName");
        //    hParameters.Remove("NoParse");
        //    hParameters.Remove("Ignore");

        //    try
        //    {
        //        if (l_NoParse == "1")
        //            sResult = process.m_runtime.AppCache.RequestTemplateText(l_SubTemplateName);
        //        else
        //        {
        //            string sWarning = "";
        //            JCmd o = process.m_runtime.AppCache.RequestTemplateTree(l_SubTemplateName, ref sWarning);
        //            StringBuilder sbInclude = new StringBuilder();
        //            sbInclude.Clear();
        //            CProcess p = new CProcess(process.m_runtime, l_SubTemplateName);
        //            string sExit = "";
        //            p.Run(o, hParameters, sbInclude, ref sExit);
        //            sErrorVerbose = sError = sExit;
        //            sResult = sbInclude.ToString();
        //        }
        //        sResult = sResult.Replace("%d_export", "%d");
        //        sResult = sResult.Replace("%dash_export", "%dash");
        //        sResult = sResult.Replace("%macro_export", "%macro");
        //        sResult = sResult.Replace("%percent_export", "%percent");
        //        sResult = sResult.Replace("_percent_export", "_percent");
        //        l_Conn = process.ReplaceParameters(m_Context);
        //        l_ConnID = int.Parse(ID);
        //        if (Opt1 == "1")
        //            l_TemplateName = "ImportSet";
        //        else
        //            l_TemplateName = process.ReplaceParameters(m_Name);

        //        if (l_ConnID == -99)
        //            sResult = process.m_runtime.Manager.ImportTemplate(l_Conn, l_TemplateName, sResult);
        //        else
        //            sResult = process.m_runtime.Manager.ImportTemplate(l_ConnID, l_TemplateName, sResult);
        //        process.m_runtime.AppCache.RemoveTemplates(l_TemplateName, sResult);
        //        if (sb != null) sb.Append(sResult);
        //        //sbs+=sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing IMPORT command";
        //        sErrorMessage = ex.Message;
        //        sErrorVerbose = "Error executing IMPORT command  in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
        //        process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
        //        if (l_Ignore != "1") throw new Exception(sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //        process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
        //    }
        //}
        //private void Process_EXPORT(CProcess process, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Conn; int l_ConnID;
        //    string l_Encode;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";


        //    l_Conn = process.ReplaceParameters(m_Context);
        //    l_ConnID = int.Parse(ID);
        //    l_TemplateName = process.ReplaceParameters(m_Name);
        //    l_Encode = process.ReplaceParameters(m_Value2);

        //    try
        //    {
        //        if (l_ConnID == -99)
        //            sResult = process.m_runtime.Manager.ExportTemplate(l_Conn, l_TemplateName);
        //        else
        //            sResult = process.m_runtime.Manager.ExportTemplate(l_ConnID, l_TemplateName);
        //        sResult = Utils.Encode(sResult, l_Encode); ;
        //        sResult = sResult.Replace("%d", "%d_export");
        //        sResult = sResult.Replace("%dash", "%dash_export");
        //        sResult = sResult.Replace("%macro", "%macro_export");
        //        sResult = sResult.Replace("%percent", "%percent_export");
        //        sResult = sResult.Replace("_percent", "_percent_export");
        //        if (sb != null) sb.Append(sResult);
        //        //sbs+=sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing EXPORT command";
        //        sErrorMessage = ex.Message;
        //        sErrorVerbose = "Error executing EXPORT command  in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
        //        process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //        process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
        //    }
        //}
        private void Process_DELETE(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_TemplateName, l_Conn; int l_ConnID;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";


            l_Conn = process.ReplaceParameters(m_Context);
            l_ConnID = int.Parse(ID);
            l_TemplateName = process.ReplaceParameters(m_Name);

            try
            {
                sResult = process.m_runtime.AppCache.RemoveTemplates(l_TemplateName,"");
                if (sb != null) sb.Append(sResult);
                //sbs+=sResult;
            }
            catch (Exception ex)
            {
                sError = "Error executing DELETE command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing DELETE command  in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }
        //private void Process_DELETE(CProcess process, StringBuilder sb)
        //{
        //    string sResult = "";
        //    string l_TemplateName, l_Conn; int l_ConnID;
        //    string sError = "";
        //    string sErrorVerbose = "", sErrorMessage = "";
        //    string l_ErrorParamName = "ERROR";

            
        //    l_Conn = process.ReplaceParameters(m_Context);
        //    l_ConnID = int.Parse(ID);
        //    l_TemplateName = process.ReplaceParameters(m_Name);

        //    try
        //    {
        //        if (l_ConnID == -99)
        //            sResult = process.m_runtime.Manager.RemoveTemplate(l_Conn, l_TemplateName);
        //        else
        //            sResult = process.m_runtime.Manager.RemoveTemplate(l_ConnID, l_TemplateName);
        //        if (sb != null) sb.Append(sResult);
        //        //sbs+=sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        sError = "Error executing DELETE command";
        //        sErrorMessage = ex.Message;
        //        sErrorVerbose = "Error executing DELETE command  in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
        //        process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
        //    }

        //    if (l_ErrorParamName != "" && sError != "")
        //    {
        //        process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
        //        process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
        //        process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
        //    }
        //}
        private void Process_TRANSFER(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_SourceTemplateName, l_SourceConn; int l_SourceConnID;
            string l_TargetTemplateName, l_TargetConn; int l_TargetConnID;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";
            
            l_SourceConn = process.ReplaceParameters(m_Context);
            l_SourceConnID = int.Parse(ID);
            l_SourceTemplateName = process.ReplaceParameters(m_Name);
            l_TargetConn = process.ReplaceParameters(m_Value2);
            l_TargetConnID = int.Parse(Value3);
            l_TargetTemplateName = process.ReplaceParameters(m_Value);

            try
            {
                if (Opt1 == "1")
                {
                   string postfix = "";
                    if ((l_SourceConn == l_TargetConn) && (l_SourceConnID == l_TargetConnID)) postfix = "_Copy";
                    l_TargetTemplateName = "ImportSet";
//                    sResult = process.m_runtime.AppCache.ExportTemplatesLike(l_SourceTemplateName, postfix);
                }
                else
                {
                    if (l_TargetTemplateName == "") l_TargetTemplateName = l_SourceTemplateName;
                    if ((l_SourceConn == l_TargetConn) && (l_SourceConnID == l_TargetConnID) && (l_SourceTemplateName == l_TargetTemplateName)) l_TargetTemplateName += "_Copy";
                    sResult = process.m_runtime.AppCache.ExportTemplates(l_SourceTemplateName);
                }
                sResult = process.m_runtime.AppCache.ImportTemplates(l_TargetTemplateName, sResult);
                process.m_runtime.AppCache.RemoveTemplates(l_TargetTemplateName, sResult);
                if (sb != null) sb.Append(sResult);
                //sbs+=sResult;
            }
            catch (Exception ex)
            {
                sError = "Error executing TRANSFER command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing TRANSFER command  in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }

        private void Process_FILE2(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            string l_InType, l_InVal;
            string l_OutType, l_OutVal;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";

            
            l_InType = process.ReplaceParameters(m_Name);
            l_InVal = process.ReplaceParameters(m_Value);
            l_OutType = process.ReplaceParameters(m_Context);
            l_OutVal = process.ReplaceParameters(m_ID);

            try
            {
                sResult = "[[FILE." + l_InType + l_InVal + l_OutType + l_OutVal + "]]";
                if (sb != null) sb.Append(sResult);
                //sbs+=sResult;
            }
            catch (Exception ex)
            {
                sError = "Error executing FILE command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing FILE command  in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }

            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
        }
        #region FILE
        private string GetFullFilePath(string sPath)
        {//GetFullPath, GetPath, GetFullFilePath, GetConfigPath
            string sFullPath = null;
            try
            {
                sFullPath  = Path.Combine(m_XFilesPath, sPath);
            }
            catch(Exception)
            {
                sFullPath = GetFullFilePathRoot(sPath);
            }
            return new DirectoryInfo( sFullPath).FullName;
        }
        private string GetFullFilePathRoot(string sPath)
        {//GetFullPath, GetPath, GetFullFilePath, GetConfigPath
            //////System.IO.FileInfo file = new FileInfo(filePath);
            //////file.Directory.Create(); // If the directory already exists, this method does nothing.
            //////System.IO.File.WriteAllText(file.FullName, content);

            if (sPath == null) sPath = "";
            if (sPath != "")
            {
                if (sPath.StartsWith("~"))
                    sPath = HttpContext.Current.Server.MapPath("~") + sPath.Substring(1);
                else
                    if (!Path.IsPathRooted(sPath))
                        if ((HttpContext.Current != null) && (HttpContext.Current.Server != null)) sPath = HttpContext.Current.Server.MapPath(Path.Combine("~", sPath));
                //if (toCreate) if (!Directory.Exists(sPath))
                //    Directory.CreateDirectory(sPath);
            }
            return sPath;
        }

        private string ReadURIFile(string sURI, ref byte[] m_FileContent)
        {
            string m_Error = "";
            int iMaxAttempts = 3;
            try
            {
                WebClient client = new WebClient();

                int iAttempt = 1;
                while (m_FileContent == null)
                {
                    try
                    {
                        m_FileContent = client.DownloadData(sURI);
                    }
                    catch (Exception )
                    {
                        if (iAttempt >= iMaxAttempts) throw;
                        else iAttempt++;
                    }
                }
            }
            catch (Exception err)
            {
                m_Error = "Error reading file uri (" + sURI + "): " + err.Message;
            }
            return m_Error;
        }
        private string ReadHTTPFile(string sURI, ref byte[] m_FileContent,CProcess process)
        {
            string m_Error = "";
            int iMaxAttempts = 3;
            try
            {
                string strUserName = process.GetParameterValue("FileUserName");
                string strPassword = process.GetParameterValue("FileUserPassword");
                string strDomain = process.GetParameterValue("FileUserDomain");
                string strProxyUserName = process.GetParameterValue("FileProxyUserName");
                string strProxyPassword = process.GetParameterValue("FileProxyUserPassword");
                string strProxyDomain = process.GetParameterValue("FileProxyUserDomain");
                string strProxyUrl = process.GetParameterValue("FileProxyUrl");
                string strRequestHeaders = process.GetParameterValue("FileRequestHeaders");

                int iAttempt = 1;
                while (m_FileContent == null)
                {
                    try
                    {
                        System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURI);
                        request.Method = "GET";

                        foreach (string strRequestHeader in Utils.MySplit(strRequestHeaders, '|'))
                        {
                            if (strRequestHeader.Split('~').Length == 2)
                            {
                                if ((strRequestHeader.Split('~')[0].ToLower()) == "accept")
                                {
                                    request.Accept = strRequestHeader.Split('~')[1];
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "connection")
                                {
                                    request.Connection = strRequestHeader.Split('~')[1];
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-length")
                                {
                                    request.ContentLength = long.Parse(strRequestHeader.Split('~')[1]);
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "content-type")
                                {
                                    request.ContentType = strRequestHeader.Split('~')[1];
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "date") { }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "expect")
                                {
                                    request.Expect = strRequestHeader.Split('~')[1];
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "host") { }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "if-modified-since")
                                {
                                    request.IfModifiedSince = DateTime.Parse(strRequestHeader.Split('~')[1]);
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "range") { }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "referer")
                                {
                                    request.Referer = strRequestHeader.Split('~')[1];
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "transfer-encoding")
                                {
                                    request.TransferEncoding = strRequestHeader.Split('~')[1];
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "user-agent")
                                {
                                    request.UserAgent = strRequestHeader.Split('~')[1];
                                }
                                else if ((strRequestHeader.Split('~')[0].ToLower()) == "proxy-connection") { }
                                else
                                {
                                    request.Headers.Add(strRequestHeader.Split('~')[0], strRequestHeader.Split('~')[1]);
                                }
                            }
                        }

                        if ((strUserName == "" || strUserName == null) && (strPassword == "" || strPassword == null) && (strDomain == "" || strDomain == null))
                        {
                            /* Set the credentials to be like impersonate */
                            request.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                        else
                        {
                            request.Credentials = new System.Net.NetworkCredential(strUserName, strPassword, strDomain);
                        }

                        if (strProxyUrl != "" && strProxyUrl != null)
                        {

                            WebProxy proxy = new WebProxy();
                            proxy.Address = new Uri(strProxyUrl);
                            if (strProxyUserName != "" && strProxyUserName != null)
                            {
                                proxy.Credentials = new NetworkCredential(strProxyUserName, (strProxyPassword == null ? "" : strProxyPassword), (strProxyDomain == null ? "" : strProxyDomain));
                            }
                            request.Proxy = proxy;
                        }
                        WebResponse response = null;
                        try { response = request.GetResponse(); }
                        catch (WebException webex)
                        {
                            String data = String.Empty;
                            if (webex.Response != null)
                            {
                                StreamReader r = new StreamReader(webex.Response.GetResponseStream());
                                data = r.ReadToEnd();
                                r.Close();
                            }
                            throw new Exception(webex.Message + "\r\n" + data);
                        }
                        int intLength = (int)response.ContentLength;
                        m_FileContent = new byte[intLength];
                        //response.GetResponseStream().Read(m_FileContent, 0, intLength);
                        byte[] buffer = new byte[4096];
                        Stream responseStream = response.GetResponseStream();
                        MemoryStream memStream = new MemoryStream();
                        int count = 0;
                        do{
                            count = responseStream.Read(buffer, 0, buffer.Length);
                            memStream.Write(buffer, 0, count);
                        }
                        while (count!=0);
                        m_FileContent = memStream.ToArray();

                    }
                    catch (Exception )
                    {
                        if (iAttempt >= iMaxAttempts) throw;
                        else iAttempt++;
                    }
                }
            }
            catch (Exception err)
            {
                m_Error = "Error reading file http (" + sURI + "): " + err.Message;
            }
            return m_Error;
        }
        private string ReadNFSFile(string sFile, ref byte[] m_FileContent)
        {
            string m_Error="";
            int iMaxAttempts = 3;
            try
            {
                int iAttempt = 1;
                while (m_FileContent == null)
                {
                    try
                    {
                        sFile = GetFullFilePath(sFile);
                        m_FileContent = File.ReadAllBytes(sFile);
                    }
                    catch (Exception )
                    {
                        if (iAttempt >= iMaxAttempts) throw;
                        else iAttempt++;
                    }
                }

            }
            catch (Exception err)
            {
                m_Error = "Error reading file nfs (" + sFile + "): " + err.Message;
            }
            return m_Error;
        }
        private string WriteNFSFile(string sFile, byte[] m_FileContent)
        {
            string m_Error = "";
            FileStream fs = null;
            BinaryWriter bw = null;
            try
            {
                sFile = GetFullFilePath(sFile);

                System.IO.FileInfo file = new FileInfo(sFile);
                if (m_FileContent == null)
                {

                    File.Delete(sFile);
                }
                else
                {
                    file.Directory.Create(); // If the directory already exists, this method does nothing.

                    if (File.Exists(sFile))
                    {
                        File.Delete(sFile);
                    }
                    string sTempFile = Path.GetTempFileName();

                    fs = File.Open(sTempFile, FileMode.OpenOrCreate);
                    bw = new BinaryWriter(fs);

                    bw.Write(m_FileContent);
                    bw.Close();
                    fs.Close();
                    File.Move(sTempFile, sFile);
                }
            }
            catch (Exception err)
            {
                m_Error = "Error writing file nfs (" + sFile + "): " + err.Message;
            }
            finally
            {
                if (bw != null)
                    bw.Close();
                if (fs != null)
                    fs.Close();
            }
            return m_Error;
        }
        private string ReadUploadFile(HttpContext m_HttpContext, string sIndex, ref byte[] m_FileContent, ref string m_FileName, ref string m_FileExtension, ref string m_FileContentType)
        {
            string m_Error = "";
            try
            {
                if (sIndex == "") sIndex = "0";
                int iIndex = Convert.ToInt16(sIndex);
                HttpPostedFile objHttpPostedFile = m_HttpContext.Request.Files[iIndex];
                if (objHttpPostedFile == null)
                    m_Error = "Error reading file upload (" + sIndex + "): " + " not found";
                else
                {
                    string strFullFileName = objHttpPostedFile.FileName;
                    m_FileContentType = objHttpPostedFile.ContentType;
                    int intLength = objHttpPostedFile.ContentLength;
                    m_FileName = System.IO.Path.GetFileName(strFullFileName); /* only the attched file name not its path */
                    m_FileExtension = System.IO.Path.GetExtension(strFullFileName).ToLower();
                    m_FileContent = new byte[intLength];
                    objHttpPostedFile.InputStream.Read(m_FileContent, 0, intLength);

                    objHttpPostedFile = null;
                }
            }
            catch (Exception err)
            {
                m_Error = "Error reading file upload (" + sIndex + "): " + err.Message;
            }
            return m_Error;

        }
        private string WriteResponseFile(HttpContext m_HttpContext, string m_FileName, string sContentType, byte[] m_FileContent, string sInline)
        {
            string m_Error = "";
            try
            {
                //HttpContext m_HttpContext = (HttpContext)process.m_runtime.myHttpContext  ;
                /* Clear Response buffer, set type and write data into Output stream */
                m_HttpContext.Response.Clear();
                m_HttpContext.Response.ContentType = sContentType;

                /*
                 * Abount disposition:
                 * type = attachment -> let the user save
                 * type = inline -> try to show the content in the browser
                 */

                if (sInline == "1")
                {
                    /* Setting the filename with type=inline only makes sense when the browser falls back to type attachment (when it cannot show the file inline) */
                    m_HttpContext.Response.AppendHeader("Content-Disposition", "inline; filename=" + m_FileName);
                }
                else
                {
                    m_HttpContext.Response.AppendHeader("Content-Disposition", "attachment; filename=" + m_FileName);
                }

                m_HttpContext.Response.OutputStream.Write(m_FileContent, 0, m_FileContent.Length);
                m_HttpContext.Response.OutputStream.Flush();
                m_HttpContext.ApplicationInstance.CompleteRequest();
                //m_HttpContext.Response.End();
            }
            catch (Exception err)
            {
                m_Error = "Error writing file response (" + sContentType + "): " + err.Message;
            }
            return m_Error;
        }
        #endregion
        private void Process_FILE(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            string l_InType, l_InVal;
            string l_OutType, l_OutVal;
            string sError = "";
            string sErrorVerbose = "", sErrorMessage = "";
            string l_ErrorParamName = "ERROR";

            string m_Error = "";
            string m_FileName = "";
            string m_FileExtension = "";
            byte[] m_FileContent = null;
            string m_FileContentType = "";
            string m_FileID = "";

            m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
            l_InType = process.ReplaceParameters(m_Name);
            l_InVal = process.ReplaceParameters(m_Value);
            l_OutType = process.ReplaceParameters(m_Context);
            l_OutVal = process.ReplaceParameters(m_ID);

            try
            {
                switch (l_InType)
                {
                    case "UPLOAD":
                        HttpContext m_HttpContext = (HttpContext)process.m_runtime.myHttpContext;
                        m_Error = ReadUploadFile(m_HttpContext, l_InVal, ref m_FileContent, ref m_FileName, ref m_FileExtension, ref m_FileContentType);
                        break;
                    case "NFS":
                        m_FileName = Path.GetFileName(l_InVal);
                        m_FileExtension = Path.GetExtension(l_InVal).ToLower();
                        m_FileContentType = "";
                        m_Error = ReadNFSFile(l_InVal, ref m_FileContent);
                        break;
                    case "URI":
                        m_FileName = Path.GetFileName(l_InVal);
                        m_FileExtension = Path.GetExtension(l_InVal).ToLower();
                        m_FileContentType = "";
                        m_Error = ReadURIFile(l_InVal, ref m_FileContent);
                        break;
                    case "HTTP":
                        m_FileName = Path.GetFileName(l_InVal);
                        m_FileExtension = Path.GetExtension(l_InVal).ToLower();
                        m_FileContentType = "";
                        m_Error = ReadHTTPFile(l_InVal, ref m_FileContent, process);
                        break;
                    //case "TEMP":
                    //    ReadNFSFile(HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_InVal);
                    //    ReadURIFile(".\\Attachments\\" + l_InVal);
                    //    break;
                    //case "DB":
                    //    ReadDBFile(l_InVal);
                    //    break;
                    //case "BLOB":
                    //    ReadBlobFile(l_InVal);
                    //    break;
                    case "PAR":
                        m_FileName = (string)process.TemplateParams[l_InVal + "FileName"];
                        m_FileExtension = (string)process.TemplateParams[l_InVal + "FileExtension"];
                        string sContent = (string)process.TemplateParams[l_InVal];
                        m_FileContent = System.Text.Encoding.Default.GetBytes(sContent);
                        break;
                    case "PAR8":
                        m_FileName = (string)process.TemplateParams[l_InVal + "FileName"];
                        m_FileExtension = (string)process.TemplateParams[l_InVal + "FileExtension"];
                        string sContent8 = (string)process.TemplateParams[l_InVal];
                        m_FileContent = Encoding.UTF8.GetBytes(sContent8);
                        break;
                    case "PAR64":
                        m_FileName = (string)process.TemplateParams[l_InVal + "FileName"];
                        m_FileExtension = (string)process.TemplateParams[l_InVal + "FileExtension"];
                        string sContent64 = (string)process.TemplateParams[l_InVal];
                        m_FileContent = Convert.FromBase64String(sContent64);
                        break;
                    case "DEL":
                        m_FileName = "";
                        m_FileExtension = "";
                        m_FileContent = null;
                        break;
                    default:
                        throw new Exception("Invalid source type");
                        //break;
                }
                if (m_Error != "") throw new Exception(m_Error);

                process.TemplateParams["FileName"] = m_FileName;
                process.TemplateParams["FileExtension"] = m_FileExtension;
                process.TemplateParams["FileContentType"] = m_FileContentType;


                if (l_OutType == "") { l_OutType = "PAR"; l_OutVal = "File"; }
                switch (l_OutType)
                {
                    case "RESPONSE":
                        string sInline = "1";
                        sInline = (string)process.TemplateParams["FileResponseInline"];
                        if (sInline != "0") sInline = "1";
                        m_FileID = m_FileName;
                        HttpContext m_HttpContext = (HttpContext)process.m_runtime.myHttpContext;
                        WriteResponseFile(m_HttpContext, m_FileName, l_OutVal, m_FileContent, sInline);
                        break;
                    case "NFS":
                        m_FileID = m_FileName;
                        m_Error = WriteNFSFile(l_OutVal, m_FileContent);
                        break;
                    //case "URI":
                    //    WriteURIFile(l_OutVal);
                    //    break;
                    //case "HTTP":
                    //    WriteHTTPFile(l_OutVal);
                    //    break;
                    //case "TEMP":
                    //    WriteNFSFile(HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_OutVal);
                    //    SetPar("FilePath", HttpContext.Current.Server.MapPath(".\\Attachments\\") + l_OutVal);
                    //    break;
                    //case "DB":
                    //    WriteDBFile(l_OutVal);
                    //    break;
                    //case "BLOB":
                    //    WriteBlobFile(l_OutVal);
                    //    break;
                    case "PAR":
                        string sOutVal = System.Text.Encoding.Default.GetString(m_FileContent);
                        process.TemplateParams[l_OutVal] = sOutVal;
                        process.TemplateParams[l_OutVal + "FileName"] = m_FileName;
                        process.TemplateParams[l_OutVal + "FileExtension"] = m_FileExtension;
                        m_FileID = m_FileName;
                        //WritePARFile(l_OutVal);
                        break;
                    case "PAR8":
                        string sOutVal8 = Encoding.UTF8.GetString(m_FileContent);
                        process.TemplateParams[l_OutVal] = sOutVal8;
                        process.TemplateParams[l_OutVal + "FileName"] = m_FileName;
                        process.TemplateParams[l_OutVal + "FileExtension"] = m_FileExtension;
                        m_FileID = m_FileName;
                        //WritePARFile(l_OutVal);
                        break;
                    case "PAR64":
                        string sOutVal64 = Convert.ToBase64String(m_FileContent, 0, m_FileContent.Length);
                        process.TemplateParams[l_OutVal] = sOutVal64;
                        process.TemplateParams[l_OutVal + "FileName"] = m_FileName;
                        process.TemplateParams[l_OutVal + "FileExtension"] = m_FileExtension;
                        m_FileID = m_FileName;
                        //WritePAR64File(l_OutVal);
                        break;
                    default:
                        throw new Exception("Invalid target type");
                        //break;
                }
                if (m_Error != "") throw new Exception(m_Error);
                process.TemplateParams["FileID"] = m_FileID;
                process.TemplateParams["FileError"] = "";

            }
            catch (Exception ex)
            {
                sError = "Error executing FILE command";
                sErrorMessage = ex.Message;
                sErrorVerbose = "Error executing FILE command in template " + process.m_templateName + " at line + " + m_Line + ";" + Environment.NewLine + ex.Message;
                process.m_runtime.Message(process.GetParameterValue("_SESSIONID_"), "Warn", sErrorVerbose);
            }
            if (l_ErrorParamName != "" && sError != "")
            {
                process.TemplateParams[l_ErrorParamName] = (process.TemplateParams[l_ErrorParamName] == null ? "" : process.TemplateParams[l_ErrorParamName] + "\r\n\r\n") + sError;
                process.TemplateParams[l_ErrorParamName + "Verbose"] = (process.TemplateParams[l_ErrorParamName + "Verbose"] == null || (string)process.TemplateParams[l_ErrorParamName + "Verbose"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Verbose"] + "\r\n\r\n") + sErrorVerbose;
                process.TemplateParams[l_ErrorParamName + "Message"] = (process.TemplateParams[l_ErrorParamName + "Message"] == null || (string)process.TemplateParams[l_ErrorParamName + "Message"] == "" ? "" : process.TemplateParams[l_ErrorParamName + "Message"] + "\r\n\r\n") + sErrorMessage;
            }
            //sResult = "[[" + "FILE" + "]]";
            //if (sb != null) sb.Append(sResult);
        }
        private void Process_USEPARAMETERS(CProcess process, StringBuilder sb)
        {
            string sResult = "";
            process.m_UseGlobalParams = true;
            if (sb != null) sb.Append(sResult);
        }
        internal void ProcessCommands(CProcess process, StringBuilder sb)
        {
            //string sResult = "";
            if ((m_Commands != null) && (m_Commands.Count > 0))
            {
                foreach (JCmd command in m_Commands)
                {
                    command.ProcessObject(process, sb);
                    string sExitType = process.sExitType;// .GetParameterValue("__exit_type", "none");
                    if (sExitType != "")
                    {
                        break;
                    }
                    if (process.m_runtime.ExitRequest) break;

                }
            }
        }
        internal void DisplayCommand(StringBuilder sb)
        {
            DisplayCommand(sb, true);
        }
        internal void DisplayCommand(StringBuilder sb, bool isFirst)
        {
            string sResult = "";
            if (!isFirst) sResult += ",";
            sResult += "{";
            sResult += "\"Line\":\"" + Line + "\"";
            sResult += ",\"Type\":\"" + Type + "\"";
            sResult += ",\"ParamName\":\"" + ParamName + "\"";
            sResult += ",\"Name\":\"" + Name + "\"";
            sResult += ",\"Value\":\"" + Value + "\"";
            sResult += ",\"Context\":\"" + Context + "\"";
            sResult += ",\"ID\":\"" + ID + "\"";
            sResult += ",\"Opt1\":\"" + Opt1 + "\"";
            sResult += ",\"Opt2\":\"" + Opt2 + "\"";
            sResult += ",\"Value2\":\"" + Value2 + "\"";
            sResult += ",\"Value3\":\"" + Value3 + "\"";
            sResult += ",\"Value2\":\"" + Value2 + "\"";
            if (sb != null) sb.Append(sResult);
            DisplayArray("Params",m_Params, sb);
            DisplayArray("Commands",m_Commands, sb);
            if (m_ElseIf != null)
            {
                if (sb != null) sb.Append(",\"ElseIf\":");
                m_ElseIf.DisplayCommand(sb);
            }
            sResult = "}";
            if (sb != null) sb.Append(sResult);
        }
        internal void DisplayArray(string sName,IList<JCmd> m_Commands, StringBuilder sb)
        {
            if ((m_Commands != null) && (m_Commands.Count > 0))
            {
                if (sb != null) sb.Append( ",\"" + sName + "\":[");
                bool isFirst = true;
                foreach (JCmd command in m_Commands)
                {
                    command.DisplayCommand(sb,isFirst);
                    isFirst = false;
                }
                if (sb != null) sb.Append("]");
            }
        }

        internal void Sintax4NewLine(StringBuilder sb, int level, string boolNewLine)
        {
            if ((boolNewLine == "true") && (sb.Length > 0))
            {
                sb.Append("\r\n");
                for (int i = 0; i < level; i++) sb.Append("\t");
            }
            else
            {
                string s = sb.ToString();
                int numLines = s.Length - s.Replace(Environment.NewLine, string.Empty).Length;
                int l = Convert.ToInt32(m_Line);
                if (l > numLines)
                {
                    for (int i = numLines; i < l; i++)
                    {
                        sb.Append("\r\n");
                    }
                    for (int i = 0; i < level; i++) sb.Append("\t");
                }
            }

                    //if (((boolNewLine == "1" || boolNewLine == "else")) && (sb.Length > 0))
                    //{
                    //    sb.Append("\r\n");
                    //    for (int i = 0; i < level; i++) sb.Append("\t");
                    //}

        }
        internal string Sintax4(StringBuilder sb, int level, string boolNewLine)
        {
            string newLine = boolNewLine;
            if (m_Type == "Text") newLine = "false";
            //if (m_Type == "IF")
            //    newLine = "false";
            //if (m_Type == "IFI")
            //    newLine = "false";
            Sintax4NewLine(sb, level, newLine);
          
            newLine = "true";
            switch (m_Type)
            {
                case "GenericCommand":
                    if (m_Commands != null)
                        Sintax4Array(sb, m_Commands, level + 1, "false");
                    newLine = "false";
                    break;
                case "Text":
                    string sText = m_Value;
                    while (sText.EndsWith("\t")) sText = sText.Substring(0, sText.Length - 1);
                    if (sText.EndsWith("\r\n"))
                    {
                        sText = sText.Substring(0, sText.Length - 2);
                        newLine = "true";
                    }
                    else
                    {
                        sText = m_Value;
                        newLine = "false";
                    }
                    sb.Append(sText);
                    break;
                case "JLOOP":
                    sb.Append("#JLOOP");
                    if (m_ParamName != null) sb.Append("." + m_ParamName);
                    if (m_Name != null) sb.Append("." + m_Name);
                    if (m_Value != null) sb.Append("." + m_Value);
                    if (m_Value2 != null) sb.Append("." + m_Value2);
                    if (m_Value3 != null) sb.Append("." + m_Value3);
                    if (m_Context != null) sb.Append("." + m_Context);
                    sb.Append("#");
                    if (m_Commands != null)
                        Sintax4Array(sb, m_Commands, level + 1, boolNewLine);
                    Sintax4NewLine(sb, level, boolNewLine);
                    sb.Append("#ENDJLOOP." + m_ParamName + "#");
                    break;
                //case "IF":
                //case "IFI":
                //    if (m_Commands != null)
                //        Sintax4Array(sb, m_Commands, level, boolNewLine);
                //    Sintax4NewLine(sb, level, boolNewLine);
                //    sb.Append("#ENDIF#");
                //    break;
                //case "SUBIF1":
                //    sb.Append("#IF(");
                //    if (m_Params != null)
                //        Sintax4Array(sb, m_Params, -1, false);
                //    sb.Append(")#");
                //    if (m_Commands != null)
                //        Sintax4Array(sb, m_Commands, level + 1, boolNewLine);
                //    newLine = boolNewLine;
                //    break;
                //case "SUBIF":
                //    sb.Append("#ELSEIF(");
                //    if (m_Params != null)
                //        Sintax4Array(sb, m_Params, -1, false);
                //    sb.Append(")#");
                //    if (m_Commands != null)
                //        Sintax4Array(sb, m_Commands, level + 1, boolNewLine);
                //    newLine = boolNewLine;
                //    break;
                //case "SUBIFI1":
                //    sb.Append("#IFI.");
                //    sb.Append(m_P1);
                //    sb.Append("#");
                //    if (m_Commands != null)
                //        Sintax4Array(sb, m_Commands, level + 1, boolNewLine);
                //    newLine = boolNewLine;
                //    break;
                //case "SUBIFI":
                //    sb.Append("#ELSEIFI.");
                //    sb.Append(m_P1);
                //    sb.Append("#");
                //    if (m_Commands != null)
                //        Sintax4Array(sb, m_Commands, level + 1, boolNewLine);
                //    newLine = boolNewLine;
                //    break;
                case "IF":
                    if (boolNewLine == "else")
                        sb.Append("#ELSEIF");
                    else
                        sb.Append("#IF");
                    if (m_Name == "IFIP")
                    {
                        sb.Append(".");
                        sb.Append(m_Value);
                        sb.Append("#");
                    }
                    else
                    {
                        sb.Append("(");
                        if (m_Params != null)
                            Sintax4Array(sb, m_Params, -1, "false");
                        sb.Append(")#");
                    }
                    //if (m_Commands != null)
                    //    Sintax4Array(sb, m_Commands, level+1, boolNewLine);
                    Sintax4NewLine(sb, level, boolNewLine);
                    if (m_Commands != null)
                        Sintax4Array(sb, m_Commands, level+1, "true");
                    if (m_ElseIf != null)
                        m_ElseIf.Sintax4(sb, level+1, "else");
//                    Sintax4NewLine(sb, level, boolNewLine);
//                    Sintax4NewLine(sb, level, "true");
                    if (boolNewLine != "else")
                    {
                        Sintax4NewLine(sb, level, boolNewLine);
                        sb.Append("#ENDIF#");
                    }
                    break;
                case "SVSET":
                    if (m_Opt1 == "1")
                        sb.Append("#SETNULLVAL");
                    else
                        sb.Append("#SETVAL");
                    if (m_Value != null) sb.Append("." + m_Value);
                    if (m_Name != null) sb.Append("." + m_Name);
                    if (m_Context != null) sb.Append("." + m_Context);
                    if (m_ID != null) sb.Append("." + m_ID);
                    sb.Append("#");
                    break;
                case "SPAR":
                    if (m_Opt1 == "1")
                        sb.Append("#SPARP");
                    else if (m_Opt2 == "1")
                        sb.Append("#SPARA");
                    else
                        sb.Append("#SPAR");
                    if (m_ParamName != null) sb.Append("." + m_ParamName);
                    if (m_Value != null) sb.Append("." + m_Value);
                    sb.Append("#");
                    break;
                case "JPAR":
                    sb.Append("#JPAR");
                    if (m_ParamName != null) sb.Append("." + m_ParamName);
                    if (m_Name != null) sb.Append("." + m_Name);
                    if (m_Value != null) sb.Append("." + m_Value);
                    sb.Append("#");
                    break;
                case "PAR":
                    sb.Append("#PAR");
                    if (m_ParamName != null) sb.Append("." + m_ParamName);
                    if ((m_Value2 != "None") && (m_Value3 != ""))
                    {
                        if (m_Value2 != null) sb.Append("." + m_Value2);
                        if (m_Value3 != null) sb.Append("." + m_Value3);
                    }
                    sb.Append("#");
                    break;
                case "JDATA":
                    sb.Append("#JDATA");
                    if (m_ParamName != null) sb.Append("." + m_ParamName);
                    if (m_Name != null) sb.Append("." + m_Name);
                    if (m_Value != null) sb.Append("." + m_Value);
                    if (m_Context != null) sb.Append("." + m_Context);
                    sb.Append("#");
                    break;
                //case "XDOCBLOCK":
                //    sb.Append("#STARTBLOCK#");
                //    if (m_P1 != null) sb.Append("." + m_P1);
                //    sb.Append("#ENDBLOCK#");
                //    break;
                //case "XDOCCOMMENT":
                //    sb.Append("#STARTCOMMENT#");
                //    if (m_P1 != null) sb.Append("." + m_P1);
                //    sb.Append("#ENDCOMMENT#");
                //    break;
                default:
                    sb.Append("#");
                    sb.Append(m_Type);
                    if (m_ParamName != null) sb.Append("." + m_ParamName);
                    if (m_Name != null) sb.Append("." + m_Name);
                    if (m_Value != null) sb.Append("." + m_Value);
                    if (m_Context != null) sb.Append("." + m_Context);
                    if (m_ID != null) sb.Append("." + m_ID);
                    if (m_Opt1 != null) sb.Append("." + m_Opt1);
                    if (m_Opt2 != null) sb.Append("." + m_Opt2);
                    if (m_Value2 != null) sb.Append("." + m_Value2);
                    if (m_Value3 != null) sb.Append("." + m_Value3);
                    if (m_Params != null)
                    {
                        sb.Append("(");
                        Sintax4Array(sb, m_Params, -1, "false");
                        sb.Append(")");
                    }
                    sb.Append("#");
                    //if (level > -1) if (newLine) sb.Append("\r\n");
                    break;
            }

            //if (m_Commands != null)
            //    Sintax4Array(sb, m_Commands, level + 1);
            //if (m_ElseIf != null)
            //    m_ElseIf.Sintax4(sb,level);
            return newLine;
        }
        internal string Sintax4Array(StringBuilder sb, IList<JCmd> m_Commands, int level, string newLine)
        {
            string bNewLine = newLine;
            if ((m_Commands != null) && (m_Commands.Count > 0))
            {
                foreach (JCmd command in m_Commands)
                {
                    bNewLine = command.Sintax4(sb, level, bNewLine);
                }
            }
            //            return bNewLine;
            return newLine;
        }

#endregion

        //internal string ProcessObject1(CProcess process, StringBuilder sb)
        //{
        //    DateTime dt1 = DateTime.Now;
        //    string sResult = "";
        //    string sType = m_Type;
        //    sType = sType.ToUpper();
        //    //System.Diagnostics.Debug.WriteLine( sType );
        //    switch (sType)
        //    {
        //        case "TEXT":
        //            sResult += Process_TEXT(process, sb);
        //            break;
        //        case "DEFPAR":
        //            sResult += Process_DEFPAR(process, sb);
        //            break;
        //        case "SPAR":
        //            sResult += Process_SPAR(process, sb);
        //            break;
        //        case "PAR":
        //            sResult += Process_PAR(process, sb);
        //            break;
        //        case "IIF":
        //            sResult += Process_IIF(process, sb);
        //            break;
        //        case "IF":
        //            sResult += Process_IF(process, sb);
        //            break;
        //        case "JLOOP":
        //            sResult += Process_JLOOP(process, sb);
        //            break;
        //        case "JKEYS":
        //            sResult += Process_JKEYS(process, sb);
        //            break;
        //        case "JPAR":
        //            sResult += Process_JPAR(process, sb);
        //            break;
        //        case "JDATA":
        //            sResult += Process_JDATA(process, sb);
        //            break;
        //        case "CFG":
        //            sResult += Process_CFG(process, sb);
        //            break;
        //        case "FUNC":
        //            sResult += Process_FUNC(process, sb);
        //            break;
        //        case "MSG":
        //            sResult += Process_MSG(process, sb);
        //            break;
        //        case "SVSET":
        //            sResult += Process_SVSET(process, sb);
        //            break;
        //        case "SVGET":
        //            sResult += Process_SVGET(process, sb);
        //            break;
        //        case "SVDEL":
        //            sResult += Process_SVDEL(process, sb);
        //            break;
        //        case "INCLUDE":
        //            sResult += Process_INCLUDE(process, sb);
        //            break;
        //        case "INCLUDEONCE":
        //            sResult += Process_INCLUDEONCE(process, sb);
        //            break;
        //        case "EXIT":
        //            sResult += Process_EXIT(process, sb);
        //            break;
        //        case "THROW":
        //            sResult += Process_THROW(process, sb);
        //            break;
        //        case "IMPORT":
        //            sResult += Process_IMPORT(process, sb);
        //            break;
        //        case "EXPORT":
        //            sResult += Process_EXPORT(process, sb);
        //            break;
        //        case "DELETE":
        //            sResult += Process_DELETE(process, sb);
        //            break;
        //        case "TRANSFER":
        //            sResult += Process_TRANSFER(process, sb);
        //            break;
        //        case "FILE":
        //            sResult += Process_FILE(process, sb);
        //            break;
        //        case "USEPARAMETERS":
        //            process.m_UseGlobalParams = true;
        //            break;
        //        default:
        //            sResult = "[[" + sType + "]]";
        //            if (sb != null) sb.Append(sResult);
        //            //sbs+=sResult;
        //            break;
        //    }
        //    DateTime dt2 = DateTime.Now;
        //    process.m_runtime.AddTimespan(sType, dt2.Subtract(dt1).TotalMilliseconds);
        //    //return sResult;
        //    return "";//            return sResult;
        //}

        
        #region static
        internal string doFUNC(string l_Operation, string l_Value1, string l_Value2, string l_Value3, string l_Value4, string l_Value5, string l_Value6, string strFormat, CProcess process)
        {
            string sResult = "";
            StringBuilder sJSON;
            double doubleVal, dVal;
            DateTime datetimeVal, datetimeVal2;
            TimeSpan timespanVal2;
            bool bVal = false, bVal4=false;
            FileAttributes attr;
            System.IO.FileInfo fileinfo;
            byte[] binaryData;

            int i_Value = 0, iVal2 = 0, iVal3 = 0, iVal4 = 0, iVal5 = 0, iVal6 = 0;
            Regex rgx = null;
            CultureInfo ci_in = CultureInfo.InvariantCulture;
            CultureInfo ci = CultureInfo.InvariantCulture;
            strFormat = Utils.ParseFormat(strFormat, out  ci_in, out ci);
            switch (l_Operation)
            {
                case "guid":
                    sResult = Guid.NewGuid().ToString();
                    break;
                case "plus":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal + dVal;
                    if (l_Value3 != "")
                    {
                        if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as double;");
                        doubleVal = doubleVal + dVal;
                    }
                    if (l_Value4 != "")
                    {
                        if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as double;");
                        doubleVal = doubleVal + dVal;
                    }
                    if (l_Value5 != "")
                    {
                        if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as double;");
                        doubleVal = doubleVal + dVal;
                    }
                    if (l_Value6 != "")
                    {
                        if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as double;");
                        doubleVal = doubleVal + dVal;
                    }
                    sResult = doubleVal.ToString(strFormat, ci);
                    break;
                case "minus":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal - dVal;
                    if (l_Value3 != "")
                    {
                        if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as double;");
                        doubleVal = doubleVal - dVal;
                    }
                    if (l_Value4 != "")
                    {
                        if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as double;");
                        doubleVal = doubleVal - dVal;
                    }
                    if (l_Value5 != "")
                    {
                        if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as double;");
                        doubleVal = doubleVal - dVal;
                    }
                    if (l_Value6 != "")
                    {
                        if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as double;");
                        doubleVal = doubleVal - dVal;
                    }
                    sResult = doubleVal.ToString(strFormat, ci);
                    break;
                case "mult":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal * dVal;
                    if (l_Value3 != "")
                    {
                        if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as double;");
                        doubleVal = doubleVal * dVal;
                    }
                    if (l_Value4 != "")
                    {
                        if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as double;");
                        doubleVal = doubleVal * dVal;
                    }
                    if (l_Value5 != "")
                    {
                        if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as double;");
                        doubleVal = doubleVal * dVal;
                    }
                    if (l_Value6 != "")
                    {
                        if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as double;");
                        doubleVal = doubleVal * dVal;
                    }
                    sResult = doubleVal.ToString(strFormat, ci);
                    break;
                case "div":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal / dVal;
                    if (l_Value3 != "")
                    {
                        if (!double.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as double;");
                        doubleVal = doubleVal / dVal;
                    }
                    if (l_Value4 != "")
                    {
                        if (!double.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as double;");
                        doubleVal = doubleVal / dVal;
                    }
                    if (l_Value5 != "")
                    {
                        if (!double.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as double;");
                        doubleVal = doubleVal / dVal;
                    }
                    if (l_Value6 != "")
                    {
                        if (!double.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                            throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as double;");
                        doubleVal = doubleVal / dVal;
                    }
                    sResult = doubleVal.ToString(strFormat, ci);
                    break;
                case "mod":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal % dVal;
                    sResult = doubleVal.ToString(strFormat, ci);
                    break;
                case "trunc":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out doubleVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    if (l_Value2 == "") l_Value2 = "1";
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as double;");
                    doubleVal = doubleVal % dVal;
                    doubleVal = Math.Truncate(doubleVal);
                    sResult = doubleVal.ToString(strFormat, ci);
                    break;
                case "string_concat":
                    sResult = l_Value1 + l_Value2 + l_Value3 + l_Value4 + l_Value5 + l_Value6;
                    break;
                case "string_length":
                    doubleVal = l_Value1.Length;
                    sResult = doubleVal.ToString(strFormat, ci);
                    break;
                case "string_trim":
                    l_Value1 = l_Value1.Trim();
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_trimend":
                    l_Value1 = l_Value1.TrimEnd();
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_trimstart":
                    l_Value1 = l_Value1.TrimStart();
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_toupper":
                    l_Value1 = l_Value1.ToUpper();
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_tolower":
                    l_Value1 = l_Value1.ToLower();
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_substring":
                    if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    if (l_Value3 != "")
                    {
                        if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
                            throw new Exception("Parameter Value2: " + l_Value3 + " cannot evaluate as int;");
                        l_Value1 = l_Value1.Substring(iVal2, iVal3);
                    }
                    else
                        l_Value1 = l_Value1.Substring(iVal2);
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_startswith":
                    l_Value1 = (l_Value1.StartsWith(l_Value2)) ? "1" : "0";
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_endswith":
                    l_Value1 = (l_Value1.EndsWith(l_Value2)) ? "1" : "0";
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_replace":
                    l_Value1 = l_Value1.Replace(l_Value2, l_Value3);
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_contains":
                    l_Value1 = (l_Value1.Contains(l_Value2)) ? "1" : "0";
                    sResult = l_Value1;//.ToString(strFormat, ci);
                    break;
                case "string_indexof":
                    i_Value = l_Value1.IndexOf(l_Value2);
                    if (l_Value3 != "")
                    {
                        if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
                            throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as int;");
                        i_Value = l_Value1.IndexOf(l_Value2, iVal3);
                    }
                    if (l_Value4 != "")
                    {
                        if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
                            throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as int;");
                        i_Value = l_Value1.IndexOf(l_Value2, iVal3, iVal4);
                    }
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_day":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as double;");
                    i_Value = datetimeVal.Day;
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_month":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Month;
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_year":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Year;
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_hour":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Hour;
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_minute":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Minute;
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_second":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Second;
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_millisecond":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    i_Value = datetimeVal.Millisecond;
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_ticks":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    sResult = datetimeVal.Ticks.ToString(strFormat, ci);
                    break;
                case "datetime_dayofweek":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    sResult = ((int)datetimeVal.DayOfWeek).ToString(strFormat, ci);
                    break;
                case "datetime_dayofyear":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    sResult = ((int)datetimeVal.DayOfYear).ToString(strFormat, ci);
                    break;
                case "datetime_subtract":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!TimeSpan.TryParse(l_Value2, out timespanVal2))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as timespan;");
                    datetimeVal = datetimeVal.Subtract(timespanVal2);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_add":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!TimeSpan.TryParse(l_Value2, out timespanVal2))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as timespan;");
                    datetimeVal = datetimeVal.Add(timespanVal2);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_subtractduration":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    try
                    {
                        timespanVal2 = System.Xml.XmlConvert.ToTimeSpan(l_Value2);
                    }
                    catch (Exception )
                    {
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as duration;");
                    }
                    datetimeVal = datetimeVal.Subtract(timespanVal2);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addduration":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    try
                    {
                        timespanVal2 = System.Xml.XmlConvert.ToTimeSpan(l_Value2);
                    }
                    catch (Exception )
                    {
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as duration;");
                    }
                    datetimeVal = datetimeVal.Add(timespanVal2);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addmilliseconds":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddMilliseconds(dVal);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addseconds":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddSeconds(dVal);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addminutes":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddMinutes(dVal);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addhours":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddHours(dVal);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_adddays":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!double.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddDays(dVal);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addmonths":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddMonths(iVal2);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_addyears":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    datetimeVal = datetimeVal.AddYears(iVal2);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    //sResult = datetimeVal.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    break;
                case "datetime_addworkminutes":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!int.TryParse(l_Value2, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as int;");
                    if (l_Value3 == "") l_Value3 = "0";
                    if (l_Value4 == "") l_Value4 = "0";
                    if (l_Value5 == "") l_Value5 = "0";
                    if (l_Value6 == "") l_Value6 = "0";
                    if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
                        throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
                        throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out iVal5))
                        throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out iVal6))
                        throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as int;");
                    datetimeVal = Utils.DateTime_AddWorkMinutes(datetimeVal, iVal2, iVal3, iVal4, iVal5, iVal6);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime_diffworkminutes":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!DateTime.TryParse(l_Value2, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal2))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (l_Value3 == "") l_Value3 = "0";
                    if (l_Value4 == "") l_Value4 = "0";
                    if (l_Value5 == "") l_Value5 = "0";
                    if (l_Value6 == "") l_Value6 = "0";
                    if (!int.TryParse(l_Value3, System.Globalization.NumberStyles.Any, ci_in, out iVal3))
                        throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
                        throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value5, System.Globalization.NumberStyles.Any, ci_in, out iVal5))
                        throw new Exception("Parameter Value5: " + l_Value5 + " cannot evaluate as int;");
                    if (!int.TryParse(l_Value6, System.Globalization.NumberStyles.Any, ci_in, out iVal6))
                        throw new Exception("Parameter Value6: " + l_Value6 + " cannot evaluate as int;");
                    i_Value = Utils.DateTime_CalcWorkMinutes(datetimeVal, datetimeVal2, iVal3, iVal4, iVal5, iVal6);
                    sResult = i_Value.ToString(strFormat, ci);
                    break;
                case "datetime_diffdatetime":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    if (!DateTime.TryParse(l_Value2, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal2))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    timespanVal2 = datetimeVal2.Subtract(datetimeVal);
                    //datetimeVal = new DateTime(timespanVal2.Ticks);
                    //sResult = datetimeVal.ToString(strFormat, ci);
                    sResult = timespanVal2.TotalMilliseconds.ToString();
                    break;
                case "json_path":
                    sJSON = new StringBuilder();
                    Utils.myPathParamsRender(sJSON, l_Value1);
                    sResult = sJSON.ToString();
                    sJSON.Clear();
                    sJSON = null;
                    break;
                case "json_query":
                    sJSON = new StringBuilder();
                    Utils.myQueryRender(sJSON, l_Value1);
                    sResult = sJSON.ToString();
                    sJSON.Clear();
                    sJSON = null;
                    break;
                case "xml2json":
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(l_Value1);
                    sResult = JsonConvert.SerializeXmlNode(doc);
                    break;
                case "json2xml":
                    XmlDocument doc1 = JsonConvert.DeserializeXmlNode(l_Value1);
                    sResult = doc1.OuterXml;
                    break;
                case "is_json":
                    try
                    {
                        JObject o1 = JObject.Parse(l_Value1);
                        o1 = null;
                        sResult = "1";
                    }
                    catch (Exception)
                    {
                        sResult = "0";
                    }
                    break;
                case "is_jsonarray":
                    try
                    {
                        JArray o2 = JArray.Parse(l_Value1);
                        o2 = null;
                        sResult = "1";
                    }
                    catch (Exception)
                    {
                        sResult = "0";
                    }
                    break;
                case "is_xml":
                    try
                    {
                        XmlDocument doc2 = new XmlDocument();
                        doc2.LoadXml(l_Value1);
                        doc2 = null;
                        sResult = "1";
                    }
                    catch (Exception)
                    {
                        sResult = "0";
                    }
                    break;
                case "is_int":
                    if (int.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out iVal2))
                        sResult = "1";
                    else
                        sResult = "0";
                    break;
                case "is_double":
                    if (double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        sResult = "1";
                    else
                        sResult = "0";
                    break;
                case "is_date":
                case "is_datetime":
                    if (DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        sResult = "1";
                    else
                        sResult = "0";
                    break;
                case "is_timespan":
                    if (TimeSpan.TryParse(l_Value1, out timespanVal2))
                        sResult = "1";
                    else
                        sResult = "0";
                    break;
                case "regex_replace":
                    if (l_Value4 != "")
                    {
                        //#FUNC.Node_Name.RegexReplace..%Node_Name%.[\.:/]._.1#                     
                        if (!int.TryParse(l_Value4, System.Globalization.NumberStyles.Any, ci_in, out iVal4))
                            throw new Exception("Parameter Value4: " + l_Value4 + " cannot evaluate as int;");
                        rgx = new Regex(l_Value2);
                        sResult = rgx.Replace(l_Value1, l_Value3, iVal4);
                    }
                    else
                    {
                        //#FUNC.Node_Name.RegexReplace..%Node_Name%.[\.:/]._.#                     
                        sResult = Regex.Replace(l_Value1, l_Value2, l_Value3);
                    }
                    break;
                case "jwt_token":
                    //header,payload,key,alg=HS256
                    sResult = JWT.JsonWebToken.JWTToken(l_Value1, l_Value2, l_Value3, l_Value5);
                    break;
                case "jwtencode":
                case "jwt_encode":
                    //payload,key,alg=HS256
                    sResult = JWT.JsonWebToken.JWTEncode(l_Value1, l_Value2, l_Value4);
                    break;
                case "jwtdecode":
                case "jwt_decode":
                    //token,key,verify=1
                    if (l_Value3 == "") l_Value3 = "true";
                    if (!bool.TryParse(l_Value3, out bVal))
                        throw new Exception("Parameter Value3: " + l_Value3 + " cannot evaluate as bool;");
                    sResult = JWT.JsonWebToken.JWTDecode(l_Value1, l_Value2, bVal);
                    break;
                case "salt":
                    if (l_Value2 == "") l_Value2 = "false";
                    if (!bool.TryParse(l_Value2, out bVal))
                        throw new Exception("Parameter Value2: " + l_Value2 + " cannot evaluate as bool;");
                    sResult = Utils.SaltKey(l_Value1, bVal);
                    break;
                case "jsonminify": case "json_minify":
                    //JObject om = JObject.Parse(l_Value1);
                    JToken om = JToken.Parse(l_Value1);
                    sResult = om.ToString(Newtonsoft.Json.Formatting.None);
                    break;
                case "jsonbeautify": case "json_beautify":
                    //JObject ob = JObject.Parse(l_Value1);
                    JToken ob = JToken.Parse(l_Value1);
                    sResult = ob.ToString(Newtonsoft.Json.Formatting.Indented);
                    break;
                case "datetime2milliseconds":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    sResult = (TimeZoneInfo.ConvertTimeToUtc(datetimeVal) -                   new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalMilliseconds.ToString("F0");
                    break;
                case "milliseconds2datetime":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value1 + " cannot evaluate as double;");
                    datetimeVal = (new DateTime(1970, 1, 1)).AddMilliseconds(dVal);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "datetime2unixtimestamp":
                    if (!DateTime.TryParse(l_Value1, ci_in, System.Globalization.DateTimeStyles.AssumeLocal, out datetimeVal))
                        throw new Exception("Parameter Value1: " + l_Value1 + " cannot evaluate as datetime;");
                    sResult = Utils.DateTimeToUnixTimeStamp(datetimeVal).ToString("F0");
                    break;
                case "unixtimestamp2datetime":
                    if (!double.TryParse(l_Value1, System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        throw new Exception("Parameter Value2: " + l_Value1 + " cannot evaluate as double;");
                    datetimeVal = Utils.UnixTimeStampToDateTime(dVal);
                    sResult = datetimeVal.ToString(strFormat, ci);
                    break;
                case "md5":
                    sResult = Utils.ComputeMD5(l_Value1);
                    break;
                case "sha1":
                    sResult = Utils.ComputeSHA1(l_Value1);
                    break;
                case "sha256":
                    sResult = Utils.ComputeSHA256(l_Value1);
                    break;
                case "sha384":
                    sResult = Utils.ComputeSHA384(l_Value1);
                    break;
                case "sha512":
                    sResult = Utils.ComputeSHA512(l_Value1);
                    break;
                case "hmacsha1":
                    sResult = Utils.ComputeHMACSHA1(l_Value1, l_Value2);
                    break;
                case "facebookhash":
                    sResult = Utils.ComputeFacebookHash(l_Value1, l_Value2);
                    break;
                case "hash":
                    sResult = Utils.ComputeSHA1(l_Value2 + l_Value1);
                    break;
                case "encrypt":
                    l_Value2 = Utils.SaltKey(l_Value2, false);
                    sResult = Utils.ComputeEncrypt(l_Value1, l_Value2);
                    break;
                case "decrypt":
                    l_Value2 = Utils.SaltKey(l_Value2, false);
                    sResult = Utils.ComputeDecrypt(l_Value1, l_Value2);
                    break;
                case "compute_encrypt":
                    sResult = Utils.ComputeEncrypt(l_Value1, l_Value2);
                    break;
                case "compute_decrypt":
                    sResult = Utils.ComputeDecrypt(l_Value1, l_Value2);
                    break;
                case "rijndael_encrypt":
                    sResult = Utils.ComputeRijndaelEncrypt(l_Value1, l_Value2);
                    break;
                case "rijndael_decrypt":
                    sResult = Utils.ComputeRijndaelDecrypt(l_Value1, l_Value2);
                    break;
                case "file_exists":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    fileinfo = new FileInfo(l_Value1);
                    if (fileinfo.Exists)
                        sResult = "1";
                    else
                        sResult = "0";
                    break;
                case "file_fullname":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    fileinfo = new FileInfo(l_Value1);
                    if (fileinfo.Exists)
                        sResult = fileinfo.FullName;
                    else
                        sResult = "";
                    break;
                case "file_create_text":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    fileinfo = new FileInfo(l_Value1);
                    fileinfo.Directory.Create(); // If the directory already exists, this method does nothing.
                    File.WriteAllText(fileinfo.FullName, l_Value2);
                    sResult = l_Value1;
                    break;
                case "file_create":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    fileinfo = new FileInfo(l_Value1);
                    fileinfo.Directory.Create(); // If the directory already exists, this method does nothing.
                    binaryData = Encoding.Default.GetBytes(l_Value2);
                    File.WriteAllBytes(fileinfo.FullName, binaryData);
                    sResult = l_Value1;
                    break;
                case "file_create8":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    fileinfo = new FileInfo(l_Value1);
                    fileinfo.Directory.Create(); // If the directory already exists, this method does nothing.
                    binaryData = Encoding.UTF8.GetBytes(l_Value2);
                    File.WriteAllBytes(fileinfo.FullName, binaryData);
                    sResult = l_Value1;
                    break;
                case "file_create64":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    fileinfo = new FileInfo(l_Value1);
                    fileinfo.Directory.Create(); // If the directory already exists, this method does nothing.
                    if ((l_Value2.Length % 4) > 0) l_Value2 += new string('=', 4 - (l_Value2.Length % 4));
                    binaryData = Convert.FromBase64String(l_Value2);
                    File.WriteAllBytes(fileinfo.FullName, binaryData);
                    sResult = l_Value1;
                    break;
                case "file_copy":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    l_Value2 = GetFullFilePath(l_Value2);
                    attr = File.GetAttributes(l_Value1);
                    if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        File.SetAttributes(l_Value1, attr ^ FileAttributes.ReadOnly);
                    File.Copy(l_Value1, l_Value2);
                    sResult = l_Value2;
                    break;
                case "file_rename":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    l_Value2 = GetFullFilePath(l_Value2);
                    attr = File.GetAttributes(l_Value1);
                    if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        File.SetAttributes(l_Value1, attr ^ FileAttributes.ReadOnly);
                    File.Move(l_Value1, l_Value2);
                    sResult = l_Value2;
                    break;
                case "file_remove":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    attr = File.GetAttributes(l_Value1);
                    if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        File.SetAttributes(l_Value1, attr ^ FileAttributes.ReadOnly);
                    File.Delete(l_Value1);
                    sResult = l_Value1;
                    break;
                case "dir_exists":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    sResult = Directory.Exists(l_Value1) ? "1" : "0";
                    break;
                case "dir_create":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    Directory.CreateDirectory(l_Value1);
                    sResult = l_Value1;
                    break;
                case "dir_copy":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    l_Value2 = GetFullFilePath(l_Value2);
                    dir_Copy(l_Value1, l_Value2, false);
                    sResult = l_Value2;
                    break;
                case "dir_merge":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    l_Value2 = GetFullFilePath(l_Value2);
                    dir_Copy(l_Value1, l_Value2, true);
                    sResult = l_Value2;
                    break;
                case "dir_rename":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    l_Value2 = GetFullFilePath(l_Value2);
                    Directory.Move(l_Value1, l_Value2);
                    sResult = l_Value2;
                    break;
                case "dir_remove":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    sResult = DeleteDirectory(l_Value1, true, true).ToString();
                    break;
                case "dir_clear":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    try
                    {
                        bVal = Convert.ToBoolean(l_Value2);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            bVal = Convert.ToBoolean(Convert.ToInt32(l_Value2));
                        }
                        catch (Exception)
                        {
                        }
                    };
                    sResult = DeleteDirectory(l_Value1, bVal, false).ToString();
                    break;
                case "path":
                    //sResult = System.Web.HttpContext.Current.Server.MapPath("~");
                    sResult = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~"), l_Value1);
                    break;
                case "file_json":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    try
                    {
                        bVal = Convert.ToBoolean(l_Value2);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            bVal = Convert.ToBoolean(Convert.ToInt32(l_Value2));
                        }
                        catch (Exception)
                        {
                        }
                    };
                    sResult = "[" + GetJsonFiles(l_Value1, bVal).Substring(1) + "]";
                    break;
                case "dir_json":
                    m_XFilesPath = process.m_runtime.AppCache.XFilesPath;
                    l_Value1 = GetFullFilePath(l_Value1);
                    try
                    {
                        bVal = Convert.ToBoolean(l_Value2);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            bVal = Convert.ToBoolean(Convert.ToInt32(l_Value2));
                        }
                        catch (Exception)
                        {
                        }
                    };
                    sResult =  GetJsonDirectory(l_Value1, bVal);
                    break;
                case "get_params":
                    sResult = process.GetJsonParameters();
                    break;
                case "get_templates":
                    sResult = process.m_runtime.AppCache.AppCacheCommand("GET");
                    break;
                case "get_sv":
                    sResult = process.m_runtime.SVCache.SVCacheCommand("GET");
                    break;
                case "expr":
                    DataTable dt = new DataTable();
                    string expr = process.ReplaceParameters(l_Value1);
                    sResult = dt.Compute(expr, "").ToString();
                    break;
                case "xml_sign":
                    //xml, certFilePath, CertPassword
                    sResult = FileCertSignXml(l_Value1, l_Value2, l_Value3);
                    break;
                case "xml_verify":
                    //xmlsigned, certFilePath
                    sResult = FileCertVerifyXml(l_Value1, l_Value2);
                    break;
                case "xml_signstore":
                    // xml, storename, storeLocation, CertKey/CertThumbprint, FindMethod = SubjectName
                    sResult = StoreCertSignXml(l_Value1, l_Value2, l_Value3, l_Value4, l_Value5);
                    break;
                case "xml_verifystore":
                    // xmlsigned, storename, storeLocation, CertKey/CertThumbprint, FindMethod = SubjectName
                    sResult = StoreCertVerifyXml(l_Value1, l_Value2, l_Value3, l_Value4, l_Value5);
                    break;
                case "adfs_login":
                    // xmlsigned, storename, storeLocation, CertKey/CertThumbprint, FindMethod = SubjectName
                    sResult = AdfsLogin(l_Value1, l_Value2, l_Value3);
                    break;
                case "adfs_logout":
                    // xmlsigned, storename, storeLocation, CertKey/CertThumbprint, FindMethod = SubjectName
                    sResult = AdfsLogout(l_Value1,l_Value2);
                    break;
                case "adfs_adduri":
                    // xmlsigned, storename, storeLocation, CertKey/CertThumbprint, FindMethod = SubjectName
                    sResult = AdfsAddUri(l_Value1);
                    break;
                case "ws_addticket":
                    // ticket, user
                    sResult = process.m_runtime.Clients.addTicket(l_Value1, l_Value2);
                    break;
                case "ws_removeticket":
                    // ticket
                    sResult = process.m_runtime.Clients.removeTicket(l_Value1);
                    break;
                case "ws_removeuser":
                    // user
                    sResult = process.m_runtime.Clients.removeUser(l_Value1);
                    break;
                case "ws_sendticket":
                    // ticket, message
                    process.m_runtime.Clients.sendTicket(l_Value1,l_Value2);
                    break;
                case "ws_senduser":
                    // user, message
                    sResult = process.m_runtime.Clients.sendUser(l_Value1, l_Value2);
                    break;
                case "ws_getusers":
                    // user, open
                    sResult = sResult = process.m_runtime.Clients.getUsers(l_Value1);
                    break;
                case "ws_gettickets":
                    // user, open
                    sResult = process.m_runtime.Clients.getTickets(l_Value1, l_Value2);
                    break;
                case "ws_getticket":
                    // ticket, open
                    sResult = process.m_runtime.Clients.getTicket(l_Value1, l_Value2);
                    break;
                default:
                    throw new Exception("FUNC not recognized");
            }
            return sResult;
        }
        internal static void  doFUNC_1() { }
        //E:\web_project\IRIS4\Iris4_Bamboo\xml-data\build-dir\IRIS4-DEV-JOB1\Client
        private string dir_Copy(string sourceDir, string targetDir, bool overwrite)
        {
            Directory.CreateDirectory(targetDir);

            foreach (var file in Directory.GetFiles(sourceDir))
                File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)), overwrite);

            foreach (var directory in Directory.GetDirectories(sourceDir))
                dir_Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)), overwrite);
            return "";
        }
        public string AdfsAddUri(string sUri)
        {
            // Claims
            //#FUNC.r.adfs_adduri..%trust%#
            //Uri uri = new Uri("https://5.175.84.232/xDocClaims/test/a1");
            Uri uri = new Uri(sUri);
            if (!FederatedAuthentication.ServiceConfiguration.AudienceRestriction.AllowedAudienceUris.Contains(uri))
                FederatedAuthentication.ServiceConfiguration.AudienceRestriction.AllowedAudienceUris.Add(uri);
            return "";
        }

        public string AdfsLogin(string issuer, string trust, string callbackUrl)
        {
            // Claims
            //      #SPAR.url1.%issuer%?wa=wsignin1.0&amp;wtrealm=%trust%&amp;wctx=rm%3d0%26id%3dpassive%26ru%3d%callback.urlencode%#
            string myUrl = "";
            IClaimsIdentity claimsPrincipal = Thread.CurrentPrincipal.Identity as IClaimsIdentity;

            if (!claimsPrincipal.IsAuthenticated)
            {
                if (issuer != "")
                    myUrl = issuer + "?wa=wsignin1.0&wtrealm=" + trust + "&wctx=rm%3d0%26id%3dpassive%26ru%3d" + HttpUtility.UrlEncode(callbackUrl);
                else
                {
                    if (FederatedAuthentication.WSFederationAuthenticationModule == null) throw (new Exception("WSFederationAuthenticationModule invalid"));
                    Microsoft.IdentityModel.Protocols.WSFederation.SignInRequestMessage signInRequest = FederatedAuthentication.WSFederationAuthenticationModule.CreateSignInRequest("passive", callbackUrl, false);
                    myUrl = signInRequest.RequestUrl;
                }
                HttpContext.Current.Response.Redirect(myUrl);
            }
            //string myUrl = "";
            //if (STSUrl != "")
            //    myUrl = STSUrl + "?wa=signoutcleanup1.0&wreply=" + callbackUrl;
            //else
            //{
            //    if (FederatedAuthentication.WSFederationAuthenticationModule == null) throw (new Exception("WSFederationAuthenticationModule invalid"));
            //    Microsoft.IdentityModel.Protocols.WSFederation.SignInRequestMessage signInRequest = FederatedAuthentication.WSFederationAuthenticationModule.CreateSignInRequest("passive", callbackUrl, false);
            //    myUrl = signInRequest.RequestUrl;
            //}
            //HttpContext.Current.Response.Redirect(myUrl);
            ////error ???
            return "";
        }

        public string AdfsLogout(string issuer, string trust)
        {
            // Claims
            //      #SPAR.url2.%issuer%?wa=wsignoutcleanup1.0&amp;wreply=%trust2%#
            string myUrl = "";
            IClaimsIdentity claimsPrincipal = Thread.CurrentPrincipal.Identity as IClaimsIdentity;

            if (claimsPrincipal.IsAuthenticated)
            {
                if (issuer != "")
                    myUrl = issuer + "?wa=wsignoutcleanup1.0&wreply=" + trust;
                else
                {
                    if (FederatedAuthentication.WSFederationAuthenticationModule == null) throw (new Exception("WSFederationAuthenticationModule invalid"));
                    myUrl = FederatedAuthentication.WSFederationAuthenticationModule.Issuer + "?wa=wsignoutcleanup1.0&wreply=" + FederatedAuthentication.WSFederationAuthenticationModule.Realm;
                    FederatedAuthentication.WSFederationAuthenticationModule.SignOut(true);
                }
                HttpContext.Current.Response.Redirect(myUrl, true);
            }
            //            Microsoft.IdentityModel.Protocols.WSFederation.SignInRequestMessage signInRequest = FederatedAuthentication.WSFederationAuthenticationModule.CreateSignInRequest("passive", callbackUrl, false);
            //            string myurl = signInRequest.RequestUrl;
            //            HttpContext.Current.Response.Redirect(myurl);
            //FederatedAuthentication.WSFederationAuthenticationModule.SignOut(true);
            //HttpContext.Current.Response.Redirect(callbackUrl);
            return "";
        }
        
        public sealed class RSAPKCS1SHA256SignatureDescription : SignatureDescription
	{
		/// <summary>
		///     Construct an RSAPKCS1SHA256SignatureDescription object. The default settings for this object
		///     are:
		///     <list type="bullet">
		///         <item>Digest algorithm - <see cref="SHA256Managed" /></item>
		///         <item>Key algorithm - <see cref="RSACryptoServiceProvider" /></item>
		///         <item>Formatter algorithm - <see cref="RSAPKCS1SignatureFormatter" /></item>
		///         <item>Deformatter algorithm - <see cref="RSAPKCS1SignatureDeformatter" /></item>
		///     </list>
		/// </summary>
		public RSAPKCS1SHA256SignatureDescription()
		{
			KeyAlgorithm = typeof(RSACryptoServiceProvider).FullName;
			DigestAlgorithm = typeof(SHA256Managed).FullName;   // Note - SHA256CryptoServiceProvider is not registered with CryptoConfig
			FormatterAlgorithm = typeof(RSAPKCS1SignatureFormatter).FullName;
			DeformatterAlgorithm = typeof(RSAPKCS1SignatureDeformatter).FullName;
		}

		/// <summary>
		/// Create deformatter
		/// </summary>
		/// <param name="key">The key</param>
		/// <returns>Formatter Info</returns>
		public override AsymmetricSignatureDeformatter CreateDeformatter(AsymmetricAlgorithm key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}

			RSAPKCS1SignatureDeformatter deformatter = new RSAPKCS1SignatureDeformatter(key);
			deformatter.SetHashAlgorithm("SHA256");
			return deformatter;
		}

		/// <summary>
		/// Create formatter
		/// </summary>
		/// <param name="key">The key</param>
		/// <returns>Signature Formatter</returns>
		public override AsymmetricSignatureFormatter CreateFormatter(AsymmetricAlgorithm key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}

			RSAPKCS1SignatureFormatter formatter = new RSAPKCS1SignatureFormatter(key);
			formatter.SetHashAlgorithm("SHA256");
			return formatter;
		}
	}


        // #FUNC..xml_sign..%MyXML%.c:\cert\.p12.myPass# => xml:string
        public static string FileCertSignXml(string strXml, string strCertificateLocation, string strCertificatePassword)
        {
            X509Certificate2 certificate = new X509Certificate2(strCertificateLocation, strCertificatePassword, X509KeyStorageFlags.Exportable);
            CspParameters cspParams = new CspParameters(24);
            cspParams.KeyContainerName = "XML_DISG_RSA_KEY";
            RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);
            if (certificate.PrivateKey == null) throw new Exception("Certificate contains no PrivateKey");
            rsaKey.FromXmlString(certificate.PrivateKey.ToXmlString(true));

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.LoadXml(strXml);

            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            SignedXml signedXml = new SignedXml(xmlDoc);
            signedXml.SigningKey = rsaKey;
            signedXml.SignedInfo.CanonicalizationMethod = "http://www.w3.org/2001/10/xml-exc-c14n#";
            signedXml.SignedInfo.SignatureMethod = @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

            // Create a reference to be signed.
            Reference reference = new Reference();
            reference.Uri = "";
            reference.DigestMethod = @"http://www.w3.org/2001/04/xmlenc#sha256";
            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);
            signedXml.AddReference(reference);

            KeyInfo keyInfo = new KeyInfo();
            KeyInfoName kin = new KeyInfoName();
            kin.Value = certificate.Thumbprint;
            keyInfo.AddClause(kin);
            signedXml.KeyInfo = keyInfo;

            signedXml.ComputeSignature();
            XmlElement xmlDigitalSignature = signedXml.GetXml();
            xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
            return xmlDoc.OuterXml;
        }
        // #FUNC..xml_verify.%MyXML%.c:\cert\.cer# => 0/1:string
        public static string FileCertVerifyXml(string strXmlSigned, string strCertificateLocation) //(XmlDocument Doc, RSA Key)
        {
            X509Certificate2 certificate = new X509Certificate2(strCertificateLocation);
            RSACryptoServiceProvider rsaKey = (RSACryptoServiceProvider)certificate.PublicKey.Key;

            XmlDocument Doc = new XmlDocument();
            Doc.PreserveWhitespace = true;
            Doc.LoadXml(strXmlSigned);

            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            SignedXml signedXml = new SignedXml(Doc);

            XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");
            if (nodeList.Count != 1)
            {
                throw new CryptographicException("Verification failed: No, or more than one, signature(s) was found in the document.");
            }
            signedXml.LoadXml((XmlElement)nodeList[0]);

            return signedXml.CheckSignature(rsaKey) ? "1" : "0";
        }
        // #FUNC..xml_sign..%MyXML%.Sto# => xml:string
        // #FUNC..xml_signstore.%MyXML%.%storeName%.%storeLocation%.%certKey%..%storeFindBy%# =>  xml:string
        public static string StoreCertSignXml(string strXml, string strStoreName, string strStoreLocation, string strCertificateKey, string strStoreFindBy)
        {
            X509Certificate2 certificate = FindCertificateInStore(strStoreName, strStoreLocation, strCertificateKey, strStoreFindBy);
            if (certificate == null)
                throw new Exception("Certificate not found");
            CspParameters cspParams = new CspParameters(24);
            cspParams.KeyContainerName = "XML_DISG_RSA_KEY";
            RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);
            if (certificate.PrivateKey == null) throw new Exception("Certificate contains no PrivateKey");
            rsaKey.FromXmlString(certificate.PrivateKey.ToXmlString(true));

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.LoadXml(strXml);

            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            SignedXml signedXml = new SignedXml(xmlDoc);
            signedXml.SigningKey = rsaKey;
            signedXml.SignedInfo.CanonicalizationMethod = "http://www.w3.org/2001/10/xml-exc-c14n#";
            signedXml.SignedInfo.SignatureMethod = @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

            // Create a reference to be signed.
            Reference reference = new Reference();
            reference.Uri = "";
            reference.DigestMethod = @"http://www.w3.org/2001/04/xmlenc#sha256";
            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);
            signedXml.AddReference(reference);

            KeyInfo keyInfo = new KeyInfo();
            KeyInfoName kin = new KeyInfoName();
            kin.Value = certificate.Thumbprint;
            keyInfo.AddClause(kin);
            signedXml.KeyInfo = keyInfo;

            signedXml.ComputeSignature();
            XmlElement xmlDigitalSignature = signedXml.GetXml();
            xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
            return xmlDoc.OuterXml;
        }
        // #FUNC..xml_verifystore.%MyXMLSigned%.%storeName%.%storeLocation%.%certKey%..%storeFindBy%# => 0/1:string
        // xmlsigned, storeName, storeLocation, CertKey/CertThumbprint, FindMethod = SubjectName
        public static string StoreCertVerifyXml(string strXmlSigned, string strStoreName, string strStoreLocation, string strCertificateKey, string strStoreFindBy)
        {
            X509Certificate2 certificate = FindCertificateInStore(strStoreName, strStoreLocation, strCertificateKey, strStoreFindBy);
            if (certificate == null)
                throw new Exception("Certificate not found");
            RSACryptoServiceProvider rsaKey = (RSACryptoServiceProvider)certificate.PublicKey.Key;

            XmlDocument Doc = new XmlDocument();
            Doc.PreserveWhitespace = true;
            Doc.LoadXml(strXmlSigned);

            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            SignedXml signedXml = new SignedXml(Doc);

            XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");
            if (nodeList.Count != 1)
            {
                throw new CryptographicException("Verification failed: No, or more than one, signature(s) was found in the document.");
            }
            signedXml.LoadXml((XmlElement)nodeList[0]);

            return signedXml.CheckSignature(rsaKey) ? "1" : "0";
        }
        private static X509Certificate2 FindCertificateInStore(string strStoreName, string strStoreLocation, string strCertificateKey, string strStoreFindBy)
        {
            StoreName sn = StoreName.My;
            StoreLocation sl = StoreLocation.LocalMachine;
            X509FindType ft = X509FindType.FindBySubjectName;
            switch (strStoreName.ToLower())
            {
                case "addressbook":
                    sn = StoreName.AddressBook;
                    break;
                case "authroot":
                    sn = StoreName.AuthRoot;
                    break;
                case "ca":
                    sn = StoreName.CertificateAuthority;
                    break;
                case "certificateauthority":
                    sn = StoreName.CertificateAuthority;
                    break;
                case "disallowed":
                    sn = StoreName.Disallowed;
                    break;
                case "my":
                    sn = StoreName.My;
                    break;
                case "root":
                    sn = StoreName.Root;
                    break;
                case "trustedpeople":
                    sn = StoreName.TrustedPeople;
                    break;
                case "trustedpublisher":
                    sn = StoreName.TrustedPublisher;
                    break;
                case "":
                    sn = StoreName.My;
                    break;
                default:
                    throw new Exception("Invalid certificate StoreName: " + strStoreName);
                    //break;
            }
            switch (strStoreLocation.ToLower())
            {
                case "localmachine":
                    sl = StoreLocation.LocalMachine;
                    break;
                case "currentuser":
                    sl = StoreLocation.CurrentUser;
                    break;
                case "":
                    sl = StoreLocation.LocalMachine;
                    break;
                default:
                    throw new Exception("Invalid certificate StoreLocation: " + strStoreLocation);
                    //break;
            }
            strStoreFindBy = strStoreFindBy.ToLower();
            if (strStoreFindBy.StartsWith("findby")) strStoreFindBy = strStoreFindBy.Substring(6);
            switch (strStoreFindBy)
            {
                case "applicationpolicy":
                    ft = X509FindType.FindByApplicationPolicy;
                    break;
                case "certificatepolicy":
                    ft = X509FindType.FindByCertificatePolicy;
                    break;
                case "extension":
                    ft = X509FindType.FindByExtension;
                    break;
                case "issuerdistinguishedname":
                    ft = X509FindType.FindByIssuerDistinguishedName;
                    break;
                case "issuername":
                    ft = X509FindType.FindByIssuerName;
                    break;
                case "keyusage":
                    ft = X509FindType.FindByKeyUsage;
                    break;
                case "serialnumber":
                    ft = X509FindType.FindBySerialNumber;
                    break;
                case "subjectdistinguishedname":
                    ft = X509FindType.FindBySubjectDistinguishedName;
                    break;
                case "subjectkeyidentifier":
                    ft = X509FindType.FindBySubjectKeyIdentifier;
                    break;
                case "subjectname":
                    ft = X509FindType.FindBySubjectName;
                    break;
                case "templatename":
                    ft = X509FindType.FindByTemplateName;
                    break;
                case "thumbprint":
                    ft = X509FindType.FindByThumbprint;
                    break;
                case "timeexpired":
                    ft = X509FindType.FindByTimeExpired;
                    break;
                case "timenotyetvalid":
                    ft = X509FindType.FindByTimeNotYetValid;
                    break;
                case "timevalid":
                    ft = X509FindType.FindByTimeValid;
                    break;
                case "":
                    ft = X509FindType.FindByThumbprint;
                    break;
                default:
                    throw new Exception("Invalid certificate StoreFindType: " + strStoreFindBy);
                    //break;
            }
            if (strCertificateKey == "") return null;
            X509Store certStore = new X509Store(sn, sl);
            certStore.Open(OpenFlags.ReadOnly);
            var certCollection = certStore.Certificates.Find(ft, strCertificateKey, false);
            certStore.Close();
            if (certCollection.Count > 0)
            {
                return certCollection[0];
            }
            else
            {
                return null;
            }
        }

        //private static string FileCertSignXml(string strXml, string strCertificateLocation, string strCertificatePassword)
        //{
        //    X509Certificate2 certificate = new X509Certificate2(strCertificateLocation, strCertificatePassword, X509KeyStorageFlags.Exportable);
        //    CspParameters cspParams = new CspParameters(24);
        //    cspParams.KeyContainerName = "XML_DISG_RSA_KEY";
        //    RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);
        //    rsaKey.FromXmlString(certificate.PrivateKey.ToXmlString(true));

        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.PreserveWhitespace = true;
        //    xmlDoc.LoadXml(strXml);

        //    CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
        //    SignedXml signedXml = new SignedXml(xmlDoc);
        //    signedXml.SigningKey = rsaKey;
        //    signedXml.SignedInfo.CanonicalizationMethod = "http://www.w3.org/2001/10/xml-exc-c14n#";
        //    signedXml.SignedInfo.SignatureMethod = @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

        //    // Create a reference to be signed.
        //    Reference reference = new Reference();
        //    reference.Uri = "";
        //    reference.DigestMethod = @"http://www.w3.org/2001/04/xmlenc#sha256";
        //    XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
        //    reference.AddTransform(env);
        //    signedXml.AddReference(reference);

        //    KeyInfo keyInfo = new KeyInfo();
        //    KeyInfoName kin = new KeyInfoName();
        //    kin.Value = certificate.Thumbprint;
        //    keyInfo.AddClause(kin);
        //    signedXml.KeyInfo = keyInfo;

        //    signedXml.ComputeSignature();
        //    XmlElement xmlDigitalSignature = signedXml.GetXml();
        //    xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
        //    return xmlDoc.OuterXml;
        //}
        //// #FUNC..xml_verify.%MyXML%.c:\cert\.cer# => 0/1:string
        //private static string FileCertVerifyXml(string strXmlSigned, string strCertificateLocation) //(XmlDocument Doc, RSA Key)
        //{
        //    X509Certificate2 certificate = new X509Certificate2(strCertificateLocation);
        //    RSACryptoServiceProvider rsaKey = (RSACryptoServiceProvider)certificate.PublicKey.Key;

        //    XmlDocument Doc = new XmlDocument();
        //    Doc.PreserveWhitespace = true;
        //    Doc.LoadXml(strXmlSigned);

        //    CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
        //    SignedXml signedXml = new SignedXml(Doc);

        //    XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");
        //    if (nodeList.Count != 1)
        //    {
        //        throw new CryptographicException("Verification failed: No, or more than one, signature(s) was found in the document.");
        //    }
        //    signedXml.LoadXml((XmlElement)nodeList[0]);

        //    return signedXml.CheckSignature(rsaKey) ? "1" : "0";
        //}
        //// #FUNC..xml_sign..%MyXML%.Sto# => xml:string
        //// #FUNC..xml_signstore.%MyXML%.%storeName%.%storeLocation%.%certKey%..%storeFindBy%# =>  xml:string
        //private static string StoreCertSignXml(string strXml, string strStoreName, string strStoreLocation, string strCertificateKey,  string strStoreFindBy) 
        //{
        //    X509Certificate2 certificate = FindCertificateInStore(strStoreName,strStoreLocation,strCertificateKey,strStoreFindBy);
        //    if (certificate == null)
        //        throw new Exception("Certificate not found");
        //    CspParameters cspParams = new CspParameters(24);
        //    cspParams.KeyContainerName = "XML_DISG_RSA_KEY";
        //    RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider(cspParams);
        //    rsaKey.FromXmlString(certificate.PrivateKey.ToXmlString(true));

        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.PreserveWhitespace = true;
        //    xmlDoc.LoadXml(strXml);

        //    CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
        //    SignedXml signedXml = new SignedXml(xmlDoc);
        //    signedXml.SigningKey = rsaKey;
        //    signedXml.SignedInfo.CanonicalizationMethod = "http://www.w3.org/2001/10/xml-exc-c14n#";
        //    signedXml.SignedInfo.SignatureMethod = @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

        //    // Create a reference to be signed.
        //    Reference reference = new Reference();
        //    reference.Uri = "";
        //    reference.DigestMethod = @"http://www.w3.org/2001/04/xmlenc#sha256";
        //    XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
        //    reference.AddTransform(env);
        //    signedXml.AddReference(reference);

        //    KeyInfo keyInfo = new KeyInfo();
        //    KeyInfoName kin = new KeyInfoName();
        //    kin.Value = certificate.Thumbprint;
        //    keyInfo.AddClause(kin);
        //    signedXml.KeyInfo = keyInfo;

        //    signedXml.ComputeSignature();
        //    XmlElement xmlDigitalSignature = signedXml.GetXml();
        //    xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
        //    return xmlDoc.OuterXml;
        //}
        //// #FUNC..xml_verifystore.%MyXMLSigned%.%storeName%.%storeLocation%.%certKey%..%storeFindBy%# => 0/1:string
        //// xmlsigned, storeName, storeLocation, CertKey/CertThumbprint, FindMethod = SubjectName
        //private static string StoreCertVerifyXml(string strXmlSigned, string strStoreName, string strStoreLocation, string strCertificateKey,  string strStoreFindBy) 
        //{
        //    X509Certificate2 certificate = FindCertificateInStore(strStoreName,strStoreLocation,strCertificateKey,strStoreFindBy);
        //    if (certificate == null)
        //        throw new Exception("Certificate not found");
        //    RSACryptoServiceProvider rsaKey = (RSACryptoServiceProvider)certificate.PublicKey.Key;

        //    XmlDocument Doc = new XmlDocument();
        //    Doc.PreserveWhitespace = true;
        //    Doc.LoadXml(strXmlSigned);

        //    CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
        //    SignedXml signedXml = new SignedXml(Doc);

        //    XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");
        //    if (nodeList.Count != 1)
        //    {
        //        throw new CryptographicException("Verification failed: No, or more than one, signature(s) was found in the document.");
        //    }
        //    signedXml.LoadXml((XmlElement)nodeList[0]);

        //    return signedXml.CheckSignature(rsaKey) ? "1" : "0";
        //}
        //private static X509Certificate2 FindCertificateInStore(string strStoreName, string strStoreLocation, string strCertificateKey, string strStoreFindBy)
        //{
        //    StoreName sn = StoreName.My;
        //    StoreLocation sl = StoreLocation.LocalMachine;
        //    X509FindType ft = X509FindType.FindBySubjectName;
        //    switch ( strStoreName.ToLower()){
        //        case "addressbook":
        //            sn = StoreName.AddressBook;
        //            break;
        //        case "authroot":
        //            sn = StoreName.AuthRoot;
        //            break;
        //        case "certificateauthority":
        //            sn = StoreName.CertificateAuthority;
        //            break;
        //        case "disallowed":
        //            sn = StoreName.Disallowed;
        //            break;
        //        case "my":
        //            sn = StoreName.My;
        //            break;
        //        case "root":
        //            sn = StoreName.Root;
        //            break;
        //        case "trustedpeople":
        //            sn = StoreName.TrustedPeople;
        //            break;
        //        case "trustedpublisher":
        //            sn = StoreName.TrustedPublisher;
        //            break;
        //        default:
        //            sn = StoreName.My;
        //            break;
        //    }
        //    switch ( strStoreLocation.ToLower()){
        //        case "localmachine":
        //            sl = StoreLocation.LocalMachine;
        //            break;
        //        case "currentuser":
        //            sl = StoreLocation.CurrentUser;
        //            break;
        //        default:
        //            sl = StoreLocation.LocalMachine;
        //            break;
        //    }
        //    strStoreFindBy=strStoreFindBy.ToLower();
        //    if(strStoreFindBy.StartsWith("findby")) strStoreFindBy = strStoreFindBy.Substring(6);
        //    switch ( strStoreFindBy){
        //        case "applicationpolicy":
        //             ft = X509FindType.FindByApplicationPolicy;
        //            break;
        //        case "certificatepolicy":
        //             ft = X509FindType.FindByCertificatePolicy;
        //            break;
        //        case "extension":
        //             ft = X509FindType.FindByExtension;
        //            break;
        //        case "issuerdistinguishedname":
        //             ft = X509FindType.FindByIssuerDistinguishedName;
        //            break;
        //        case "issuername":
        //             ft = X509FindType.FindByIssuerName;
        //            break;
        //        case "keyusage":
        //             ft = X509FindType.FindByKeyUsage;
        //            break;
        //        case "serialnumber":
        //             ft = X509FindType.FindBySerialNumber;
        //            break;
        //        case "subjectdistinguishedname":
        //             ft = X509FindType.FindBySubjectDistinguishedName;
        //            break;
        //        case "subjectkeyidentifier":
        //             ft = X509FindType.FindBySubjectKeyIdentifier;
        //            break;
        //        case "subjectname":
        //             ft = X509FindType.FindBySubjectName;
        //            break;
        //        case "templatename":
        //             ft = X509FindType.FindByTemplateName;
        //            break;
        //        case "thumbprint":
        //             ft = X509FindType.FindByThumbprint;
        //            break;
        //        case "timeexpired":
        //             ft = X509FindType.FindByTimeExpired;
        //            break;
        //        case "timenotyetvalid":
        //             ft = X509FindType.FindByTimeNotYetValid;
        //            break;
        //        case "timevalid":
        //             ft = X509FindType.FindByTimeValid;
        //            break;
        //        default:
        //             ft = X509FindType.FindBySubjectName;
        //            break;
        //    }
        //    if (strCertificateKey=="") return null;
        //    X509Store certStore = new X509Store(sn, sl);
        //    certStore.Open(OpenFlags.ReadOnly);
        //    var certCollection = certStore.Certificates.Find(ft, strCertificateKey, false);
        //    certStore.Close();
        //    if (certCollection.Count > 0)
        //    {
        //        return certCollection[0];
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        public static int ClearDirectory(string path)
        {
            return DeleteDirectory(path, false, false);
        }
        public static int DeleteDirectory(string path)
        {
            return DeleteDirectory(path, false, true);
        }

        public static int DeleteDirectory(string path, bool recursive,bool delete_me)
        {
            int i = 0;
            try
            {
                // Delete all files and sub-folders?
                if (recursive)
                {
                    // Yep... Let's do this
                    var subfolders = Directory.GetDirectories(path);
                    foreach (var s in subfolders)
                    {
                        i += DeleteDirectory(s, recursive, delete_me);
                    }
                }

                // Get all files of the folder
                var files = Directory.GetFiles(path);
                foreach (var f in files)
                {
                    var attr = File.GetAttributes(f);
                    if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        File.SetAttributes(f, attr ^ FileAttributes.ReadOnly);
                    }
                    File.Delete(f);
                    i++;
                }
                if (delete_me)
                {
                    Directory.Delete(path);
                    i++;
                }
            }
            catch (Exception e) {
                throw e;
            };
            return i;
        }

        public static string GetJsonFiles(string path, bool recursive)
        {
            string s = "";
            try
            {
                string[] files = Directory.GetFiles(path);
                foreach (string f in files)
                {
                    s += ",\"" + Utils._JsonEscape(f) + "\"";
                }
                if (recursive)
                {
                    var subfolders = Directory.GetDirectories(path);
                    foreach (var sf in subfolders)
                    {
                        s += GetJsonFiles(sf, recursive);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            };
            s += "";
            return s;
        }
        public static string GetJsonDirectory(string path, bool recursive)
        {
            string s ="{";
            s += "\"" + Utils._JsonEscape(path) + "\":[";
            bool bFirst = true;
            try
            {
                string[] files = Directory.GetFiles(path);

                foreach (string f in files)
                {
                    if (bFirst) bFirst = false;
                    else s += ",";
                    s += "\"" + Utils._JsonEscape(f) + "\"";
                }
                if (recursive)
                {
                    var subfolders = Directory.GetDirectories(path);
                    foreach (var sf in subfolders)
                    {
                        if (bFirst) bFirst = false;
                        else s += ",";
                        s += GetJsonDirectory(sf, recursive);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            };
            s += "]}";
            return s;
        }
        //static string ComputeEncrypt(string input,string key)
        //{
        //    string sKey = "Kubion" + key + "Chessm@sterChessm@ster";
        //    sKey = sKey.Substring(3, 24);
        //    return Encrypt(input, sKey);
        //}
        //static string ComputeDecrypt(string input, string key)
        //{
        //    string sKey = "Kubion" + key + "Chessm@sterChessm@ster";
        //    sKey = sKey.Substring(3, 24);
        //    return Decrypt(input, sKey);
        //}
        //public static string Encrypt(string input, string key)
        //{
        //    byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
        //    TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
        //    tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
        //    tripleDES.Mode = CipherMode.ECB;
        //    tripleDES.Padding = PaddingMode.PKCS7;
        //    ICryptoTransform cTransform = tripleDES.CreateEncryptor();
        //    byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
        //    tripleDES.Clear();
        //    return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        //}

        //public static string Decrypt(string input, string key)
        //{
        //    byte[] inputArray = Convert.FromBase64String(input);
        //    TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
        //    tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
        //    tripleDES.Mode = CipherMode.ECB;
        //    tripleDES.Padding = PaddingMode.PKCS7;
        //    ICryptoTransform cTransform = tripleDES.CreateDecryptor();
        //    byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
        //    tripleDES.Clear();
        //    return UTF8Encoding.UTF8.GetString(resultArray);
        //}
        //static string ComputeSHA1(string input)
        //{
        //    using (SHA1Managed sha1 = new SHA1Managed())
        //    {
        //        var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
        //        var sb = new StringBuilder(hash.Length * 2);

        //        foreach (byte b in hash)
        //        {
        //            // can be "x2" if you want lowercase
        //            sb.Append(b.ToString("x2"));
        //        }

        //        return sb.ToString();
        //    }
        //}
        //private static string ComputeMD5(string input)
        //{
        //    using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
        //    {
        //        var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
        //        var sb = new StringBuilder(hash.Length * 2);

        //        foreach (byte b in hash)
        //        {
        //            // can be "x2" if you want lowercase
        //            sb.Append(b.ToString("x2"));
        //        }

        //        return sb.ToString();
        //    }
        //}

        //public static double DateTimeToUnixTimeStamp(DateTime dateTime)
        //{
        //    return (TimeZoneInfo.ConvertTimeToUtc(dateTime) -
        //           new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
        //}
        //public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        //{
        //    // Unix timestamp is seconds past epoch
        //    System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        //    dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        //    return dtDateTime;
        //}
        #endregion
        #region conditions
        internal static bool RegExMatch(string sRegEx, string sValue, bool bIgnoreCase)
        {
            Regex objPattern;
            if (bIgnoreCase)
                objPattern = new Regex(sRegEx, RegexOptions.IgnoreCase);
            else
                objPattern = new Regex(sRegEx);

            return objPattern.IsMatch(sValue);
        }
        internal static bool EvalCondition(string condition)
        {
            bool condResult = false;

            if (condition == "")
                condResult = true;
            else if (condition.Contains("===="))
            {
                int i = condition.IndexOf("====");
                if (RegExMatch(condition.Substring(i + 4).Trim(), condition.Substring(0, i).Trim(), false))
                    condResult = true;
            }
            else if (condition.Contains("!==="))
            {
                int i = condition.IndexOf("!===");
                if (!RegExMatch(condition.Substring(i + 4).Trim(), condition.Substring(0, i).Trim(), false))
                    condResult = true;
            }
            else if (condition.Contains("==="))
            {
                int i = condition.IndexOf("===");
                if (RegExMatch(condition.Substring(i + 3).Trim(), condition.Substring(0, i).Trim(), true))
                    condResult = true;
            }
            else if (condition.Contains("!=="))
            {
                int i = condition.IndexOf("!==");
                if (!RegExMatch(condition.Substring(i + 3).Trim(), condition.Substring(0, i).Trim(), true))
                    condResult = true;
            }
            else if (condition.Contains("=="))
            {
                int i = condition.IndexOf("==");
                if (condition.Substring(i + 2).Trim() == condition.Substring(0, i).Trim())
                    condResult = true;
            }
            else if (condition.Contains("!="))
            {
                int i = condition.IndexOf("!=");
                if (condition.Substring(i + 2).Trim() != condition.Substring(0, i).Trim())
                    condResult = true;
            }
            else if (condition.Contains(">>>"))
            {
                int i = condition.IndexOf(">>>");
                double do1, do2;
                if (double.TryParse(condition.Substring(0, i).Trim(), out do1))
                    if (double.TryParse(condition.Substring(i + 3).Trim(), out do2))
                        if (do1 > do2)
                            condResult = true;
            }
            else if (condition.Contains(">>"))
            {
                int i = condition.IndexOf(">>");
                if (condition.Substring(0, i).Trim().CompareTo(condition.Substring(i + 2).Trim()) > 0)
                    condResult = true;
            }
            else if (condition.Contains("<<<"))
            {
                int i = condition.IndexOf("<<<");
                double do1, do2;
                if (double.TryParse(condition.Substring(0, i).Trim(), out do1))
                    if (double.TryParse(condition.Substring(i + 3).Trim(), out do2))
                        if (do2 > do1)
                            condResult = true;
            }
            else if (condition.Contains("<<"))
            {
                int i = condition.IndexOf("<<");
                if (condition.Substring(0, i).Trim().CompareTo(condition.Substring(i + 2).Trim()) < 0)
                    condResult = true;
            }
            else
            {
                DataTable dt = new DataTable();
                condResult = false;
                condResult = (dt.Compute(condition, "").ToString() == "True") ? true : false;
            }
            return condResult;
        }
        internal static string JSON_Split(string sData, string sColSep)
        {
            string[] sCols;                     //array of rows
            StringBuilder sb = new StringBuilder();
            string sSep = "<xdoc_separator>";
            sSep = sSep.Replace(sColSep, "");

            sb.Append("[");
            sData = sData.Replace("`", sSep);
            sData = sData.Replace(sColSep, "`");

            sCols = Utils.MySplit(sData, '`');
            for (int j = 0; j < sCols.Length; j++)
            {
                if (j > 0) sb.Append(",");
                sb.Append("\""); sb.Append(Utils._JsonEscape(sCols[j].Replace(sSep, "`"))); sb.Append("\"");
            }
            sb.Append("]");

            return sb.ToString();
        }
        internal static string JSON_Split(string sData, char cColSep)
        {
            string[] sCols;                     //array of rows
            StringBuilder sb = new StringBuilder();

            sb.Append("[");
            if (cColSep == ' ') cColSep = '|';


            sCols = Utils.MySplit(sData, cColSep);
            for (int j = 0; j < sCols.Length; j++)
            {
                if (j > 0) sb.Append(",");
                sb.Append("\""); sb.Append(Utils._JsonEscape(sCols[j])); sb.Append("\"");
            }
            sb.Append("]");

            return sb.ToString();
        }

        internal static string JSON_SplitXX(string sData, char cRowSep, char cColSep, string sHeader)
        {
            string[] sRows;                     //array of rows
            string[] sCols;                     //array of rows
            string[] sColNames;                 //array of column names
            int iCols;                          //number of columns
            int iRows;                          //number of rows
            StringBuilder sb = new StringBuilder();

            sb.Append("[");

            //why space to |?
            //if (cColSep == ' ') cColSep = '|';
            //if (cRowSep == ' ') cRowSep = '|';

            sRows = Utils.MySplit(sData, cRowSep);
            sColNames = Utils.MySplit(sHeader, cColSep);
            iCols = sColNames.GetLength(0);
            iRows = sRows.GetLength(0);

            for (int i = 0; i < iRows; i++)
            {
                if (i > 0) sb.Append(",");
                sb.Append("{");
                sCols = Utils.MySplit(sRows[i], cColSep);
                for (int j = 0; (j < iCols) && (j < sCols.Length); j++)
                {
                    if (sColNames[j] == "") sColNames[j] = "Col_" + j.ToString();
                    if (j > 0) sb.Append(",");
                    sb.Append("\""); sb.Append(Utils._JsonEscape(sColNames[j])); sb.Append("\":\""); sb.Append(Utils._JsonEscape(sCols[j])); sb.Append("\"");
                }
                sb.Append("}");
            }
            sb.Append("]");

            return sb.ToString();
        }

        internal static string JSON_SplitXX_3_4(string sData, char cRowSep, char cColSep, string sHeader)
        {
            string[] sRows;                     //array of rows
            string[] sCols;                     //array of rows
            string[] sColNames;                 //array of column names
            int iCols;                          //number of columns
            int iRows;                          //number of rows
            StringBuilder sb = new StringBuilder();

            sb.Append("[");
            
            //why space to |?
            //if (cColSep == ' ') cColSep = '|';
            //if (cRowSep == ' ') cRowSep = '|';

            sRows = Utils.MySplit(sData, cRowSep);
            sColNames = Utils.MySplit(sHeader, cColSep);
            iCols = sColNames.GetLength(0);
            iRows = sRows.GetLength(0);

            for (int i = 0; i < iRows; i++)
            {
                if (i > 0) sb.Append(",");
                sb.Append("{");
                sCols = Utils.MySplit(sRows[i], cColSep);
                for (int j = 0; (j < iCols) && (j < sCols.Length); j++)
                {
                    if (j > 0) sb.Append(",");
                    sb.Append("\""); sb.Append(Utils._JsonEscape(sColNames[j])); sb.Append("\":\""); sb.Append(Utils._JsonEscape(sCols[j])); sb.Append("\"");
                }
                sb.Append("}");
            }
            sb.Append("]");

            return sb.ToString();
        }
        internal bool EvalCondition4E(string op1, string op2)
        {
            return RegExMatch(op2, op1, false);
        }
        internal bool EvalConditionN3E(string op1, string op2)
        {
            return !RegExMatch(op2, op1, false);
        }
        internal bool EvalCondition3E(string op1, string op2)
        {
            return RegExMatch(op2, op1, true);
        }
        internal bool EvalConditionN2E(string op1, string op2)
        {
            return !RegExMatch(op2, op1, true);
        }
        internal bool EvalCondition2E(string op1, string op2)
        {
            return (op2== op1);
        }
        internal bool EvalConditionN1E(string op1, string op2)
        {
            return (op2!= op1);
        }
        internal bool EvalCondition3G(string op1, string op2)
        {
            double do1, do2;
            return (double.TryParse(op1, out do1)) && (double.TryParse(op2, out do2)) && (do1 > do2);
        }
        internal bool EvalCondition2G(string op1, string op2)
        {
            return (op1.CompareTo(op2) > 0);
        }
        internal bool EvalCondition3L(string op1, string op2)
        {
            double do1, do2;
            return (double.TryParse(op1, out do1)) && (double.TryParse(op2, out do2)) && (do1 < do2);
        }
        internal bool EvalCondition2L(string op1, string op2)
        {
            return (op1.CompareTo(op2) < 0);
        }
        internal bool EvalConditionTrue(string op1, string op2)
        {
            return (true);
        }
        internal bool EvalConditionSQL(string op1)
        {
            bool condResult = false;
            try
            {
                    DataTable dt = new DataTable();
                    condResult = (dt.Compute(op1, "").ToString() == "True") ? true : false;

            }
            catch(Exception ){};
            return condResult;
        }


        #endregion

    }
    [Serializable]
    public class JCmdCollection : CollectionBase
    {

        public JCmd this[int index]
        {
            get
            {
                return ((JCmd)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        public int Add(JCmd value)
        {
            return (List.Add(value));
        }

        public int IndexOf(JCmd value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, JCmd value)
        {
            List.Insert(index, value);
        }

        public void Remove(JCmd value)
        {
            List.Remove(value);
        }

        public bool Contains(JCmd value)
        {
            // If value is not of type JCmd, this will return false.
            return (List.Contains(value));
        }

        protected override void OnInsert(int index, Object value)
        {
            try
            {
                JCmd temp = (JCmd)value;
            }
            catch
            {
                throw new ArgumentException("value must be of type JCmd.");
            }
        }

        protected override void OnRemove(int index, Object value)
        {
            try
            {
                JCmd temp = (JCmd)value;
            }
            catch
            {
                throw new ArgumentException("value must be of type JCmd.");
            }
        }

        protected override void OnSet(int index, Object oldValue, Object newValue)
        {
            try
            {
                JCmd temp = (JCmd)newValue;
            }
            catch
            {
                throw new ArgumentException("value must be of type JCmd.");
            }
        }

        protected override void OnValidate(Object value)
        {
            try
            {
                JCmd temp = (JCmd)value;
            }
            catch
            {
                throw new ArgumentException("value must be of type JCmd.");
            }
        }

    }

}
