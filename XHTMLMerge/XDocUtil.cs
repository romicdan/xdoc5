﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace XHTMLMerge
{
    public class Utils
    {
        public const string
    CT_ENDCDATA = "}}>",
    CT_ENDCDATA2 = "}.}.>",
    CT_ENDCDATA3 = "}..}..>";
        //--DBCC CHECKIDENT('s_templates', RESEED, 409)
        public static string[] MySplitParams(string sVal, out string sParams)
        {
            return MySplitParams(sVal, out sParams, 9);
        }

        public static string[] MySplitParams(string sVal, out string sParams, int lastIndex)
        {
            sParams = "";
            int pi1 = sVal.IndexOf('(');
            int pi2 = sVal.LastIndexOf(')');
            if (pi1 >= 0)
            {
                if (pi2 > pi1) sParams = sVal.Substring(pi1 + 1, pi2 - pi1 - 1);
                sVal = sVal.Remove(pi1);
            }
            else
                sVal = sVal.Remove(sVal.Length - 1);

            char cSeparator = '.';
            //sVal = sVal.Replace("\\\\", "<_backslash_>");
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            // also "<_tilda_>" inside .'<here inside>'.
            //string sVal = "a.'b1.b2'.c.d.'e1.e2'.f";
            //char cSeparator = '.';
            if (cSeparator != '%')
            {
                while (sVal.IndexOf(cSeparator + "'") >= 0 && (sVal + cSeparator).IndexOf("'" + cSeparator) >= 0)
                {
                    int li1 = sVal.IndexOf(cSeparator + "'");
                    int li2 = (sVal + cSeparator).IndexOf("'" + cSeparator);
                    if (li2 - li1 - 2 > 0)
                        sVal = sVal.Substring(0, li1) + cSeparator + sVal.Substring(li1 + 2, li2 - li1 - 2).Replace(cSeparator.ToString(), "<_tilda_>") + sVal.Substring(li2 + 1);
                }
            }

            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                if (i > lastIndex)
                    aData[lastIndex] = aData[lastIndex] + cSeparator + aData[i].Replace("<_tilda_>", cSeparator.ToString());
                else
                    aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());

                //aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\\\");
                //if (aData[i].EndsWith("\\\\")) 
                //    aData[i] = aData[i].Substring(0, aData[i].Length - 1);
            }
            return aData;
        }

        public static string[] MySplitParams_old5(string sVal, out string sParams,int lastIndex)
        {
            sParams = "";
            int pi1 = sVal.IndexOf('(');
            int pi2 = sVal.LastIndexOf(')');
            if (pi1 >= 0)
            {
                if (pi2 > pi1) sParams = sVal.Substring(pi1+1,pi2-pi1-1);
                sVal = sVal.Remove(pi1);
            }
            else
                sVal = sVal.Remove(sVal.Length-1);

            char cSeparator = '.';
            //sVal = sVal.Replace("\\\\", "<_backslash_>");
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            // also "<_tilda_>" inside .'<here inside>'.
            //string sVal = "a.'b1.b2'.c.d.'e1.e2'.f";
            //char cSeparator = '.';
            while (sVal.IndexOf(cSeparator + "'") >= 0 && (sVal + cSeparator).IndexOf("'" + cSeparator) >= 0)
            {
                int li1 = sVal.IndexOf(cSeparator + "'");
                int li2 = (sVal + cSeparator).IndexOf("'" + cSeparator);
                sVal = sVal.Substring(0, li1) + cSeparator + sVal.Substring(li1 + 2, li2 - li1 - 2).Replace(cSeparator.ToString(), "<_tilda_>") + sVal.Substring(li2 + 1);
            }

            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                if (i > lastIndex)
                    aData[lastIndex] = aData[lastIndex] + cSeparator + aData[i].Replace("<_tilda_>", cSeparator.ToString());
                else
                    aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());

                //aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\\\");
                //if (aData[i].EndsWith("\\\\")) 
                //    aData[i] = aData[i].Substring(0, aData[i].Length - 1);
            }
            return aData;
        }

        public static string[] MySplit(string sVal)
        {
            return MySplit(sVal, 9);
        }

        public static string[] MySplit(string sVal, int lastIndex)
        {
            if (sVal.EndsWith("#")) sVal = sVal.Remove(sVal.Length - 1);
            return MySplit(sVal, '.', lastIndex);
        }
        public static string[] MySplit(string sVal, char cSeparator)
        {
            return MySplit(sVal, cSeparator, 1000);
        }
        public static string[] MySplit(string sVal, char cSeparator, int lastIndex)
        {
            //sVal = sVal.Replace("\\\\", "<_backslash_>");
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            // also "<_tilda_>" inside .'<here inside>'.
            //string sVal = "a.'b1.b2'.c.d.'e1.e2'.f";
            //char cSeparator = '.';
            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                if (i > lastIndex)
                    aData[lastIndex] = aData[lastIndex] + cSeparator + aData[i].Replace("<_tilda_>", cSeparator.ToString());
                else
                    aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());
                //aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\\\");
                //if (aData[i].EndsWith("\\\\")) 
                //    aData[i] = aData[i].Substring(0, aData[i].Length - 1);
            }
            return aData;
        }

        public static string[] MySplit_quotes(string sVal, char cSeparator, int lastIndex)
        {
            //sVal = sVal.Replace("\\\\", "<_backslash_>");
            sVal = sVal.Replace("\\" + cSeparator.ToString(), "<_tilda_>");
            // also "<_tilda_>" inside .'<here inside>'.
            //string sVal = "a.'b1.b2'.c.d.'e1.e2'.f";
            //char cSeparator = '.';
            while (sVal.IndexOf(cSeparator + "'") >= 0 && (sVal + cSeparator).IndexOf("'" + cSeparator) >= 0)
            {
                int li1 = sVal.IndexOf(cSeparator + "'");
                int li2 = (sVal + cSeparator).IndexOf("'" + cSeparator);
                if (li2 - li1 - 2 > 0)
                    sVal = sVal.Substring(0, li1) + cSeparator + sVal.Substring(li1 + 2, li2 - li1 - 2).Replace(cSeparator.ToString(), "<_tilda_>") + sVal.Substring(li2 + 1);
                else
                    break;
            }

            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                if (i > lastIndex)
                    aData[lastIndex] = aData[lastIndex] + cSeparator + aData[i].Replace("<_tilda_>", cSeparator.ToString());
                else
                    aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());
                //aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString()).Replace("<_backslash_>", "\\\\");
                //if (aData[i].EndsWith("\\\\")) 
                //    aData[i] = aData[i].Substring(0, aData[i].Length - 1);
            }
            return aData;
        }
        public static string _JsonEscape(string aText)
        {
            //“ Is iets anders dan “  « » “ ”
            char[] escapeChars = new char[] { '\\', '\"', '“', '”', '\n', '\r', '\t', '\b', '\f' };
            StringBuilder s = new StringBuilder();
            if (aText != null)
            {
                int j = 0;
                int i = aText.IndexOfAny(escapeChars);
                while (i != -1)
                {
                    if (i != j) s.Append(aText.Substring(j, i - j));
                    switch (aText[i])
                    {
                        case '\\': s.Append("\\\\"); break;
                        case '\"': s.Append("\\\""); break;
                        case '“': s.Append("\\\""); break;
                        case '”': s.Append("\\\""); break;
                        case '\n': s.Append("\\n"); break;
                        case '\r': s.Append("\\r"); break;
                        case '\t': s.Append("\\t"); break;
                        case '\b': s.Append("\\b"); break;
                        case '\f': s.Append("\\f"); break;
                    }
                    j = i + 1;
                    i = aText.IndexOfAny(escapeChars, j);
                }
                s.Append(aText.Substring(j));
            }
            return s.ToString();
        }

        public static string v3_JsonEscape(string aText)
        {
            string result = "";
            if (aText != null)
                foreach (char c in aText)
                {
                    switch (c)
                    {
                        case '\\': result += "\\\\"; break;
                        case '\"': result += "\\\""; break;
                        case '\n': result += "\\n"; break;
                        case '\r': result += "\\r"; break;
                        case '\t': result += "\\t"; break;
                        case '\b': result += "\\b"; break;
                        case '\f': result += "\\f"; break;
                        default: result += c; break;
                    }
                }
            return result;
        }

        public static string Hashtable2Json(Hashtable hash, string except)
        {
            StringBuilder sJSON = new StringBuilder();
            sJSON.Append("{");
            bool bFirst = true;
            SortedList vecKeys = new SortedList();
            foreach (string key in hash.Keys)
                if (!key.StartsWith(except))
                    vecKeys.Add(key, key);

            foreach (string key in vecKeys.Values)
            {
                if (bFirst) bFirst = false;
                else sJSON.Append(",");
                if (hash[key] is Hashtable)
                {
                    sJSON.Append("\""); sJSON.Append(Utils._JsonEscape(key)); sJSON.Append("\":"); sJSON.Append(Hashtable2Json((Hashtable)hash[key])); sJSON.Append("");
                }
                else
                {
                    sJSON.Append("\""); sJSON.Append(Utils._JsonEscape(key)); sJSON.Append("\":\""); sJSON.Append(Utils._JsonEscape(hash[key].ToString())); sJSON.Append("\"");
                }

            }
            sJSON.Append("}");
            return sJSON.ToString();
        }

        public static string Hashtable2Json(Hashtable hash)
        {
            StringBuilder sJSON = new StringBuilder();
            sJSON.Append("{");
            bool bFirst = true;
            SortedList vecKeys = new SortedList();
            foreach (string key in hash.Keys)
                vecKeys.Add(key, key);

            foreach (string key in vecKeys.Values)
            {
                if (bFirst) bFirst = false;
                else sJSON.Append(",");
                if (hash[key] is Hashtable)
                {
                    sJSON.Append("\""); sJSON.Append(Utils._JsonEscape(key)); sJSON.Append("\":"); sJSON.Append(Hashtable2Json((Hashtable)hash[key])); sJSON.Append("");
                }
                else
                {
                    sJSON.Append("\""); sJSON.Append(Utils._JsonEscape(key)); sJSON.Append("\":\""); sJSON.Append(Utils._JsonEscape(hash[key].ToString())); sJSON.Append("\"");
                }

            }
            sJSON.Append("}");
            return sJSON.ToString();
        }

        public static string _JsonEscape(bool b)
        {
            return b ? "1" : "0";
        }
        public static string _JsonEscape(int i)
        {
            return i.ToString();
        }
        public static int myPathParamsRender(StringBuilder sJSON, string sPath)
        {
            int i = 0;
            sJSON.Append("{");
            string[] aPath = sPath.Split('/');
            string sIdsPath = "";
            string sIdsPath1 = "";
            sIdsPath = aPath[0];
            for (i = 0; i < aPath.Length; i++)
            {
                sJSON.Append("\"");
                sJSON.Append(_JsonEscape("Path" + (i + 1).ToString()));
                sJSON.Append("\":\"");
                sJSON.Append(_JsonEscape(aPath[i]));
                sJSON.Append("\",");
                if (i > 0)
                {
                    sJSON.Append("\"");
                    sJSON.Append(_JsonEscape(aPath[i - 1]));
                    sJSON.Append("\":\"");
                    sJSON.Append(_JsonEscape(aPath[i]));
                    sJSON.Append("\",");
                }
                if (i > 0)
                {
                    if (i % 2 == 0)
                    {
                        sIdsPath += "_" + aPath[i];
                        sIdsPath1 += "_ID";
                    }
                    else
                    {
                        sIdsPath += "_ID";
                        sIdsPath1 += "_" + aPath[i];
                    }

                }
            }
            if (i > 0)
            {
                sJSON.Append("\"PathLast\":\"");
                sJSON.Append(_JsonEscape(aPath[i - 1]));
                sJSON.Append("\",");
            }
            if (i > 1)
            {
                sJSON.Append("\"PathLast1\":\"");
                sJSON.Append(_JsonEscape(aPath[i - 2]));
                sJSON.Append("\",");
            }
            sJSON.Append("\"IDsPath\":\"");
            sJSON.Append(_JsonEscape(sIdsPath));
            sJSON.Append("\",");
            if (sIdsPath1.StartsWith("_")) sIdsPath1 = sIdsPath1.Substring(1);
            sJSON.Append("\"IDsPath1\":\"");
            sJSON.Append(_JsonEscape(sIdsPath1));
            sJSON.Append("\",");
            if (sIdsPath.EndsWith("_ID"))
            {
                sJSON.Append("\"IDsPath-1\":\"");
                sJSON.Append(_JsonEscape(sIdsPath.Substring(0, sIdsPath.Length - 3)));
                sJSON.Append("\",");
            }
            if (sIdsPath1.EndsWith("_ID"))
            {
                sJSON.Append("\"IDsPath1-1\":\"");
                sJSON.Append(_JsonEscape(sIdsPath1.Substring(0, sIdsPath1.Length - 3)));
                sJSON.Append("\",");
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(_JsonEscape(i.ToString()));
            sJSON.Append("\"");
            sJSON.Append("}");
            return i;
        }
        public static int myQueryRender(StringBuilder sJSON, string sQuery)
        {
            if (sQuery == null) return 0;
            if (sQuery.IndexOf("?") != -1)
            {
                if (sQuery.EndsWith("?")) return 0;
                sQuery = sQuery.Substring(sQuery.IndexOf("?") + 1);
            }
            string[] aParams = sQuery.Split('&');
            sJSON.Append("{");
            int i = 0;
            foreach (string sParam in aParams)
            {
                string parName = "", parVal = "";
                int io1 = sParam.IndexOf("=");
                if (io1 == -1)
                    parName = sParam;
                else
                {
                    parName = sParam.Substring(0, io1);
                    if (io1 + 1 < sParam.Length)
                        parVal = sParam.Substring(io1 + 1);
                }
                if (parName != "")
                {
                    //string val = HttpUtility.UrlDecode(parVal);
                    i++;
                    string val = XDocUrlDecode(parVal);
                    sJSON.Append("\"");
                    sJSON.Append(_JsonEscape(parName));
                    sJSON.Append("\":\"");
                    sJSON.Append(_JsonEscape(val));
                    sJSON.Append("\",");
                }
            }
            sJSON.Append("\"Count\":\"");
            sJSON.Append(_JsonEscape(i.ToString()));
            sJSON.Append("\"");
            sJSON.Append("}");
            return i;
        }
        public static string Encode(string s, EncodeOption encOption)
        {
            string retVal = s;

            switch (encOption)
            {
                case EncodeOption.Encode:
                    retVal = XDocHtmlEncode(XDocUrlEncode(retVal));
                    //retVal = HttpUtility.HtmlEncode(XDocUrlEncode(retVal));
                    break;
                case EncodeOption.Decode:
                    retVal = XDocUrlDecode(XDocHtmlDecode(retVal));
                    //retVal = XDocUrlDecode(HttpUtility.HtmlDecode(retVal));
                    break;
                case EncodeOption.HTMLDecode:
                    retVal = XDocHtmlDecode(retVal);
                    //retVal = HttpUtility.HtmlDecode(retVal);
                    break;
                case EncodeOption.HTMLEncode:
                    retVal = XDocHtmlEncode(retVal);
                    //retVal = HttpUtility.HtmlEncode(retVal);
                    break;
                case EncodeOption.URLDecode:
                    retVal = XDocUrlDecode(retVal);
                    break;
                case EncodeOption.URLEncode:
                    retVal = XDocUrlEncode(retVal);
                    break;
                case EncodeOption.EndCDATA:
                    retVal = retVal.Replace(Utils.CT_ENDCDATA2, Utils.CT_ENDCDATA3);
                    retVal = retVal.Replace(Utils.CT_ENDCDATA, Utils.CT_ENDCDATA2);
                    retVal = retVal.Replace("]]>", Utils.CT_ENDCDATA);
                    break;
                case EncodeOption.SQLEscape:
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.PAREscape:
                    retVal = retVal.Replace(".", "\\.");
                    retVal = retVal.Replace("#", "%23");
                    retVal = retVal.Replace("(", "%28");
                    retVal = retVal.Replace(")", "%29");
                    break;
                case EncodeOption.MACROEscape:
                    retVal = retVal.Replace(",", "\\,");
                    retVal = retVal.Replace(")", "\\)");
                    break;
                case EncodeOption.XDOCEncode:
                    retVal = retVal.Replace("#", "%23");
                    retVal = retVal.Replace("(", "%28");
                    retVal = retVal.Replace(")", "%29");
                    break;
                case EncodeOption.XDOCDecode:
                    retVal = retVal.Replace("%23", "#");
                    retVal = retVal.Replace("%28", "(");
                    retVal = retVal.Replace("%29", ")");
                    break;
                case EncodeOption.EncodeSQLEscape:
                    retVal = XDocHtmlEncode(XDocUrlEncode(retVal));
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlEncode(XDocUrlEncode(retVal));
                    break;
                case EncodeOption.DecodeSQLEscape:
                    retVal = XDocUrlDecode(XDocHtmlDecode(retVal));
                    retVal = retVal.Replace("'", "''");
                    //retVal = XDocUrlDecode(HttpUtility.HtmlDecode(retVal));
                    break;
                case EncodeOption.HTMLDecodeSQLEscape:
                    retVal = XDocHtmlDecode(retVal);
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlDecode(retVal);
                    break;
                case EncodeOption.HTMLEncodeSQLEscape:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("'", "''");
                    //retVal = HttpUtility.HtmlEncode(retVal);
                    break;
                case EncodeOption.URLDecodeSQLEscape:
                    retVal = XDocUrlDecode(retVal);
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.URLEncodeSQLEscape:
                    retVal = XDocUrlEncode(retVal);
                    retVal = retVal.Replace("'", "''");
                    break;
                case EncodeOption.JSEscape:
                    retVal = retVal.Replace("\\", "\\\\");
                    retVal = retVal.Replace("'", "\\'");
                    retVal = retVal.Replace("\"", "\\\"");
                    retVal = retVal.Replace("`", "\\`");
                    retVal = retVal.Replace("\r", "\\r");
                    retVal = retVal.Replace("\n", "\\n");
                    break;
                case EncodeOption.HTMLEncodeJSEscape:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("\\", "\\\\");
                    retVal = retVal.Replace("'", "\\'");
                    retVal = retVal.Replace("\"", "\\\"");
                    retVal = retVal.Replace("`", "\\`");
                    break;
                case EncodeOption.Text2HTML:
                    retVal = XDocHtmlEncode(retVal);
                    retVal = retVal.Replace("&#13;&#10;", "<br>");
                    break;
                case EncodeOption.Base64Encode8:
                    byte[] binaryData8 = Encoding.UTF8.GetBytes(retVal);
                    retVal = Convert.ToBase64String(binaryData8, 0, binaryData8.Length);
                    break;
                case EncodeOption.Base64Encode:
                    byte[] binaryData = Encoding.Default.GetBytes(retVal);
                    retVal = Convert.ToBase64String(binaryData, 0, binaryData.Length);
                    break;
                case EncodeOption.Base64Decode:
                    if ((retVal.Length % 4)>0) retVal += new string('=', 4 - (retVal.Length % 4));
                    byte[] binaryDataD = Convert.FromBase64String(retVal);
                    //retVal = Encoding.UTF8.GetString(binaryDataD, 0, binaryDataD.Length);
                    retVal = System.Text.Encoding.Default.GetString(binaryDataD);
                    break;
                case EncodeOption.JSONEscape:
                    //DateTime dt1 = DateTime.Now;
                    //Utils.v3_JsonEscape(retVal);
                    //DateTime dt2 = DateTime.Now;
                    //System.Diagnostics.Debug.WriteLine("_JsonEscape_old  = " + dt2.Subtract(dt1).TotalMilliseconds.ToString());
                    // Utils._JsonEscape(retVal);
                    //DateTime dt3 = DateTime.Now;
                    //System.Diagnostics.Debug.WriteLine("_JsonEscape  = " + dt3.Subtract(dt2).TotalMilliseconds.ToString());
                    //Utils.v3_JsonEscape(retVal);
                    //DateTime dt4 = DateTime.Now;
                    //System.Diagnostics.Debug.WriteLine("_JsonEscape_old  = " + dt4.Subtract(dt3).TotalMilliseconds.ToString());
                    // Utils._JsonEscape(retVal);
                    //DateTime dt5 = DateTime.Now;
                    //System.Diagnostics.Debug.WriteLine("_JsonEscape_  = " + dt5.Subtract(dt4).TotalMilliseconds.ToString());
                    retVal = Utils._JsonEscape(retVal);
                    break;
                case EncodeOption.None:
                    break;
                default:
                    break;
            }

            return retVal;
        }

        public static string Encode(string s, string encOption)
        {
            return Encode(s, GetEncodeOption(encOption));
        }
        public static EncodeOption GetEncodeOption(string s)
        {
            EncodeOption encOpt = EncodeOption.None;
            foreach (string enumName in Enum.GetNames(typeof(EncodeOption)))
            {
                if (s.ToUpper().Trim() == enumName.ToUpper())
                {
                    encOpt = (EncodeOption)Enum.Parse(typeof(EncodeOption), enumName, false);
                    break;
                }
            }
            return encOpt;
        }
        public static string ParseFormat(string strFormat, out CultureInfo ci_in, out CultureInfo ci)
        {
            ci_in = CultureInfo.InvariantCulture;
            ci = CultureInfo.InvariantCulture;
            if (strFormat != "")
            {
                strFormat = strFormat.Replace("%dash%", "#");
                if (strFormat.Contains("|"))
                {
                    string strFormat1 = strFormat.Substring(strFormat.IndexOf("|") + 1);
                    if (strFormat1 == "NL") strFormat1 = "nl-NL";
                    if (strFormat1 == "US") strFormat1 = "en-US";
                    try { ci_in = CultureInfo.CreateSpecificCulture(strFormat1); }
                    catch (Exception ) { };
                    strFormat = strFormat.Substring(0, strFormat.IndexOf("|"));
                }
                if (strFormat.StartsWith("NL"))
                {
                    strFormat = strFormat.Substring(2);
                    ci = CultureInfo.CreateSpecificCulture("nl-NL");
                }
                if (strFormat.StartsWith("US"))
                {
                    strFormat = strFormat.Substring(2);
                    ci = CultureInfo.CreateSpecificCulture("en-US");
                }
            }
            return strFormat;
        }
        public static string ApplyFormat(string oVal, string strFormat)
        {
            string retVal = oVal;
            CultureInfo ci_in = CultureInfo.InvariantCulture;
            CultureInfo ci = CultureInfo.InvariantCulture;
            bool b_isU = false;

            if (strFormat != "")
            {
                strFormat = ParseFormat(strFormat, out ci_in, out ci);
                if (strFormat.StartsWith("U"))
                {
                    b_isU = true;
                    strFormat = strFormat.Substring(1);
                }
                if (strFormat.StartsWith(">")) { retVal = oVal.ToString().ToUpper(); }
                else if (strFormat.StartsWith("<")) { retVal = oVal.ToString().ToLower(); }
                else
                {
                    double dVal;
                    DateTime dtVal;
                    if (double.TryParse(oVal.ToString(), System.Globalization.NumberStyles.Any, ci_in, out dVal))
                        retVal = dVal.ToString(strFormat, ci);
                    else if (DateTime.TryParse(oVal.ToString(), ci_in, System.Globalization.DateTimeStyles.None, out dtVal))
                        retVal = dtVal.ToString(strFormat, ci);
                    else
                        retVal = oVal.ToString();
                }
                if (b_isU)
                {
                    retVal = retVal.Replace(',', '.');
                }
            }
            return retVal;
        }

        private static string XDocHtmlDecode(string retVal)
        {
            retVal = HttpUtility.HtmlDecode(retVal);
            return retVal;
        }
        private static string XDocHtmlEncode(string retVal)
        {
            retVal = HttpUtility.HtmlEncode(retVal);
            retVal = retVal.Replace("\r\n", "&#13;&#10;");
            retVal = retVal.Replace("\r", "&#13;&#10;");
            retVal = retVal.Replace("\n", "&#13;&#10;");
            return retVal;
        }

        public static string XDocUrlDecode(string retVal)
        {
            string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
            if (sEncoding == null) sEncoding = "";
            if (sEncoding == "") sEncoding = "utf-8";
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
            if (enc != null)
                retVal = HttpUtility.UrlDecode(retVal, enc);
            else
                retVal = HttpUtility.UrlDecode(retVal, System.Text.Encoding.Default);

            return retVal;
        }
        public static string XDocUrlEncode(string retVal)
        {
            string sEncoding = System.Configuration.ConfigurationManager.AppSettings["Encoding"];
            if (sEncoding == null) sEncoding = "";
            if (sEncoding == "") sEncoding = "utf-8";
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(sEncoding);
            if (enc != null)
                retVal = HttpUtility.UrlEncode(retVal, enc);
            else
                retVal = HttpUtility.UrlEncode(retVal, System.Text.Encoding.Default);
            retVal = retVal.Replace("+", "%20");
            retVal = retVal.Replace("!", "%21");
            retVal = retVal.Replace("(", "%28");
            retVal = retVal.Replace(")", "%29");
            retVal = retVal.Replace("'", "%27");

            return retVal;
        }
        public static string L_String(string strString)
        {
            if (strString == null) return "";
            else return strString.Replace("'", "''");
        }

        public static DateTime DateTime_AddWorkMinutes(DateTime now, int iMinutes, int iStartHour, int iEndHour, int iStartHourFriday, int iEndHourFriday)
        {
            DateTime later = now;
            int iDayMinutes = 0;

            if (iStartHour == 0) iStartHour = 9;
            if (iEndHour == 0) iEndHour = 17;
            if (iStartHourFriday == 0) iStartHourFriday = iStartHour;
            if (iEndHourFriday == 0) iEndHourFriday = iEndHour;

            if (iStartHour > iEndHour) throw new Exception("Startuur van de dag moet groter zijn dan einduur");
            if (iStartHourFriday > iEndHourFriday) throw new Exception("Startuur van de vrijdag moet groter zijn dan einduur");

            int iMinutesPerDay = (iEndHour - iStartHour) * 60;
            int iMinutesPerFriday = (iEndHourFriday - iStartHourFriday) * 60;

            while (iMinutes >= iMinutesPerDay)
            {
                if (later.DayOfWeek == DayOfWeek.Friday)
                    iDayMinutes = iMinutesPerFriday;
                else
                    iDayMinutes = iMinutesPerDay;
                later = later.AddDays(1);
                if (later.DayOfWeek == DayOfWeek.Saturday)
                {
                    later = later.AddDays(2);
                }
                iMinutes -= iDayMinutes;
            }

            later = later.AddMinutes(iMinutes);
            if (later.Hour > 17)
            {
                later = later.AddHours(15);
            }
            if (later.DayOfWeek == DayOfWeek.Saturday)
            {
                later = later.AddDays(2);
            }
            else if (later.DayOfWeek == DayOfWeek.Sunday)
            {
                later = later.AddDays(1);
            }

            return later;
        }
        public static int DateTime_CalcWorkMinutes(DateTime start, DateTime end, int iStartHour, int iEndHour, int iStartHourFriday, int iEndHourFriday)
        {
            if (iStartHour == 0) iStartHour = 9;
            if (iEndHour == 0) iEndHour = 17;
            if (iStartHourFriday == 0) iStartHourFriday = iStartHour;
            if (iEndHourFriday == 0) iEndHourFriday = iEndHour;

            if (iStartHour > iEndHour) throw new Exception("Startuur van de dag moet groter zijn dan einduur");
            if (iStartHourFriday > iEndHourFriday) throw new Exception("Startuur van de vrijdag moet groter zijn dan einduur");
            if (start > end) throw new Exception("Start datum moet kleiner zijn dan einddatum");

            int count = 0;
            for (DateTime i = start; i < end; i = i.AddMinutes(1))
            {
                if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday)
                {
                    if (i.DayOfWeek == DayOfWeek.Friday)
                    {
                        if (i.TimeOfDay.Hours >= iStartHourFriday && i.TimeOfDay.Hours < iEndHourFriday)
                            count++;
                    }
                    else if (i.TimeOfDay.Hours >= iStartHour && i.TimeOfDay.Hours < iEndHour)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public static Hashtable CaseInsensitiveHashtable()
        {
            return CaseInsensitiveHashtable(null);
        }
        public static Hashtable CaseInsensitiveHashtable(Hashtable oldHash)
        {
            Hashtable newHash = CollectionsUtil.CreateCaseInsensitiveHashtable();
            if (oldHash != null)
            {
                foreach (DictionaryEntry de in oldHash)
                    newHash.Add(de.Key, de.Value);
            }
            return newHash;
        }
        public static Hashtable HashtableFromQueryString(string queryString)
        {
            Hashtable newHash = CaseInsensitiveHashtable();
            if (queryString == null)
                return newHash;

            if (queryString.IndexOf("?") != -1)
            {
                if (queryString.EndsWith("?"))
                    return newHash;
                queryString = queryString.Substring(queryString.IndexOf("?") + 1);
            }
            string[] parameters = queryString.Split('&');
            foreach (string parameter in parameters)
            {
                string parName = "", parVal = "";

                int io1 = parameter.IndexOf("=");
                if (io1 == -1)
                    parName = parameter;
                else
                {
                    parName = parameter.Substring(0, io1);
                    if (io1 + 1 < parameter.Length)
                        parVal = parameter.Substring(io1 + 1);
                }

                if (parName != "")
                {
                    //string val = HttpUtility.UrlDecode(parVal);
                    string val = XDocUrlDecode(parVal);
                    if (newHash.ContainsKey(parName))
                    {
                        val = newHash[parName].ToString() + "," + val;
                        newHash[parName] = val;
                    }
                    else
                        newHash.Add(parName, val);
                }
            }

            return newHash;
        }
        public static string QueryStringFromHashtable(Hashtable parameters)
        {
            string ret = "";
            if (parameters == null)
                return ret;

            foreach (DictionaryEntry de in parameters)
            {
                ret += de.Key.ToString() + "=" + de.Value.ToString() + "&";
            }
            if (ret.EndsWith("&"))
                ret = ret.Remove(ret.Length - 1);

            return ret;
        }

        public static string SaltKey(string key, bool day )
        {
            //string sJWTSecret = "Kubion";
            //if (System.Configuration.ConfigurationManager.AppSettings["Signature"] != null) sJWTSecret = (string)System.Configuration.ConfigurationManager.AppSettings["Signature"];
            //string sD = "Kubion";
            //if (day) sD = System.DateTime.Now.ToString("yyyyMMdd");
            //if (key == "") key = sJWTSecret;
            //key = key + "Chessm@ster123!M";
            //key = key.Substring(0, 16);
            //key = sD + key + sD;
            //return key;
            string sSecret = "";
            if (System.Configuration.ConfigurationManager.AppSettings["Signature"] != null) sSecret = (string)System.Configuration.ConfigurationManager.AppSettings["Signature"];
            string sD = "Kubion";
            if (day) sD = System.DateTime.Now.ToString("yyyyMMdd");
            string sKey = sD + key + sSecret + "Chessm@sterChessm@ster";
            sKey = sKey.Substring(3, 24);
            return sKey;
        }

        public static string SaltKey(string key)
        {
            string sSecret = "";
            if (System.Configuration.ConfigurationManager.AppSettings["Signature"] != null) sSecret = (string)System.Configuration.ConfigurationManager.AppSettings["Signature"];
            string sKey = "Kubion" + key + sSecret + "Chessm@sterChessm@ster";
            sKey = sKey.Substring(3, 24);
            return sKey;
        }
        public static string Encrypt(string input, string key)
        {
            return ComputeEncrypt(input, key);
        }
        public static string Decrypt(string input, string key)
        {
            //salting
            return ComputeDecrypt(input, key);
        }
        public static string ComputeEncrypt(string input, string key)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string ComputeDecrypt(string input, string key)
        {
            byte[] inputArray = Convert.FromBase64String(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        public static string ComputeRijndaelEncrypt(string input, string key)
        {
            return new SimplerAES(key).Encrypt(input);
        }
        public static string ComputeRijndaelDecrypt(string input, string key)
        {
            return new SimplerAES(key).Decrypt(input);
        }
        public static string ComputeSHA256(string input)
        {
            using (SHA256Managed sha256 = new System.Security.Cryptography.SHA256Managed())
            {
                var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }
        public static string ComputeSHA384(string input)
        {
            using (SHA384Managed sha384 = new System.Security.Cryptography.SHA384Managed())
            {
                var hash = sha384.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }
        public static string ComputeSHA512(string input)
        {
            using (SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed() )
            {
                var hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }
        public static string ComputeSHA1(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }
        public static string ComputeMD5(string input)
        {
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }

        public static string ComputeHMACSHA1(string input, string key)
        {
            var secretKey = Encoding.UTF8.GetBytes(key);
            var hmac = new HMACSHA1(secretKey);
            hmac.Initialize();
            var bytes = Encoding.UTF8.GetBytes(input);
            var rawHmac = hmac.ComputeHash(bytes);
            return Convert.ToBase64String(rawHmac).ToLower();           
        }

        public static string ComputeFacebookHash(string input, string key)
        {
            /*
             Please note that the calculation is made on the escaped unicode version of the payload, with lower case hex digits.
             If you just calculate against the decoded bytes, you will end up with a different signature.
             For example, the string äöå should be escaped to \u00e4\u00f6\u00e5.
             */
            input = EncodeNonAsciiCharacters(input);

            var secretKey = Encoding.UTF8.GetBytes(key);
            var hmac = new HMACSHA1(secretKey);
            hmac.Initialize();
            var bytes = Encoding.UTF8.GetBytes(input);
            var rawHmac = hmac.ComputeHash(bytes);

            return ByteArrayToString(rawHmac).ToLower();
        }


        private static string EncodeNonAsciiCharacters(string value)
        {
            var sb = new StringBuilder();
            foreach (var c in value)
            {
                if (c > 127)
                {
                    var encodedValue = "\\u" + ((int)c).ToString("x4");
                    sb.Append(encodedValue);
                }
                else
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        private static string ByteArrayToString(byte[] ba)
        {
            var hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }
        public static double DateTimeToUnixTimeStamp(DateTime dateTime)
        {
            return (TimeZoneInfo.ConvertTimeToUtc(dateTime) -
                   new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

    }
    public enum EncodeOption
    {
        //this enum must not be modified. Its names are used to identify command keywords when parsing
        None,
        Encode,
        Decode,
        HTMLEncode,
        HTMLDecode,
        URLEncode,
        URLDecode,
        EndCDATA,
        EncodeSQLEscape,
        DecodeSQLEscape,
        HTMLEncodeSQLEscape,
        HTMLDecodeSQLEscape,
        URLEncodeSQLEscape,
        URLDecodeSQLEscape,
        SQLEscape,
        PAREscape,
        MACROEscape,
        XDOCEncode,
        XDOCDecode,
        JSEscape,
        HTMLEncodeJSEscape,
        Text2HTML,
        Base64Encode8,
        Base64Encode,
        Base64Decode,
        JSONEscape
    }
}
